
// Vice 3.7 Testing
// @kickass:emulator.runtime=C:\Games2\Retro\Commodore\Emulators\GTK3VICE-3.7-win64\bin\x64sc.exe

// Vice 3.6 Testing
// kickass:emulator.runtime=C:\Games2\Retro\Commodore\Emulators\GTK3VICE-3.6.1-win64\bin\x128.exe

// External Cartridge in 40 columns >= 3.7
// @kickass:emulator.options=${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename}
// kickass:emulator.options=-cartfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} 


//==============================================================================
//                        Cartridge Example 2
//
//  Use segmentdef to define the memory in the correct place in the file, 
//  use .pseudopc to execute the .segment Bank_00_0 at address $8000 when
//  the C64 starts up.
//==============================================================================

.label RomBankSelector  = $DE00

// VIC-II Registers
.label EXTCOL           = $D020

// Keyboard Routines
.label XMAX             = $0289    // Max keyboard buffer size
.label RPTFLG           = $028A    // Which Keys Will Repeat?
.label SCNKEY           = $FF9F    // scan keyboard - kernal routine
.label GETIN            = $ffe4    // read keyboard buffer - kernal routine


.segment CARTRIDGE_FILE [outBin="cart.bin"]
.segmentout [segments = "Cartridge_Header"]
.segmentout [segments = "Bank_00_0"]
.segmentout [segments = "Bank_End"]

.segmentdef Cartridge_Header [start=$0000, min=$0000, max=$004F, fill]
.segmentdef Bank_00_0 [start=$0050, min=$0050, max=$1FFF, fill]
.segmentdef Bank_End [start=$2000, min=$0, max=$7e04f, fill]

.encoding "ascii"
.segment Cartridge_Header
	.text "C64 CARTRIDGE   "
	.byte $00, $00      //header length
	.byte $00, $40      //header length
	.word $0001         //version
	.word $0500         //crt type
	.byte $00           //exrom line
	.byte $00           //game line
	.byte $00, $00, $00, $00, $00, $00  //unused
	.text "Cartridge Example 2"

*=$0040
     SET_CARTRIDGE_HEADER_V(0);

.segment Bank_00_0
.pseudopc $8000 
{
     .word coldstart            // coldstart vector
     .word warmstart            // warmstart vector
     .byte $C3, $C2, $CD, $38, $30  // "CBM8O". autostart string

     coldstart:
     {
          //	KERNAL RESET ROUTINE
          sei
          stx $D016
          jsr $FDA3           // prepare IRQ
          jsr $FD50           // init memory. Rewrite this routine to speed up boot process.
          jsr $FD15           // init I/O
          jsr $FF5B           // init video
          cli                 // disable interrupts
     }

     warmstart:
     {
          jsr textSetup
          jmp loop

          textSetup: 
          {
               ldx #0
               
               loop: 
                    lda text,x     // Load next char value
                    cmp #$FF       // Have we reached the end of the string
                    beq out        // If null, jump to out:
                    sta $0400,x    // Write char to screen
                    inx
                    jmp loop
               out:
                    rts

               text: 
                    .encoding "screencode_upper"
                    .text "ROM BANK 0"
                    .byte $FF
          }
          
          loop: 
          {
               inc $D020
               jmp loop            // infinite loop
          }
     } 
}

.segment Bank_End
* = $9FFF
     .byte 0


.macro SET_CARTRIDGE_HEADER_V(wBank)
{
	.encoding "ascii"
	.text "CHIP"
	.byte $00, $00, $20, $10    //chip length
	.byte $00, $00              //chip type
	.byte >wBank, <wBank        //bank
	.byte $80, $00              //adress
	.byte $20, $00              //length   
}

.macro SET_CARTRIDGE_ROM_BANK_V(bRomBank)
{
	lda #bRomBank				// Store the rom bank
	ora #%10000000				// Ensure that bit 8 is set
	sta RomBankSelector			// Store the value in the ROM Bank selector memory address
}
