
#define TEST
#import "../system/sys.asm"

// ICall($1300, $30)
// ICall1($1300, $30, $0800)
// ICall2($1300, $30, $0800, $4000)
// ICall2($1300, $30, $0800, $4000)

.macro Test(val) {
    lda #val
}

.assert "Test", { Test(0) },
{
    lda #$0
}


.assert "Test ICall", { ICall($1300, $30) },
{

    lda #$00
    sta $fb 
    lda #$15
    sta $fc 
        
    lda #$00
    sta $fd 
    lda #$16
    sta $fe 
        
    lda #$30

    jsr $1300

}

.assert "Test ICall1", { ICall1($1300, $30, $0800) },
{
    lda #$00
    sta $fb 
    lda #$08
    sta $fc 
        
    lda #$00
    sta $fd 
    lda #$16
    sta $fe 
        
    lda #$30
    jsr $1300

}

.assert "Test ICall2", { ICall2($1300, $30, $0800, $4000) },
{

    lda #$00
    sta $fb 
    lda #$08
    sta $fc 
        
    lda #$00
    sta $fd 
    lda #$40
    sta $fe 
        
    lda #$30
    jsr $1300

}

.assert "Test ICall", { ICall2($a000, $30, $0800, $4000) },
{

    lda #$00
    sta $fb 
    lda #$08
    sta $fc 
        
    lda #$00
    sta $fd 
    lda #$40
    sta $fe 
        
    lda #$30
    jsr $a000

}

