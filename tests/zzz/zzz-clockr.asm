
#importonce 

/* 
	Print TOD Clock
*/

#import "samples/_sample.asm"

TestStart("zzz-clockr", "Read The TOD Clock")

.segment app_data

time:

	.text @"00:00:00\$0d\$00"

app_run:

	Print(time)

	SetAppState(APP_STATE_STOP)
	
	rts


__AppStateHandler__(APP_STATE_RUN, app_run)

TestEnd()
