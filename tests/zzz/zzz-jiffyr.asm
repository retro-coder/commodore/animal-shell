
#importonce 

/* 
	Jiffy Clock Testing
*/

#import "samples/_sample.asm"

TestStart("zzz-jiffyr", "Jiffy Clock Testing")

.segment app_data

p_num2str:

	p_core_util_num2str 

num:

	.fill 4, $00

s_low:
{
		.text @"low: "
	low: 	.text @"    \$00"
}

s_med:
{
		.text @"med: "
	med: 	.text @"    \$00"
}

s_hi:
{
		.text @"hi : "
	hi: 	.text @"    \$00"
}

.segment app_code

app_start:

	lda	#$00
	sta	$a0
	sta	$a1 
	sta	$a2

	rts 

app_stop:


	StorePointer(num, p_num2str.addr)

	lda	$a0
	sta	num 

        ICall2(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str, s_hi.hi)
        NPrintFromAddress(s_hi)

	lda	$a1
	sta	num 

        ICall2(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str, s_med.med)
        NPrintFromAddress(s_med)


	lda	$a2
	sta	num 

        ICall2(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str, s_low.low)
        NPrintFromAddress(s_low)


	rts	

app_run:

	ldx	#$00

	!:

	dex	
	beq	!+
	jmp	!-

	!:

	rts


__AppStateHandler__(APP_STATE_START, app_start)
__AppStateHandler__(APP_STATE_STOP, app_stop)
__AppStateHandler__(APP_STATE_RUN, app_run)

TestEnd()
