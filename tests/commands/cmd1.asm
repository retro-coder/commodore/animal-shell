#import "commands/command.asm"
#import "core/core.asm"

.filenamespace testdriver

// .segment command_data "test_command"


.segment command_code "test_command"

__CommandInit__()

.eval c_info.name = "command1"

__CommandCallHandler__(I_CMD_CALL_INIT)
inc $d020
rts

__CommandHandler__()


.file [name="cmd1.prg", segments="command"]

