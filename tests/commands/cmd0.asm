#import "commands/command.asm"

.filenamespace testdriver

.segment command_code "test_command"

__CommandInit__()

.eval c_info.name = "command0"

__CommandHandler__()

.file [name="cmd0.prg", segments="command"]
