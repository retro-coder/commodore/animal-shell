
#define C128
// #define LOG_DEBUG

.filenamespace __command__

#import "commands/command.asm"

.segment command_code

__CommandStart__()

__CommandStateHandler__(I_CMD_CALL_RUN, run)

.segment command_code

run:

	SetCommandState(COMMAND_STATE_STOP)
	rts
	

__CommandFinish__()

.file [name="command.prg", segments="command"]
