#import "../../driver/driver.asm"

.filenamespace testdriver
.encoding "petscii_mixed"

.segment driver_code "base_driver"

__DriverInit__()

.eval d_info.name = "driver1"

__DriverHandler__()

.file [name="driver1.prg", segments="driver"]
