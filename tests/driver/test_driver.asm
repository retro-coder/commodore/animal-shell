#import "../../driver/driver.asm"

.filenamespace testdriver

__DriverInit__()

__DriverCallHandler__(I_DRV_CALL_INIT)

.segment driver_code "call_init"

        __LogDebug__("** my init handler")

	inc	$d020
	rts


__DriverHandler__()

.file [name="driver.prg", segments="driver"]
