#import "../../driver/driver.asm"

.filenamespace testdriver
.encoding "petscii_mixed"

.segment driver_code "base_driver"

__DriverInit__()

.eval d_info.name = "driver0"

__DriverHandler__()

.file [name="driver0.prg", segments="driver"]
