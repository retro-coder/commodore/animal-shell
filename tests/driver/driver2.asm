#import "../../driver/driver.asm"

.filenamespace testdriver
.encoding "petscii_mixed"

.segment driver_code "base_driver"

__DriverInit__()

__DriverHandler__()

.file [name="driver2.prg", segments="driver"]
