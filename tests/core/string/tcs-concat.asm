#importonce

.encoding "petscii_mixed"

#import "_test.asm"

TestStart("tcs-concat", "Unit Tests For CORE_STR_CONCAT")

.segment app_data

        .const hello = @"hello\$00"
        .const world = @"world\$00"
        .const there = @"there\$00"
        .const hello_world = @"hello world\$00"
        .const hello_there = @"hellothereworld\$00"

	s_hello: .text hello
        s_world: .text world
        s_there: .text there

        s_hello_world: .text hello_world
        s_hello_there: .text hello_there

        s_result: .fill 32,$00

.segment app_parms

        p_concat: p_core_str_concat_in 

	__AppStateHandler__(APP_STATE_RUN, app_run)

.segment app_code

	app_run:

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)

        examineTest(test1)
        examineTest(test2)
	
        c64unitExit()

        test1: {

        StorePointer(s_hello, p_concat.stringa)
        StorePointer(s_world, p_concat.stringb)
        lda #1
        sta p_concat.options
        ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, s_result)

        .var len = hello_world.size()
        assertMemoryEqual(s_result, s_hello_world, len, "should be 'hello world'")
        rts

        }

        test2: {

        StorePointer(s_hello, p_concat.stringa)
        StorePointer(s_there, p_concat.stringb)
        lda #0
        sta p_concat.options
        ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, s_result)

        StorePointer(s_result, p_concat.stringa)
        StorePointer(s_world, p_concat.stringb)
        ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, s_result)

        .var len = hello_there.size()
        assertMemoryEqual(s_result, s_hello_there, len, "should be 'hellothereworld'")
        rts     

        }

TestEnd()
 