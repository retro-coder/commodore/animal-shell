#importonce

/*
	Animal Shell Unit Test

	test-app.asm

*/

.encoding "petscii_mixed"

.var coreBinaryLocation = $6000

#import "/Users/paul/Workspace/retro-coder/c64unit/cross-assemblers/kick-assembler/macros.asm"
#import "/Users/paul/Workspace/retro-coder/c64unit/cross-assemblers/kick-assembler/symbols.asm"

#import "samples/_sample.asm"

TestStart("test-app", "Unit Testing With C64Unit")

.segment app_data

stack_ptr: .byte $00

.segment app_parms

.segment app_code

app_run:
{
	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN) 

	lda #<exitFailedTests
	sta c64unit_ExitWithFail + 11
	lda #>exitFailedTests
	sta c64unit_ExitWithFail + 12

	lda #<exitTests
	sta c64unit_ExitWithSuccess + 14
	lda #>exitTests
	sta c64unit_ExitWithSuccess + 15 

	tsx
	stx stack_ptr

	c64unitX(0,0)

	examineTest(testSumToAccumulator)
	examineTest(testSumToAccumulatorWithCustomMessage)
	examineTest(testSumToZeroPage)
	
	examineTest(testForGreater)
	examineTest(testForGreaterOrEqual)
	examineTest(testForLess)

	examineTest(testSumToAccumulatorWithDataSet)
	examineTest(testSumToZeroPageWithDataSet)
	examineTest(testForGreaterWithDataSet)
	examineTest(testForGreaterOrEqualWithDataSet)

	examineTest(testCarryFlagSet)
	examineTest(testZeroFlagSet)
	examineTest(testDecimalFlagSet)
	examineTest(testOverflowFlagSet)
	examineTest(testNegativeFlagSet)

	// examineTest(testStackPointerEqual) -- very specific to basic 
	examineTest(testMockMethod)
	examineTest(testMockMethodsHaveBeenUnmocked)
	examineTest(testSubtract16bit)
	examineTest(testSubtract16bitDataSet)
	examineTest(testSubtract16bitDataSetWithLoHi)
	examineTest(testCompareMemoryLocations)

	c64unitExit()

	jmp exitTests


exitFailedTests:

	// pla	
	// pla	
	// pla	
	// pla	

	ldx stack_ptr
	txs

exitTests:

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	SetAppState(APP_STATE_STOP)
	rts


}

__AppStateHandler__(APP_STATE_RUN, app_run)

TestEnd()

.macro @c64unitX(exitToBasicMode, screenLocation) {

	lda #exitToBasicMode
	jsr c64unit_InitExitToBasicMode
	
	.if (screenLocation == 0) {
		ldx #<$400
		ldy #>$400
	} else {
		ldx #<screenLocation
		ldy #>screenLocation
	}
	jsr c64unit_InitScreenLocation
	
	jsr c64unit_InitScreen
}


.segment app_code "logic"
// Include domain logic, i.e. classes, methods and tables
.import source "../c64unit-examples/kick-assembler/src/sum-to-accumulator.asm"
.import source "../c64unit-examples/kick-assembler/src/sum-to-zero-page.asm"
.import source "../c64unit-examples/kick-assembler/src/get-x-coordinate.asm"
.import source "../c64unit-examples/kick-assembler/src/is-accessible.asm"
.import source "../c64unit-examples/kick-assembler/src/simple-controller.asm"

// Testsuite with all test cases
.import source "../c64unit-examples/kick-assembler/tests/test-cases/sum-to-accumulator/test.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/sum-to-accumulator/test-with-custom-message.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/sum-to-zero-page/test.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/great-or-greater/test-for-greater.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/great-or-greater/test-for-greater-or-equal.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/great-or-greater/test-for-less.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/sum-to-accumulator-with-data-set/test.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/sum-to-zero-page-with-data-set/test.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/great-or-greater-with-data-set/test-for-greater.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/great-or-greater-with-data-set/test-for-greater-or-equal.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/status-flags/test-carry-flag-set.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/status-flags/test-zero-flag-set.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/status-flags/test-decimal-flag-set.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/status-flags/test-overflow-flag-set.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/status-flags/test-negative-flag-set.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/stack-pointer/test-stack-pointer-equal.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/mock-method/test.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/mock-method/test-methods-unmocked.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/subtract-16-bit/test.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/subtract-16-bit/test-data-set.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/subtract-16-bit/test-data-set-with-lo-hi.asm"
.import source "../c64unit-examples/kick-assembler/tests/test-cases/compare-memory-locations/test.asm"

.segment app_code "c64unit"
* = coreBinaryLocation "c64unit"
.import binary "/Users/paul/Workspace/retro-coder/c64unit/bin/core6000.bin"



