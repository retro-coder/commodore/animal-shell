
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

/*

	Test the internal logging system that
	can be used by developers.

	Simply DEFINE the correct debug level
	in your main KickAssembler file, or pass
	it to the assembler when you are building it.

*/

#import "samples/_sample.asm"


.filenamespace testapp

TestStart("ts-logging", "Test System Logging")

.segment app_code

app_run:

	__LogWarn__("Warning")
	__LogError__("Error")
	__LogDebug__("Debug")
	__LogDetail__("Detail")
	__LogTrace__("Trace")
	__LogFine__("Fine")

        SetAppState(APP_STATE_STOP)

        rts

__AppStateHandler__(APP_STATE_RUN, app_run)

TestEnd()
