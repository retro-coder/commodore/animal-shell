#importonce

.encoding "petscii_mixed"

#import "_test.asm"

TestStart("test-mem", "Memory Unit Testing With C64Unit")

.segment app_data

        .const hello = @"hello\$00"
        .const world = @"world\$00"
        .const there = @"there\$00"
        .const hello_world = @"hello world\$00"
        .const hello_there = @"hellothereworld\$00"

	s_hello: .text hello
        s_world: .text world
        s_there: .text there

        s_hello_world: .text hello_world
        s_hello_there: .text hello_there

        s_result: .fill 32,$00

        d_list: ds_core_list_info

        p_list_create: p_core_list_create_in 
        p_list: p_core_list_in 

        .pseudopc $7000 {

                list:                
        }
        

.segment app_parms

        p_concat: p_core_str_concat_in 

	__AppStateHandler__(APP_STATE_RUN, app_run)

.segment app_code

	app_run:

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)

        examineTest(test1)
        examineTest(test2)
        examineTest(test3)
        // examineTest(test4)
        examineTest(test5)
	
        c64unitExit()

        test1: {

        .var len = hello.size()
        assertMemoryEqual(s_hello, s_hello, len, "should equal 'hello'")
        rts

        }

        test2: {

        StorePointer(s_hello, p_concat.stringa)
        StorePointer(s_world, p_concat.stringb)
        lda #1
        sta p_concat.options
        ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, s_result)

        .var len = hello_world.size()
        assertMemoryEqual(s_result, s_hello_world, len, "should be 'hello world'")
        rts

        }

        test3: {

        StorePointer(s_hello, p_concat.stringa)
        StorePointer(s_there, p_concat.stringb)
        lda #0
        sta p_concat.options
        ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, s_result)

        StorePointer(s_result, p_concat.stringa)
        StorePointer(s_world, p_concat.stringb)
        ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, s_result)

        .var len = hello_there.size()
        assertMemoryEqual(s_hello_there, s_result, len, "should be 'hellothereworld'")
        rts     

        }

        test4: {
        
        StorePointer(list, p_list_create.list)
        
        lda #5
        sta p_list_create.size
        sta p_list_create.length

        lda #0
        sta p_list_create.storage
        sta p_list_create.delim

        ICall1(I_CORE_CALL, I_CORE_LIST_CREATE, p_list_create)

        StorePointer(list, p_list)
                
        ICall1(I_CORE_CALL, I_CORE_LIST_INFO, p_list)
        
        assertMemoryEqual(z_list, ADDR_OUT, 14, "list info should match")

        rts

        .const addr = $7000
        .var list_lo_table = addr + 15
        .var list_hi_table = list_lo_table + 5
        .var list_data = list_hi_table + 5

        z_list:
                .byte $ff
                .byte $ff
                .byte $05
                .byte $05
                .byte $00
                .byte $00
                .byte <list_lo_table
                .byte >list_lo_table
                .byte <list_hi_table
                .byte >list_hi_table
                .byte $00
                .byte $00
                .byte $00
                .byte $00

        }

        test5: {
        
        StorePointer(list, p_list_create.list)
        
        lda #5
        sta p_list_create.size
        sta p_list_create.length

        lda #1
        sta p_list_create.storage
        lda #0
        sta p_list_create.delim

        ICall1(I_CORE_CALL, I_CORE_LIST_CREATE, p_list_create)

        StorePointer(list, p_list)
                
        ICall1(I_CORE_CALL, I_CORE_LIST_INFO, p_list)
        
        assertMemoryEqual(z_list, ADDR_OUT, 14, "list info should match")

        rts

        .const addr = $7000
        .var list_lo_table = addr + 14
        .var list_hi_table = list_lo_table + 5
        .var list_data = list_hi_table + 5

        z_list:
                .byte $ff
                .byte $ff
                .byte $05
                .byte $05
                .byte $01
                .byte $00
                .byte <list_lo_table
                .byte >list_lo_table
                .byte <list_hi_table
                .byte >list_hi_table
                .byte <list_data
                .byte >list_data
                .byte <list_data
                .byte >list_data

        }

TestEnd()
 