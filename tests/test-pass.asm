#importonce

.encoding "petscii_mixed"

#import "_test.asm"

TestStart("test-pass", "Passing Unit Testing With C64Unit")

.segment app_parms

	__AppStateHandler__(APP_STATE_RUN, app_run)

.segment app_code

	app_run:

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)

	examineTest(testPass)
	c64unitExit()

	testPass:

	lda #1
	assertEqualToA(1, "Pass")
	rts

TestEnd()
