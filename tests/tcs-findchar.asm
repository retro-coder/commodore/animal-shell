#importonce

.encoding "petscii_mixed"

#import "_test.asm"

	TestStart("tcs-findchar", "Core String FindChar Call")

.segment app_data

	i_findchar: p_core_str_findchar_in 
	o_findchar: p_core_str_findchar_out 

	stringa: .text @"hello\$00"

.segment app_parms

	__AppStateHandler__(APP_STATE_RUN, app_run)

.segment app_code

	app_run:

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)

	examineTest(test_found_1)
	c64unitExit()

	test_found_1: {

	StorePointer(stringa, i_findchar.string)
	lda #'e'
	sta i_findchar.search
	ICall2(I_CORE_CALL, I_CORE_STR_FINDCHAR, i_findchar, o_findchar)
	assertCarryFlagNotSet("Carry Flag Should Be Clear")
	lda o_findchar.pos
	assertEqualToA(1, "Return Position Should Be [1]")

	rts

	}

	TestEnd()
