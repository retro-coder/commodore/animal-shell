
## System Call Interface Structure
This is an example of a source file structure that
can be used for a shell or core interface call.

``` c++
#importonce
#import "core/_core.asm"
#import "core/core.asm"

// use the base location for filenamespace

.filenamespace __core__

// next level for the group of calls it belongs in

.namespace __str__ 
{

// the name of the actual call interface (only one word)

.namespace __split__ 
{

// all segment names should have a combination of group/call 


// this segment is used to store constant data like
// string for printing. it should never be used for
// any type of variable storage because it might be
// in a rom and not writeable -- instead you can
// use the system area space

.segment core_data "str_split"

// short strings can be on the same line

string0:	.text "hello"

// longer string should be on a separate line
string1:

		.text @"this is some really long string/$00"

.segment core_virtual "str_split"

// parameters and working storage should be stored in
// temp space using virtual address
//
// there is a max of 32 bytes allocated in TEMP
// storage

.pseudopc CORE_ZERO_TEMP {
	
	// parameters can be named using shorthand
	p_split:		p_core_str_split_in
	p_list_append: 	p_core_list_append_in 

	// working storage should start with t,z or x
	t_string:		p_type_address 

	// another example
	z_next:			p_type_address 

	// and two more examples
	x_quote:		p_type_byte
	x_last:			p_type_byte
}

.segment core_code "str_split"

// create label matching the base/group/name of the call

core_str_split:

	// your code goes here

	lda	#$00
	sta	$d020

	// make sure you return

	rts

	// use the handler macro to add the call
	// use the call # defined in the VARS file

	__CoreHandler__(I_CORE_STR_SPLIT, core_str_split)
}

}
```