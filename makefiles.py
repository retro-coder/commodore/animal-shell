
commands = [
    {'src': 'command/command_dir.asm', 'out': 'dir.prg', 'filename': 'dir', 'check': ['system/**/*.asm']},
    {'src': 'command/command_dos.asm', 'out': 'dos.prg', 'filename': 'dos', 'check': ['system/**/*.asm']},
    {'src': 'command/command_convert.asm', 'out': 'convert.prg', 'filename': 'convert', 'check': ['system/**/*.asm']},
    {'src': 'command/command_fast.asm', 'out': 'fast.prg', 'filename': 'fast', 'check': ['system/**/*.asm']},
    {'src': 'command/command_peek.asm', 'out': 'peek.prg', 'filename': 'peek', 'check': ['system/**/*.asm']},
    {'src': 'command/command_pdir.asm', 'out': 'pdir.prg', 'filename': 'pdir', 'check': ['system/**/*.asm']},
    {'src': 'command/command_run.asm', 'out': 'run.prg', 'filename': 'run', 'check': ['system/**/*.asm']},
    {'src': 'command/command_sig.asm', 'out': 'sig.prg', 'filename': 'sig', 'check': ['system/**/*.asm']},
    {'src': 'command/command_type.asm', 'out': 'type.prg', 'filename': 'type', 'check': ['system/**/*.asm']},
    {'src': 'command/command_ver.asm', 'out': 'ver.prg', 'filename': 'ver', 'check': ['system/**/*.asm']},
]

drivers = [
    {'src': 'driver/driver_test.asm', 'out': 'driver_test.prg', 'filename': 'driver.d', 'check': ['system/**/*.asm', 'drivers/**/*.asm']},
]

libraries = [
    {'src': 'library/library_test.asm', 'out': 'library_test.prg', 'filename': 'library.l', 'check': ['system/**/*.asm', 'library/**/*.asm']},
]

command_ver = [
    {'src': 'command/command_ver.asm', 'out': 'ver.prg', 'filename': 'ver', 'force': True},
]

apps = [
    {'src': 'app/hello.asm'},
]

app_samples = [
    {'src': 'samples/app/app0.asm'},
    {'src': 'samples/app/app1.asm'},
    {'src': 'samples/app/app2.asm'},
    {'src': 'samples/app/app3.asm'},
    {'src': 'samples/app/app4.asm'},
    {'src': 'samples/app/appinfo.asm'},
    {'src': 'samples/app/test-drivepoll.asm'},
    {'src': 'samples/kick/kick.asm'},
    {'src': 'samples/kick/kdir.asm'},
    {'src': 'samples/kick/kcmd.asm'},
    {'src': 'samples/kick/kopenerr.asm'},
    {'src': 'samples/app/minimal.asm'},
    {'src': 'samples/app/simple.asm'},
    {'src': 'samples/core/str/cs-concat.asm'},
    {'src': 'samples/core/str/cs-split.asm'},
    {'src': 'samples/core/str/cs-split-e0.asm'},
    {'src': 'samples/core/list/cl-create.asm'},
    {'src': 'samples/core/list/cl-create-var.asm'},
    {'src': 'samples/core/list/cl-first.asm'},
    {'src': 'samples/core/list/cl-firstlast.asm'},
    {'src': 'samples/core/list/cl-get.asm'},
    {'src': 'samples/core/list/cl-info.asm'},
    {'src': 'samples/core/list/cl-append.asm'},
    {'src': 'samples/core/list/cl-append-var.asm'},
    {'src': 'samples/core/list/cl-large-var.asm'},
    {'src': 'samples/core/list/cl-clear.asm'},
    {'src': 'samples/core/list/cl-clear-var.asm'},
    {'src': 'samples/core/list/cl-create-multi.asm'},
    {'src': 'samples/core/list/cl-iterate.asm'},
    {'src': 'samples/core/list/cl-large.asm'},
    {'src': 'samples/core/list/cl-last.asm'},
    {'src': 'samples/core/list/cl-next.asm'},
    {'src': 'samples/core/list/cl-vget.asm'},
    {'src': 'samples/core/file/cf-getc.asm'},
    {'src': 'samples/core/file/cf-getc2.asm'},
    {'src': 'samples/core/file/cf-open.asm'},
    {'src': 'samples/core/io/ci-print.asm'},
    {'src': 'samples/core/util/cu-str2num.asm'},
    {'src': 'samples/core/util/cu-num2str.asm'},
    {'src': 'samples/xmem/xm-loaddrv.asm'},
    {'src': 'samples/test1-seq.asm', 'filename': 'test1', 'type': 's'},
    {'src': 'samples/test2-seq.asm', 'filename': 'test2', 'type': 's'},
]

app_test_samples = [
]

unit_tests = [
    {'src': 'tests/zzz/zzz-run-basic.asm'},
    {'src': 'tests/test-app.asm', 'check': ['tests/_test.asm']},
    {'src': 'tests/test-pass.asm', 'check': ['tests/_test.asm']},
    {'src': 'tests/test-fail.asm', 'check': ['tests/_test.asm']},
    {'src': 'tests/test-mem.asm', 'check': ['tests/_test.asm']},
    {'src': 'tests/tcs-findchar.asm', 'check': ['tests/_test.asm']},
]

boot_64 = [
    {'src': 'build/build_boot_64.asm', 'out': 'boot.64.prg', 'filename': 'boot.64', 'check': ['c64/boot.asm']},
    {'src': 'build/build_boot_64g.asm', 'out': 'bootg.64.prg', 'filename': 'bootg.64', 'check': ['c64/boot.asm']},
    {'src': 'build/build_loader_64.asm', 'out': 'loader.64.prg', 'filename': 'loader.64', 'check': ['c64/loader.asm']},
    {'src': 'build/build_core_64.asm', 'out': 'core.64.prg', 'filename': 'core.64', 'check': ['core/**/*.asm']},
    {'src': 'build/build_core_64g.asm', 'out': 'coreg.64.prg', 'filename': 'coreg.64', 'check': ['core/**/*.asm']},
    {'src': 'build/build_shell_64.asm', 'out': 'shell.64.prg', 'filename': 'shell.64', 'check': ['shell/**/*.asm']},
]

boot_128 = [
    {'src': 'build/build_boot_128.asm', 'out': 'boot.128.prg', 'filename': 'boot.128', 'check': ['c128/boot.asm']},
    {'src': 'build/build_boot_128g.asm', 'out': 'bootg.128.prg', 'filename': 'bootg.128', 'check': ['c128/boot.asm']},
    {'src': 'build/build_loader_128.asm', 'out': 'loader.128.prg', 'filename': 'loader.128', 'check': ['c128/loader.asm']},
    {'src': 'build/build_core_128.asm', 'out': 'core.128.prg', 'filename': 'core.128', 'check': ['core/**/*.asm']},
    {'src': 'build/build_core_128g.asm', 'out': 'coreg.128.prg', 'filename': 'coreg.128', 'check': ['core/**/*.asm']},
    {'src': 'build/build_shell_128.asm', 'out': 'shell.128.prg', 'filename': 'shell.128', 'check': ['shell/**/*.asm']},
]


# disk_build_64 = [
#     {'src': 'build/build_boot_64.asm', 'out': 'boot.prg', 'filename': 'boot', 'check': ['c64/boot.asm']},
#     {'src': 'build/build_loader_64.asm', 'out': 'loader.prg', 'filename': 'loader', 'check': ['c64/loader.asm']},
#     {'src': 'build/build_core.asm', 'out': 'core.prg', 'filename': 'core', 'check': ['core/**/*.asm']},
#     {'src': 'build/build_shell.asm', 'out': 'shell.prg', 'filename': 'shell', 'check': ['shell/**/*.asm']},
# ]

# disk_build_128 = [
#     {'src': 'build/build_boot.asm', 'out': 'boot.prg', 'filename': 'boot', 'check': ['c128/boot.asm']},
#     {'src': 'build/build_loader_128.asm', 'out': 'loader.prg', 'filename': 'loader', 'check': ['c128/loader.asm']},
#     {'src': 'build/build_core.asm', 'out': 'core.prg', 'filename': 'core', 'check': ['core/**/*.asm']},
#     {'src': 'build/build_shell.asm', 'out': 'shell.prg', 'filename': 'shell', 'check': ['shell/**/*.asm']},
# ]

cart_64 = [
    {'src': 'build/build_cart_64.asm', 'out': 'cart64.bin', 'filename': 'shell', 'check': ['build/**/*.asm','c64/**/*.asm','core/**/*.asm','shell/**/*.asm']},
    {'src': 'build/build_cart_64g.asm', 'out': 'cart64g.bin', 'filename': 'shell', 'check': ['build/**/*.asm','c64/**/*.asm','core/**/*.asm','shell/**/*.asm']},
]

cart_128 = [
    {'src': 'build/build_cart_128.asm', 'out': 'cart128.bin', 'filename': 'shell', 'check': ['build/**/*.asm','c128/**/*.asm','core/**/*.asm','shell/**/*.asm']},
    {'src': 'build/build_cart_128g.asm', 'out': 'cart128g.bin', 'filename': 'shell', 'check': ['build/**/*.asm','c128/**/*.asm','core/**/*.asm','shell/**/*.asm']},
    {'src': 'build/build_int_128.asm', 'out': 'int128.bin', 'filename': 'shell', 'check': ['build/**/*.asm','c128/**/*.asm','core/**/*.asm','shell/**/*.asm']},
    {'src': 'build/build_int_128g.asm', 'out': 'int128g.bin', 'filename': 'shell', 'check': ['build/**/*.asm','c128/**/*.asm','core/**/*.asm','shell/**/*.asm']},
]

other_apps = [
        
    {'src': 'wic64/wic64-0.asm', 'out': 'wic64-0.prg', 'filename': 'wic64-0', 'build': False},
    {'src': 'wic64/wic64-1.asm', 'out': 'wic64-1.prg', 'filename': 'wic64-1', 'build': False},
    {'src': 'wic64/wic64-2.asm', 'out': 'wic64-2.prg', 'filename': 'wic64-2', 'build': False},
        
]

# make_disk_64_files = disk_build_64 + commands + app_samples +[]
# make_disk_128_files = disk_build_128 + commands + app_samples + []
# make_test_files = []
# make_app_files = []

boot_disk_files = boot_128 + boot_64 + commands + drivers + libraries + []
