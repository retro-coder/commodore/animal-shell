
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "system/sys.asm"
#import "_library_var.asm"
#import "_library_dat.asm"

.struct DriverInfo {

	signature,
	platform,
	major,
	minor,
	name,
	description,
	copyright,
	author,
	website,
	email
}

.var __lib_info__ = DriverInfo()

.eval __lib_info__.name = "NONE"
.eval __lib_info__.platform = LIBRARY_PLATFORM_ANY
.eval __lib_info__.description = "No Description"
.eval __lib_info__.copyright = "No Copyright"
.eval __lib_info__.major = 0
.eval __lib_info__.minor = 0
.eval __lib_info__.author = "No Author"
.eval __lib_info__.website = "No Website"
.eval __lib_info__.email = "No Email"

/*
	Library Information
*/
.macro __LibraryInfo__(info) {

	.if (info.name != null) { .eval __lib_info__.name = info.name }
	.if (info.description != null) { .eval __lib_info__.description = info.description }
	.if (info.platform != null) { .eval __lib_info__.platform = info.platform }
	.if (info.copyright != null) { .eval __lib_info__.copyright = info.copyright }
	.if (info.major != null) { .eval __lib_info__.major = info.major }
	.if (info.minor != null) { .eval __lib_info__.minor = info.minor }
	.if (info.author != null) { .eval __lib_info__.author = info.author }
	.if (info.website != null) { .eval __lib_info__.website = info.website }
	.if (info.email != null) { .eval __lib_info__.email = info.email }
}

.macro __LibraryStart__() {

.segment library_info

	i_signature:	.text LIBRARY_SIGNATURE
	i_platform:	.byte __lib_info__.platform
	i_major:	.byte __lib_info__.major
	i_minor:	.byte __lib_info__.minor
	i_name:		.byte <dr_name, >dr_name
	i_description:	.byte <dr_desc, >dr_desc
	i_copyright:	.byte <dr_copyright, >dr_copyright
	i_author:	.byte <dr_author, >dr_author
	i_website:	.byte <dr_website, >dr_website
	i_email:	.byte <dr_email, >dr_email

.segment library_sys

	clear:          .fill 16, $ee

.segment library_data

	dr_name:	.text __lib_info__.name + @"\$00"
	dr_desc:        .text __lib_info__.description + @"\$00"
	dr_copyright:   .text __lib_info__.copyright + @"\$00"
	dr_author:      .text __lib_info__.author + @"\$00"
	dr_website:     .text __lib_info__.website + @"\$00"
	dr_email:       .text __lib_info__.email + @"\$00"

.segment library_call

	jmp __library_handler__

.segment library_handler

	nop
	nop
	nop

	__library_handler__:

	lda SYS_CALL
}

.macro __LibraryHandler__(call, handler) {

.segment driver_virtual

	cmp #call
	bne !+
	jmp handler

	!:
      
}

.macro __LibraryEnd__() {

	rts
}
