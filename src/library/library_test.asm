
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#import "library/library.asm"

.filenamespace test

.segment library_code "library_test"

__LibraryStart__()

__LibraryEnd__()

.file [name="library_test.prg", segments="library"]
