
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#import "system/sys.asm"

.segment library [segments="library_call, library_info, library_sys, library_data, library_handler, library_code, library_parms, library_virtual"]

.segmentdef library_call        [start=ADDR_LIB]
.segmentdef library_info	[start=ADDR_LIB_INFO]
.segmentdef library_sys		[start=ADDR_LIB_SYS]
.segmentdef library_data	[start=ADDR_LIB_DATA]
.segmentdef library_handler	[startAfter="library_data"]
.segmentdef library_code	[startAfter="library_handler"]
.segmentdef library_parms	[start=ADDR_WORK, virtual]
.segmentdef library_virtual	[virtual]

