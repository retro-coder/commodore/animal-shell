
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 
#import "system/sys.asm"

// library signature
.const LIBRARY_SIGNATURE        = "##l0"

/*
	LIBRARY PLATFORM
*/
.const LIBRARY_PLATFORM_ANY	= $00
.const LIBRARY_PLATFORM_128	= $01
.const LIBRARY_PLATFORM_64	= $02
