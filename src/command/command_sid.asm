
#importonce 

#import "command/command.asm"
#import "core/core.asm"
#import "shell/shell.asm"
#import "system/sys.asm"


.encoding "petscii_mixed"

.filenamespace command


.segment command_parms

.segment command_data

	p_list_get:
	ds_core_list_get_in 

p_file_loadat:

	ds_core_file_loadat_in 


.segment command_code

__CommandInit__()

.eval c_info.name = "sid"

__CommandCallHandler__(I_CMD_CALL_RUN)

	lda	#$02
	sta	ADDR_IN
	ICall(I_SHELL_CALL, I_SHELL_CMD_PARM_GET)
	NPrintFromPointer(ADDR_OUT)

	// right now assuming we are using one of gregs sids that start at $2000
	// and that the offset is $7c. eventually we will need to read the 
	// sid header and handle it accordingly

	// load sid interrupt handler into bank 0 @ $1300

	ldx	#$00

	!:

	lda	bank0_handler, x
	sta	$1300, x
	inx
	cpx	#bank0_handler_end - bank0_handler
	bne	!-

	// inject sid player code into bank 1 @ $1300

	// ** complicated -- easier to just load a file from disk I think?

	ldx 	#$00
	ldy	#$00

	!:

	txa	
	pha	

	lda	#$00
	sta	$fb
	lda	#$13
	sta	$fc 
	lda	#$fb
	sta	$02b9
	lda	bank1_handler, x
	ldx	#$01
	jsr	$ff77

	iny 
	pla 
	tax	
	inx
	cpx	#bank1_handler_end - bank1_handler 
	bne	!-

	// load sid into bank 1 @ $2000

	StorePointer(ADDR_OUT, p_file_loadat.filename)
	lda	#$08
	sta	p_file_loadat.device
	lda	#<8068
	sta	p_file_loadat.address
	lda	#>8068
	sta	p_file_loadat.address + 1
	lda	#$01
	sta	p_file_loadat.bank
	lda	#$01
	sta	p_file_loadat.messages
	ICall1(I_CORE_CALL, I_CORE_FILE_LOADAT, p_file_loadat) 

	// inject sid player interrupt into $0315

        sei
        lda #<irq 
        sta $0314
        lda #>irq 
        sta $0315
        cli

        rts

.segment command_code "bank0_handler"

bank0_handler:

.pseudopc $1300
{

irq:
	sty	$08
	stx	$07
	sta	$06
	php 
	pla 
	sta	$05
	lda	#$01
	sta	$02 
	lda	#$13
	sta	$03
	lda	#$00
	sta	$04
	jsr	$ff6e
	lda	$05
	pha 
	lda	$06
	ldx	$07
	ldy	$08
	plp	
	// inc 	$d021
	// inc	$0400

old_irq:

        jmp	$fa65
}

bank0_handler_end:

.segment command_code "bank1_handler"

bank1_handler:
.pseudopc $1300
{
	lda state
	beq init
	jmp play

	init:

	lda $ff00
	sta mff00 
	lda #%01111110
	sta $ff00
	lda #$00
	jsr $2000
	// inc $d020
	lda mff00
	sta $ff00
	lda #$01
	sta state
	rts 

	play:

	lda $ff00
	sta mff00 
	lda #%01111110
	sta $ff00
	// inc $d020
	jsr $2003
	// dec $d020
	// inc $d021
	lda mff00
	sta $ff00
	rts 

	mff00: .byte $00
	state: .byte $00

}

bank1_handler_end:

__CommandHandler__()



.file [name="sid.prg", segments="command"]
