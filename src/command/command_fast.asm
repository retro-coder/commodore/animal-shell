
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 

#import "command/command.asm"
#import "core/core.asm"
#import "system/sys.asm"

	.encoding "petscii_mixed"

	.var info = CommandInfo()

	.eval info.name = "FAST"
	.eval info.platform = COMMAND_PLATFORM_C128 

	__CommandInfo__(info)

	__CommandStart__()

.segment command_data

	__CommandStateHandler__(I_CMD_CALL_RUN, run)

.segment command_code

/*
	this code was basically copied
	from the FAST call in basic
	kernal at $77b3 without	the
	change to BANK15
*/

	run:

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)

	lda $d011
	and #$6f
	sta $d011
	lda #$01
	sta $d030
	
	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	rts

	__CommandEnd__()

.file [name="fast.prg", segments="command"]
