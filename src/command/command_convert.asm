
#import "command/command.asm"
#import "core/core.asm"
#import "shell/shell.asm"
#import "system/sys.asm"

.encoding "petscii_mixed"


.filenamespace command

.segment command_parms

	.pseudopc ADDR_IN { p_str2num: p_core_util_str2num }
	.pseudopc ADDR_IN { p_num2str: p_core_util_num2str }
	
.segment command_data

	num:	.byte $00,$00,$00,$00
	space:	.text @" \$00"
	dot:	.text @"."
	ptr:	.word $0000
	break: .text @"\$0d\$00"


	__CommandStart__()

	__CommandStateHandler__(I_CMD_CALL_RUN, run)

.segment command_code

	run:

        PrintFromAddress(break)
	PrintFromAddress(space)

	ICall(I_SHELL_CALL, I_SHELL_PARM_FIRST)

        ICall(I_CORE_CALL, I_CORE_BANK_SYS_IN)

        CopyPointer(SYS_CALL_RETURN, ADDR_ZERO_SHARED)
        StorePointer(ADDR_WORK, ADDR_ZERO_SHARED + 2)
        CopyStringFromPointers(ADDR_ZERO_SHARED, ADDR_ZERO_SHARED + 2)
        
        ICall(I_CORE_CALL, I_CORE_BANK_SYS_OUT)

        // convert the string to a number

        StorePointer(ADDR_WORK, p_str2num)
        ICall2(I_CORE_CALL, I_CORE_UTIL_STR2NUM, p_str2num, num)

	// convert the number to dec/hex/binary and print

	StorePointer(num, p_num2str.addr)
	lda	#' '
	sta	p_num2str.type
	ICall1(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str)
	PrintFromAddress(ADDR_OUT)
	PrintFromAddress(space)

	StorePointer(num, p_num2str.addr)
	lda	#'$'
	sta	p_num2str.type
	ICall1(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str)
	PrintFromAddress(ADDR_OUT)
	PrintFromAddress(space)

	StorePointer(num, p_num2str.addr)
	lda	#'%'
	sta	p_num2str.type
	ICall1(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str)
	PrintFromAddress(ADDR_OUT)
	NPrintFromAddress(space)

	rts

	__CommandEnd__()

.file [name="convert.prg", segments="command"]
