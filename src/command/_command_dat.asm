
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "system/sys.asm"

.pseudocommand @ds_command_info {

	signature:	p_type_signature 
	platform:	p_type_byte
	type:		p_type_number 
	major:		p_type_byte 
	minor:		p_type_byte 
	name:		p_type_pointer 
	description:	p_type_pointer 
	copyright:	p_type_pointer 
	author:		p_type_pointer 
	website:	p_type_pointer 
	email:		p_type_pointer 
	help:		p_type_pointer
	reserved:	p_type_reserved(20)
}

.pseudocommand @ds_command_system_space {

	state:		p_type_byte
	next_state:	p_type_byte  
	reserved:	p_type_reserved(14)
}
