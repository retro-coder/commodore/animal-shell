
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "_command_seg.asm"
#import "_command_mac.asm"
#import "_command_var.asm"
#import "_command_sci.asm"
#import "_command_dat.asm"
#import "_command_par.asm"
