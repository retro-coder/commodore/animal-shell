
/*
	Shows the Signature of a file when present.

	Every Animal Shell compatible program will contain
	a signature immediately after the jump call at its
	start address.

	This command is similar to the INFO command except
	that it does not need to read in the entire
	header so it is much faster.

	These are the following signature patterns:

		#A0#	Application
		#C0#	Command
		#D0#	Driver
		#L0#	Library

	If no compatible signature is found it will
	display

		No Animal Shell Signature Found
*/

#importonce 

#import "command/command.asm"
#import "core/core.asm"
#import "shell/shell.asm"
#import "system/sys.asm"

.filenamespace command

.encoding "petscii_mixed"

.segment command_data

	s_app:	.text @"Application\$00"
	s_cmd:	.text @"Command\$00"
	s_drv:	.text @"Driver\$00"
	s_lib:	.text @"Library\$00"

	s_none:	.text @"\$0dNo Animal Shell Signature Found\$00"

	.var info=CommandInfo()

	.eval info.name = "SIG"

	__CommandInfo__(info)

	__CommandStart__()

	__CommandStateHandler__(I_CMD_CALL_RUN, run)

.segment command_code

	run:

	NPrintFromAddress(s_none)
	rts

	__CommandEnd__()

.file [name="sig.prg", segments="command"]
