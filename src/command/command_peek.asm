
#import "command/command.asm"
#import "core/core.asm"
#import "shell/shell.asm"
#import "system/sys.asm"

.encoding "petscii_mixed"

.filenamespace command

.segment command_parms

        .pseudopc ADDR_IN {
                p_str2num: p_core_util_str2num
        }

        .pseudopc ADDR_IN {
                p_num2str: p_core_util_num2str
        }

.segment command_data

        test:	.text @"1024 $0400; 173 $AD; 685 $02ad, \"A\"\$00"
        fmt: 	.text @"%d %x; %d %x; %d %x; \"%s\"\$00"
        num:	.byte $00,$00,$00,$00
        space:	.text @" \$00"
        numb:	.byte $00
        numw:	.byte $00
        comma:	.text @", \$00"
        quote:	.text @"\"\$00"
        break: .text @"\$0d\$00"

        val:	.byte $00, $00
        ptr:	.word $0000

        help:   .text @"PEEK [$]address\$00"

        __CommandStart__()

        __CommandStateHandler__(I_CMD_CALL_RUN, run)

.segment command_code

        run:

        PrintFromAddress(break)
        PrintFromAddress(space)

        // get first parm -> should be string

        ICall(I_SHELL_CALL, I_SHELL_PARM_FIRST)

        bcc !+
        NPrintFromAddress(help)
        rts       

        !:

        ICall(I_CORE_CALL, I_CORE_BANK_SYS_IN)

        CopyPointer(SYS_CALL_RETURN, ADDR_ZERO_SHARED)
        StorePointer(ADDR_WORK, ADDR_ZERO_SHARED + 2)
        CopyStringFromPointers(ADDR_ZERO_SHARED, ADDR_ZERO_SHARED + 2)
        
        ICall(I_CORE_CALL, I_CORE_BANK_SYS_OUT)

        // convert the string to a number

        StorePointer(ADDR_WORK, p_str2num)
        ICall2(I_CORE_CALL, I_CORE_UTIL_STR2NUM, p_str2num, num)

        // convert the number to dec/hex/binary and print

        StorePointer(num, p_num2str.addr)

        lda #' '
        sta p_num2str.type

        ICall1(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str)

        PrintFromAddress(ADDR_OUT)
        PrintFromAddress(space)

        StorePointer(num, p_num2str.addr)

        lda #'$'
        sta p_num2str.type

        ICall1(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str)

        PrintFromAddress(ADDR_OUT)
        PrintFromAddress(comma)

        // take first 2 bytes as lo/hi and store in zp

        lda num 
        sta SYS_CALL
        lda num + 1
        sta $fb

        // get value at address

        ldy #$00
        lda (SYS_CALL), y
        sta numb

        // A = *SCII-code

        // if A<32 then...
        cmp #$20
        bcc ddRev

        // if A<96 then...
        cmp #$60
        bcc dd1

        // if A<128 then...
        cmp #$80
        bcc dd2

        // if A<160 then...
        cmp #$a0
        bcc dd3

        // if A<192 then...
        cmp #$c0
        bcc dd4

        // if A<255 then...
        cmp #$ff
        bcc ddRev

        // A=255, then A=126
        lda #$7e

        // A=255, then A=126
        lda #$5e
        bne ddEnd

        // if A=96..127 then strip bits 5 and 7
        dd2:
        and #$5f
        bne ddEnd

        // if A=128..159, then set bit 6
        dd3:
        ora #$40
        bne ddEnd

        // if A=160..191 then flip bits 6 and 7
        dd4:
        eor #$c0
        bne ddEnd

        // if A=32..95 then strip bits 6 and 7
        dd1:
        and #$3f
        bpl ddEnd

        // flip bit 7 (reverse on when off and vice versa)
        ddRev:	
        eor #$80
        ddEnd:

        sta val

        // convert byte value to string

        lda numb

        sta num
        lda #$00
        sta num + 1
        sta num + 2
        sta num + 3

        StorePointer(num, p_num2str.addr)

        lda #' '
        sta p_num2str.type

        ICall1(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str)

        PrintFromAddress(ADDR_OUT)
        PrintFromAddress(space)

        StorePointer(num, p_num2str.addr)

        lda #'$'
        sta p_num2str.type

        ICall1(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str)

        PrintFromAddress(ADDR_OUT)
        PrintFromAddress(comma)

        PrintFromAddress(quote)
        PrintFromAddress(val)
        PrintFromAddress(quote)

        NPrintFromAddress(space)

        rts

        __CommandEnd__()

.file [name="peek.prg", segments="command"]
