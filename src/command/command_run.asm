
/*
	Run a Program from the Command Line.

	The use of this command will let a person
	run a program in memory. 

	When a filename is specified, the application
	will be loaded into memory prior to running.

	If no filename is specified, the current
	application in memory will be run.

	When there is no program loaded, running this
	command will produce an error.
*/

#importonce 

#import "command/_command.asm"
#import "app/app.asm"
#import "core/core.asm"
#import "shell/shell.asm"
#import "system/sys.asm"

.encoding "petscii_mixed"

.filenamespace command

.segment command_parms "command_run"

	.pseudopc ADDR_IN {
		p_list_get: p_shell_cmd_parm_get_in
	}

	.pseudopc ADDR_IN {
		p_file_load: p_core_file_load_in
	}

	.pseudopc ADDR_APP_INFO {
		app_info: ds_app_info	
	}

.segment command_data "command_run"

	err1: .text @"SEC000 - No Animal Shell Application loaded\$00"
	err2: .text @"SEC010 - File not found\$00"

	.var info = CommandInfo()
	.eval info.name = "RUN"

	__CommandInfo__(info)
		
	__CommandStart__()

	__CommandStateHandler__(I_CMD_CALL_RUN, run)

.segment command_code "command_run"

	run:

	lda #$01
	sta ADDR_IN
	ICall(I_SHELL_CALL, I_SHELL_PARM_GET)
	bcc !+
	jmp no_file_parm

	!:
	lda #1
	sta p_file_load.show 

	lda SHELL_DEVICE
	sta p_file_load.device

	lda ADDR_OUT
	sta p_file_load.filename
	lda ADDR_OUT + 1
	sta p_file_load.filename + 1

	ICall1(I_CORE_CALL, I_CORE_FILE_LOAD, p_file_load)
	bcc start
	jmp error2

	start:

	SetShellState(SHELL_STATE_APP)
	SetAppState(APP_STATE_INIT)

	rts

	no_file_parm:

	lda app_info.signature + 0
	cmp #'#'
	bne error1
	lda app_info.signature + 1
	cmp #'#'
	bne error1
	lda app_info.signature + 2
	cmp #'a'
	bne error1
	lda app_info.signature + 3
	cmp #'0'
	bne error1

	// if it matches then run the program

	jmp start

	error1:
	PrintFromAddress(err1)
	rts

	error2:
	PrintFromAddress(err2)
	rts

	__CommandEnd__()

.file [name="run.prg", segments="command"]
