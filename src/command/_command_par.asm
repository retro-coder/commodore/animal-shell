
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "system/sys.asm"

.pseudocommand @p_command_get_in {

	// which attribute to pull from the command header
	attribute: p_type_byte

}