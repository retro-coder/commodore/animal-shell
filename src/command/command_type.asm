
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 

#import "command/command.asm"
#import "core/core.asm"
#import "shell/shell.asm"
#import "system/sys.asm"

.encoding "petscii_mixed"

.filenamespace command

.segment command_data

	p_open: p_core_file_open_in 
	p_open_out: p_core_file_open_out 
	p_getc_in: p_core_file_getc_in 
	p_concat: p_core_str_concat_in
	w_filename: p_type_string(16) 

        err1: .text @"\$0dSEC000 Filename Missing\$00"
	err2: .text @"SEC010 - File not found\$00"
	type: .text @",s\$00"

        .var info = CommandInfo()
        .eval info.name = "TYPE"

	s_init: .text @"command>type>init\$00"
	s_start: .text @"command>type>start\$00"
	s_run: .text @"command>type>run\$00"
	s_stop: .text @"command>type>stop\$00"

        __CommandInfo__(info)

        __CommandStart__()

        __CommandStateHandler__(I_CMD_CALL_INIT, command_type_init)
        __CommandStateHandler__(I_CMD_CALL_START, command_type_start)
        __CommandStateHandler__(I_CMD_CALL_RUN, command_type_run)
        __CommandStateHandler__(I_CMD_CALL_STOP, command_type_stop)

.segment command_code "command_type"

	command_type_init:{

	__LogTrace__("command>type>init")
	rts
	
	}

	command_type_start: {

	__LogTrace__("command>type>start")

	ICall(I_SHELL_CALL, I_SHELL_PARM_FIRST)
	bcc !+
	jmp error1

        !:

	// StorePointer(filename, p_open.filename)	
	CopyWord(ADDR_OUT, p_open.filename)
	
	// open file on current device number
	
	lda	SHELL_DEVICE
	sta	p_open.device
	lda	#1
	sta	p_open.show
	
	ICall2(I_CORE_CALL, I_CORE_FILE_OPEN, p_open, p_open_out)

        bcc !+
        jmp error2

	!:

	rts
	
        error1:

        // ICall(I_SHELL_CALL, I_SHELL_PARM_GET)
        NPrintFromAddress(err1)
	SetCommandState(COMMAND_STATE_STOP)
	rts

        error2:

        // ICall(I_SHELL_CALL, I_SHELL_PARM_GET)
        NPrintFromAddress(err2)
	SetCommandState(COMMAND_STATE_STOP)
	rts

	}

	command_type_stop: {

	__LogTrace__("command>type>stop")

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)
	lda	p_open_out
	jsr	JCLOSE
	jsr	$ffcc
	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	rts

	}

        command_type_run: {

	__LogTrace__("command>type>run")

	lda	p_open_out
	sta 	p_getc_in.file

        !:

	ICall1(I_CORE_CALL, I_CORE_FILE_GETC, p_getc_in)

	bcs	eof

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)
	lda	ADDR_OUT
	jsr	$ffd2
	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	jmp	!-

	eof:

        rts

	}

        __CommandEnd__()

.file [name="type.prg", segments="command"]
