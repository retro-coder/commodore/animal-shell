
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 

.segment sys_virtual

.pseudopc ADDR_CMD_SYS {
__command_system__:
ds_command_system_space
}

// set the state for the next cycle
.macro @SetCommandState (state) {

lda #state 
sta __command_system__.next_state

}
