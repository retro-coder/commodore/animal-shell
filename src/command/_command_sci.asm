
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.label I_CMD_CALL		= ADDR_CMD

.const I_CMD_CALL_LOAD		= $10
.const I_CMD_CALL_UNLOAD	= $15
.const I_CMD_CALL_INIT		= $20
.const I_CMD_CALL_GET		= $30
.const I_CMD_CALL_SET		= $35
.const I_CMD_CALL_RUN		= $40
.const I_CMD_CALL_STEP		= $45
.const I_CMD_CALL_START		= $50
.const I_CMD_CALL_STOP		= $60
.const I_CMD_CALL_EXIT		= $90

.const I_CMD_ATTR_ID		= $01
.const I_CMD_ATTR_NAME		= $02
.const I_CMD_ATTR_TYPE		= $03
.const I_CMD_ATTR_PLATFORM	= $04
.const I_CMD_ATTR_DESC		= $05
