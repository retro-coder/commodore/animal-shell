
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "core/core.asm"
#import "_command_var.asm"
#import "_command_int.asm"

.struct CommandInfo {

	signature,
	platform,
	type,
	major,
	minor,
	name,
	description,
	copyright,
	author,
	website,
	email,
	help

}

	.var __cmd_info__ = CommandInfo()

	.eval __cmd_info__.signature = COMMAND_SIGNATURE
	.eval __cmd_info__.name = "NONE"
	.eval __cmd_info__.type = COMMAND_TYPE_NONE
	.eval __cmd_info__.platform = COMMAND_PLATFORM_ANY
	.eval __cmd_info__.description = "No Description"
	.eval __cmd_info__.copyright = "No Copyright"
	.eval __cmd_info__.major = 0
	.eval __cmd_info__.minor = 0
	.eval __cmd_info__.author = "No Author"
	.eval __cmd_info__.website = "No Website"
	.eval __cmd_info__.email = "No Email"
	.eval __cmd_info__.help = "No Help Available"

.macro __CommandInfo__(info) {

	.if (info.signature != null) { .eval __cmd_info__.signature = info.signature }
	.if (info.name != null) { .eval __cmd_info__.name = info.name }
	.if (info.description != null) { .eval __cmd_info__.description = info.description }
	.if (info.type != null) { .eval __cmd_info__.type = info.type }
	.if (info.platform != null) { .eval __cmd_info__.platform = info.platform }
	.if (info.copyright != null) { .eval __cmd_info__.copyright = info.copyright }
	.if (info.major != null) { .eval __cmd_info__.major = info.major }
	.if (info.minor != null) { .eval __cmd_info__.minor = info.minor }
	.if (info.author != null) { .eval __cmd_info__.author = info.author }
	.if (info.website != null) { .eval __cmd_info__.website = info.website }
	.if (info.email != null) { .eval __cmd_info__.email = info.email }
	.if (info.help != null) { .eval __cmd_info__.help = info.help }

}

.macro __CommandStart__() {

	.segment command_info

	i_signature:	.text __cmd_info__.signature
	i_type:		.byte __cmd_info__.type
	i_platform:	.byte __cmd_info__.platform
	i_major:	.byte __cmd_info__.major
	i_minor:	.byte __cmd_info__.minor
	i_name:		.byte <d_name, >d_name
	i_description:	.byte <d_description, >d_description
	i_copyright:	.byte <d_copyright, >d_copyright
	i_author:	.byte <d_author, >d_author
	i_website:	.byte <d_website, >d_website
	i_email:	.byte <d_email, >d_email
	i_help:		.byte <dr_help, >dr_help

.segment command_sys

	clear:		.fill 16, $ee

.segment command_data

	d_name:		.text __cmd_info__.name + @"\$00"
	d_description:	.text __cmd_info__.description + @"\$00"
	d_copyright:	.text __cmd_info__.copyright + @"\$00"
	d_author:	.text __cmd_info__.author + @"\$00"
	d_website:	.text __cmd_info__.website + @"\$00"
	d_email:	.text __cmd_info__.email + @"\$00"
	dr_help:	.text __cmd_info__.help + @"\$00"

.segment command_call

	jmp __command_handler__

.segment command_handler

	nop
	nop
	nop

	__command_handler__:

	lda SYS_CALL

}

.macro __CommandStateHandler__(state, handler) {

.segment command_virtual

.segment command_handler

	cmp #state
	bne !+
	jmp handler

	!:
}


.macro __CommandEnd__() {

.segment command_handler

	rts
}

