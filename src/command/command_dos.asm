
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 

#import "command/command.asm"
#import "core/core.asm"
#import "system/sys.asm"
#import "shell/shell.asm"

	.var info = CommandInfo()

	.eval info.name = "DOS" 

	__CommandInfo__(info)

	__CommandStart__()

.segment command_data

	__CommandStateHandler__(I_CMD_CALL_RUN, run)

.segment command_code

	.const setnam = $ffbd
	.const setlfs = $ffba
	.const open   = $ffc0
	.const close  = $ffc3
	.const chkin  = $ffc6
	.const chrin  = $ffcf
	.const bsout  = $ffd2
	.const clrch  = $ffcc
	.const chkout = $ffc9
	.const chrout = $ffd2

	.const temp   = 253
	.const charret = $0d
	.const space = $20

	run:

	ICall(I_SHELL_CALL, I_SHELL_PARM_FIRST)

	lda $ff
	bne !+

	CopyPointer(SYS_CALL_RETURN, ADDR_ZERO_SHARED)
	StorePointer(cmd, ADDR_ZERO_SHARED + 2)

	ICall(I_CORE_CALL, I_CORE_BANK_SYS_IN)
	CopyStringFromPointers(ADDR_ZERO_SHARED, ADDR_ZERO_SHARED + 2)
	ICall(I_CORE_CALL, I_CORE_BANK_SYS_OUT)

	jmp loaddir

	!:

	.encoding "petscii_upper"
	
	lda #'U'
	sta cmd + 0
	lda #'I'
	sta cmd + 1
	lda #13
	sta cmd + 2
	lda #0
	sta cmd + 3

	.encoding "petscii_mixed"

	loaddir:
	
	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN) 

	lda #$00
	ldy #$00
	ldy #$00
	jsr setnam

	lda #$0f
	ldx SHELL_DEVICE
	bne skip
	ldx #8

	skip:

	ldy #$0f
	jsr setlfs

	jsr open
	bcs error

	ldx #$0f
	jsr chkout

	ldy #$00

	loop1:

	lda cmd,y
	beq !+
	jsr chrout
	iny 
	jmp loop1

	!:

	status:

	lda SHELL_DEVICE
	sta ADDR_IN
	ICall(I_CORE_CALL, I_CORE_IO_STATUS)
	rts
	

	//now close the file

	close_all:

	lda #$0f        //file #
	jsr close       //- close the file
	jsr clrch       // restore i/o

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	rts

	error:

	jmp close_all

	__CommandEnd__()

.segment command_code

.encoding "petscii_upper"

	cmd:
	.fill 255, $ee


.file [name="dos.prg", segments="command"]
