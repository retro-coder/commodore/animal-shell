
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.encoding "petscii_mixed"

// command states

.const COMMAND_STATE_NONE 	= $00
.const COMMAND_STATE_INIT 	= $10
.const COMMAND_STATE_START 	= $20
.const COMMAND_STATE_STOP 	= $30
.const COMMAND_STATE_RUN 	= $40
.const COMMAND_STATE_HELP	= $50
.const COMMAND_STATE_EXIT	= $90

// command types

.const COMMAND_TYPE_NONE	= $00

// command platforms

.const COMMAND_PLATFORM_ANY	= $00
.const COMMAND_PLATFORM_C128 	= $01
.const COMMAND_PLATFORM_C64 	= $02

// command signature

.const COMMAND_SIGNATURE		= "##c0"

