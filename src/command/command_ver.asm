
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 

#import "command/command.asm"
#import "core/core.asm"
#import "system/sys.asm"

	.encoding "petscii_mixed"

	.var info = CommandInfo()

	.eval info.name = "VER" 

	__CommandInfo__(info)

	__CommandStart__()

.segment command_data

	s_version:

	.text @"\$0dAnimal Shell\$0d"
	.text @"Version " + cmdLineVars.get("build_version") + @" "
	.text @"(Build " + cmdLineVars.get("build_number") + @")\$0d"
	.text @"Copyright 2022-2025 Paul Hocker\$0d" 
	.text @"Licensed under the MIT License\$0d"
	.text cmdLineVars.get("build_date") + @"\$0d"
	.text @"\$00"
	__CommandStateHandler__(I_CMD_CALL_RUN, run)

.segment command_code

	run:

	PrintFromAddress(s_version)
	rts

	__CommandEnd__()

.file [name="ver.prg", segments="command"]
