
#importonce 

#import "command/command.asm"
#import "core/core.asm"
#import "shell/shell.asm"
#import "system/sys.asm"

.encoding "petscii_mixed"

.segment command_parms

.segment command_data

        __CommandStart__()

        __CommandStateHandler__(I_CMD_CALL_RUN, run)

.segment command_code

        .const setnam = $ffbd
        .const setlfs = $ffba
        .const open   = $ffc0
        .const close  = $ffc3
        .const chkin  = $ffc6
        .const chrin  = $ffcf
        .const bsout  = $ffd2
        .const clrch  = $ffcc

        .const temp   = 253
        .const charret = $0d
        .const space = $20

        run:


	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)

        lda #$01        // length is 1        
        ldx #<dir       // lo byte pointer to file name.
        ldy #>dir       // hi byte pointer to file name.
        jsr setnam      // - call setnam

        lda #$01        // file # 1
        ldx SHELL_DEVICE         // current device number
        ldy #$00        // channel # 0
        jsr setlfs      // - call setlfs

        jsr open        // - call open

        // read in the bytes and display (skipping line links etc)

        ldx #$01        // file #1
        jsr chkin       // - call chkin to set input file #.

        jsr chrin       // - ignore starting address (2 bytes)
        jsr chrin 

        skip:

        jsr chrin       // - ignore pointer to next line (2 bytes)

        bck1:

        jsr chrin

        line:

        jsr chrin       // - get line # lo.

        sta temp        // - store lo of line # @ temp
        jsr chrin       // - get hi of line #

        // ldx temp
        // jsr $bdcd 

        lda #space
        jsr bsout       // - print space

        gtasc:

        jsr chrin       // - start printing filename until 
                        //end of line.
        beq chck        //(Zero signifies eol).
        jsr bsout       // - Print character
        sec
        bcs gtasc       //and jump back.

        chck:
        lda #charret    // - Else we need to start the next line
        jsr bsout       //Print a carriage return.

        jsr chrin       // - And get the next pointer
        bne bck1        // If non-zero go, strip other ptr,
                        //and continue.

        jsr chrin       // - Else check 2nd byte of pointer
        bne line        // as if both 0 then = end of directory.

        // had 3 0's in a row so end of prog
        // now close the file.

        lda #$01        // file # to close
        jsr close       // - so close it
        jsr clrch       // - clear all channels


	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

        rts             //- and return to basic

        __CommandEnd__()

        .encoding "petscii_upper"
         
        //FILENAME string
        dir:           .text "$"

.file [name="dir.prg", segments="command"]
