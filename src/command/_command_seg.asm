
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "system/sys.asm"

.segment command [segments="command_call, command_info, command_sys, command_data, command_handler, command_code, command_parms, command_virtual"]

.segmentdef command_call	[start=ADDR_CMD]
.segmentdef command_info	[start=ADDR_CMD_INFO]
.segmentdef command_sys		[start=ADDR_CMD_SYS]
.segmentdef command_data	[start=ADDR_CMD_DATA]
.segmentdef command_parms	[startAfter="command_data"]
.segmentdef command_handler	[startAfter="command_parms"]
.segmentdef command_code	[startAfter="command_handler"]
.segmentdef command_virtual	[virtual]
