
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/


.encoding "petscii_mixed"

#importonce

#import "system/sys.asm"
#import "core/core.asm"
#import "shell/shell.asm"

.filenamespace _loader

.segment loader [segments="loader_header, loader_code"]
.segmentdef loader_header [start=ADDR_CART]
.segmentdef loader_code [startAfter="loader_header"]


.segment loader_header

.word coldstart
.word coldstart

.byte $C3,$C2,$CD,$38,$30

.segment loader_code

coldstart:

sei


#if CART

stx $d016
jsr $fda3
jsr $fd50
jsr $fd15
jsr $ff5b

#else 

lda $01
and #%110
sta $01

#endif 

// turn on kernal messages

lda #$c0
sta $9d

ldx #0 

!:

lda runner,x
sta ADDR_RUNNER,x

inx
cpx #runner_end - runner_start
bne !-

lda #<romirq
sta $0314
lda #>romirq
sta $0315

lda #<ramirq
sta $fffe
lda #>ramirq
sta $ffff

cli

PrintFromAddress(s_start)

// initialize the shell
ICall(I_SHELL_CALL, I_SHELL_INIT)

// initialize the core
ICall(I_CORE_CALL, I_CORE_INIT)

// jmp ADDR_SHELL 
ICall(I_SHELL_CALL, I_SHELL_RUN)

!:

ICall(I_CORE_CALL, I_CORE_RESERVED1)
ICall(I_CORE_CALL, I_CORE_RESERVED2)

jmp !-

s_start:
.text @"\$0d\$0dstarting . . .\$0d\$00"

runner:

.pseudopc ADDR_RUNNER
{

runner_start:

screen:

ldx #$28
ldy #$18
rts 

romirq:

inc $0400
jsr $ffea
jsr $ea87
lda $dc0d
jmp endirq

ramirq:

pha
txa
pha
tya
pha

notbrk:

#if CART
#else
lda #%110
sta $01
#endif

inc $0401
jsr $ffea
jsr $ea87
lda $dc0d

#if CART
#else
lda #%000
sta $01
#endif

endirq:

//clear top line

ldx #36
lda #32
!:
sta $0404,x
dex
bne !-
sta $0404,x

pla
tay
pla
tax
pla

rti

runner_end:

}


#if CART
#else

.file [name="loader.64.prg", segments="loader"]

#endif
