
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "system/sys.asm"

.encoding "petscii_mixed"

.filenamespace __boot__

.segment boot[segments="boot_code"]
.segmentdef boot_code[]

.segment boot_code "basic"

.const fn_loader = "loader.64"
.const fn_core = "coreg.64"
.const fn_shell = "shell.64"

.var fl_loader = fn_loader.size()
.var fl_core = fn_core.size()
.var fl_shell = fn_shell.size()

BasicUpstart2(main)

.segment boot_code "boot"

* = $0810

main:

lda #%00000110
sta $01

lda #192
sta $9d

lda #fl_loader
ldx #<f_loader
ldy #>f_loader
jsr loadfile

lda #fl_core
ldx #<f_core
ldy #>f_core
jsr loadfile

lda #fl_shell
ldx #<f_shell
ldy #>f_shell
jsr loadfile

jmp $8009

f_loader:
.text fn_loader

f_shell:
.text fn_shell

f_core:
.text  fn_core

loadfile:

jsr $ffbd
lda #$01
ldx $ba
bne skip
ldx #$08

skip:

ldy #$01
jsr $ffba
lda #$00
jsr $ffd5
bcs error
rts

error:

jsr $ffd2
rts

.file [name="bootg.64.prg", segments="boot"]
