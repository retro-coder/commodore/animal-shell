
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.filenamespace __shell__

.struct InterfaceHandler {
	id
}

.var interface_handlers = Hashtable()

.macro __ShellInit__() {
	.eval interface_handlers = Hashtable()
}

.macro __ZZZHandler__(id) {

	.if (interface_handlers.containsKey(id)) {
		.error "Interface Handler Id [" + id + "] Has Already Been Used."
	}

	.var _ih = InterfaceHandler()
	.eval _ih.id = id 

	.eval interface_handlers.put(id, _ih)

	.segment shell_handler

	cmp #id
	bne !+
	jmp _interface

	!:

.segment shell_code

	_interface:

	nop
}

.macro __ShellHandler__(id, handler) {
	
	.if (interface_handlers.containsKey(id)) {
		.error "Shell Interface Handler Id [" + id + "] Has Already Been Used."
	}

	.var _ih = InterfaceHandler()
	.eval _ih.id = id 

	.eval interface_handlers.put(id, _ih)

	.segment shell_handler

	cmp #id
	bne !+
	jmp handler

	!:
}

.macro @SetShellState(state) {
	lda #state
	sta SHELL_STATE
}

.macro @SetShellMode(mode) {
	lda #mode
	sta SHELL_MODE
}

.macro @SetShellCliState(state) {
	lda #state
	sta SHELL_CLI_NEXT_STATE	
}
