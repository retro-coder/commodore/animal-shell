
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

/*
	shell_main.asm

	This controls the main flow of the
	shell.

	There are two modes of the Shell

		SHELL_MODE_BATCH		Run the shell in the Background using a Script
		SHELL_MODE_INTERACTIVE		Run the Shell with a Prompt

	The Shell state will determine what
	is currently active.

		SHELL_STATE_CLI			The command line interface is running
		SHELL_STATE_APP			Applications are running
		SHELL_STATE_QUIT		The Shell is in a state of Quitting

	The Shell CLI state will determine what
	the CLI is doing.

		SHELL_CLI_STATE_NONE			Nothing
		SHELL_CLI_STATE_START			Start the CLI
		SHELL_CLI_STATE_RUN			Run the CLI
		SHELL_CLI_STATE_STOP			Stop the CLI
*/

#import "core/core.asm"

.filenamespace __shell__

.namespace __main__ {

.segment shell_virtual "shell_main"

	.pseudopc ADDR_APP_SYS {
		__app_sys__: ds_app_system_space
	}
	
	.pseudopc ADDR_CMD_SYS {
		__cmd_sys__: ds_command_system_space
	}

.segment shell_code "shell_main"

shell_main: {

	__LogFine__("shell>main")

	lda SHELL_STATE

	cmp #SHELL_STATE_CLI
	bne !+
	jmp __cli__

	!:
	cmp #SHELL_STATE_APP
	bne !+
	jmp __app__

	!:
	cmp #SHELL_STATE_CMD
	bne !+
	jmp __cmd__

	!:
	jmp shell_main
}

__cli__: {

	lda SHELL_CLI_STATE 

	cmp #SHELL_CLI_STATE_NONE 
	bne !+ 
	jmp __end__

	!:
	cmp #SHELL_CLI_STATE_START
	bne !+ 
	jmp __cli_start__

	!:
	cmp #SHELL_CLI_STATE_RUN
	bne !+ 
	jmp __cli_run__

	!:
	cmp #SHELL_CLI_STATE_STOP
	bne !+ 
	jmp __cli_stop__

	!:
	jmp __end__
}

__cli_none__: {

	__LogTrace__("shell>main>cli>none")
	jmp __end__

}

__cli_start__: {

	__LogTrace__("shell>main>cli>start")
	SetShellCliState(SHELL_CLI_STATE_RUN)
	jsr __shell__.__cli__.shell_cli_start
	jmp __end__

}

__cli_run__: {

	__LogFine__("shell>main>cli>run")
	SetShellCliState(SHELL_CLI_STATE_RUN)
	jsr __shell__.__cli__.shell_cli_run
	jmp __end__

}

__cli_stop__: {

	__LogTrace__("shell>main>cli>stop")
	SetShellCliState(SHELL_CLI_STATE_NONE)
	jsr __shell__.__cli__.shell_cli_stop
	SetShellState(SHELL_STATE_APP)
	jmp __end__

}

__app__: {

	// get last keypress

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)

	jsr $ffe4

#if C128
	lda $d5
#else
	lda $c5
#endif

	sta SHELL_LAST_KEYPRESS

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	lda SHELL_LAST_KEYPRESS
	sta $0403

	// escape pressed

	cmp #72
	bne !+
	jmp stop

	
	// run-stop pressed

	!:
	cmp #KEY_RUNSTOP
	bne !+
	jmp stop

	!:
	jmp start

	stop:

	SetAppState(APP_STATE_STOP)
	jmp __end__

	start:

	lda __app_sys__.state

	cmp #APP_STATE_NONE 
	bne !+
	jmp __app_state_none__

	!:
	cmp #APP_STATE_INIT
	bne !+
	jmp __app_state_init__

	!:
	cmp #APP_STATE_START
	bne !+
	jmp __app_state_start__

	!:
	cmp #APP_STATE_RUN 
	bne !+
	jmp __app_state_run__

	!:
	cmp #APP_STATE_STOP
	bne !+
	jmp __app_state_stop__

	!:
	jmp __end__

}

__app_state_none__: {

	__LogTrace__("shell>main>app>state>none")
	jmp __end__

}

__app_state_init__: {

	__LogTrace__("shell>main>app>state>init")
	SetAppState(APP_STATE_START)
	jsr ADDR_APP
	jmp __end__

}

__app_state_start__: {

	__LogTrace__("shell>main>app>state>start")
	SetAppState(APP_STATE_RUN)
	ICall(ADDR_APP, SIGSTART)
	jmp __end__

}

__app_state_run__: {

	__LogFine__("shell>main>app>state>run")
	ICall(ADDR_APP, SIGRUN)
	jmp __end__

}

__app_state_stop__: {

	__LogTrace__("shell>main>app>state>stop")
	SetAppState(APP_STATE_NONE)
	SetShellState(SHELL_STATE_CLI)
	SetShellCliState(SHELL_CLI_STATE_START)
	ICall(ADDR_APP, SIGSTOP)
	jmp __end__

}

__cmd__: {

	lda __cmd_sys__.state

	cmp #COMMAND_STATE_NONE
	bne !+
	jmp __cmd_state_none__

	!:
	cmp #COMMAND_STATE_INIT
	bne !+
	jmp __cmd_state_init__

	!:
	cmp #COMMAND_STATE_START
	bne !+
	jmp __cmd_state_start__

	!:
	cmp #COMMAND_STATE_STOP
	bne !+
	jmp __cmd_state_stop__

	!:
	cmp #COMMAND_STATE_RUN
	bne !+
	jmp __cmd_state_run__

	!:
	jmp __end__

}

__cmd_state_init__: {

	__LogTrace__("shell>main>cmd>state>init")
	SetCommandState(COMMAND_STATE_START)
	ICall(ADDR_CMD, I_CMD_CALL_INIT)
	jmp __end__

}

__cmd_state_none__: {
		
	__LogTrace__("shell>main>cmd>state>none")
	jmp __end__

}

__cmd_state_run__: {

	__LogTrace__("shell>main>cmd>state>run")
	SetCommandState(COMMAND_STATE_STOP)
	ICall(ADDR_CMD, I_CMD_CALL_RUN)
	jmp __end__

}

__cmd_state_start__: {

	__LogTrace__("shell>main>cmd>state>start")
	SetCommandState(COMMAND_STATE_RUN)
	ICall(ADDR_CMD, I_CMD_CALL_START)
	jmp __end__

}

__cmd_state_stop__: {

	__LogTrace__("shell>main>cmd>state>stop")
	SetCommandState(COMMAND_STATE_NONE)
	ICall(ADDR_CMD, I_CMD_CALL_STOP)
	SetShellState(SHELL_STATE_CLI)	
	SetShellCliState(SHELL_CLI_STATE_START)
	jmp __end__

}

__end__: {

	lda SHELL_CLI_NEXT_STATE
	sta SHELL_CLI_STATE

	// update state in app space

	lda __app_sys__.next_state
	sta __app_sys__.state

	// update command state

	lda __cmd_sys__.next_state
	sta __cmd_sys__.state

	jmp shell_main

}

}
