
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.filenamespace __shell__

.namespace __init__ {

.segment shell_virtual "shell_cli_init"

	.pseudopc ADDR_IN {
		p_list_create: p_core_list_create_in
	}

	.pseudopc ADDR_IN {
		p_catalog_create: p_shell_catalog_create_in
	}
	

	__ShellHandler__(I_SHELL_INIT, shell_init)

.segment shell_code "shell_init"

shell_init: {

	__LogTrace__("shell>init")

	// lowercase

	SetShellState(SHELL_STATE_NONE)

	lda $ba
	bne !+

	// default to 8 when nothing is there

	lda #8
	
	!:

	sta SHELL_DEVICE

	jsr __shell__.__cli__.__init__.shell_cli_init

	// create memory allocation list (XL)

	StorePointer(SHELL_XL, p_list_create.list)

	lda #48
	sta p_list_create.length
	lda #4
	sta p_list_create.size
	lda #0
	sta p_list_create.storage
	lda #0
	sta p_list_create.delim

	ICall(I_CORE_CALL, I_CORE_LIST_CREATE)

	
	// create driver catalog (DC)

	StorePointer(SHELL_DC, p_catalog_create.list)
	ICall(I_SHELL_CALL, I_SHELL_CATALOG_CREATE)

	
	// create command catalog (CC)

	StorePointer(SHELL_CC, p_catalog_create.list)
	ICall(I_SHELL_CALL, I_SHELL_CATALOG_CREATE)

	
	// create library catalog (LC)

	StorePointer(SHELL_LC, p_catalog_create.list)
	ICall(I_SHELL_CALL, I_SHELL_CATALOG_CREATE)

	
	// create application catalog (AC)

	StorePointer(SHELL_AC, p_catalog_create.list)
	ICall(I_SHELL_CALL, I_SHELL_CATALOG_CREATE)

	// -- use only when testing is system mem is good when running c64 cart
	// -- caution! this will break things if you leave it here
	
	// ICall(I_CORE_CALL, I_CORE_BANK_SYS_IN)
	
	rts 
	
}

}
