
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.const SHELL_STATE_NONE		= $00
.const SHELL_STATE_CLI		= $10
.const SHELL_STATE_APP		= $20
.const SHELL_STATE_CMD		= $30

.const SHELL_MODE_BATCH		= $10
.const SHELL_MODE_INTERACTIVE	= $20


.const SHELL_CLI_STATE_NONE	= $00
.const SHELL_CLI_STATE_START	= $10
.const SHELL_CLI_STATE_RUN	= $20
.const SHELL_CLI_STATE_STOP	= $30


// CHARACTER CODES
// (appendix c c128 mapping the c128)


// delete
.const CHAR_DELETE		= $14
// return
.const CHAR_RETURN		= $0d
// escape (c128 only)
.const CHAR_ESCAPE		= $1b


// KEYSCAN CODES
// (appendix c c128 mapping the c128)


// run/stop
.const KEY_RUNSTOP		= $3f

.label SHELL_BUFFER		= ADDR_BUFFER
// .label SHELL_TOKENS		= ADDR_WORK


.segment sys_zero_shell "shell_zero_vars"

// current shell state
SHELL_STATE:			p_type_byte

// state shell is moving to in next cycle
SHELL_NEXT_STATE:		p_type_byte

// number of jobs running
SHELL_JOBS:			p_type_byte 

// which mode is shell in
SHELL_MODE:			p_type_byte

// current cli state
SHELL_CLI_STATE:		p_type_byte 

// state cli is moving to in next cycle
SHELL_CLI_NEXT_STATE:		p_type_byte

// current cli buffer position
SHELL_BUFFER_POS:		p_type_byte

// current pos in the token list
SHELL_TOKEN_POS:		p_type_byte 

// current device on shell prompt
SHELL_DEVICE:			p_type_byte

// last keypress
SHELL_LAST_KEYPRESS:		p_type_byte

// temp space for shell calls
SHELL_ZERO_TEMP:		p_type_byte


// SYSTEM STORAGE AREA

.segment sys_area_shell "shell_cmd_buffer"

SHELL_CMD_BUFFER:		.fill 256 * 1,0

.segment sys_area_shell "shell_cmd_tokens"

SHELL_CMD_TOKENS:		.fill 256 * 2,0

.segment sys_area_shell "shell_lists"

// expanded memory catalog list
SHELL_XL:			.fill 256 * 1,0


.segment sys_area_shell "shell_catalogs"

// shell driver catalog
SHELL_DC:			.fill 256 * 1,0
// shell command catalog
SHELL_CC:			.fill 256 * 1,0
// shell library catalog
SHELL_LC:			.fill 256 * 1,0
// shell app catalog
SHELL_AC:			.fill 256 * 1,0

.segment sys_area_shell "shell_prompt"

SHELL_PROMPT:			.fill 32, 0

