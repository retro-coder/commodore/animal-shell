
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.filenamespace __shell__

.namespace __cmd__ {

#import "shell_cmd_load.asm"
#import "shell_cmd_run.asm"

}
