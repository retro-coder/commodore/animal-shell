
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "shell/_shell.asm"
#import "core/core.asm"

.filenamespace __shell__

.namespace __cmd__ 
{

.namespace __load__ 
{

.segment shell_data "shell_cmd_load"

	s_error1:
	.text @"\$0dSFE001 Command Not Found\$00"

	s_error2:
	.text @"\$0dSFE002 Not Animal Shell Command\$00"

	s_error3:
	.text @"\$0dSFE003 Device Not Ready\$00"

.segment shell_virtual "shell_cmd_load"

	.pseudopc ADDR_IN {
		file_load: p_core_file_load_in  
	}

	.pseudopc ADDR_IN {
		p_open: p_core_file_open_in 
	}
	
	.pseudopc ADDR_IN {
		p_getc_in: p_core_file_getc_in 
	}

	.pseudopc ADDR_OUT {
		p_open_out: p_core_file_open_out 
	}

	.pseudopc ADDR_ZERO_SHARED {

		t_file: p_type_byte 
		w_device: p_type_byte 
		t_header: p_type_pointer 
	}
	

	__ShellHandler__(I_SHELL_CMD_LOAD, shell_cmd_load)

.segment shell_code "shell_cmd_load"

	shell_cmd_load:

	__LogTrace__("shell>cmd>load")

	// get command entered

	ICall(I_SHELL_CALL, I_SHELL_PARM_COMMAND)

	bcc !+
	jmp error1

        !:

	StorePointer(ADDR_OUT, p_open.filename) 
	
	// open file on current device number
	
	lda SHELL_DEVICE
	sta p_open.device
	sta w_device
	lda #1
	sta p_open.show
	
	ICall2(I_CORE_CALL, I_CORE_FILE_OPEN, p_open, p_open_out)

        // when not found start looping thru all serial devices

	bcs !+

	// otherwise, jump to found routine

	jmp found

	!:

	// get command entered

	ICall(I_SHELL_CALL, I_SHELL_PARM_COMMAND)

	bcc !+
	jmp error1

	!:
	
	lda #8
	sta w_device

        !:

	StorePointer(ADDR_OUT, p_open.filename) 
	
	// open file on current device number
	
	lda w_device
	sta p_open.device
	lda #1
	sta p_open.show
	
	ICall2(I_CORE_CALL, I_CORE_FILE_OPEN, p_open, p_open_out)

        // did it open?

	bcc found

	// if not, try the next device/path

	inc w_device
	lda w_device
	cmp #13
	bne !-

	// nothing found, return an error

        jmp error1

	found:

	// prepare to load the first 16 bytes from the file

	lda p_open_out
	sta t_file

	ldx #16
	ldy #0
	StorePointer(ADDR_DATA, t_header)

	lda t_file
	sta p_getc_in.file

        !:

	tya 
	pha
	txa
	pha

	ICall1(I_CORE_CALL, I_CORE_FILE_GETC, p_getc_in)

	bcs eof

	sta ADDR_WORK

	pla	
	tax
	pla	
	tay

	lda ADDR_WORK
	sta (t_header), y

	iny 
	dex

	beq close

	jmp !-

	eof:

	// todo: does this solve c64 issue

	pla	
	pla

	close:

	// close the files

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)
	lda t_file
	sta p_getc_in.file
	jsr JCLOSE
	jsr JCLRCH
	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	// check the signature in the header

	lda ADDR_DATA + 8
	cmp #COMMAND_SIGNATURE.charAt(0) 
	bne error2
	lda ADDR_DATA + 9
	cmp #COMMAND_SIGNATURE.charAt(1) 
	bne error2
	lda ADDR_DATA + 10
	cmp #COMMAND_SIGNATURE.charAt(2) 
	bne error2
	lda ADDR_DATA + 11
	cmp #COMMAND_SIGNATURE.charAt(3) 
	bne error2

	jmp !+

	error1:

	NPrintFromAddress(s_error1)
	sec
	rts

	error2:

	// ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)
	// lda t_file
	// sta p_getc_in.file
	// jsr JCLOSE
	// jsr $ffcc
	// ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)
	NPrintFromAddress(s_error2)
	sec
	rts

	!:

	// get the command entered, again

	ICall(I_SHELL_CALL, I_SHELL_PARM_COMMAND)
	bcc !+
	jmp error1

        !:

	StorePointer(ADDR_OUT, file_load.filename) 

	lda #0
	sta file_load.show
	lda w_device
	sta file_load.device

	// this time load it
	
	ICall(I_CORE_CALL, I_CORE_FILE_LOAD)

        rts


	/*

	// -- old code

	StorePointer(SHELL_CMD_TOKENS, first.list)
	ICall1(I_CORE_CALL, I_CORE_LIST_FIRST, first)

	ldy #0
	lda (SYS_CALL_RETURN), y
	sta file_load.filename
	iny 
	lda (SYS_CALL_RETURN), y
	sta file_load.filename + 1

	lda #0
	sta file_load.show

	lda #8
	sta w_device

	next:

	// lda SHELL_DEVICE
	sta file_load.device

	ICall(I_CORE_CALL, I_CORE_FILE_LOAD)

	bcc sig

	inc w_device
	lda w_device
	cmp #13
	beq error1
	jmp next

	sig:

	// check signature

	lda info.signature
	cmp #COMMAND_SIGNATURE.charAt(0) 
	bne error2
	lda info.signature + 1
	cmp #COMMAND_SIGNATURE.charAt(1) 
	bne error2
	lda info.signature + 2
	cmp #COMMAND_SIGNATURE.charAt(2) 
	bne error2
	lda info.signature + 3
	cmp #COMMAND_SIGNATURE.charAt(3) 
	bne error2
	clc
	rts

	*/


	
}

}
