
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "shell/_shell.asm"
#import "core/core.asm"

.filenamespace __shell__

.namespace __cmd__ {

.namespace __run__ {

.segment shell_virtual "shell_cmd_run"

	__ShellHandler__(I_SHELL_CMD_RUN, shell_cmd_run)

.segment shell_code "shell_cmd_run"

shell_cmd_run: {

	__LogTrace__("shell>cmd>run")

	ICall(I_CMD_CALL, I_CMD_CALL_INIT)
	ICall(I_CMD_CALL, I_CMD_CALL_RUN)

	rts
}

}

}
