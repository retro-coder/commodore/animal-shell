
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

/*
	Animal Shell
*/

.filenamespace __shell__

#import "_shell.asm"
#import "core/core.asm"
#import "app/_app_dat.asm"
#import "app/_app_var.asm"
#import "app/_app_int.asm"
#import "command/_command_dat.asm"
#import "command/_command_var.asm"
#import "command/_command_int.asm"
#import "command/_command_sci.asm"

__ShellInit__()

{
.segment shell_call

jmp _shell_handler

.segment shell_handler


_shell_handler:

// clear errors

lda #$00
sta $ff
clc

lda SYS_CALL

#import "shell_init.asm"
#import "shell_start.asm" 
#import "shell_run.asm"
#import "shell_main.asm"
#import "shell_cli.asm"

#import "cat/shell_cat.asm"
#import "cli/shell_cli.asm"
#import "cmd/shell_cmd.asm"
#import "parm/shell_parm.asm"

// #import "shell_token.asm"
// #import "shell_env.asm"
// #import "shell_cmd.asm"
// #import "shell_app.asm"

.segment shell_virtual

.pseudopc ADDR_IN { p_num2str: p_core_util_num2str }
.pseudopc ADDR_WORK { str: .fill 32, $20 }
.pseudopc ADDR_WORK { num: .fill 4, 0 }


.segment shell_handler

#if LOG_DEBUG

__Message__("* UNHANDLED SHELL CALL [")

lda SYS_CALL
sta num
lda #0
sta num + 1
sta num + 2
sta num + 3

StorePointer(ADDR_WORK, p_num2str.addr)

lda #'$'
sta p_num2str.type
lda #1
sta p_num2str.length

ICall2(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str, str)

PrintFromPointer(str)

__NMessage__("] *")

#endif 

rts
	
}
