
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "../system/sys.asm"

.filenamespace __shell__

.segment shell [segments="shell_call, shell_handler, shell_data, shell_code, shell_virtual"]

.segmentdef shell_call 		[start=ADDR_SHELL]
.segmentdef shell_data 		[startAfter="shell_call"]
.segmentdef shell_handler 	[startAfter="shell_data"]
.segmentdef shell_code 		[startAfter="shell_handler"]
.segmentdef shell_virtual	[virtual]
