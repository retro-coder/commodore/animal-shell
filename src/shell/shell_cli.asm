
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "_shell.asm"
#import "core/core.asm"

.filenamespace __shell__

.namespace __cli__ {

.segment shell_virtual "shell_cli"

	.pseudopc ADDR_IN {
		parm_get_in: p_shell_cmd_parm_get_in
	}

	.pseudopc ADDR_IN {
		shell_cmd_parse: p_shell_cli_parse_in
	}

	.pseudopc ADDR_IN {
		first: p_core_list_in
	}

	.pseudopc ADDR_IN {
		get: p_core_list_get_in
		
	}

	.pseudopc ADDR_IN {
		concat: p_core_str_concat_in
	}
	

	.pseudopc ADDR_IN {
		num2str: p_core_util_num2str
	}
	

.segment shell_data "shell_cli"

	newline:
	.text @"\$0d\$00"

	pre:
	.text @"\$0d\$99\$00"

	sep:
	.text @">\$20\$9b\$00"

	colon:
	.text @":\$00"

	nullchar:
	.text @"\$00"



shell_cli_start: {

	jsr make_prompt

	lda #$00
	sta SHELL_BUFFER_POS

	// the prompt text is stored here after being created

	PrintFromAddress(ADDR_DATA)

	rts
}

shell_cli_run: {

	lda SHELL_BUFFER_POS
	sta $0402

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)

	jsr $ffe4
	sta SHELL_LAST_KEYPRESS

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	lda SHELL_LAST_KEYPRESS
	bne !+

	jmp !end+

	!:

	sta $0403

	// return key

	cmp #CHAR_RETURN
	bne !+
	jmp enter_pressed

	// delete key

	!:
	cmp #CHAR_DELETE
	bne !+
	jmp delete_pressed

	// escape key

	!:
	cmp #CHAR_ESCAPE
	bne !+
	jmp runstop_pressed

	// check for valid characters to include in buffer

	!:
	cmp#$80
	bcc !+
	jmp check_keycodes

	!:
	cmp #$20
	bcc check_keycodes

	print:

	ldy SHELL_BUFFER_POS

	// make sure we are not at the end of the buffer

	cpy #80
	bne !+
	jmp check_keycodes

	!:
	sta SHELL_BUFFER, y
	iny 
	sty SHELL_BUFFER_POS

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)

	lda SHELL_LAST_KEYPRESS
	jsr $ffd2

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	jmp !end+

	check_keycodes:

	// get last keycodes pressed

#if C128
	lda $d5
#else
	lda $c5
#endif
	sta $0404

	jmp !end+

	enter_pressed:

	PrintFromAddress(newline)

	// add zero delimeter to end of command buffer

	lda #$00
	ldy SHELL_BUFFER_POS
	sta SHELL_BUFFER, y
	iny 
	sty SHELL_BUFFER_POS


	// parse buffer

	StorePointer(SHELL_BUFFER, shell_cmd_parse.buffer)
	ICall1(I_SHELL_CALL, I_SHELL_CLI_PARSE, shell_cmd_parse)

	// get first item and do some checks

	StorePointer(SHELL_CMD_TOKENS, get.list)
	lda #0
	sta get.index
	ICall1(I_CORE_CALL, I_CORE_LIST_VGET, get)

	// reusing sys call zp pointers -- risky :)

	nop
	nop
	nop

	// check for @ command
	ldx #0
	lda ADDR_OUT,x
	cmp #'@'
	bne !+
	jmp read_status

	!:

	// check for device changes	

	ldx #0
	lda ADDR_OUT,x
	cmp #'8'
	bne !+
	inx	
	lda ADDR_OUT,x
	cmp #':'
	bne !+
	lda #8
	sta SHELL_DEVICE
	jmp change_prompt

	!:
	
	ldx #0
	lda ADDR_OUT,x
	cmp #'9'
	bne !+
	inx	
	lda ADDR_OUT,x
	cmp #':'
	bne !+
	lda #9
	sta SHELL_DEVICE
	jmp change_prompt

	!:
	
	ldx #0
	lda ADDR_OUT,x
	cmp #'1'
	bne !+
	inx	
	lda ADDR_OUT,x
	cmp #'0'
	bne !+
	inx	
	lda ADDR_OUT,x
	cmp #':'
	bne !+
	lda #10
	sta SHELL_DEVICE
	jmp change_prompt

	!:
	
	ldx #0
	lda ADDR_OUT,x
	cmp #'1'
	bne !+
	inx	
	lda ADDR_OUT,x
	cmp #'1'
	bne !+
	inx	
	lda ADDR_OUT,x
	cmp #':'
	bne !+
	lda #11
	sta SHELL_DEVICE
	jmp change_prompt

	!:
	
	ldx #0
	lda ADDR_OUT,x
	cmp #'1'
	bne !+
	inx	
	lda ADDR_OUT,x
	cmp #'2'
	bne !+
	inx	
	lda ADDR_OUT,x
	cmp #':'
	bne !+
	lda #12
	sta SHELL_DEVICE
	jmp change_prompt

	!:
	
	ldx #0
	lda ADDR_OUT,x
	cmp #'1'
	bne !+
	inx	
	lda ADDR_OUT,x
	cmp #'6'
	bne !+
	inx	
	lda ADDR_OUT,x
	cmp #':'
	bne !+
	lda #16
	sta SHELL_DEVICE
	jmp change_prompt

	!:

	load_command:

	!:
	ICall(I_SHELL_CALL, I_SHELL_CMD_LOAD)
	bcc start_command
	jmp runstop_pressed

	// start the command process

	start_command:

	SetShellState(SHELL_STATE_CMD)	
	SetShellCliState(SHELL_CLI_STATE_NONE)
	SetCommandState(COMMAND_STATE_INIT)

	jmp !end+

	// handle trying to remove a character from the buffer

	delete_pressed:

	// are we already at the beginning?

	ldx SHELL_BUFFER_POS
	cpx #$00
	beq !end+

	dex	
	stx SHELL_BUFFER_POS

	// and print the delete key
	// todo: this needs to be changed to not use $0403 :)

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)

	lda $0403
	jsr $ffd2

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	jmp !end+

	change_prompt:

	runstop_pressed:

	// restart the command line

	SetShellCliState(SHELL_CLI_STATE_START)

	!end:

	rts

	read_status:

	lda SHELL_DEVICE
	sta ADDR_IN
	ICall(I_CORE_CALL, I_CORE_IO_STATUS)	

	jmp runstop_pressed
}

make_prompt: {


	// clear shell prompt

	lda #0
	ldx #32

	!:

	sta ADDR_DATA,x 
	dex
	bne !-
	sta ADDR_DATA,x 

	// convert drive number to string

	lda #0
	sta ADDR_WORK + 0
	sta ADDR_WORK + 1
	sta ADDR_WORK + 2
	sta ADDR_WORK + 3

	StorePointer(ADDR_WORK, num2str.addr)
	
	lda SHELL_DEVICE
	sta ADDR_WORK + 0
	lda	#' '
	sta	num2str.type
	ICall2(I_CORE_CALL, I_CORE_UTIL_NUM2STR, num2str, ADDR_DATA)

	// make the prompt

	lda #0
	sta concat.options

	StorePointer(pre, $fb)
	jsr copy_str

	StorePointer(ADDR_BUFFER, concat.stringa)
	StorePointer(ADDR_DATA, concat.stringb)
	ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, concat, ADDR_DATA)

	StorePointer(colon, $fb)
	jsr copy_str

	StorePointer(ADDR_DATA, concat.stringa)
	StorePointer(ADDR_BUFFER, concat.stringb)
	ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, concat, ADDR_DATA)

	StorePointer(sep, $fb)
	jsr copy_str

	StorePointer(ADDR_DATA, concat.stringa)
	StorePointer(ADDR_BUFFER, concat.stringb)
	ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, concat, ADDR_DATA)
	
	StorePointer(nullchar, $fb)
	jsr copy_str

	StorePointer(ADDR_DATA, concat.stringa)
	StorePointer(ADDR_BUFFER, concat.stringb)
	ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, concat, ADDR_DATA)

	rts

	copy_str:
	ldy #0
	ldx #0
	!:
	lda ($fb),y
	beq !+
	sta ADDR_BUFFER,x
	inx	
	iny
	jmp !-

	!:
	sta ADDR_BUFFER,x

	rts

}


shell_cli_stop: {
	rts
}

}