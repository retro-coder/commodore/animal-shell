
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce


.const I_SHELL_CALL			= ADDR_SHELL



// SHELL

// initialize the shell
.const I_SHELL_INIT			= $00
// terminate the shell
.const I_SHELL_TERMINATE		= $ff

// start the shell
.const I_SHELL_RUN	 		= $10
// returns version information about the shell
.const I_SHELL_VERSION			= $11
// set shell mode
.const I_SHELL_SETMODE			= $12
// get shell mode
.const I_SHELL_GETMODE			= $13



// ENVIRONMENT VARS

// returns an environment variable value
.const I_SHELL_ENV_GET			= $20
// sets an environment variable
.const I_SHELL_ENV_SET	 		= $21



// COMMAND LINE INTERFACE

// init the command line
.const I_SHELL_CLI_INIT			= $30
// parse the command line interface buffer
.const I_SHELL_CLI_PARSE		= $31

// returns a parameter from the command line
// .const I_SHELL_PARM_GET		= $33
// // returns the total number of parameters on the command Line
// .const I_SHELL_PARM_COUNT		= $34
// // returns the next parameter on the command line when available
// .const I_SHELL_PARM_NEXT		= $35
// // returns the first parameter on the command Line
// .const I_SHELL_PARM_FIRST		= $36	
// // returns the command entered
// .const I_SHELL_PARM_COMMAND		= $37
// set command state
.const I_SHELL_CLI_SETSTATE		= $38
// set command state
.const I_SHELL_CLI_GETSTATE		= $39



// APPLICATION

// load an application
.const I_SHELL_APP_LOAD			= $40
// run the current app in memory
.const I_SHELL_APP_RUN			= $41
// set command state
.const I_SHELL_APP_SETSTATE		= $42
// set command state
.const I_SHELL_APP_GETSTATE		= $43



// COMMANDS

// loads a command	
.const I_SHELL_CMD_LOAD			= $50
// run the current command in memory	
.const I_SHELL_CMD_RUN			= $51	
// set command state
.const I_SHELL_CMD_SETSTATE		= $52
// set command state
.const I_SHELL_CMD_GETSTATE		= $53



// DRIVERS

// load a driver
.const I_SHELL_DRIVER_LOAD		= $60
// load a driver
.const I_SHELL_DRIVER_UNLOAD		= $61
// make a call to the driver interface
.const I_SHELL_DRIVER_CALL		= $62	



// LIBRARY

// load a library
.const I_SHELL_LIBRARY_LOAD		= $70
// unload a library
.const I_SHELL_LIBRARY_UNLOAD		= $71
// make a aall to the driver interace
.const I_SHELL_LIBRARY_CALL		= $72



// COMMAND LINE PARAMETERS

// returns the command entered
.const I_SHELL_PARM_COMMAND		= $80
// returns the total number of parameters on the command Line
.const I_SHELL_PARM_COUNT		= $81
// returns the first parameter on the command Line
.const I_SHELL_PARM_FIRST		= $82	
// returns the next parameter on the command line when available
.const I_SHELL_PARM_NEXT		= $83
// returns a parameter from the command line
.const I_SHELL_PARM_GET			= $84



// SHELL CATALOG

// create catalog
.const I_SHELL_CATALOG_CREATE		= $90
// get catalog item by index
.const I_SHELL_CATALOG_GET		= $91
// get catalog item by name
.const I_SHELL_CATALOG_GETBYNAME	= $92
// get first catalog item
.const I_SHELL_CATALOG_FIRST		= $93
// get next catalog item
.const I_SHELL_CATALOG_NEXT		= $94



// RESERVED

.const I_SHELL_RESERVED1		= $fa
.const I_SHELL_RESERVED2		= $fb
.const I_SHELL_RESERVED3		= $fc
.const I_SHELL_RESERVED4		= $fd
.const I_SHELL_RESERVED5		= $fe

