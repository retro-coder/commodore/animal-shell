
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.encoding "petscii_mixed"

#import "_shell_seg.asm"
#import "_shell_mac.asm"
#import "_shell_var.asm"
#import "_shell_sci.asm"
#import "_shell_dat.asm"
#import "_shell_par.asm"

