
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.pseudocommand @p_shell_cli_parse_in {

	// pointer to buffer location
	buffer: 	p_type_pointer 
}

.pseudocommand @p_shell_cmd_load_in {

	// pointer to the filename
	filename: 	p_type_pointer 
}

.pseudocommand @p_shell_cmd_parm_get_in {

	// paramater number to get
	index: 		p_type_byte
}

.pseudocommand @ds_core_file_sig_in {

	// pointer to the filename
        filename:	p_type_pointer 

       // device number
        device:         p_type_byte
}


.pseudocommand @p_shell_catalog_create_in {

	// pointer to the list for the catalog
        list:		p_type_pointer 
}


