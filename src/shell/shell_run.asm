
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "core/core.asm"

.filenamespace __shell__

.namespace __run__ {

.segment shell_data "shell_run"

	s_welcome:
	.text @"\$0e\$0d\$0dWelcome to Animal Shell\$00"

	s_hello:
	.text @"\$0d"
	.text @"Copyright 2022-2025 Paul Hocker\$0d" 
	.text @"Licensed under the MIT License\$0d"
	.text @"\$00"


#if CART
#if CART_EXT
	s_type:
	.text @"Boot From External ROM @ 0x8000\$00"
#else
	s_type:
	.text @"Boot From Internal ROM @ 0x8000\$00"
#endif
#else
	s_type:
	.text @"Boot From Disk\$00"
#endif

.segment shell_virtual "shell_run"

	.pseudopc ADDR_IN {
		file_load_in: p_core_file_load_in
	}

	__ShellHandler__(I_SHELL_RUN, shell_run)

.segment shell_code "shell_run"

	shell_run:

	__LogTrace__("shell=run")

	NPrintFromAddress(s_welcome)
	NPrintFromAddress(s_hello)
	NPrintFromAddress(s_type)

	lda SHELL_DEVICE
	sta file_load_in.device
	lda #$00
	sta file_load_in.show

	SetShellState(SHELL_STATE_CLI)
	SetShellCliState(SHELL_CLI_STATE_START)
	
}
