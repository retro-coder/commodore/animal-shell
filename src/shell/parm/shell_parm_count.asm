
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "shell/_shell.asm"
#import "core/core.asm"

.filenamespace __shell__

.namespace __parm__ {

.namespace __count__ {

.segment shell_virtual "shell_parm_count"

	__ShellHandler__(I_SHELL_PARM_COUNT, shell_parm_count)

.segment shell_code "shell_parm_count"

	shell_parm_count:

	__shell_parm_count__:

	__LogTrace__("shell>parm>count")

	ldy #0
	lda #99

	sta (SYS_CALL_RETURN), y

	rts
}

}
