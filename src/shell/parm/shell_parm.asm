
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.filenamespace __shell__

.namespace __parm__ {

#import "shell_parm_command.asm"
#import "shell_parm_count.asm"
#import "shell_parm_get.asm"
#import "shell_parm_first.asm"
#import "shell_parm_next.asm"

}
