
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "shell/_shell.asm"
#import "core/core.asm"

.filenamespace __shell__

.namespace __parm__ {

.namespace __command__ {

.segment shell_virtual "shell_parm_command"

	.pseudopc ADDR_IN {
		p_list: p_core_list_get_in 
	}

	__ShellHandler__(I_SHELL_PARM_COMMAND, shell_parm_command)

.segment shell_code "shell_parm_command"

	shell_parm_command:

	__shell_parm_command__:

	__LogTrace__("shell>parm>command")

	clc
		
	StorePointer(SHELL_CMD_TOKENS, p_list.list)
	lda #0
	sta p_list.index
	ICall1(I_CORE_CALL, I_CORE_LIST_VGET, p_list)

	bcc !+
	lda #1
	sta $ff

	!:

	rts
}

}
