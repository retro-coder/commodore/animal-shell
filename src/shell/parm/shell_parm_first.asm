
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "shell/_shell.asm"
#import "core/core.asm"

.filenamespace __shell__

.namespace __parm__ {

.namespace __first__ {

.segment shell_virtual "shell_parm_first"

	.pseudopc ADDR_IN {
		p_list: p_core_list_in
	}

	__ShellHandler__(I_SHELL_PARM_FIRST, shell_parm_first)

.segment shell_code "shell_parm_first"

	shell_parm_first:

	__shell_parm_first__:

	__LogTrace__("shell>parm>first")

	clc
		
	StorePointer(SHELL_CMD_TOKENS, p_list.list)
	ICall1(I_CORE_CALL, I_CORE_LIST_FIRST, p_list)
	ICall1(I_CORE_CALL, I_CORE_LIST_NEXT, p_list)

	bcc !+
	lda #1
	sta $ff

	!:

	rts
}

}
