
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "shell/_shell.asm"
#import "core/core.asm"

.filenamespace __shell__

.namespace __cli__ {

.namespace __parse__ {

.segment shell_virtual "shell_cli_parse"

	.pseudopc ADDR_DATA {
		cmd_parse: p_shell_cli_parse_in
	}

	.pseudopc ADDR_IN {
		str_split: p_core_str_split_in
	}

	.pseudopc ADDR_IN {
		p_list: p_core_list_in
	}

	__ShellHandler__(I_SHELL_CLI_PARSE, shell_cli_parse)

.segment shell_code "shell_cli_parse"

shell_cli_parse: {

	__LogTrace__("shell>cli>parse")

	SaveParms(cmd_parse, 3)

	// clear the list

	StorePointer(SHELL_CMD_TOKENS, p_list.list)
	ICall(I_CORE_CALL, I_CORE_LIST_CLEAR)

	// split string into token list

	StorePointer(SHELL_CMD_TOKENS, str_split.list)

	lda cmd_parse.buffer
	sta str_split.string
	lda cmd_parse.buffer + 1
	sta str_split.string + 1

	lda #32
	sta str_split.delim

	lda #0
	sta str_split.options

	ICall1(I_CORE_CALL, I_CORE_STR_SPLIT, str_split)

	rts
}

}

}
