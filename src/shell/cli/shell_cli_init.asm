
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "shell/_shell.asm"
#import "core/core.asm"

.filenamespace __shell__

.namespace __cli__ {

.namespace __init__ {

.segment shell_virtual "shell_cli_init"

	.pseudopc ADDR_IN {
		p_list_create: p_core_list_create_in
	}

	.pseudopc ADDR_IN {
		p_catalog_create: p_shell_catalog_create_in
	}
	

.segment shell_code "shell_cli_init"

	shell_cli_init:

	__shell_cli_init__:

	__LogTrace__("shell>cli>init")
	
	// create command token list

	StorePointer(SHELL_CMD_TOKENS, p_list_create.list)

	lda #128
	sta p_list_create.length
	lda #32
	sta p_list_create.size
	lda #1
	sta p_list_create.storage
	lda #0
	sta p_list_create.delim

	ICall(I_CORE_CALL, I_CORE_LIST_CREATE)

	rts
}

}
