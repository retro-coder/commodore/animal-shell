
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.filenamespace __shell__

.namespace __start__ {

.segment shell_data "shell_start"

.segment shell_virtual "shell_start"

.segment shell_code "shell_start"

	shell_start:

	__LogTrace__("shell>start")

}
