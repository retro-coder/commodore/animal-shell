
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "shell/_shell.asm"
#import "core/core.asm"

.filenamespace __shell__

.namespace __cat__ {

.namespace __create__ {

.segment shell_virtual "shell_cat_create"

	.pseudopc ADDR_IN {
		p_create: p_shell_catalog_create_in
	}

        .pseudopc ADDR_IN {
                p_list_create: p_core_list_create_in
        }

	__ShellHandler__(I_SHELL_CATALOG_CREATE, shell_catalog_create)

.segment shell_code "shell_cat_create"

	shell_catalog_create:

        SaveParms(p_create, 1)
        
        __shell_catalog_create__:

	__LogTrace__("shell>catalog>create")

	clc

	lda #12
	sta p_list_create.length
        lda #18
        sta p_list_create.size
        lda #0
        sta p_list_create.storage
	ICall1(I_CORE_CALL, I_CORE_LIST_CREATE, p_list_create)

	bcc !+
	lda #1
	sta $ff

	!:

	rts
}

}
