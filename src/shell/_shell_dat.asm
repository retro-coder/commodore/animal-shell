
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.pseudocommand @ds_shell_driver_item {

	// interal id of driver
	id:		p_type_byte

	// driver name
	name:		p_type_string(16)

	// expanded memory index
	index:		p_type_byte
}

.pseudocommand @ds_shell_command_item {

	// interal id of command
	id:		p_type_byte

	// command name
	name:		p_type_string(16)
	
	// expanded memory index	
	index:		p_type_byte
}

.pseudocommand @ds_shell_library_item {

	// interal id of command
	id:		p_type_byte

	// command name
	name:		p_type_string(16)
	
	// expanded memory index	
	index:		p_type_byte
}

.pseudocommand @ds_shell_application_item {

	// interal id of program
	id:		p_type_byte

	// app name
	name:		p_type_string(16)

	// expanded memory index
	index:		p_type_byte
}
