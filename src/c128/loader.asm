
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "system/sys.asm"
#import "core/core.asm"
#import "shell/shell.asm"

.filenamespace __loader__

.segment loader [segments="loader_header, loader_code"]

.segmentdef loader_header [start=ADDR_CART]
.segmentdef loader_code [startAfter="loader_header"]

.segment loader_header

	jmp coldstart
	jmp coldstart

	.byte $ff
	.text @"CBM"

/*
	this needs to come after the CBM
	text or it will not have the
	correct encoding and the
	cartridge will not load
*/

.encoding "petscii_mixed"

.segment loader_code

	coldstart: {

	sei 

	// we never plan to return to basic, steal everything
	ldx #$ff
	txs

	// turn on kernal messages

	lda #$c0
	sta $9d

	// add 1 row margin on top of screen

	lda #$01
	sta $e5

	lda #BANK_CONFIG		
	sta $ff00

	lda #<romirq
	sta $0314
	lda #>romirq
	sta $0315

	lda #<ramirq
	sta $fffe
	lda #>ramirq
	sta $ffff

	// copy runner location at $1000

	ldx #0 

	!:
	lda runner,x
	sta ADDR_RUNNER,x

	inx
	cpx #runner_end - runner_start
	bne     !-

	cli

	PrintFromAddress(s_startup)

	// initialize the core
	ICall(I_CORE_CALL, I_CORE_INIT)

	// initialize the shell
	ICall(I_SHELL_CALL, I_SHELL_INIT)

	// shell run
	ICall(I_SHELL_CALL, I_SHELL_RUN)

	!:
	inc	$0403
	jmp 	!-

	s_startup:
	.text @"\$0d\$0dstarting . . .\$0d\$00"

}


	runner:

	.pseudopc ADDR_RUNNER
	{

	runner_start:

	romirq:

	inc $0400
	jmp $fa65

	ramirq:

	pha
	txa
	pha
	tya
	pha

	lda $ff00
	pha

	lda #0
	sta $ff00
	inc $0401
	jmp $fa65

	runner_end:

	}

#if CART
#else
.file [name="loader.128.prg", segments="loader"]
#endif