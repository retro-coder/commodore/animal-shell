
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "system/sys.asm"

.encoding "petscii_mixed"

.filenamespace __boot__


.segment boot[segments="boot_code"]
.segmentdef boot_code[]

.segment boot_code "basic"

.const fn_loader = "loader.128"
.const fn_core = "core.128"
.const fn_shell = "shell.128"

.var fl_loader = fn_loader.size()
.var fl_core = fn_core.size()
.var fl_shell = fn_shell.size()

BasicUpstart128(main)

.segment boot_code "boot"

* = $1d00

main:

lda #$80
sta $9d

lda #fl_loader
ldx #<f_loader
ldy #>f_loader
jsr loadfile

lda #fl_core
ldx #<f_core
ldy #>f_core
jsr loadfile

lda #fl_shell
ldx #<f_shell
ldy #>f_shell
jsr loadfile

lda #BANK_CONFIG		
sta $ff00

jmp ADDR_LOADER + $0a

f_loader:
.text fn_loader

f_shell:
.text fn_shell

f_core:
.text  fn_core

loadfile:
        
jsr $ffbd
lda #$00
tax
jsr $ff68
lda #$01
ldx $ba  
bne skip
ldx #$08

skip:

ldy #$01
jsr $ffba
lda #$00
jsr $ffd5
bcs error
rts

error:

jsr $ff7d
.text @"error\$00"

rts

.macro BasicUpstart128(address) {

.pc = $1c01 "c128_basic_start"
.word upstartEnd
.word 10
.byte $9e
.text toIntString(address)
.byte 0

upstartEnd:

.word 0
.pc = $1c0e "basic_end"

}

.file [name="boot.128.prg", segments="boot"]
