/*
	Copyright 2022, SpockerDotNet LLC

	auto.asm

	Creates an Autoboot Disk file for C-128

	This program follows the specs for building a boot
	sector file as described in the C-128
	Programmers Guide.

	Page 446. $FF53 BOOT CALL
	
*/

#import "system/sys.asm"

.filenamespace _auto

.segmentdef auto [start=$b00]

.segment auto

	.text 	@"CBM"
	.word 	$0000
	.byte 	$00
	.byte 	$00
	
	.text 	@"ANIMAL SHELL"
	.byte 	$00

	.text 	@"SHELL.A"
	.byte 	$00

	jmp	ADDR_SHELL	

	.align 	$ff
	
	
