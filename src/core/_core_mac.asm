
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

/*
	Define the Macros for the Core Library

*/

.filenamespace __core__

.struct InterfaceHandler {
	id
}

.var interface_handlers = Hashtable()

.macro __CoreInit__() {
}

.macro __CoreHandler__(id, handler) {

	.if (interface_handlers.containsKey(id)) {
		.error "Core Interface Handler Id [" + id + "] has already been used."
	}

	// store the call id in our lookup table

	.var _ih = InterfaceHandler()
	.eval _ih.id = id 
	.eval interface_handlers.put(id, _ih)

	.segment core_handler

	cmp #id
	bne !+
	jmp handler

	!:

}
