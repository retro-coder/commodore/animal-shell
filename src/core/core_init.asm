
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "_core.asm"

.filenamespace __core__

.namespace __init__ {

	__CoreHandler__(I_CORE_INIT, core_init)

.segment core_code "core_init"

	core_init:

	__LogTrace__("core>init")

	jsr __core__.__mem__.__init__.__core_mem_init__

	// set default next file number

	lda #200
	sta CORE_ZERO_NEXTFILE

	lda #0
	sta CORE_ZERO_CURRFILE

	 // 512K REU

	lda #8
	sta CORE_ZERO_XM_BANKS

	rts
}