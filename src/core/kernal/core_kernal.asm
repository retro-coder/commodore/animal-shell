/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __kernal__
{

/*
	JSETNAM

	length	the size of the filename
	filename	the address in lo/hi format to the filename


	.a	filename length
	.x	filename low byte
	.y	filename high byte

*/
.macro @JSETNAM(length, filename) {

	.const JSETNAM = $ffbd

	__LogTrace__("core>kernal>jsetnam")

	lda	length 
	ldx	filename
	ldy	filename + 1

	jsr JSETNAM

	rts

}

}
