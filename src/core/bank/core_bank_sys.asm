
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __bank__ {

.namespace __sys__ {

	__CoreHandler__(I_CORE_BANK_SYS_IN, __core_bank_sys_in__)
	__CoreHandler__(I_CORE_BANK_SYS_OUT, __core_bank_sys_out__)

.segment core_code "__core_bank_sys_in__"

	__core_bank_sys_in__:

	sei
	pha

#if C128
#else
#if CART
	lda $01
	and #%11111110
	sta $01
#else
	lda #%000
	sta $01
#endif 
#endif 
	pla

	rts

.segment core_code "__core_bank_sys_out__"

	__core_bank_sys_out__:

	pha	

#if C128
#else
#if CART
	lda $01
	ora #%1
	sta $01
#else
	lda #%110
	sta $01
#endif
#endif
	pla
	cli

	rts
}

}