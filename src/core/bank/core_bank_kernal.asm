
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce

#import "core/_core.asm"

.filenamespace __core__

.namespace __bank__ {

.namespace __kernal__ {

	__CoreHandler__(I_CORE_BANK_KERNAL_IN, __core_bank_kernal_in__)
	__CoreHandler__(I_CORE_BANK_KERNAL_OUT, __core_bank_kernal_out__)

.segment core_code "__core_bank_kernal_in__"

	__core_bank_kernal_in__:

	sei
	pha

#if C128
	lda $ff00
	and #%11001111 
	sta $ff00
#else
#if CART
#else
	lda #%110
	sta $01
#endif 
#endif
	pla

	rts

.segment core_code "__core_bank_kernal_out__"

	__core_bank_kernal_out__:

	pha	

#if C128
	lda	$ff00
	ora	#%00110000 
	sta	$ff00
#else
#if CART
#else
	lda #%000
	sta $01
#endif
#endif
	pla
	cli
	rts
}

}