
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __bank__ {

#import "core_bank_kernal.asm"
#import "core_bank_io.asm"
#import "core_bank_sys.asm"
	
}
