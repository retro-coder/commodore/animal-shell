
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce 


/*
	INPUT PARAMATERS
*/

/*
	FILE
*/

.pseudocommand @p_core_file_getc_in {	

	// file number
	file:		p_type_byte 
}

.pseudocommand @p_core_file_load_in {

	// pointer to a zero delimited filename
	filename:	p_type_pointer  

	// device number (8-16)
	device:		p_type_byte 

	// 0 = hide kernal messages (default); 1 = show kernal messages   	
	show:		p_type_flag
}

.pseudocommand @p_core_file_loadat_in {

	// pointer to the zero delimeted filename in memory
	filename:	p_type_pointer

	// device number (8-30)
	device:		p_type_byte 

	// 0 = hide kernal messages (default); 1 = show kernal messages
	show:		p_type_flag

	// optional memory location to load file
	address:	p_type_address
}

.pseudocommand @p_core_file_open_in {

	// address of the delimited string
	filename:	p_type_address 

	// length in bytes to print
	device:		p_type_byte 

	// 0 = hide kernal messages (default); 1 = show kernal messages
	show:		p_type_flag 
}

.pseudocommand @p_core_file_close_in {

	// file number to close
	file:		p_type_byte 
}

/*
	LIST
*/

.pseudocommand @p_core_list_append_in {

	// pointer to the list	
	list:		p_type_pointer  

	// pointer to the item to append
	item:		p_type_pointer

	// number of characters to append
	length:	p_type_byte
}

.pseudocommand @p_core_list_clear_in {

	// address of the list	
	list:		p_type_address  

	// optional parm to clear the data table
	options:	p_type_bits
}

.pseudocommand @p_core_list_create_in {

	// pointer to the list	
	list:		p_type_pointer   

	// length of each item in the list
	length:		p_type_byte

	// the max number of items in the list
	size:		p_type_byte

	// storage method to use
	storage:	p_type_byte

	// variable storage delimeter
	delim:		p_type_char
}

.pseudocommand @p_core_list_get_in {

	// pointer to the list	
	list:		p_type_pointer

	// index of item to return 
	index:		p_type_byte
}

.pseudocommand @p_core_list_in {

	// pointer to the list	
	list:		p_type_pointer
}

.pseudocommand @p_core_list_remove_in {

	// pointer to the list	
	list:		p_type_pointer

	// index of item to remove
	index:		p_type_byte
}


/*
	IO
*/

.pseudocommand @p_core_io_print_in {

	// pointer to the delimited string
	string:		p_type_pointer

	// length in bytes to print
	length:		p_type_byte

	// optional delimeter character (default is $00)
	delim:		p_type_char
}

/*
	MEMORY
*/

.pseudocommand @p_core_mem_swap_in {

	// what bank in memory to use
        mem_bank:       p_type_byte

	// what page in memory to start from
        mem_page:       p_type_byte 

	// the size (in pages) to swap
        mem_size:       p_type_byte 

       // starting page to swap with on computer
        addr_page:      p_type_byte 

       // bank to use on the computer (c-128 only 0 or 1)	
        addr_bank:      p_type_byte 
}

.pseudocommand @p_core_mem_fetch_in {

       // what bank in memory to use
        mem_bank:       p_type_byte 

       // what page in memory to start from
        mem_page:       p_type_byte 

       // the size (in pages) to swap
        mem_size:       p_type_byte 

       // starting page on computer
        addr_page:      p_type_byte 

       // c128 bank to use on the computer
        addr_bank:      p_type_byte 
}

.pseudocommand @p_core_mem_stash_in {

       // what bank in memory to use
        mem_bank:       p_type_byte 

       // what page in memory to start from
        mem_page:       p_type_byte 

       // the size (in pages) to swap
        mem_size:       p_type_byte 

       // starting page on computer
        addr_page:      p_type_byte 

       // c128 bank to use on the computer
        addr_bank:      p_type_byte 
}

.pseudocommand @p_core_mem_alloc_in {

	// the number of pages to allocate
        size:       	p_type_byte 
}


/*
	STRING
*/

.pseudocommand @p_core_str_concat_in {
	
	// pointer to the first string
	stringa:	p_type_pointer

	// pointer to the second string
	stringb:	p_type_pointer 

	// options 
	//
	// 	bit 0	add space between word (on=yes)
	//
	options:	p_type_bits
}

.pseudocommand @p_core_str_findchar_in {

	// pointer to the zero delimited string
	string:		p_type_pointer 

	// character to find in the string
	search:		p_type_char
}

.pseudocommand @p_core_str_findstr_in {

	// pointer to the zero delimited string
	string:		p_type_pointer 

	// pointer to the zero delimited string to search for
	search:		p_type_pointer
}

.pseudocommand @p_core_str_len_in {

	// address in memory of the zero delimited string
	string:		p_type_pointer 

	// the delimiter of the string (default is $00)
	delim:		p_type_char
}

.pseudocommand @p_core_str_split_in {

	// pointer to the zero delimited string
	string:		p_type_pointer

	// pointer to the list to store the split string
	list:		p_type_address   

	// the delimiter of the string (default is $00)
	delim:		p_type_char

	// options
	//
	//	bit 0	treat quoted words as one string
	//
	options:	p_type_bits
}


/*
	UTILS
*/


.pseudocommand @p_core_util_num2str {

	type:		p_type_byte
	addr:		p_type_pointer
	length:		p_type_byte 

}

.pseudocommand @p_core_util_str2num {
	addr:	p_type_pointer
}


/*
	OUTPUT PARAMATERS
*/


/*
	FILE
*/

.pseudocommand @p_core_file_open_out {

	// open file number
	file:		p_type_byte

	// device status code
	status:		p_type_byte

	// device status message
	message:	p_type_string(64)
}

/*
	LIST
*/

.pseudocommand @p_core_list_item_out {

	// the pointer to the item in the list
	item:		p_type_pointer
}

/*
	MEMORY
*/

.pseudocommand @p_core_mem_alloc_out {

	// bank number allocated
	bank:		.byte $00

	// starting page number allocated
	page:		.byte $00
}


/*
	STRING
*/

.pseudocommand @p_core_str_len_out {

	// the length of the string
	length:		p_type_byte
}

.pseudocommand @p_core_str_findchar_out {

	// the position of where string was found
	pos:		p_type_byte
}

.pseudocommand @ds_core_str_split_in {

        address:	.word $0000
        char:		.byte $20
	options:	.byte $00
}


