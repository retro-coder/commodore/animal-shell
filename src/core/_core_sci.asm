
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce 

// core interface entry point
.const I_CORE_CALL 			= ADDR_CORE

// initialize the core
.const I_CORE_INIT			= $00
// terminate the core
.const I_CORE_TERMINATE			= $FF

// UTILS

// convert a number to a string
.const I_CORE_UTIL_NUM2STR		= $10
// convert a string to a number
.const I_CORE_UTIL_STR2NUM		= $11

// CORE

// load a file to its absolute memory location
.const I_CORE_FILE_LOAD 		= $20
// load a file to a specific memory address
.const I_CORE_FILE_LOADAT 		= $21
// load a file into expanded memory
.const I_CORE_FILE_LOADX 		= $22
// open a file
.const I_CORE_FILE_OPEN			= $24
// close a file
.const I_CORE_FILE_CLOSE		= $25
// get a single byte from an open file
.const I_CORE_FILE_GETC			= $26
// get a record from an open file
.const I_CORE_FILE_GETS			= $27 
// get a block of data from an open file
.const I_CORE_FILE_READ			= $28
// move the next file access
.const I_CORE_FILE_SEEK			= $29
// write a single byte to the open file
.const I_CORE_FILE_PUTC			= $2a
// write a record to the open file
.const I_CORE_FILE_PUTS			= $2b
// write a block of data to an open file
.const I_CORE_FILE_WRITE		= $2c

// IO

// print a string to the console
.const I_CORE_IO_PRINT 			= $30
// print a string to the console folowed by linefeed
.const I_CORE_IO_NPRINT 		= $31
// print a formatted string
.const I_CORE_IO_PRINTF			= $32
// read device i/o status
.const I_CORE_IO_STATUS			= $33

// LISTS

// create a new list
.const I_CORE_LIST_CREATE 		= $40
// get an item from a list
.const I_CORE_LIST_GET 			= $41
// get List Information
.const I_CORE_LIST_INFO			= $42
// return the first item from a list
.const I_CORE_LIST_FIRST 		= $43
// return the last item from a list
.const I_CORE_LIST_LAST 		= $44
// return the next item from a list
.const I_CORE_LIST_NEXT 		= $45
// add an item to the end of a list
.const I_CORE_LIST_APPEND		= $46
// remove all items from a list
.const I_CORE_LIST_CLEAR		= $47
// free memory used by a list
.const I_CORE_LIST_FREE			= $48
// remove item from a list
.const I_CORE_LIST_REMOVE		= $49
// return value from a list
.const I_CORE_LIST_VGET			= $4a


.const I_CORE_STACK_PUSH 		= $50 	// Push data onto the stack, return index in accumulator
.const I_CORE_STACK_PULL 		= $51 	// Pull data from the stack

// MEMORY

// initialize the memory
.const I_CORE_MEM_INIT 			= $60
// stash to expanded memory
.const I_CORE_MEM_STASH 		= $61
// fetch from expanded memory
.const I_CORE_MEM_FETCH 		= $62
// swap from expanded memory
.const I_CORE_MEM_SWAP 			= $63
// allocate expanded memory
.const I_CORE_MEM_ALLOC 		= $64
// deallocate expanded memory
.const I_CORE_MEM_DEALLOC 		= $65


// BANKING

// bank KERNAL in
.const I_CORE_BANK_KERNAL_IN		= $70
// bank KERNAL out
.const I_CORE_BANK_KERNAL_OUT		= $71
// bank IO in
.const I_CORE_BANK_IO_IN		= $72
// bank IO out
.const I_CORE_BANK_IO_OUT		= $73
// bank system storage area in
.const I_CORE_BANK_SYS_IN		= $74
// bank system storage area out
.const I_CORE_BANK_SYS_OUT		= $75



// STRING

// concatenate two strings
.const I_CORE_STR_CONCAT		= $a0
// split a string
.const I_CORE_STR_SPLIT 		= $a1
// return the length of a string
.const I_CORE_STR_LEN			= $a2
// get string from memory
.const I_CORE_STR_GET			= $a3
// strip characters from string
.const I_CORE_STR_STRIP			= $a4
// compare strings for equality
.const I_CORE_STR_EQ			= $a5
// find first occurance of character in a string
.const I_CORE_STR_FINDCHAR		= $a6
// find first occurance of string in a string
.const I_CORE_STR_FINDSTR		= $a6


// RESERVED

.const I_CORE_RESERVED1			= $fa
.const I_CORE_RESERVED2			= $fb
.const I_CORE_RESERVED3			= $fc
.const I_CORE_RESERVED4			= $fd
.const I_CORE_RESERVED5			= $fe

