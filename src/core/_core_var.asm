
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

// ZERO PAGE VARS

.segment sys_zero_core "core_zero_vars"

// shared from address
CORE_ZERO_FROM:		p_type_address

// shared to address
CORE_ZERO_TO:		p_type_address

// next logical file to open
CORE_ZERO_NEXTFILE:	p_type_byte 

// curent logical file
CORE_ZERO_CURRFILE:	p_type_byte

// total expanded memory banks
CORE_ZERO_XM_BANKS:	p_type_byte

// current expanded memory bank in use
CORE_ZERO_XM_BANK:	p_type_byte


// ZERO PAGE TEMP SPACE

.segment sys_zero_core "core_zero_temp"
// temp space for core calls
CORE_ZERO_TEMP:	.fill 16, $ee

.segment sys_zero_core "core_zero_list"
// temp space for list calls
CORE_ZERO_LIST:	.fill 24, $00


// SYSTEM STORAGE AREA

// xmem bank allocation table
.segment sys_area_core "core_table_xmem_banks"
CORE_TABLE_XMEM_BANKS:	.fill 256, 0

// xmem bank page allocation table
.segment sys_area_core "core_table_xmem_pages"
CORE_TABLE_XMEM_PAGES:	.fill 256,0

.segment sys_area_core "core_list_stack"
CORE_LIST_STACK:	.fill 256, 0

.segment sys_area_core "core_list_buffer"
CORE_LIST_BUFFER:	.fill 256, 0

.segment sys_area_core "core_list_names"
CORE_LIST_NAMES:	.fill 256, 0