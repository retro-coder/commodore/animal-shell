
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce

/*
	Core Segments
*/

.filenamespace __core__

#import "system/sys.asm"

.segment core [segments="core_call, core_data, core_handler, core_code, core_code_mem, core_code_input, core_virtual"]

.segmentdef core_call		[start=ADDR_CORE]
.segmentdef core_data 		[startAfter="core_call"]
.segmentdef core_handler 	[startAfter="core_data"]
.segmentdef core_code 		[startAfter="core_handler"]
.segmentdef core_code_mem 	[start=ADDR_CORE_MEM]
.segmentdef core_code_input 	[start=ADDR_CORE_INPUT]

.segmentdef core_virtual	[virtual]

