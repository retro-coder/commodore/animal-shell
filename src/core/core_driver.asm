
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "_core.asm"


/*
	Core Driver

	Adds support for drivers in the Core. A driver is useful to abstract calls between the shell
	and the hardware. For example, disk drives might use a different syntax for doing a directory
	and the driver will ensure that the experience for the user is consistent even though the
	language might be different.

	Driver Allocation Table

		.byte	Id		The Unique Identifier (Used as a cross reference to the Type)
		.byte	Pointer		The Pointer in the Memory Allocation Table (MAT)

	
*/

.filenamespace __core__

.namespace __driver__ {

}