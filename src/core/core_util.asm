
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce

#import "_core.asm"

.filenamespace __core__

.namespace __util__ 
{
	.pseudopc $30 {
		z_ptr01: .word 0
		z_stridx: .byte 0
		z_pfac: .byte 0
		z_sfac: .byte 0
	}
		


	__CoreHandler__(I_CORE_UTIL_NUM2STR, util_num2str)
	__CoreHandler__(I_CORE_UTIL_STR2NUM, util_str2num)

	.segment core_data

	// .segment core_parms


	/*

	http://6502.org/source/strings/ascii-to-32bit.html
	
		Function: I_CORE_UTIL_NUM2STR

		Converts a Number to a Printable String

		Example:

		--- ASM
		p_num2str:
		
			ds_core_util_splitstr_in

		string:
		
			.fill 32,$00

		number:

			.byte $00,$c0,$00,$00


		// the lo/hi address of the string to split

		lda	#<number
		sta	p_num2str.number
		lda	#>number
		sta	p_num2str.number

		ICall(I_CORE, I_CORE_UTIL_SPLITSTR, p_num2str, string)
		PrintFromAddress(string)
		---

		Input: 
		
		<p_core_util_num2str>

		Output:

		


	*/

	.segment core_code "util_num2str"

	util_num2str:
	{

	.label zpptr = $30

		// clear temp vars

		// ldx	#80
		// lda	#$00

		// !:

		// sta	zpptr,x
		// dex 
		// bne	!-
		

		// lda	#$10
		// sta	ADDR_WORK
		// lda	#$00
		// sta	ADDR_WORK + 1
		// lda	#$ff
		// sta	ADDR_WORK + 2
		// lda	#$80
		// sta	ADDR_WORK + 3

	        // ldx #<ADDR_WORK      //binary value address lsb
	        // ldy #>ADDR_WORK      //binary value address msb

		ldy #$00
	        lda (SYS_CALL_PARMS),y      //binary value address lsb
		pha	
		iny  
	        lda (SYS_CALL_PARMS),y      //binary value address lsb
		tax
		iny  
	        lda (SYS_CALL_PARMS),y      //binary value address lsb
		tay
		pla			

	        // lda #'%'         //radix character, see below

		// ldx	$fb 
		// ldy	$fc
		
	        // ora #%10000000   //radix suppression, see below

	        jsr __num2str__         //perform conversion
	        stx zpptr          //save string address lsb
	        sty zpptr+1        //save string address msb
	        tay                //string length
	loop:    lda (zpptr),y      //copy string to...
	         sta (SYS_CALL_RETURN),y      //copy string to...
	        // sta ADDR_OUT,y      //safe storage, will include...
	        dey                //the terminator
	        bpl loop

		// lda #<ADDR_OUT
		// sta zpptr 
		// lda #>ADDR_OUT 
		// sta zpptr+1

		// ldy #$00

		// !:

		// lda (zpptr), y 
		// beq !+
		// jsr $ffd2 
		// iny
		// jmp !-
		
		// !:

		rts 
	}

	.segment core_code "util_str2num"

	util_str2num:
	{


		ldy #$00
	        lda (SYS_CALL_PARMS),y      //binary value address lsb
		tax
		iny  
	        lda (SYS_CALL_PARMS),y      //binary value address lsb
		tay
		
		// lda	$fb
		// tax 
		// lda	$fc 
		// tay 

		// ldx #<numstr
		// ldy #>numstr
		jsr __str2num__

		ldx #$00
		ldy #$00

		bcs error

		!:


		// lda __str2num__.pfac, x
		lda z_pfac, x
		sta (SYS_CALL_RETURN), y
		inx 
		iny
		cpx #$04
		bne !-
		jmp end

error:
		lda #RED
		sta $d020
		
end:

		rts

numstr: .text @"1024\$00"

	}

	__num2str__:
	{

		.const a_hexdec ='a'-'9'-2            //hex to decimal difference
		.const m_bits   =32                   //operand bit size
		.const m_cbits  =48                   //workspace bit size
		.const m_strlen =m_bits+1             //maximum printable string length
		.const n_radix  =4                    //number of supported radices
		.const s_pfac   =m_bits/8             //primary accumulator size
		.const s_ptr    =2                    //pointer size
		.const s_wrkspc =m_cbits/8            //conversion workspace size

		.label ptr01    =$30
		.label pfac     =ptr01+s_ptr          //primary accumulator
		.label wrkspc01 =pfac+s_pfac          //conversion...
		.label wrkspc02 =wrkspc01+s_wrkspc    //workspace
		.label formflag =wrkspc02+s_wrkspc    //string format flag
		.label radix    =formflag+1           //radix index
		.label stridx   =radix+1              //string buffer index
		.label strbuf   =stridx+m_strlen+1        //conversion string buffer


		binstr:   stx ptr01             //operand pointer LSB
			sty ptr01+1           //operand pointer MSB
			tax                   //protect radix
			ldy #s_pfac-1         //operand size
		//
		binstr01: lda (ptr01),y         //copy operand to...
			sta pfac,y            //workspace
			dey
			bpl binstr01
		//
			iny
			sty stridx            //initialize string index
		//
		//	--------------
		//	evaluate radix
		//	--------------
		//
			txa                   //radix character
			asl                   //extract format flag &...
			ror formflag          //save it
			lsr                   //extract radix character
			ldx #n_radix-1        //total radices
		//
		binstr03: cmp radxtab,x         //recognized radix?
			beq binstr04          //yes
		//
			dex
			bne binstr03          //try next
		//
		//	------------------------------------
		//	radix not recognized, assume decimal
		//	------------------------------------
		//
		binstr04: stx radix             //save radix index for later
			txa                   //converting to decimal?
			bne binstr05          //no
		//
		//	------------------------------
		//	prepare for decimal conversion
		//	------------------------------
		//
			jsr facbcd            //convert operand to BCD
			lda #0
			beq binstr09          //skip binary stuff
		//
		//	-------------------------------------------
		//	prepare for binary, octal or hex conversion
		//	-------------------------------------------
		//
		binstr05: bit formflag
			bmi binstr06          //no radix symbol wanted
		//
			lda radxtab,x         //radix table
			sta strbuf            //prepend to string
			inc stridx            //bump string index
		//
		binstr06: ldx #0                //operand index
			ldy #s_wrkspc-1       //workspace index
		//
		binstr07: lda pfac,x            //copy operand to...
			sta wrkspc01,y        //workspace in...
			dey                   //big-endian order
			inx
			cpx #s_pfac
			bne binstr07
		//
			lda #0
		//
		binstr08: sta wrkspc01,y        //pad workspace
			dey
			bpl binstr08
		//
		//	----------------------------
		//	set up conversion parameters
		//	----------------------------
		//
		binstr09: sta wrkspc02          //initialize byte counter
			ldy radix             //radix index
			lda numstab,y         //numerals in string
			sta wrkspc02+1        //set remaining numeral count
			lda bitstab,y         //bits per numeral
			sta wrkspc02+2        //set
			lda lzsttab,y         //leading zero threshold
			sta wrkspc02+3        //set
		//
		//	--------------------------
		//	generate conversion string
		//	--------------------------
		//
		binstr10: lda #0
			ldy wrkspc02+2        //bits per numeral
		//
		binstr11: ldx #s_wrkspc-1       //workspace size
			clc                   //avoid starting carry
		//
		binstr12: rol wrkspc01,x        //shift out a bit...
			dex                   //from the operand or...
			bpl binstr12          //BCD conversion result
		//
			rol                   //bit to .A
			dey
			bne binstr11          //more bits to grab
		//
			tay                   //if numeral isn't zero...
			bne binstr13          //skip leading zero tests
		//
			ldx wrkspc02+1        //remaining numerals
			cpx wrkspc02+3        //leading zero threshold
			bcc binstr13          //below it, must convert
		//
			ldx wrkspc02          //processed byte count
			beq binstr15          //discard leading zero
		//
		binstr13: cmp #10               //check range
			bcc binstr14          //is 0-9
		//
			adc #a_hexdec         //apply hex adjust
		//
		binstr14: adc #'0'              //change to ASCII
			ldy stridx            //string index
			sta strbuf,y          //save numeral in buffer
			inc stridx            //next buffer position
			inc wrkspc02          //bytes=bytes+1
		//
		binstr15: dec wrkspc02+1        //numerals=numerals-1
			bne binstr10          //not done
		//
		//	-----------------------
		//	terminate string & exit
		//	-----------------------
		//
			lda #0
			ldx stridx            //printable string length
			sta strbuf,x          //terminate string
			txa
			ldx #<strbuf          //converted string LSB
			ldy #>strbuf          //converted string MSB
			clc                   //all okay
			// cld			// clear decimal mode as well

			rts
		//
		//================================================================================
		//
		//CONVERT PFAC INTO BCD
		//
		//	---------------------------------------------------------------
		//	Uncomment noted instructions if this code is to be used  on  an
		//	NMOS system whose interrupt handlers do not clear decimal mode.
		//	---------------------------------------------------------------
		//
		facbcd:   ldx #s_pfac-1         //primary accumulator size -1
		//
		facbcd01: lda pfac,x            //value to be converted
			pha                   //protect
			dex
			bpl facbcd01          //next
		//
			lda #0
			ldx #s_wrkspc-1       //workspace size
		//
		facbcd02: sta wrkspc01,x        //clear final result
			sta wrkspc02,x        //clear scratchpad
			dex
			bpl facbcd02
		//
			inc wrkspc02+s_wrkspc-1
			php                   //!!! uncomment for NMOS MPU !!!
			sei                   //!!! uncomment for NMOS MPU !!!
			sed                   //select decimal mode
			ldy #m_bits-1         //bits to convert -1
		//
		facbcd03: ldx #s_pfac-1         //operand size
			clc                   //no carry at start
		//
		facbcd04: ror pfac,x            //grab LS bit in operand
			dex
			bpl facbcd04
		//
			bcc facbcd06          //LS bit clear
		//
			clc
			ldx #s_wrkspc-1
		//
		facbcd05: lda wrkspc01,x        //partial result
			adc wrkspc02,x        //scratchpad
			sta wrkspc01,x        //new partial result
			dex
			bpl facbcd05
		//
			clc
		//
		facbcd06: ldx #s_wrkspc-1
		//
		facbcd07: lda wrkspc02,x        //scratchpad
			adc wrkspc02,x        //double &...
			sta wrkspc02,x        //save
			dex
			bpl facbcd07
		//
			dey
			bpl facbcd03          //next operand bit
		//
			plp                   //!!! uncomment for NMOS MPU !!!
			ldx #0
		//
		facbcd08: pla                   //operand
			sta pfac,x            //restore
			inx
			cpx #s_pfac
			bne facbcd08          //next
		//
			rts
		//
		//================================================================================
		//
		//PER RADIX CONVERSION TABLES
		//
		bitstab:  .byte 4,1,3,4         //bits per numeral
		lzsttab:  .byte 2,9,2,3         //leading zero suppression thresholds
		numstab:  .byte 12,48,16,12     //maximum numerals
		// radxtab:  .byte 0,"%@$"         //recognized symbols
		radxtab:  .text @"\$00%@$"         //recognized symbols
		//
		//================================================================================
		//
		//STATIC STORAGE
		//
		// strbuf:   *=*+m_strlen+1        //conversion string buffer
		//
		//================================================================================

	}

	__str2num__:
	{

		.const a_maskuc =%01011111            //case conversion mask
		.const a_hexnum ='a'-'9'-1            //hex to decimal difference
		.const n_radix  =4                    //number of supported radixes
		.const s_fac    =4                    //binary accumulator size


		
		.label ptr01    =$0030                  //input string pointer
		.label stridx   =ptr01+2              //string index
		.label pfac     =stridx+1             //primary accumulator
		.label sfac     =pfac+s_fac           //secondary accumulator
		//
		//DYNAMIC STORAGE
		//
		.label bitsdig  =sfac+4                 //bits per digit	
		.label curntnum =sfac+5                 //numeral being processed
		.label radxflag =sfac+6                 //$80 = processing base-10
		.label valdnum  =sfac+7                 //valid range +1 for selected radix


		strbin:  

			lda	#$00
			sta	bitsdig
			sta	curntnum 
			sta	radxflag
			sta	valdnum
			sta	ptr01 
			sta	ptr01+1
			sta	ptr01+2
			sta	ptr01+3
			sta	ptr01+4
			sta	ptr01+5
			sta	ptr01+6



			stx ptr01             //save string pointer LSB
			sty ptr01+1           //save string pointer MSB
			lda #0
			ldx #s_fac-1          //accumulator size
		//
		strbin01: sta pfac,x            //clear
			dex
			bpl strbin01
		//
		//	------------------------
		//	process radix if present
		//	------------------------
		//
			tay                   //starting string index
			clc                   //assume no error for now
			lda (ptr01),y         //get a char
			bne strbin02
		//
			rts                   //null string, so exit
		//
		strbin02: ldx #n_radix-1
		//
			
		strbin03: cmp radxtab,x         //recognized radix?
			beq strbin04          //yes
		//
			dex
			bpl strbin03          //try next
		//
			stx radxflag          //assuming decimal...
			inx                   //which might be wrong
		//
		strbin04: lda basetab,x         //number bases table
			sta valdnum           //set valid numeral range
			lda bitstab,x         //get bits per digit
			sta bitsdig           //store
			txa                   //was radix specified?
			beq strbin06          //no
			//  beq strbin05          //no
		//
			iny                   //move past radix
		//
		strbin05: sty stridx            //save string index
		//
		//	--------------------------------
		//	process number portion of string
		//	--------------------------------
		//
		strbin06: clc                   //assume no error for now
			lda (ptr01),y         //get numeral
			beq strbin17          //end of string
		//
			inc stridx            //point to next
			cmp #'a'              //check char range
			bcc strbin07          //not ASCII LC
		//
			cmp #'z'+1
			bcs strbin08          //not ASCII LC
		//
			and #a_maskuc         //do case conversion
		//
		strbin07: sec
		//
		strbin08: sbc #'0'              //change numeral to binary
			bcc strbin16          //numeral > 0
		//
			cmp #10
			bcc strbin09          //numeral is 0-9
		//
			sbc #a_hexnum         //do a hex adjust
		//
		strbin09: cmp valdnum           //check range
			bcs strbin17          //out of range
		//
			sta curntnum          //save processed numeral
			bit radxflag          //working in base 10?
			bpl strbin11          //no
		//
		//	-----------------------------------------------------------
		//	Prior to combining the most recent numeral with the partial
		//	result, it is necessary to left-shift the partial result
		//	result 1 digit.  The operation can be described as N*base,
		//	where N is the partial result & base is the number base.
		//	N*base with binary, octal & hex is a simple repetitive
		//	shift.  A simple shift won't do with decimal, necessitating
		//	an (N*8)+(N*2) operation.  PFAC is copied to SFAC to gener-
		//	ate the N*2 term.
		//	-----------------------------------------------------------
		//
			ldx #0
			ldy #s_fac            //accumulator size
			clc
		//
		strbin10: lda pfac,x            //N
			rol                   //N=N*2
			sta sfac,x
			inx
			dey
			bne strbin10
		//
			bcs strbin17          //overflow = error
		//
		strbin11: ldx bitsdig           //bits per digit
		//
		strbin12: asl pfac              //compute N*base for binary,...
			rol pfac+1            //octal &...
			rol pfac+2            //hex or...
			rol pfac+3            //N*8 for decimal
			bcs strbin17          //overflow
		//
			dex
			bne strbin12          //next shift
		//
			bit radxflag          //check base
			bpl strbin14          //not decimal
		//
		//	-------------------
		//	compute (N*8)+(N*2)
		//	-------------------
		//
			ldx #0                //accumulator index
			ldy #s_fac
		//
		strbin13: lda pfac,x            //N*8
			adc sfac,x            //N*2
			sta pfac,x            //now N*10
			inx
			dey
			bne strbin13
		//
			bcs strbin17          //overflow
		//
		//	-------------------------------------
		//	add current numeral to partial result
		//	-------------------------------------
		//
		strbin14: clc
			lda pfac              //N
			adc curntnum          //N=N+D
			sta pfac
			ldx #1
			ldy #s_fac-1
		//
		strbin15: lda pfac,x
			adc #0                //account for carry
			sta pfac,x
			inx
			dey
			bne strbin15
		//
			bcs strbin17          //overflow
		//
		//	----------------------
		//	ready for next numeral
		//	----------------------
		//
			ldy stridx            //string index
			bpl strbin06          //get another numeral
		//
		//	----------------------------------------------
		//	if string length > 127 fall through with error
		//	----------------------------------------------
		//
		strbin16: sec                   //flag an error
		//
		strbin17: rts                   //done
		//
		//================================================================================
		//
		//CONVERSION TABLES
		//
		basetab:  .byte 10,2,8,16       //number bases per radix
		bitstab:  .byte 3,1,3,4         //bits per digit per radix
		radxtab:  .text " %@$"          //valid radix symbols
		//
		//================================================================================
	}

}
