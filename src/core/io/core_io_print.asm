/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __io__ {

.namespace __print__ {

.segment core_virtual "core_io_print"

	.pseudopc CORE_ZERO_TEMP {
		io_print: p_core_io_print_in
	}

	__CoreHandler__(I_CORE_IO_PRINT, core_io_print)

.segment core_code "core_io_print"

	core_io_print:

	SaveParms(io_print, 4)

	__core_io_print__:

	__LogTrace__("core>io>print")

	ldy #$00

	!next:

	lda (io_print.string), y
	beq !+
	cmp io_print.delim
	beq !+

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__
	jsr $ffd2
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__ 

	iny
	lda io_print.length
	beq !next-
	cpy io_print.length
	beq !+
	jmp !next-

	!:

	rts
}
	
}
