
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.filenamespace __core__

.namespace __io__ {

#import "core_io_print.asm"
#import "core_io_nprint.asm"
#import "core_io_status.asm"
#import "core_io_test.asm"

}
