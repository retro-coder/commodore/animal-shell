/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __io__ {

.namespace __nprint__ {

	__CoreHandler__(I_CORE_RESERVED1, core_io_reserved1)

.segment core_code "core_io_reserved1"

core_io_reserved1:

	__LogTrace__("core>io>ioreserved1")
	inc $0402
	rts

	__CoreHandler__(I_CORE_RESERVED2, core_io_reserved2)

.segment core_code "core_io_reserved2"

core_io_reserved2:

	__LogTrace__("core>io>ioreserved2")

	jsr __core__.__bank__.__sys__.__core_bank_sys_in__

	inc $8000
	lda $8000
	sta $0403

	jsr __core__.__bank__.__sys__.__core_bank_sys_out__

	rts
}
	
}
