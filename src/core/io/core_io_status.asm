/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __io__ {

.namespace __nprint__ {

.segment core_virtual "core_io_status"

	.pseudopc CORE_ZERO_TEMP {
		io_status: p_type_byte 
	}

	__CoreHandler__(I_CORE_IO_STATUS, core_io_status)

.segment core_code "core_io_status"

	core_io_status:

	SaveParms(io_status, 1)

	__core_io_status__:

	__LogTrace__("core>io>status")

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__

	status:

	lda #$00
	jsr JSETNAM

	lda #$0f
	ldx io_status
	ldy #$0f
	jsr JSETLFS

	jsr JOPEN

	ldx #$0f
	
	jsr JCHKIN

	!:

	jsr JCHRIN
	jsr JBSOUT
	cmp #13
	bne !-

	close_all:

	lda #$0f
	jsr JCLOSE
	jsr JCLRCH

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	rts

	error:

	jmp close_all
}
	
}

