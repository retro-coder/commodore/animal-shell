/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __io__ {

.namespace __nprint__ {

.segment core_virtual "core_io_nprint"

	.pseudopc CORE_ZERO_TEMP {
		io_print: p_core_io_print_in
	}

	__CoreHandler__(I_CORE_IO_NPRINT, core_io_nprint)

.segment core_code "core_io_nprint"

	core_io_nprint:

	SaveParms(io_print, 4)

	__core_io_nprint__:

	__LogTrace__("core>io>nprint")

	jsr __core__.__io__.__print__.__core_io_print__

	// print carriage return

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__ 
	lda	#13
	jsr	JBSOUT
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	rts
}
	
}
