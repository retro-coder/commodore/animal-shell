# CORE
This library controls much of the low level interaction between the Shell and the Devices on your computer. In most cases, we do not try to recreate the wheel and use the Kernal API when it makes sense.

You can use any of the Functions in the Core by using its Call Interface. For more information on how to use the System Call Interface, consult the Programmers Guide.

These Functions and Macros are available in the `icore.asm` import file.

## IO

### print (alias=print)
#### Macros
- Core_I_CORE_IO_PRINT( text : word )
- Core_Print( text : word )
- Print( text : word )

Prints a line of text from the given address on the console. The text should not be longer than 254 characters with a zero ($00) delimiter. If the text is longer than 254 characters, it will be truncated. This function uses the kernal routine $ffd2 so it can print out any type of text including control characters. 

This function is used so often that we decided to create a couple other simpler aliases and macros to use it.
#### See Also
[printf](), [println]()
#### Data Structures
##### Input > core_io_print_ds
|**Parm**|**Description**|
|--------|---------------|
|.text   | the text to print with a zero delimeter (max = 254)|
##### Returns > None
#### Examples
##### Assembly
```asm
lda #<hello
sta $fa
lda #>hello
sta $fb
jsr Core.print
...
hello: .text @"hello world"
```
##### Macro
```asm
Core.Print(hello)
...
hello: .text @"hello world"
```
### printf
#### Macros
- Core_I_CORE_IO_PRINTf( text : word )
- Core_Printf( text : word )
- Printf( text : word )

Prints a line of text . . .
#### See Also
[print](), [println]()
#### Data Structure
[core_io_printf_ds](#core_io_printf_ds)
#### Examples
##### Assembly
```asm
lda #<hello_ds
sta $fa
lda #>hello_ds
sta $fb
jsr Core.printf
...
ds_hello {

        hello: .text @"hello %s, you are %n years old"
        parms: $02
        parm1: .text @"sp0cker2\$00"
        parm2: .byte $50, $00
}
```
##### Macro
```asm
Core.Printf(hello)
...
hello: .text @"hello world"
```
## LIST

## UTIL

### hex2ascii

## FILE

### load
Loads a file from a Device at the default location as specified on the first two bytes of the file.
#### Data Structures
##### input > ds_core_file_load_in [#](#ds_core_file_load_in 'ds_core_file_load_in')

|**Name**    |**Type**|**Description**|
|------------|--------|---------------|
|Filename    |.word   | the lo/hi address of the filename|
|Device      |.byte   | device number (8-16)|

##### return > Nothing

### loadat
Loads a file into a specific Address and Bank and ignores the first two bytes of the file.
#### Data Structures
##### input > ds_core_file_loadat_in [#](#ds_core_file_loadat_in 'ds_core_file_loadat_in')

|**Name**    |**Type**|**Description**|
|------------|--------|---------------|
|Filename    |.word   | the lo/hi address of the filename|
|Device      |.byte   | device number (8-16)|
|Address     |.word   | the lo/hi address of where to load the file|
|Bank        |.byet   | the bank number to use when loading the file|

##### return > Nothing
