
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.encoding "petscii_mixed"

#import "_core_seg.asm"
#import "_core_mac.asm"
#import "_core_var.asm"
#import "_core_sci.asm"
#import "_core_dat.asm"
#import "_core_par.asm" 

