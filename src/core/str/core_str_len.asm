
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __str__ {

.namespace __len__ {

.segment core_virtual "core_str_len"

	__CoreHandler__(I_CORE_STR_LEN, core_str_len)

.segment core_code "core_str_len"

	core_str_len:

	__core_str_len__:

	__LogTrace__("core>str>len")

	CopyPointer(SYS_CALL_PARMS, CORE_ZERO_FROM)
	
	ldx #0
	ldy #0

	!:
	lda (CORE_ZERO_FROM), y
	beq !+
	inx 
	iny 

	jmp !-

	!:

	// reset error flag
	
	clc

	// return length to caller

	txa	
	ldy #0
	sta (SYS_CALL_RETURN), y

	rts
}
	
}
