
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __str__ {

.namespace __findchar__ {

.segment core_virtual "core_str_findchar"

	.pseudopc CORE_ZERO_TEMP {
		p_findchar: p_core_str_findchar_in 
	}

	__CoreHandler__(I_CORE_STR_FINDCHAR, core_str_findchar)

.segment core_code "core_str_len"

	core_str_findchar:

	SaveParms(p_findchar, 3)
	
	__core_str_findchar__:

	__LogTrace__("core>str>find")

	ldx #0
	ldy #0

	!:
	lda (p_findchar.string), y
	beq not_found
        cmp p_findchar.search
	beq found
	inx 
	iny 

	jmp !-

	found:

	// return index and clear carry

	txa	
	ldy #0
	sta (SYS_CALL_RETURN), y
	clc

	rts

	not_found:

	// return null and set carry

	lda #0
	ldy #0
	sta (SYS_CALL_RETURN), y
	sec

	rts
}
	
}
