/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __str__ {

#import "core_str_concat.asm"
#import "core_str_len.asm"
#import "core_str_split.asm"
#import "core_str_findchar.asm"

}

