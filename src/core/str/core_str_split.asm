
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"
#import "core/core.asm"

.filenamespace __core__

.namespace __str__ {

.namespace __split__ {

.segment core_virtual "core_str_split"

	.pseudopc CORE_ZERO_TEMP {
	
		p_split_in:	p_core_str_split_in
		p_list_append:	p_core_list_append_in 

		// working address to string we are splitting
		t_string:	p_type_address 

		// working address to the next word/string
		t_next:		p_type_address 

		// are we inside a string with quotes?
		t_quote:	p_type_byte

		// last char
		t_last:		p_type_byte

	}

	__CoreHandler__(I_CORE_STR_SPLIT, core_str_split)

.segment core_code "core_str_split"

	core_str_split:

	SaveParms(p_split_in, 6)

	__core_str_split__:

	__LogTrace__("core>str>split")

	// store start of string

	lda p_split_in.string
	sta t_string
	lda p_split_in.string + 1
	sta t_string + 1	

	// save current address

	lda t_string
	sta t_next
	lda t_string + 1
	sta t_next + 1

	// next char in string

	ldx #0

	// get next char and store it for later

	!loop:

	jsr __core__.__bank__.__sys__.__core_bank_sys_in__

	ldy #0
	lda (t_string), y
	sta t_last

	// always check for end of string first

	!chk_zero:
	cmp #$00
	beq !end_of_string+

	// check for quote option

	lda p_split_in.options
	beq !+

	cmp #$01
	beq !chk_quote+

	!:
	jmp !chk_split+

	// is the next char a quote?

	!chk_quote:
	lda t_last

	// is the next char a quote?

	cmp #$22

	// no, check for split

	beq !+

	// are we in a quoted string?

	lda t_quote

	// yes, get next char

	bne !next_char+

	// no, check for split

	jmp !chk_split+

	// is this the first quote?

	!:
	lda t_quote

	// yes, start in quote mode

	beq !start_quote+

	// no, clear in quote mode

	lda #$00
	sta t_quote

	jmp !next_char+

	!start_quote:
	lda #$01
	sta t_quote
	jmp !next_char+

	// check for split delimeter

	!chk_split:
	lda t_last
	cmp p_split_in.delim
	beq !next_split+

	// increase address for next char

	!next_char:
	inc16 t_string
	inx

	// and loop

	jmp !loop-

	!next_split:
	jsr !add_item+
	ldx#$00

	// increase address for next char

	inc16 t_string

	// and store our next word address

	lda t_string
	sta t_next
	lda t_string + 1
	sta t_next + 1

	// and loop

	jmp !loop-

	!end_of_string:
	jsr !add_item+

	jsr __core__.__bank__.__sys__.__core_bank_sys_out__

	rts

	!add_item:

	lda p_split_in.list
	sta p_list_append.list
	lda p_split_in.list + 1
	sta p_list_append.list + 1
	lda t_next
	sta p_list_append.item
	lda t_next + 1
	sta p_list_append.item + 1
	txa
	sta p_list_append.length

	ICall1(I_CORE_CALL, I_CORE_LIST_APPEND, p_list_append)

	rts	
}

}
