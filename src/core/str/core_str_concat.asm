
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __str__ {

.namespace __concat__ {

.segment core_virtual "core_str_concat"

	.pseudopc CORE_ZERO_TEMP {
		p_concat: p_core_str_concat_in
	}

	__CoreHandler__(I_CORE_STR_CONCAT, core_str_concat)

.segment core_code "core_str_concat"

	core_str_concat:

	SaveParms(p_concat, 6)

	__core_str_concat__:

	__LogTrace__("core>str>concat")

	jsr __core__.__bank__.__sys__.__core_bank_sys_in__

	ldy #$00
	ldx #$00

	!:
	lda (p_concat.stringa), y
	beq !+
	sta ADDR_WORK, x
	inx
	iny
	jmp !-

	// should we add a space

	!:
	lda p_concat.options
	beq !+
	lda #$20	// space
	sta ADDR_WORK, x
	inx

	// copy string b to output

	!:
	ldy #$00

	!:
	lda (p_concat.stringb), y
	beq !+
	sta ADDR_WORK, x
	inx
	iny
	jmp !-

	// add end of string

	!:
	lda #$00
	sta ADDR_WORK, x

	// return work string to parm out

	ldy #$00
	lda #<ADDR_WORK
	sta p_concat.stringa
	lda #>ADDR_WORK
	sta p_concat.stringa + 1

	!:

	lda (p_concat.stringa), y
	beq !+
	sta (SYS_CALL_RETURN), y
	iny
	jmp !-

	!:
	sta (SYS_CALL_RETURN), y

	jsr __core__.__bank__.__sys__.__core_bank_sys_out__

	rts
}
	
}
