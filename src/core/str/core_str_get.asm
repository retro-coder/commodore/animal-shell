
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __str__ {

.namespace __get__ {

.segment core_virtual "core_str_get"

	__CoreHandler__(I_CORE_STR_GET, core_str_get)

.segment core_code "core_str_get"

	core_str_get:

	__LogTrace__("core>str>get")

	ldy #0

	!:

	lda (SYS_CALL_PARMS), y
	beq !+

	iny 
	
	sta p_len.string
	iny
	lda (SYS_CALL_PARMS), y
	sta p_len.string + 1

	ldx #$00
	ldy #$00

	!:
	lda (p_len.string), y
	beq !+
	inx 
	iny 

	jmp !-

	!:

	txa	
	ldy #$00
	sta (SYS_CALL_RETURN), y

	rts
}
	
}
