
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __file__ {

.namespace __open__ {

	__CoreHandler__(I_CORE_FILE_OPEN, core_file_open)

.segment core_virtual "core_file_open"

	.pseudopc CORE_ZERO_TEMP {
		file_open: p_core_file_open_in
		s_device: p_type_byte 
	}

	.pseudopc ADDR_IN {
		str_len: p_core_str_len_in
	}
	

.segment core_code "core_file_open"

	core_file_open:

	SaveParms(file_open, 4)

	// save device for later

	lda file_open.device
	sta s_device

	__core_file_open__:

	__LogTrace__("core>file>open")

	// show kernal messages?

	lda file_open.show 
	bne !+

	lda #$00
	sta $9d

	!:

	// try to open file

	jsr __do_filename__
	jsr __do_setnam__
	jsr __do_setlfs__
	jsr __do_open__

	// check for problems

	bcs error
	jsr __do_status__
	bcs error

	// no errors? close the file

	jsr __do__close__

	// and, finally open the file again

	jsr __do_setnam__
	jsr __do_setlfs__
	jsr __do_open__

	// reset error flag
	
	clc

	// return logical file number
	// to the caller

	ldy #0
	lda CORE_ZERO_NEXTFILE
	sta (SYS_CALL_RETURN),y

	// increase next file index

	inc CORE_ZERO_NEXTFILE
	bne !+

	// index wrap starts at 200

	lda #200
	sta CORE_ZERO_NEXTFILE

	!:

	rts

	error:
	jsr __do__close__
	sec
	rts

// ------------------------------------------------------------

	__do_filename__:

	// store filename in work space

	jsr __core__.__bank__.__sys__.__core_bank_sys_in__

	ldx #0
	ldy #0

	!:
	lda (file_open.filename), y
	sta ADDR_WORK, x

	beq !+

	inx
	iny

	cpx #16
	bne !-

	!:

	jsr __core__.__bank__.__sys__.__core_bank_sys_out__

	rts

// ------------------------------------------------------------

	__do_setnam__:

	// set BANK0 for C128

	#if C128

	lda #0
	ldx #0

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__
	jsr JSETBNK
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	#endif

	// reset return parms?

	ldy #0
	lda #0
	sta (SYS_CALL_RETURN),y
	iny
	sta (SYS_CALL_RETURN),y

	// get the filename length

	lda #<ADDR_WORK
	sta str_len.string
	lda #>ADDR_WORK
	sta str_len.string + 1

	SaveCallParms()
	ICall(I_CORE_CALL, I_CORE_STR_LEN)
	RestoreCallParms()

	lda ADDR_OUT

	ldx #<ADDR_WORK
	ldy #>ADDR_WORK

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__
	jsr JSETNAM
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	rts

// ------------------------------------------------------------

	__do_setlfs__:

	ldx s_device
	lda CORE_ZERO_NEXTFILE
	ldy #0

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__
	jsr JSETLFS
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	rts

// ------------------------------------------------------------

	__do_open__:

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__
	jsr JOPEN
	// jsr JREADSS
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	rts

// ------------------------------------------------------------

	__do__close__:

	lda CORE_ZERO_NEXTFILE

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__ 
	jsr JCLOSE
	jsr JCLRCH
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	rts

// ------------------------------------------------------------

	__do_status__:

	clc

	lda #15
	ldx s_device
	ldy #15

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__ 
	jsr $ffba
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	lda #0
	ldx #0
	ldy $0

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__ 
	jsr $ffbd
        jsr $ffc0
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

        ldx #15

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__ 
        jsr $ffc6
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	ldy #2

        loop:

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__ 
        jsr $ffcf
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	sta ($fd),y

#if LOG_DEBUG

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__ 
        jsr $ffd2
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

#endif
        cmp #13
        beq eof
	iny
        jmp loop

        eof:

	// check for 00 status

	ldy #2
	lda ($fd),y
	cmp #'0'
	bne !+
	iny 
	lda ($fd),y
	cmp #'0'
	bne !+
	lda #0
	jmp ret_close 

	!:

	// check for 62 file not found status

	ldy #2
	lda ($fd),y
	cmp #'6'
	bne !+
	iny 
	lda ($fd),y
	cmp #'2'
	bne !+
	lda #62
	jmp ret_close

	!:

	// check for 64 file type invalid

	ldy #2
	lda ($fd),y
	cmp #'6'
	bne !+
	iny 
	lda ($fd),y
	cmp #'4'
	bne !+
	lda #64
	jmp ret_close

	!:

	lda #0

        ret_close:

	ldy #1
	sta ($fd),y
	sta $ff

	// todo: checking to see if this
	// solves my problem of c64
	// header not being read because
	// of close bug in $ffc3

	lda #15

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__ 
        jsr $ffc3
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	// was there no error?

        clc
	lda $ff
	beq !+

	// otherwise set carry flag for the caller

	sec
	!:
        rts

}
	
}
