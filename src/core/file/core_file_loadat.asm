
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __file__ {

.namespace __loadat__ {

	__CoreHandler__(I_CORE_FILE_LOADAT, core_file_loadat)

.segment core_virtual "core_file_loadat"

	.pseudopc CORE_ZERO_TEMP {
		file_loadat: p_core_file_loadat_in
	}

	.pseudopc ADDR_IN {
		str_len: p_core_str_len_in
	}
	
		
.segment core_code "core_file_loadat"

	core_file_loadat:

	SaveParms(file_loadat, 6)

	__core_file_loadat__:

	__LogTrace__("core>file>loadat")

	lda file_loadat.show 
	bne !+

	lda #$00
	sta $9d

	// copy the filename to temp space
	//
	// this is necessary because system are
	// space is located under the kernal 
	// on c64 and cannot be used at the same
	// time that we are making kernal calls

	!:
	
	jsr __core__.__bank__.__sys__.__core_bank_sys_in__

	ldx #0
	ldy #0

	!:
	lda (file_loadat.filename), y
	sta ADDR_WORK, x

	beq !+

	inx
	iny

	cpx #16
	bne !-

	!:

	jsr __core__.__bank__.__sys__.__core_bank_sys_out__

	// we need to set filename bank location on c128

	#if C128

	lda #0
	ldx #0

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__
	jsr JSETBNK
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	#else

	// for c64 we need to move copy the filename to lo ram under roms

	#endif

	// get the filename length

	lda #<ADDR_WORK
	sta str_len.string
	lda #>ADDR_WORK
	sta str_len.string + 1

	ICall(I_CORE_CALL, I_CORE_STR_LEN)

	lda ADDR_OUT

	ldx #<ADDR_WORK
	ldy #>ADDR_WORK

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__
	jsr JSETNAM
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	// set logical file device

	ldx file_loadat.device

	// default secondary address to one
	// to load at the location of the 
	// file

	ldy #$01

	// if the caller has asked to load
	// the file to a different address
	// then we want to change the 
	// secondary address to allow
	// loading to specific location

	lda file_loadat.address
	bne !+
	lda file_loadat.address + 1
	beq !++

	!:
	ldy #$00

	!:
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__
	lda #$00
	jsr JSETLFS
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	lda #$00
	ldx file_loadat.address 
	ldy file_loadat.address + 1

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__
	jsr JLOAD
	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	bcs error

	rts

	error:

	__LogError__("LOADAT ERROR!")

	// store error code and set carry before returning

	lda #1
	sta $ff
	sec

	rts
}

}
