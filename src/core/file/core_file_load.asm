/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __file__ {

.namespace __load__ {

	__CoreHandler__(I_CORE_FILE_LOAD, core_file_load)

.segment core_virtual "core_file_load"

	.pseudopc CORE_ZERO_TEMP { 
		file_load: p_core_file_loadat_in
	}

.segment core_code "core_file_load"

	core_file_load:

	SaveParms(file_load, 4)

	__core_file_load__:

	__LogTrace__("core>file>load")

	lda #0
	sta file_load.address 
	sta file_load.address + 1

	/*
	the first few parms on the loadat call
	are identical so we can safely call
	it without using the sci
	*/

	jmp __core__.__file__.__loadat__.__core_file_loadat__
}

}