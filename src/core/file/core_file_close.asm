
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __file__ {

.namespace __close__ {

}

}