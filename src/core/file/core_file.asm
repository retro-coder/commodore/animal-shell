
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.filenamespace __core__

.namespace __file__ {

#import "core_file_getc.asm"
#import "core_file_load.asm"
#import "core_file_loadat.asm"
#import "core_file_open.asm"

}
