
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __file__ {

.namespace __getc__ {

	__CoreHandler__(I_CORE_FILE_GETC, core_file_getc)

.segment core_code "core_file_getc"

	core_file_getc:

	__core_file_getc__:

	__LogTrace__("core>file>getc")

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_in__

	// get logical file parameter

	ldy #0
	lda (SYS_CALL_PARMS),y
	tax 

	// set logical file for input when changed

	cpx CORE_ZERO_CURRFILE
	beq !+

	stx CORE_ZERO_CURRFILE
	jsr JCHKIN

	// get status

	!:

	jsr JREADSS	

	bne eof

	// read next buye

	jsr JCHRIN	

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	ldy #0
	sta (SYS_CALL_RETURN),y

	rts

	eof:

	jsr __core__.__bank__.__kernal__.__core_bank_kernal_out__

	and #$40
	beq error

	lda #$00
	sta (SYS_CALL_RETURN),y

	// set carry when eof is received
	
	sec

	rts

	error:

	__LogError__("GETC ERROR")

	rts 
}

}
