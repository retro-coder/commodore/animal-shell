
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
 
#import "_core.asm"

.filenamespace __core__

	__CoreInit__()

.segment core_call

	jmp _core_handler

.segment core_handler

_core_handler:

	// clear errors

	lda #$00
	sta $ff
	clc

	lda SYS_CALL

#import "core_init.asm"
#import "core_util.asm"

#import "bank/core_bank.asm"
#import "file/core_file.asm"
#import "io/core_io.asm"
#import "list/core_list.asm"
#import "mem/core_mem.asm"
#import "str/core_str.asm"

#if LOG_DEBUG

.segment core_virtual

	.pseudopc ADDR_IN {
		p_num2str: p_core_util_num2str 
	}

	.pseudopc ADDR_WORK {
		str: .fill 32, $20
	}

	.pseudopc ADDR_WORK {
		num: .fill 4, 0
	}

.segment core_handler


	__Message__("* UNHANDLED CORE CALL [")

	lda SYS_CALL
	sta num
	lda #0
	sta num + 1
	sta num + 2
	sta num + 3

	StorePointer(ADDR_WORK, p_num2str.addr)

	lda #'$'
	sta p_num2str.type
	lda #1
	sta p_num2str.length

	ICall2(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str, str)

	NPrintFromAddress(str)

	__NMessage__("] *")

#endif 

	rts 
	