
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

/*
        Core Catalog

        A catalog is a table of objects that
        are stored in memory and available 
        for usage in the Animal Shell.

        The Drivers, Apps and Commands are
        stored in Catalogs.

        Each type will have its own interface 
        for working with that catalog, while 
        this library contains a set of common
        interfaces that are used for managing
        them.

*/

#import "core.asm"

.filenamespace _core

.namespace _cat {

        .segment core_jump "cat"

                .fill (12), [$4c,$00,$00]

        .segment core_data "cat"

        .segment core_code "cat"

}