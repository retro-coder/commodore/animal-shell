
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce

#import "core/_core.asm"

.filenamespace __core__

.namespace __mem__ {

.namespace __init__ {

.segment core_data

	.pseudopc CORE_ZERO_TEMP {

                test: .byte 0,0,0

                last: p_type_byte

                size: p_type_pointer 

                done: p_type_flag

                magic:
                .text @"###"
	}

        s_hello:
        .text @"\$0d"
        .text @"Starting REU Core\$0d"
        .byte $00

        s_reu:
        .text @" REU Detected\$0d\$00"


	__CoreHandler__(I_CORE_MEM_INIT, core_mem_init)

.segment core_code_mem "core_mem_init"

	core_mem_init:

        __core_mem_init__:

	__LogTrace__("core>mem>init(reu)")

        PrintFromAddress(s_hello)

	jsr __core__.__bank__.__io__.__core_bank_io_in__

        ldx #255
        lda #0
        !:
        sta CORE_TABLE_XMEM_BANKS,x
        dex
        bne !-

        clc

        lda #'#'
        sta magic
        sta magic + 1
        sta magic + 2

        lda #0
        sta done
        ldx #8

        !:

        dex
        bne !+
        lda #1
        sta done

        !:

	// from address on computer
	
	lda #<magic
	sta $df02
	lda #>magic
	sta $df03

	// to address on reu

	lda #0
	sta $df04
	lda #0
	sta $df05

	// to bank on reu

	lda bank,x
        sta magic
	sta $df06

	// bytes to transfer

	lda #3
	sta $df07
	lda #0
	sta $df08

	// stash memory

#if C128
	lda #%10010000
	sta $df01
#else
	lda #%10010000
	sta $df01
#endif

        lda done
        beq !--

        ldx #0

        !:

	// to address on computer
	
	lda #<test
	sta $df02
	lda #>test
	sta $df03

	// from address on reu

	lda #0
	sta $df04
	lda #0
	sta $df05

	// from bank on reu

	lda bank,x
	sta $df06

	// bytes to transfer

	lda #3
	sta $df07
	lda #0
	sta $df08

        // fetch memory

#if C128
	lda #%10010001
	sta $df01
#else
	lda #%10010001
	sta $df01
#endif

	// begin check

        lda test
        cmp bank,x
        bne !+
        lda test + 1
        cmp #'#'
        bne !+
        lda test + 2
        cmp #'#'
        bne !+

        // clear last check

        lda #0
        sta test 
        sta test + 1
        sta test + 2

        // inx
        // bne !-
        // rts

        txa 
        sta last

        inx
        cpx #8
        beq !+

        jmp !-
	
        !:

        StorePointer(none, size)        

        lda last
        beq report

        ldx last
        lda lo,x
        sta size 
        lda hi,x
        sta size + 1

        report:

        lda bank,x
        tax
        lda #$ff

        !:
        sta CORE_TABLE_XMEM_BANKS,x
        dex
        bne !-

        lda #1 
        sta CORE_TABLE_XMEM_BANKS
        sta CORE_TABLE_XMEM_BANKS + 1

        PrintFromPointer(size)
        PrintFromAddress(s_reu)

       rts

        bank:
        .byte 1, 3, 7, 15, 31, 63, 127, 255
        
        lo:
        .byte <desc1, <desc2, <desc3, <desc4, <desc5, <desc6, <desc7, <desc8

        hi:
        .byte >desc1, >desc2, >desc3, >desc4, >desc5, >desc6, >desc7, >desc8

        desc1: .text @"128K\$00"
        desc2: .text @"256K\$00"
        desc3: .text @"512K\$00"
        desc4: .text @"1MB\$00"
        desc5: .text @"2MB\$00"
        desc6: .text @"4MB\$00"
        desc7: .text @"8MB\$00"
        desc8: .text @"16MB\$00"
        none: .text @"No\$00"

}

}