
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce

#import "core/_core.asm"

.filenamespace __core__

.namespace __mem__ {

.namespace __stash__ {

.segment core_data

.segment core_virtual "core_mem_stash"

	.pseudopc CORE_ZERO_TEMP {
		mem_stash: p_core_mem_stash_in 
		from: p_type_pointer 
		to: p_type_pointer
	}

	__CoreHandler__(I_CORE_MEM_STASH, core_mem_stash)

.segment core_code_mem "core_mem_stash"

	core_mem_stash:

	SaveParms(mem_stash, 5)

        __core_mem_stash__:

	__LogTrace__("core>mem>stash(georam)")

	jsr __core__.__bank__.__io__.__core_bank_io_in__

	// set bank 0 page 0

	lda mem_stash.mem_page
	sta $dffe
	lda mem_stash.mem_bank
	sta $dfff

	lda #0
	sta from
	lda mem_stash.addr_page
	sta from + 1

	lda #0
	sta to
	lda #$de
	sta to + 1

	ldx #0

	!:

	ldy #0

	!:

	lda (from),y
	sta (to),y

	iny
	bne !-

	inc from + 1
	inx
	stx $dffe 
	cpx mem_stash.mem_size
	bne !-- 

	clc
	
	// jsr __core__.__bank__.__io__.__core_bank_io_out__

	rts
}

}