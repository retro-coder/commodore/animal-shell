
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce

#import "core/_core.asm"

.filenamespace __core__

.namespace __mem__ {

.namespace __alloc__ {

.segment core_data

.segment core_virtual "core_io_alloc"

	.pseudopc ADDR_IN {
		mem_alloc: p_core_mem_alloc_in 
	}

	// .pseudopc ADDR_OUT {
	// 	mem_alloc_out: p_core_mem_alloc_out
	// }
	__CoreHandler__(I_CORE_MEM_ALLOC, core_mem_alloc)

.segment core_code_mem "core_mem_alloc"

	core_mem_alloc:

	SaveParms(mem_alloc, 1)

        __core_mem_alloc__:

	__LogTrace__("core>mem>alloc(georam)")

	// return bank number to caller
	// todo: this is fake right now

	lda #63
	ldy #0
	sta (SYS_CALL_RETURN),y

	tax
	ldy #0
	lda (SYS_CALL_PARMS),y
	tay

	clc

	!:

	dec CORE_TABLE_XMEM_BANKS,x
	dey
	bne !-

	// return page number to caller

	ldy #1
	lda CORE_TABLE_XMEM_BANKS,x
	sta (SYS_CALL_RETURN),y
        
	rts
}

}