
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __mem__ {

#if GEORAM

#import "core_mem_init_georam.asm"
#import "core_mem_alloc_georam.asm"
#import "core_mem_fetch_georam.asm"
#import "core_mem_stash_georam.asm"

#else

#import "core_mem_init_reu.asm"
#import "core_mem_alloc_reu.asm"
#import "core_mem_fetch_reu.asm"
#import "core_mem_stash_reu.asm"

#endif 
	
}
