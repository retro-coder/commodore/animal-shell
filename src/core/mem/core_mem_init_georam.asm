
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce

#import "core/_core.asm"

.filenamespace __core__

.namespace __mem__ {

.namespace __init__ {

.segment core_data

        s_hello:
        .text @"\$0d"
        .text @"Starting GeoRAM Core\$0d\$00"

        s_5mem:
        .text @"512K Detected\$00"

        s_1mem:
        .text @"1MB Detected\$00"

        s_2mem:
        .text @"2MB Detected\$00"

        s_4mem:
        .text @"4MB Detected\$00"

        s_none:
        .text @"No GeoRAM Detected\$00"

        // maxmem:
        // .byte $00,$00,$00,$00

        // mempgs:
        // .byte $00,$08,$10,$20,$40

	__CoreHandler__(I_CORE_MEM_INIT, core_mem_init)

/*
        Portions of code from

        https://c64os.com/post/georamdigi
*/

.segment core_code_mem "core_mem_init"

	core_mem_init:

        __core_mem_init__:

	__LogTrace__("core>mem>init(georam)")

        PrintFromAddress(s_hello)     
        
	jsr __core__.__bank__.__io__.__core_bank_io_in__

        ldx #255
        lda #0
        !:
        sta CORE_TABLE_XMEM_BANKS,x
        dex
        bne !-

        lda #0
        sta $dffe

        lda #%11111111
        sta $dfff
        lda #4
        sta $de00

        lda #%01111111
        sta $dfff
        lda #3
        sta $de00

        lda #%00111111
        sta $dfff
        lda #2
        sta $de00

        lda #%00011111
        sta $dfff
        lda #1
        sta $de00

        lda #%00000000
        sta $dfff

        lda #1
        sta $de00
        lda #2 
        sta $de01
        lda #3 
        sta $defe
        lda #4
        sta $deff

        lda $de00
        cmp #1
        bne nomem
        lda $de01
        cmp #2
        bne nomem 
        lda $defe
        cmp #3
        bne nomem 
        lda $deff
        cmp #4
        beq !+

        nomem:

        PrintFromAddress(s_none)
        rts

        !:

        lda #%11111111
        sta $dfff
        ldx $de00

        cpx #4
        bne !+
        jsr reserve
        PrintFromAddress(s_4mem)
        rts

        !:
        cpx #3
        bne !+
        jsr reserve
        PrintFromAddress(s_2mem)
        rts

        !:
        cpx #2
        bne !+
        jsr reserve
        PrintFromAddress(s_1mem)
        rts

        !:
        cpx #1
        bne !+
        jsr reserve
        PrintFromAddress(s_5mem)
        rts

        !:
        jmp nomem

        reserve:

        lda bank,x
        tax
        lda #63
        !:
        sta CORE_TABLE_XMEM_BANKS,x
        dex
        bne !-

        lda #1 
        sta CORE_TABLE_XMEM_BANKS
        sta CORE_TABLE_XMEM_BANKS + 1

        rts

        bank:
        .byte 0, 63, 127, 291, 255
}

}