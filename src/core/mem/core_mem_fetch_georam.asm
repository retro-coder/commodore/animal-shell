
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce

#import "core/_core.asm"

.filenamespace __core__

.namespace __mem__ {

.namespace __fetch__ {

.segment core_data

.segment core_virtual "core_mem_fetch"

	.pseudopc CORE_ZERO_TEMP {
		mem_fetch: p_core_mem_fetch_in 
		from: p_type_pointer 
		to: p_type_pointer
	}

	__CoreHandler__(I_CORE_MEM_FETCH, core_mem_fetch)

.segment core_code_mem "core_mem_fetch"

	core_mem_fetch:

	SaveParms(mem_fetch, 5)

        __core_mem_fetch__:

	__LogTrace__("core>mem>fetch(georam)")

	jsr __core__.__bank__.__io__.__core_bank_io_in__

	clc

	// switch to bank 1 page 1 for fun

	lda mem_fetch.mem_page
	sta $dffe
	lda mem_fetch.mem_bank
	sta $dfff


	lda #0
	sta from
	lda #$de
	sta from + 1

	lda #0
	sta to
	lda mem_fetch.addr_page
	sta to + 1

	// copy data from window to $5000

	ldx #0

	!:

	ldy #0

	!:

	lda (from),y
	sta (to),y

	iny
	bne !-

	inc to + 1
	inx
	stx $dffe
	cpx mem_fetch.mem_size
	bne !--

	clc

	// jsr __core__.__bank__.__io__.__core_bank_io_out__

	rts
}

}