
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce

#import "core/_core.asm"

.filenamespace __core__

.namespace __mem__ {

.namespace __fetch__ {

.segment core_data

.segment core_virtual "core_mem_fetch"

	.pseudopc ADDR_IN {
		mem_fetch: p_core_mem_fetch_in 
	}

	__CoreHandler__(I_CORE_MEM_FETCH, core_mem_fetch)

.segment core_code_mem "core_mem_fetch"

	core_mem_fetch:

	SaveParms(mem_fetch, 5)

        __core_mem_fetch__:

	__LogTrace__("core>mem>fetch(reu)")

	jsr __core__.__bank__.__io__.__core_bank_io_in__

	// to address on computer
	
	lda #0
	sta $df02
	lda mem_fetch.addr_page
	sta $df03

	// from address on reu

	lda #0
	sta $df04
	lda mem_fetch.mem_page
	sta $df05

	// from bank on reu

	lda mem_fetch.mem_bank
	sta $df06

	// bytes to transfer 12 pages = 3072

	lda #0
	sta $df07
	lda mem_fetch.mem_size
	sta $df08

	// begin operation

	clc
	
#if C128
	lda #%10010001
	sta $df01
#else
	lda #%10010001
	sta $df01
#endif

	// jsr __core__.__bank__.__io__.__core_bank_io_out__

	rts
}

}