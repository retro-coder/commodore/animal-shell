
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce

#import "core/_core.asm"

.filenamespace __core__

.namespace __mem__ {

.namespace __stash__ {

.segment core_data

.segment core_virtual "core_mem_stash"

	.pseudopc CORE_ZERO_TEMP {
		mem_stash: p_core_mem_stash_in 
	}

	__CoreHandler__(I_CORE_MEM_STASH, core_mem_stash)

.segment core_code_mem "core_mem_stash"

	core_mem_stash:

	SaveParms(mem_stash, 5)

        __core_mem_stash__:

	__LogTrace__("core>mem>stash(reu)")

	jsr __core__.__bank__.__io__.__core_bank_io_in__

	// from address on computer
	
	lda #0
	sta $df02
	lda mem_stash.addr_page
	sta $df03

	// to address on reu

	lda #0
	sta $df04
	lda mem_stash.mem_page
	sta $df05

	// to bank on reu

	lda mem_stash.mem_bank
	sta $df06

	// bytes to transfer

	lda #0
	sta $df07
	lda mem_stash.mem_size
	sta $df08

	// stash memory

	clc
	
#if C128
	lda #%10010000
	sta $df01
#else
	lda #%10010000
	sta $df01
#endif

	// jsr __core__.__bank__.__io__.__core_bank_io_out__

	rts
}

}