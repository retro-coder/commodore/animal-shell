
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)

*/

#importonce

.pseudocommand @ds_core_list_info {
	
	index:		p_type_byte
	count:		p_type_byte
	length:		p_type_byte
	size:		p_type_byte
	storage:	p_type_byte
	delim:		p_type_char
	lo:		p_type_pointer
	hi:		p_type_pointer 
	next:		p_type_pointer 
	data:		p_type_pointer 
}

.pseudocommand @ds_core_memory_list_item {

	// memory type
	type:		p_type_byte

	// list index
	index:		p_type_byte

	// start memory bank
	bank:		p_type_byte

	// start memory page
	page:		p_type_byte
}
