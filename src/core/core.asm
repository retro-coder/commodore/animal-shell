
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.
	
	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce


#import "system/sys.asm"
#import "_core_sci.asm"
#import "_core_dat.asm"
#import "_core_par.asm"


.filenamespace Core

// print text from a pointer in memory with no new line
.macro @PrintFromPointer (pointer) {
	PrintLimitedFromPointer(pointer, 0)
}

// print a limited text from a pointer in memory with no new line
.macro @PrintLimitedFromPointer (pointer, length) {

	lda pointer
	sta ADDR_IN
	lda pointer + 1
	sta ADDR_IN + 1
	lda #length
	sta ADDR_IN + 2
	sta ADDR_IN + 3

	ICall(I_CORE_CALL, I_CORE_IO_PRINT)
}

// print text from a pointer with a new line
.macro @NPrintFromPointer (pointer) {
	NPrintLimitedFromPointer(pointer,0)
}

// print a limited amount of text from a pointer with new line
.macro @NPrintLimitedFromPointer (pointer, length) {

	lda pointer
	sta ADDR_IN
	lda pointer + 1
	sta ADDR_IN + 1
	lda length
	sta ADDR_IN + 2
	lda #$00 
	sta ADDR_IN + 3

	ICall(I_CORE_CALL, I_CORE_IO_NPRINT)
}

// print text from address in memory with no linefeed
.macro @PrintFromAddress (address) {

	lda #<address
	sta ADDR_IN
	lda #>address
	sta ADDR_IN + 1
	lda #$00 
	sta ADDR_IN + 2
	sta ADDR_IN + 3

	ICall(I_CORE_CALL, I_CORE_IO_PRINT)
}


// prints a line of text from the given address followed by a line feed
.macro @NPrintFromAddress (address) {
	NPrintLimitedFromAddress(address, 0)
}

// prints a line of text from the given address followed by a line feed
.macro @NPrintLimitedFromAddress (address, length) {

	lda #<address
	sta ADDR_IN
	lda #>address
	sta ADDR_IN + 1
	lda #$00 
	sta ADDR_IN + 2
	sta ADDR_IN + 3

	ICall(I_CORE_CALL, I_CORE_IO_NPRINT)
}
