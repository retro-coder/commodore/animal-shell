/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __list__ {

.namespace __remove__ {
		
.segment core_virtual "core_list_remove"

	.pseudopc CORE_ZERO_LIST {
		
		t_info:	ds_core_list_info
		p_remove: p_core_list_remove_in
	}

	__CoreHandler__(I_CORE_LIST_REMOVE, core_list_remove)

.segment core_code "core_list_remove"

	core_list_remove:

	SaveParms(p_remove, 3)
	CopyWord(p_remove.list, SYS_CALL_PARMS)
	jsr __core__.__list__.__info__.__core_list_info__

	__core_list_remote__:

	__LogTrace__("core>list>remove")
	__LogTrace__("** WIP **")

	rts 
}

}