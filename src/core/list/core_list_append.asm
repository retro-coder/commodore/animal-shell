
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __list__ {

.namespace __append__ {
	
.segment core_virtual "core_list_append"

	.pseudopc CORE_ZERO_LIST {

		t_info:	ds_core_list_info
		p_append: p_core_list_append_in
		t_address: p_type_address
		t_next:	p_type_address  
		t_length: p_type_byte 
	}

	__CoreHandler__(I_CORE_LIST_APPEND, core_list_append)

.segment core_code "core_list_append"

	core_list_append:

	SaveParms(p_append, 5)

	__core_list_append__:

	__LogTrace__("core>list>append")

	jsr __core__.__bank__.__sys__.__core_bank_sys_in__

	ldx #14
	ldy #$00

	!:
	lda (p_append.list), y
	sta t_info, y

	iny
	dex

	bne !-

	lda t_info.length
	sta t_length

	lda p_append.length
	beq !+
	sta t_length

	// increate count and compare to max size

	!:
	inc t_info.count
	lda t_info.count
	cmp t_info.size

	// if at max size, return error

	beq __error__

	// update list index and count

	ldy #$00
	sta (p_append.list), y
	iny
	sta (p_append.list), y

	lda t_info.storage
	beq core_list_append_standard

	jmp core_list_append_variable

	core_list_append_standard:

	// get the lo byte

	lda t_info.count

	tay
	lda (t_info.lo), y
	sta t_address

	// get the hi byte

	lda (t_info.hi), y
	sta t_address + 1

	// store item into list data

	ldx #$00
	ldy #$00

	!:
	lda (p_append.item), y
	sta (t_address), y

	iny
	inx

	cpx t_length
	bne !-

	jmp __end__

	core_list_append_variable:

	// get next item address and store in lo/hi lookup table

	ldy t_info.count
	lda t_info.next
	sta t_next
	sta (t_info.lo), y
	lda t_info.next + 1
	sta t_next + 1
	sta (t_info.hi), y

	// store the data

	ldx #$00
	ldy #$00

	!:
	lda (p_append.item), y
	beq !++

	inc t_info.next
	bne !+
	inc t_info.next + 1

	!:
	sta (t_next), y

	beq !+

	iny
	inx

	cpx t_length

	bne !--

	!:
	inc t_info.next
	bne !+
	inc t_info.next + 1

	!:
	lda t_info.delim
	sta (t_next), y

	// update list with next data address

	ldy #10
	lda t_info.next
	sta (p_append.list), y
	iny
	lda t_info.next + 1
	sta (p_append.list), y

	jmp __end__

	__error__:

	sec

	// restore kernal

	__end__:

	jsr __core__.__bank__.__sys__.__core_bank_sys_out__

	rts
}

}
