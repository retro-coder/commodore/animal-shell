/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)


*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __list__ {

.namespace __vget__ {
		
.segment core_virtual "core_list_first"

	.pseudopc CORE_ZERO_LIST {

		t_info:	ds_core_list_info
		p_get: p_core_list_get_in
		p_ptr: p_type_pointer
	}

	__CoreHandler__(I_CORE_LIST_VGET, core_list_vget)

.segment core_code "core_list_vget"

	core_list_vget:

	SaveParms(p_get, 3)
	CopyWord(p_get.list, SYS_CALL_PARMS)
	jsr __core__.__list__.__info__.__core_list_info__

	__core_list_vget__:

	__LogTrace__("core>list>vget")

	// get the pointer to the list item

	jsr __core__.__list__.__get__.__core_list_get__
	bcs error
	jmp value

	error:
	rts

	value:

	jsr __core__.__bank__.__sys__.__core_bank_sys_in__

	ldy #0

	lda ADDR_OUT
	sta p_ptr
	iny
	lda ADDR_OUT + 1
	sta p_ptr + 1

	ldy #0

	!:

	lda (p_ptr),y
	beq !+

	sta (SYS_CALL_RETURN),y
	iny

	jmp !-

	!:

	lda #$00
	sta (SYS_CALL_RETURN),y

	jsr __core__.__bank__.__sys__.__core_bank_sys_out__

	rts

}

}