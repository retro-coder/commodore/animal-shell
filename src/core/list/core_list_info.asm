
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)


*/


#importonce
#import "core/_core.asm"
#import "core/mem/core_mem.asm"

.filenamespace __core__

.namespace __list__ {

.namespace __info__ {

.segment core_virtual "core_list_info"

	.pseudopc CORE_ZERO_LIST {

		t_info:	ds_core_list_info
		t_addr: p_type_word 
	}

	__CoreHandler__(I_CORE_LIST_INFO, core_list_info)

.segment core_code "core_list_info"

	core_list_info:

	CopyPointer(SYS_CALL_PARMS, t_addr)
	CopyWord(t_addr, SYS_CALL_PARMS)

	__core_list_info__:

	__LogTrace__("core>list>info")

	jsr __core__.__bank__.__sys__.__core_bank_sys_in__

	ldx #14
	ldy #00

	!:
	lda (SYS_CALL_PARMS), y
	sta (SYS_CALL_RETURN), y
	sta t_info, y

	iny
	dex

	bne !-

	jsr __core__.__bank__.__sys__.__core_bank_sys_out__

	rts
}

}
