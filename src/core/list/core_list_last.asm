/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)


*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __list__ {

.namespace __last__ {
		
.segment core_virtual "core_list_last"

	.pseudopc CORE_ZERO_LIST {
		
		t_info:	ds_core_list_info
		p_get: p_core_list_get_in
	}

	__CoreHandler__(I_CORE_LIST_LAST, core_list_last)

.segment core_code "core_list_last"

	core_list_last:

	SaveParms(p_get, 3)
	CopyWord(p_get.list, SYS_CALL_PARMS)
	jsr __core__.__list__.__info__.__core_list_info__

	__core_list_last__:

	__LogTrace__("core>list>last")


	lda t_info.count
	sta p_get.index

	jmp __core__.__list__.__get__.__core_list_get__
}

}