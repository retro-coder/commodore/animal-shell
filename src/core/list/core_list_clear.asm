
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"
#import "core/mem/core_mem.asm"

.filenamespace __core__

.namespace __list__ {

.namespace __clear__ {
		
.segment core_virtual "core_list_clear"

	.pseudopc CORE_ZERO_LIST {
		t_info: ds_core_list_info
		p_clear: p_core_list_clear_in
	}

	__CoreHandler__(I_CORE_LIST_CLEAR, core_list_clear)

.segment core_code "core_list_clear"

	core_list_clear:

	SaveParms(p_clear, 3)
	CopyWord(p_clear.list, SYS_CALL_PARMS)
	jsr __core__.__list__.__info__.__core_list_info__

	__core_list_clear__:

	__LogTrace__("core>list>clear")

	// reset index and count

	lda #$ff
	sta t_info.count
	sta t_info.index

	// reset next pointer to start of data

	lda t_info.data
	sta t_info.next
	lda t_info.data + 1
	sta t_info.next + 1

	// clear data?

	lda p_clear.options
	beq !+

	// todo: clear list data (length * size)

	// save list changes

	!:
	ldy #0
	lda t_info.count
	sta (p_clear.list), y
	ldy #1
	lda t_info.index
	sta (p_clear.list), y
	ldy #10
	lda t_info.next
	sta (p_clear.list), y
	ldy #11
	lda t_info.next + 1
	sta (p_clear.list), y

	rts
}

}
