
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.filenamespace __core__

.namespace __list__ {

#import "core_list_append.asm"
#import "core_list_create.asm"
#import "core_list_clear.asm"
#import "core_list_first.asm"
#import "core_list_get.asm"
#import "core_list_last.asm"
#import "core_list_info.asm"
#import "core_list_next.asm"
#import "core_list_remove.asm"
#import "core_list_vget.asm"

}