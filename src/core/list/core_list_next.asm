/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)


*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __list__ {

.namespace __next__ {
		
.segment core_virtual "core_list_next"

	.pseudopc CORE_ZERO_LIST {
		t_info:	ds_core_list_info
		p_get: p_core_list_get_in
	}

	__CoreHandler__(I_CORE_LIST_NEXT, core_list_next)

.segment core_code "core_list_next"

	core_list_next:

	SaveParms(p_get, 3)
	CopyWord(p_get.list, SYS_CALL_PARMS)
	jsr __core__.__list__.__info__.__core_list_info__

	__core_list_next__:

	__LogTrace__("core>list>next")

	clc

	// set index to next item

	inc t_info.index
	lda t_info.index
	cmp t_info.count
	bcc !+
	beq !+

	sec
	rts	

	!:

	clc
	lda t_info.index
	sta p_get.index

	jmp __core__.__list__.__get__.__core_list_get__
}

}