/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)


*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __list__ 
{

.namespace __get__
{
		
.segment core_virtual "core_list_get"

	.pseudopc CORE_ZERO_LIST {
		
		t_info:	ds_core_list_info
		p_get:	p_core_list_get_in 
	}

	__CoreHandler__(I_CORE_LIST_GET, core_list_get)

.segment core_code "core_list_get"

	core_list_get:

	SaveParms(p_get, 3)
	CopyWord(p_get.list, SYS_CALL_PARMS)
	jsr __core__.__list__.__info__.__core_list_info__

	__core_list_get__:

	__LogTrace__("core>list>get")

	clc

	jsr __core__.__bank__.__sys__.__core_bank_sys_in__

	// get index

	lda p_get.index

	// save index

	ldy #$00
	sta (p_get.list), y

	// set y to index

	tay

	// get lo byte location

	lda (t_info.lo),y

	// store in return parm lo

	sta ADDR_OUT

	// get hi byte location

	lda (t_info.hi),y

	// store in return parm lo

	sta ADDR_OUT + 1

	ldy #$00
	lda ADDR_OUT
	sta (SYS_CALL_RETURN),y
	iny 
	lda ADDR_OUT + 1
	sta (SYS_CALL_RETURN),y

	jsr __core__.__bank__.__sys__.__core_bank_sys_out__

	rts
}

}
