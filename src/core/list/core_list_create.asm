
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __list__ {

.namespace __create__ {

.segment core_virtual "core_list_create"

	.pseudopc CORE_ZERO_LIST {
		p_crt: p_core_list_create_in
		counter: p_type_word
		lo: p_type_word
		hi: p_type_word 
	}

	__CoreHandler__(I_CORE_LIST_CREATE, core_list_create)

.segment core_code "core_list_create"

	core_list_create:

	SaveParms(p_crt, 6)

	__core_list_create__:

	__LogTrace__("core>list>create")

	jsr __core__.__bank__.__sys__.__core_bank_sys_in__

	// initialize the list

	ldy #$00
	lda #$ff
	sta (p_crt.list),y
	iny
	sta (p_crt.list),y
	iny
	lda p_crt.length
	sta (p_crt.list),y
	iny
	lda p_crt.size
	sta (p_crt.list),y
	iny
	lda p_crt.storage
	sta (p_crt.list),y
	iny
	lda p_crt.delim
	sta (p_crt.list),y

	// store the start of the list in a counter

	lda p_crt.list
	sta counter + 0
	lda p_crt.list + 1
	sta counter + 1

	// move up 14 bytes to the start of the lo table

	ldx #14

	!:
	inc counter
	bne !+
	inc counter + 1

	!:
	dex
	bne !--

	// store current counter as lo address

	iny
	lda counter
	sta lo
	sta (p_crt.list),y
	iny
	lda counter + 1
	sta lo + 1
	sta (p_crt.list),y

	// move counter for size of the list

	ldx p_crt.size

	!:
	inc counter
	bne !+
	inc counter + 1

	!:
	dex
	bne !--

	// store current counter as hi address

	iny
	lda counter
	sta hi
	sta (p_crt.list),y
	iny
	lda counter + 1
	sta hi + 1
	sta (p_crt.list),y

	// move counter for size of the list

	ldx p_crt.size

	!:
	inc counter
	bne !+
	inc counter + 1

	!:
	dex
	bne !--

	lda p_crt.storage
	beq core_list_create_standard

	jmp core_list_create_variable

	core_list_create_standard:

	// now we are at the start of the actual data
	// we are going to spin thru each item and
	// update our lo/hi tables with each address

	ldy #$00
	ldx p_crt.size

	!:
	lda counter
	sta (lo),y
	lda counter + 1
	sta (hi),y

	// store current index

	txa 
	pha 

	// no loop thru the length of each item

	ldx p_crt.length

	// clear the table data

	!:
	tya
	pha 

	ldy #$00
	lda #$ee
	sta (counter), y

	pla
	tay

	inc counter
	bne !+
	inc counter + 1

	!:
	dex
	bne !--

	// restore the next item

	pla 
	tax 

	iny
	dex 
	bne !---

	jmp __end__

	core_list_create_variable:

	// store the current counter address as the
	// next item address we can add

	!:
	ldy #10
	lda counter
	sta (p_crt.list), y
	iny
	lda counter + 1
	sta (p_crt.list), y
	iny	
	lda counter
	sta (p_crt.list), y
	iny
	lda counter + 1
	sta (p_crt.list), y

	jmp __end__

	__end__:

	jsr __core__.__bank__.__sys__.__core_bank_sys_out__

	rts
}
	
}
