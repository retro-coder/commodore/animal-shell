
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "core/_core.asm"

.filenamespace __core__

.namespace __internal__ {

.namespace __stack__ {

.segment core_virtual "core_int_stack"

.pseudopc ADDR_IN {
p_list_create:
p_core_list_create_in 
}

.pseudopc ADDR_IN {
p_list_append:
p_core_list_append_in 
}

.pseudopc ADDR_IN {
p_address:
p_type_address 
}

.segment core_code "core_int_stack"

// create a list for the core stack

init:

StorePointer(CORE_LIST_STACK, p_list_create.list)

lda #32
sta p_list_create.length
lda #7
sta p_list_create.size
lda #0
sta p_list_create.storage
lda #0
sta p_list_create.delim

ICall(I_CORE_CALL, I_CORE_LIST_CREATE)

rts	

// push to the core zero stack

push:

StorePointer(CORE_LIST_STACK, p_list_append.list)

StorePointer(CORE_ZERO_TEMP, p_list_append.item)

ICall(I_CORE_CALL, I_CORE_LIST_APPEND)

rts

// pull from the core zero stack

pull:

StorePointer(CORE_LIST_STACK, p_address)
StorePointer(CORE_ZERO_TEMP, CORE_ZERO_TO)	

ICall(I_CORE_CALL, I_CORE_LIST_LAST)

lda ADDR_OUT
sta CORE_ZERO_FROM
lda ADDR_OUT + 1
sta CORE_ZERO_FROM + 1

// remove last item from the list

ldx #32
ldy #0

!:
lda (CORE_ZERO_FROM), y
sta CORE_ZERO_TEMP, y

iny
dex

bne !-

rts

// peek at the current zero stack

peek:

rts

	
}

}
