
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.filenamespace __core__

.namespace __internal__ {

#import "core_int_stack.asm"
	
}