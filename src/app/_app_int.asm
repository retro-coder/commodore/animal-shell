
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 

.segment sys_virtual

	.pseudopc ADDR_APP_SYS {
		__app_sys__: ds_app_system_space 
	}

// set the state for the next cycle
.macro @SetAppState (state) {

	lda #state 
	sta __app_sys__.next_state

}


