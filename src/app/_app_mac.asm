
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "system/sys.asm"
#import "_app_var.asm"
#import "_app_dat.asm"



.struct AppInfo {
 
	signature,
	platform,
	type,
	major,
	minor,
	name,
	description,
	copyright,
	author,
	website,
	email

}

.var a_info = AppInfo()

.eval a_info.name = "NONE"
.eval a_info.type = APP_TYPE_NONE
.eval a_info.platform = APP_PLATFORM_ANY
.eval a_info.description = "No Description"
.eval a_info.copyright = "No Copyright"
.eval a_info.major = 0
.eval a_info.minor = 0
.eval a_info.author = "No Author"
.eval a_info.website = "No Website"
.eval a_info.email = "No Email"

/*
	Application Information
*/
.macro __AppInfo__(info) {

	.if (info.name != null) { .eval a_info.name = info.name }
	.if (info.description != null) { .eval a_info.description = info.description }
	.if (info.type != null) { .eval a_info.type = info.type }
	.if (info.platform != null) { .eval a_info.platform = info.platform }
	.if (info.copyright != null) { .eval a_info.copyright = info.copyright }
	.if (info.major != null) { .eval a_info.major = info.major }
	.if (info.minor != null) { .eval a_info.minor = info.minor }
	.if (info.author != null) { .eval a_info.author = info.author }
	.if (info.website != null) { .eval a_info.website = info.website }
	.if (info.email != null) { .eval a_info.email = info.email }

}

/*
	Application Initializer

	Initialize the App and set all
	Signals to default to return
	to the Shell.

	The default handler for SIGRUN is
	to set SIGKILL to terminate the
	application.

*/
.macro __AppStart__() {

	.segment app_info

	i_signature:	.text APP_SIGNATURE
	i_type:		.byte 0
	i_platform:	.byte 0
	i_major:	.byte 1
	i_minor:	.byte 0
	i_name:		.byte <d_name, >d_name
	i_description:	.byte <d_description, >d_description
	i_copyright:	.byte <d_copyright, >d_copyright
	i_author:	.byte <d_author, >d_author
	i_website:	.byte <d_website, >d_website
	i_email:	.byte <d_email, >d_email

	.segment app_sys

	clear:		.fill 16, $ee

	.segment app_data

	d_name:		.text a_info.name + @"\$00"
	d_description:	.text a_info.description + @"\$00"
	d_copyright:	.text a_info.copyright + @"\$00"
	d_author:	.text a_info.author + @"\$00"
	d_website:	.text a_info.website + @"\$00"
	d_email:	.text a_info.email + @"\$00"

	.segment app_call

	jmp __app_handler__

	.segment app_handler

	__app_handler__:

	nop
	nop
	nop

	jsr __signal_handler__

	jsr __state_handler__

	rts

	.segment app_signal

	__signal_handler__:

	.segment app_state

	__state_handler__:

}

/*
	Application State Handler

	Use this Macro to couple a
	handler to a specific State
	of your Application.

	Your handler should always
	use a RTS when done so that
	it returns to the main
	Shell loop.
*/
.macro __AppStateHandler__(state, handler) {

	.segment app_virtual

	.pseudopc ADDR_APP_SYS {
		__app_sys__: ds_app_system_space
	}

	.segment app_state

	lda __app_sys__.state
	cmp #state
	bne !+
	jsr handler

	!:

}

/*
	Application Signal Handler

	Use this Macro to couple a 
	handler for a specific Signal
	that is sent to your Application.
	
	Your handler should always
	use a RTS when done so that
	it returns to the main
	Shell loop.
*/
.macro __AppSignalHandler__(signal, handler) {

	.segment app_signal

	lda SYS_CALL
	cmp #signal
	bne !+
	jsr handler

	!:
	
}

/*
	Application End

	Place this Macro at the End
	of your Application.
*/
.macro __AppEnd__() {

	.segment app_signal

	__LogFine__("** app signal not handled **")

	rts

	.segment app_state

	__LogFine__("** app state not handled **")

	rts

}
