
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "system/sys.asm"

.segment app [segments="app_call, app_info, app_sys, app_data, app_parms, app_handler, app_signal, app_state, app_code, app_virtual"]

.segmentdef app_call    [start=ADDR_APP]
.segmentdef app_info    [start=ADDR_APP_INFO]
.segmentdef app_sys	[start=ADDR_APP_SYS]
.segmentdef app_data    [start=ADDR_APP_DATA] 
.segmentdef app_parms	[startAfter="app_data"]
.segmentdef app_handler [startAfter="app_parms"]
.segmentdef app_signal 	[startAfter="app_handler"]
.segmentdef app_state 	[startAfter="app_signal"]
.segmentdef app_code    [startAfter="app_state"]
.segmentdef app_virtual	[virtual]
