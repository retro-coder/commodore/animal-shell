
#import "app/app.asm"

	__AppStart__()

.segment app_data 

	hello: .text @"\$0dhello world\$00"

.segment app_code

	run_state:

	NPrintFromAddress(hello)
	SetAppState(APP_STATE_STOP)
	rts 

	__AppStateHandler__(APP_STATE_RUN, run_state)

	__AppEnd__()

.file [name="hello.prg", segments="app"]
