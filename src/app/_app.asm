
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "_app_dat.asm"
#import "_app_var.asm"
#import "_app_seg.asm"
#import "_app_mac.asm"
#import "_app_int.asm"
