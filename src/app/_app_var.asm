
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

// application states

.const APP_STATE_NONE 		= $00
.const APP_STATE_INIT 		= $10	// called when the program is initializing
.const APP_STATE_START 		= $20	// called when the program starts
.const APP_STATE_STOP 		= $30	// called when the program stops
.const APP_STATE_RUN 		= $40	// called when the program is running

// application types

.const APP_TYPE_NONE		= $00

// application platforms

.const APP_PLATFORM_ANY		= $00
.const APP_PLATFORM_C128 	= $01
.const APP_PLATFORM_C64 	= $02

// application signature

.const APP_SIGNATURE 		= "##a0"

/*
	Signals

	Signals are modelled after Un*x signals
	in that they allow the Shell to provide
	a sort of software interrupt.

	Even though the Shell is not a true
	multi-tasking environment in the 
	sense of modern operating systems.

	It does allow for some programs to
	run in the background and does
	provide the ability to keep
	programs in memory. This is similar
	to how DOS and Windows3.1
	was able to provide many windows
	or apps open at the same time.

	We will not need all of them, instead
	we are going to cherry pick a few that
	we feel will be useful.

*/

/*
	Standard Signals
*/

.const SIGHUP 	= $0	// sent when the running program has closed
.const SIGINT 	= $1	// sent to the running program to interrupt (close)
.const SIGKILL	= $2	// sent to the running program to quit (close) immediately
.const SIGTERM	= $3	// sent to the running program to quit (close) gracefully
.const SIGUSR1	= $4	// a custom user signal for the running program
.const SIGUSR2	= $5	// a custom user signal for the running program
.const SIGSTOP	= $6	// sent to the calling program to stop for later resume
.const SIGCONT	= $7	// send to the calling program to continue from stop

/*
	Shell Signals
*/

.const SIGCIA	= $9	// sent to the running program that a CIA interrupt has occured
.const SIGVIC 	= $a	// sent to the running program that a VIC interrupt has occured
.const SIGNMI	= $b	// sent to the calling program that a NMI interrupt has occured
.const SIGRUN	= $e	// sent to the calling program during a normal run cycle
.const SIGSTART	= $f	// sent to the program to start running
