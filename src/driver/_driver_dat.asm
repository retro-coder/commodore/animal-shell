
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce
#import "system/sys.asm"

.pseudocommand @ds_driver_info {

	signature:	p_type_signature 
	platform:	p_type_byte
	type:		p_type_byte
	code:		p_type_string(4)
	connection:	p_type_byte
	major:		p_type_byte 
	minor:		p_type_byte 
	name:		p_type_pointer 
	description:	p_type_pointer 
	copyright:	p_type_pointer 
	author:		p_type_pointer 
	website:	p_type_pointer 
	email:		p_type_pointer
	reserved:	p_type_reserved(16)
}

.pseudocommand @ds_driver_get_in {

	reserved:	p_type_reserved(16)
}
