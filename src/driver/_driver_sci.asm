
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 
#import "system/sys.asm"


// driver call interface
.const I_DRV_CALL		= ADDR_DRV 

// driver interface calls
.const I_DRV_CALL_LOAD 		= $10
.const I_DRV_CALL_UNLOAD	= $15
.const I_DRV_CALL_INIT 		= $20
.const I_DRV_CALL_GET		= $30
.const I_DRV_CALL_READ		= $30
.const I_DRV_CALL_SET		= $40
.const I_DRV_CALL_WRITE		= $40

