
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "system/sys.asm"
#import "_driver_var.asm"
#import "_driver_dat.asm"

.struct DriverInfo {

	signature,
	platform,
	type,
	code,
	connect,
	major,
	minor,
	name,
	description,
	copyright,
	author,
	website,
	email
}

.var __drv_header__ = DriverInfo()

.eval __drv_header__.name = "NONE"
.eval __drv_header__.platform = DRIVER_PLATFORM_ANY
.eval __drv_header__.type = DRIVER_TYPE_NONE
.eval __drv_header__.connect = DRIVER_CONNECT_NONE
.eval __drv_header__.description = "No Description"
.eval __drv_header__.copyright = "No Copyright"
.eval __drv_header__.major = 0
.eval __drv_header__.minor = 0
.eval __drv_header__.author = "No Author"
.eval __drv_header__.website = "No Website"
.eval __drv_header__.email = "No Email"

/*
	Driver Information
*/
.macro __DriverInfo__(info) {

	.if (info.name != null) { .eval __drv_header__.name = info.name }
	.if (info.description != null) { .eval __drv_header__.description = info.description }
	.if (info.type != null) { .eval __drv_header__.type = info.type }
	.if (info.platform != null) { .eval __drv_header__.platform = info.platform }
	.if (info.copyright != null) { .eval __drv_header__.copyright = info.copyright }
	.if (info.major != null) { .eval __drv_header__.major = info.major }
	.if (info.minor != null) { .eval __drv_header__.minor = info.minor }
	.if (info.author != null) { .eval __drv_header__.author = info.author }
	.if (info.website != null) { .eval __drv_header__.website = info.website }
	.if (info.email != null) { .eval __drv_header__.email = info.email }
}

.macro __DriverStart__() {

.segment driver_info

	i_signature:	.text DRIVER_SIGNATURE
	i_platform:	.byte __drv_header__.platform
	i_type:		.byte __drv_header__.type
	i_connect:	.byte __drv_header__.connect
	i_major:	.byte __drv_header__.major
	i_minor:	.byte __drv_header__.minor
	i_name:		.byte <dr_name, >dr_name
	i_description:	.byte <dr_desc, >dr_desc
	i_copyright:	.byte <dr_copyright, >dr_copyright
	i_author:	.byte <dr_author, >dr_author
	i_website:	.byte <dr_website, >dr_website
	i_email:	.byte <dr_email, >dr_email

.segment driver_sys

	clear:          .fill 16, $ee

.segment driver_data

	dr_name:	.text __drv_header__.name + @"\$00"
	dr_desc:        .text __drv_header__.description + @"\$00"
	dr_copyright:   .text __drv_header__.copyright + @"\$00"
	dr_author:      .text __drv_header__.author + @"\$00"
	dr_website:     .text __drv_header__.website + @"\$00"
	dr_email:       .text __drv_header__.email + @"\$00"

.segment driver_call

	jmp __driver_handler__

.segment driver_handler

	nop
	nop
	nop

	__driver_handler__:

	lda SYS_CALL
}

.macro __DriverHandler__(call, handler) {

.segment driver_virtual

	cmp #call
	bne !+
	jmp handler

	!:
      
}

.macro __DriverEnd__() {

	rts
}
