
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#import "driver/driver.asm"

.filenamespace test

.segment driver_code "driver"

__DriverStart__()

__DriverEnd__()

* = $3000

.fill $400,$ee

.file [name="driver_test.prg", segments="driver"]
