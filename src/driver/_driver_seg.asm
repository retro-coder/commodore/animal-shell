
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#import "system/sys.asm"

.segment driver [segments="driver_call, driver_info, driver_sys, driver_data, driver_handler, driver_code, driver_parms, driver_virtual"]

.segmentdef driver_call		[start=ADDR_DRV]
.segmentdef driver_info		[start=ADDR_DRV_INFO]
.segmentdef driver_sys		[start=ADDR_DRV_SYS]
.segmentdef driver_data		[start=ADDR_DRV_DATA]
.segmentdef driver_handler	[startAfter="driver_data"]
.segmentdef driver_code		[startAfter="driver_handler"]
.segmentdef driver_parms	[start=ADDR_WORK, virtual]
.segmentdef driver_virtual	[virtual]

