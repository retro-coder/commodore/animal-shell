
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 
#import "system/sys.asm"


// driver signature
.const DRIVER_SIGNATURE 	= "##d0"

// driver attributes
.const DRIVER_ATTR_ID		= $01
.const DRIVER_ATTR_NAME		= $02
.const DRIVER_ATTR_TYPE		= $03
.const DRIVER_ATTR_PLATFORM	= $04
.const DRIVER_ATTR_DESC		= $05

/*
	DRIVER TYPES
*/
.const DRIVER_TYPE_NONE		= $00
// connected device is some kind of storage device
.const DRIVER_TYPE_STORAGE	= $10
// a modem used for communications (WIMODEM)
.const DRIVER_TYPE_MODEM	= $20
// a network device for communications (WIC64)
.const DRIVER_TYPE_NETWORK	= $30
// a cartridge (U2+L)
.const DRIVER_TYPE_CART		= $40
// a mouse usually connected to CPORT1
.const DRIVER_TYPE_MOUSE	= $50
// a joystick usually connected to CPORT2
.const DRIVER_TYPE_JOYSTICK	= $60
// real time clock
.const DRIVER_TYPE_RTC		= $70

/*
	CONNECTION TYPES
*/
.const DRIVER_CONNECT_NONE	= $00
// internal chip (CIA)
.const DRIVERT_CONNECT_INT	= $05
// expansion port (CARTRIDGE)
.const DRIVER_CONNECT_EPORT	= $10
// serial port (DRIVES)
.const DRIVER_CONNECT_SERIAL	= $20
// cassette port (CASSETTE)
.const DRIVER_CONNECT_CASS	= $30
// user port (WIC64)
.const DRIVER_CONNECT_UPORT	= $40
// control port 1 (MOUSE)
.const DRIVER_CONNECT_CPORT1	= $50
// control port 2 (JOYSTICK)
.const DRIVER_CONNECT_CPORT2	= $60

/*
	DRIVER PLATFORM
*/
.const DRIVER_PLATFORM_ANY	= $00
.const DRIVER_PLATFORM_128	= $01
.const DRIVER_PLATFORM_64	= $02
