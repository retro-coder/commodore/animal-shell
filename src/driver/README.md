# Drivers
A driver is a piece of code that resides between pages 28 and 36 (4K @ $1c00). This is needed in instances where a specialized function or command is used to communicate with the hardware in a non standard way. For example, there are many implementations of how to use sub-directories with commodore drives. By using a driver, we can provide one standard shell command, and use the driver to interpret that command and provide the correct syntax for that particular piece of hardware.
## Driver Types
There are different types of Hardware drivers. There are some for physical disk drives like the 1541, others for newer hardware like SD2IEC drives. Drivers for input devices like Joysticks or Mouse, and probably others that I cannot think of now.
## Driver Attributes
Each Driver provides a set of attributes that tell the Shell what it is for, and in turn how it can be used. For example, disk drive Drivers will have a type of $01 which means, well Disk Drive. The shell will use this information when it needs to find a specific type of driver to run a command or when an application needs to use a piece of hardware.
### Attributes
|**Attr**       |**Description**|
|---------------|---------------|
| ID            | the internal id assigned by shell at runtime |
| Type          | the Type of Driver (idriver.asm) |
| Code          | the internal hardware code (idriver.asm) |
| Platform      | what platform this Driver can be used on |
| Name          | the Name of the Driver |
#### ID
The ID is a unique internal identifier that the Shell will assign to the Driver when it is loaded. It will be a number between 0 and 24. 
## How A Driver Works
### Load Process
When the Shell or User tries to load a driver, the Shell will go through a number of Steps to determine if it should be loaded, where it should be stored, and then keeps a catalog of what Drivers are stored and ready to be used.
### Driver Memory
When a Driver is loaded, it is allocated a certain number of pages in memory depending on the size of the Driver. A Driver, by design, cannot be larger than 8 pages, or 2K of memory. This may change in the future if needed.
### Driver Catalog
The Catalog is an area of memory that contains important information for the Shell to get information about what Drivers are loaded. The data is stored in a table in Working Storage and is accessible by the developer using the Shell Driver API. There is a similar Catalog for Cached Commands and Active Apps in the Shell.
### Driver Catalog Storage
As metioned above, a catalog is created to store loaded Driver information to help the Shell when running. There is a maximum of 16 Drivers that can be loaded into memory at one time. Assuming that the each Driver is the full 8 pages (2k) means that we will need at whopping 32K for Drivers alone. That said, it is very unlikely that anyone would load more than around 4-6 drivers at one time. There is more information in the Shell documentation on how the Catalogs work, and how you use them in the Shell.

This catalog structure follows:

|**Attr**       |**Type**       |**Description**|
|---------------|---------------|---------------|
| id            | .byte         | the internal id assigned by shell at runtime |
| type          | .byte         | the type of driver |
| code          | .byte         | the driver code for this driver |
| bank          | .byte         | the bank where the driver is stored |
| page          | .byte         | the starting page of where the driver is stored |
| size          | .byte         | the size in pages, of the driver (max=8) |
