/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

.macro Call(interface, call) {
	
	// SaveRegisters()

	lda #call
	sta SYS_CALL

	jsr interface

	// RestoreRegisters()	
}

.macro SCall(interface, call, parm_in_addr, parm_out_addr) {

	// SaveRegisters()

	lda #<parm_in_addr
	sta $fb 
	lda #>parm_in_addr
	sta $fc 

	lda #<parm_out_addr
	sta $fd 
	lda #>parm_out_addr
	sta $fe 

	lda #call
	sta SYS_CALL

	jsr interface

	// RestoreRegisters()	

	#if TEST

	.print("")
	.print("Call")
	.print("-----")
	.print("interface       = $" + toHexString(interface))
	.print("call            = " + toHexString(call))
	.print("parm_in_addr    = $" + toHexString(parm_in_addr))
	.print("parm_out_addr   = $" + toHexString(parm_out_addr))
	.print("")

	#endif
}

.macro ICall(interface, call) {
	SCall(interface, call, ADDR_IN, ADDR_OUT)
}

.macro ICall1(interface, call, parm_in_addr) {
	SCall(interface, call, parm_in_addr, ADDR_OUT)
}

.macro ICall2(interface, call, parm_in_addr, parm_out_addr) {
	SCall(interface, call, parm_in_addr, parm_out_addr)
}

.macro SaveRegisters() {
	pha; txa; pha; tya; pha;
}

.macro RestoreRegisters() {
	pla; tay; pla; tax; pla
}

.macro SaveCallParms() {

	lda SYS_CALL
	pha	
	lda SYS_CALL_PARMS
	pha	
	lda SYS_CALL_PARMS + 1
	pha	
	lda SYS_CALL_RETURN
	pha	
	lda SYS_CALL_RETURN + 1
	pha	
	lda SYS_ERROR
	pha
}

.macro RestoreCallParms() {
	
	pla	
	sta SYS_ERROR
	pla	
	sta SYS_CALL_RETURN + 1
	pla	
	sta SYS_CALL_RETURN
	pla	
	sta SYS_CALL_PARMS + 1
	pla	
	sta SYS_CALL_PARMS
	pla	
	sta SYS_CALL
}
