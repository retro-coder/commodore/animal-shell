
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 

/*
	Stores the address to another memory location
	in big endian format
*/
.macro @StoreAddress(from, to) {

	lda #>from 
	sta to 
	lda #<from 
	sta to + 1

}

/*
	Stores the pointer to an address in memory
	in little endian format
*/
.macro @StorePointer(from, to) {

	lda #<from 
	sta to 
	lda #>from 
	sta to + 1

}

/*
	Store the word value to another
	memory address
*/
.macro @CopyWord(from, to) {
	lda from 
	sta to
	lda from + 1
	sta to + 1
}

.macro @CopyPointer(from, to) {
	ldy #0
	lda (from),y
	sta to
	iny
	lda (from),y
	sta to + 1
}

/*
	Store the byte value to another
	memory address
*/
.macro @CopyByte(from, to) {
	lda from
	sta to
}

/*
	Copy the data from a String
	Pointer to another Pointer
	in Memory.

	Parms:
		from	source memory address
		to	target memory address

	Uses:
		ADDR_ZERO_SHARED (1-4)
*/
.macro @CopyString(from, to) {
	StorePointer(from, ADDR_ZERO_SHARED)
	StorePointer(to, ADDR_ZERO_SHARED + 2)
	CopyStringFromPointers(ADDR_ZERO_SHARED, ADDR_ZERO_SHARED + 2)
	
}

/*

*/
.macro @CopyStringFromPointers(ptr_from, ptr_to) {
	CopyDataFromPointers(ptr_from, ptr_to, 0, 0)
	
}

/*

	Copy data from one memory
	location to another by length
	or using a delimeter.

	Usefull when needing to copy
	data out of ram under rom.

	Parms:
		from	source memory address
		to	target memory address
		length	how many bytes to copy (max 255)
		delim	what termination value to use

	Uses:
		ADDR_ZERO_SHARED (1-4)
*/
.macro @CopyData(from, to, length, delimeter) {

	StorePointer(from, ADDR_ZERO_SHARED)
	StorePointer(to, ADDR_ZERO_SHARED + 2)
	CopyDataFromPointers(ADDR_ZERO_SHARED, ADDR_ZERO_SHARED + 2, length, delimeter)
}

/*
	Copy data from one memory
	pointer location to another
	by length or using a
	delimiter.

*/
.macro @CopyDataFromPointers(ptr_from, ptr_to, length, delimeter) {

	ldy #0
	!:

	.if (delimeter == 0) {
		lda (ptr_from),y
		sta (ptr_to),y
		beq !+
		iny
		cpy #length
		bne !-
	} else {
		lda (ptr_from),y
		cmp #delimeter
		sta (ptr_to),y
		beq !+
		iny
		cpy #length
		bne !-
	}
	!:
	
}


.macro @StoreDirect(value, to) {
	lda #value 
	sta to
}


.macro @SaveParms(address, length) {

	ldx #length
	ldy #0

	!:

	lda (SYS_CALL_PARMS), y
	sta address, y
	iny

	dex
		
	bne !-

}


.function _16bitnextArgument(arg) {

	.if (arg.getType()==AT_IMMEDIATE) 
		.return CmdArgument(arg.getType(),>arg.getValue())

	.return CmdArgument(arg.getType(),arg.getValue()+1)
}

.pseudocommand @inc16 address {
	
	inc address
	bne !+
	inc _16bitnextArgument(address)
	!:
}

