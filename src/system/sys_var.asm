
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce


// location of environment variables
.label ADDR_ENV		= $0800

// location of runner code
.label ADDR_RUNNER	= $1000

// cart rom bank
.label ADDR_CART	= $8000

// shell entry point			 
.label ADDR_SHELL	= $8800

/*
 loader entry point
 (same as cartridge rom bank)
*/
.label ADDR_LOADER	= ADDR_CART

// core entry point
.label ADDR_CORE        = $a000

// core memory top
.label ADDR_CORE_MEM	= $b800

// core input top
.label ADDR_CORE_INPUT	= $bc00

// APPLICATION

// application entry point
.label ADDR_APP         = $4000
.label ADDR_APP_INFO	= ADDR_APP + $06
.label ADDR_APP_SYS	= ADDR_APP + $30
.label ADDR_APP_DATA	= ADDR_APP + $40


// COMMAND

// command entry point
.label ADDR_CMD     	= $1c00
.label ADDR_CMD_INFO	= ADDR_CMD + $06
.label ADDR_CMD_SYS	= ADDR_CMD + $30
.label ADDR_CMD_DATA	= ADDR_CMD + $40


// DRIVER

// driver entry point
.label ADDR_DRV		= $2800
.label ADDR_DRV_INFO	= ADDR_DRV + $06
.label ADDR_DRV_SYS	= ADDR_DRV + $30
.label ADDR_DRV_DATA	= ADDR_DRV + $40


// LIBRARY

// library entry point
.label ADDR_LIB		= $3400
.label ADDR_LIB_INFO	= ADDR_LIB + $06
.label ADDR_LIB_SYS	= ADDR_LIB + $30
.label ADDR_LIB_DATA	= ADDR_LIB + $40


// SHARED

// shared work area
.label ADDR_WORK	= $1400
// shared input parameters
.label ADDR_IN		= $1500
// shared output parameters
.label ADDR_OUT		= $1600
// shared buffer
.label ADDR_BUFFER	= $1700
// shared data
.label ADDR_DATA	= $1800


// one page of shared ram between all core functions
.label SYS_DATA		= ADDR_DATA

// start of zero page allocation
.label ADDR_ZERO	= $10
// start of zero page core data
.label ADDR_ZERO_CORE	= $20
// start of zero page shell data
.label ADDR_ZERO_SHELL	= $50
// start of zero page shared data
.label ADDR_ZERO_SHARED = $80
// end of zero page allocation
.label ADDR_ZERO_END	= $90

.label ADDR_APP_AREA	= $c000

#if C64
#if CART
// use LORAM
.label ADDR_SYS_AREA	= $8000
#else
// use HIRAM
.label ADDR_SYS_AREA	= $e000
#endif
#else
// C128
// use HIRAM
.label ADDR_SYS_AREA	= $e000
#endif

.label ADDR_SYS_SHARED	= ADDR_SYS_AREA
.label ADDR_SYS_CORE	= ADDR_SYS_AREA + (4 * $100)
.label ADDR_SYS_SHELL	= ADDR_SYS_AREA + (18 * $100)


#if C128
#if CART
#if CART_EXT
// no basic, keep io and kernal, external rom
.label BANK_CONFIG	= %00111010
#else
// no basic, keep io and kernal, internal rom
.label BANK_CONFIG	= %00110110
#endif
#else
// no basic, keep io and kernal, ram
.label BANK_CONFIG	= %00111110
#endif
#endif

// c128 bank location for disk
.label BANK_FILE	= 16

/*
	the interface to call
*/
.label SYS_CALL		= $fa
/*
	the address in lo/hi format that
	contains the data structure
	for the call parameters
*/
.label SYS_CALL_PARMS	= $fb 

/*
	the address in lo/hi format that
	contains the structure
	for the returned date
*/
.label SYS_CALL_RETURN	= $fd

/*
	if a call ends in error the
	calling function may use
	this space to provide an
	error code

	carry flag will also
	be set (sec)
*/
.label SYS_ERROR	= $ff 

/*
	an optional return value that
	can be used by the calling
	function to provide simple
	feedback to the caller
*/
.label SYS_RETURN 	= $f0

// kernal jump table entries

// common

.label JSETNAM 	= $ffbd
.label JSETLFS 	= $ffba 
.label JLOAD 	= $ffd5
.label JOPEN	= $ffc0
.label JCLOSE	= $ffc3
.label JCHKIN	= $ffc6
.label JCHRIN	= $ffcf
.label JREADSS	= $ffb7
.label JBSOUT	= $ffd2

// c128 specific

.label JSETBNK  	= $ff68
.label JBASIN	= $ffcf
.label JCLRCH	= $ffcc

// c64 specific

/*

	types

	these are not type safe but
	do provide a way to consistently create
	data structures that follow our standards

*/

// a word value that provides a physical address in memory like $e000
.pseudocommand @p_type_address {
	.word $0000		
}

// an 8 bit value (0-255)
.pseudocommand @p_type_byte {
	.byte $00
}

// a number
.pseudocommand @p_type_number {
	.fill 8, $00
}

// a character
.pseudocommand @p_type_char {
	.byte $00
}

// a small integer value representing a page boundary
.pseudocommand @p_type_page {
	.byte $00
}

// a zero or one value
.pseudocommand @p_type_flag {
	.byte $00
}

// a pointer to a location in memory in little endian format (lo/hi byte)
.pseudocommand @p_type_pointer {
	.word $00
}

// a 4 character string that identifies an object
.pseudocommand @p_type_signature {
	.fill 4,$00
}


// a 16 bit value (0-65535)
.pseudocommand @p_type_word {
	.word $00
}

// provides up to 8 optional flags for a interface call
.pseudocommand @p_type_bits {
	.byte $00
}

// provides a null terminated string
.macro @p_type_string(size) {
	.fill size,$ff
	.byte 0
}

// reserve a specific amount of memory
.macro @p_type_reserved(size) {
	.fill size,$ff
}


// reserve a number of characters that is not null terminated
.macro @p_type_chars(size) {
	.fill size,$ff
}