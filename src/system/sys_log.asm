
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "sys.asm"
#import "core/core.asm"

#if LOG_FINE 
#define LOG_TRACE 
#endif 

#if LOG_TRACE
#define LOG_DETAIL
#endif

#if LOG_DETAIL
#define LOG_DEBUG
#endif

#if LOG_DEBUG
#define LOG_ERROR
#endif

#if LOG_ERROR 
#define LOG_WARN
#endif

#if LOG_WARN
#define LOG 
#endif

#if LOG_NONE
#endif

.macro __LogDebug__(message) {

#if LOG_DEBUG
__Log__("debug ", message)
#endif

}

.macro __LogDetail__(message) {
#if LOG_DETAIL
__Log__("detail", message)
#endif
}

.macro __LogFine__(message) {
#if LOG_FINE
__Log__("fine  ", message)
#endif
}

.macro __LogTrace__(message) {
#if LOG_TRACE
__Log__("trace ", message)
#endif
}

.macro __LogWarn__(message) {
#if LOG_WARN
__Log__("warn  ", message)
#endif
}

.macro __LogError__(message) {
#if LOG_ERROR
__Log__("error ", message)
#endif
}

.macro __Log__(level, message) {

#if LOG
__print__(@"[" + level + @"] " + message + @"\$0d")
#endif

}

.macro __print__(message) {

jmp !+
__text__: 
.text @"" + message + @"\$00"

!:

lda #<__text__
sta ADDR_ZERO
lda #>__text__
sta ADDR_ZERO + 1

jsr __sys_log_print__

}

.macro __Message__(message) {
__print__(message)
}


.macro __NMessage__(message) {
__print__(message + @"\$0d")
}

.segment sys_code "sys_log"

__sys_log_print__: {

	ldy #0

	!:

	lda (ADDR_ZERO),y
	beq !+

	// kernal in
	sei
	pha

	#if C128
	lda $ff00
	and #%11001111 
	sta $ff00
	#else
	#if CART
	#else
	lda #%110
	sta $01
	#endif 
	#endif
	pla
	jsr $ffd2
	pha

	// kernal out

	#if C128
	lda $ff00
	ora #%00110000 
	sta $ff00
	#else
	#if CART
	#else
	lda #%000
	sta $01
	#endif
	#endif
	pla
	cli

	iny
	cpy #255
	bne !-

	!:

	rts

}