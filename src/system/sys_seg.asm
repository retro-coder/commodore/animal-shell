#importonce

/*
	Core Segments
*/

#import "system/sys.asm"

.segment sys [segments=" sys_zero_shared, sys_zero_core, sys_zero_shell, sys_area_shared, sys_area_core, sys_area_shell, sys_code"]
.segmentdef sys_zero_shared [virtual, start=ADDR_ZERO_SHARED]
.segmentdef sys_zero_core [virtual, start=ADDR_ZERO_CORE]
.segmentdef sys_zero_shell [virtual, start=ADDR_ZERO_SHELL]
.segmentdef sys_area_shared [virtual, start=ADDR_SYS_SHARED]
.segmentdef sys_area_core [virtual, start=ADDR_SYS_CORE]
.segmentdef sys_area_shell [virtual, start=ADDR_SYS_SHELL]
.segmentdef sys_code [start=$bc00]
.segmentdef sys_virtual[virtual]
