
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#import "build_128.asm"
#import "system/sys.asm" 
#import "shell/_build.asm"

#if CART
#else
.file [name="shell.128.prg", segments="shell"]
#endif