
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 

// TEST DRIVERS

// #import "tests/driver/base_driver.asm"
// #import "tests/driver/test_driver.asm"
// #import "drivers/cbm1541.asm"


// TEST COMMANDS

// #import "tests/commands/minimal.asm"
// #import "tests/commands/simple.asm"
// #import "tests/commands/cmd0.asm"
// #import "tests/commands/cmd1.asm"
// #import "tests/commands/cmd2.asm"

// #import "commands/command_run.asm"
// #import "commands/command_peek.asm"
// #import "commands/command_ver.asm" 
#import "command/command_dir.asm"
// #import "commands/command_convert.asm"


// ** ONLY UNCOMMENT ONE AT A TIME **


// TEST APPS

// #import "tests/test-app.asm"

// #import "tests/app/kick.asm"
// #import "tests/app/minimal.asm"
#import "tests/app/simple.asm"
// #import "tests/app/appinfo.asm"
// #import "tests/app/test_state.asm"
// #import "tests/app/test_driver_app.asm"
// #import "tests/app/test_signals.asm"
// #import "tests/app/test_heap.asm"
// #import "tests/app/app0.asm"
// #import "tests/app/app1.asm"
// #import "tests/app/app2.asm"
// #import "tests/app/app3.asm"
// #import "tests/app/app4.asm"
// #import "tests/app/new.asm"
// #import "tests/app/test_drivepoll.asm"


// CORE


// FILE

// #import "tests/core/file/tcf-open.asm"
// #import "tests/core/file/tcf-loadat.asm"
// #import "tests/core/file/tcf-getc.asm"
// #import "tests/core/file/tcf-getc2.asm"


// INTERNAL

// #import "tests/core/int/tci-push.asm"
// #import "tests/core/int/tci-pull.asm"


// IO

// #import "tests/core/io/tci-print.asm"


// LIST

// #import "tests/core/list/tcl-create.asm"
// #import "tests/core/list/tcl-create-multi.asm"
// #import "tests/core/list/tcl-create-variable.asm"
// #import "tests/core/list/tcl-append.asm"
// #import "tests/core/list/tcl-append-variable.asm"
// #import "tests/core/list/tcl-append-variable-large.asm"
// #import "tests/core/list/tcl-append-large.asm"
// #import "tests/core/list/tcl-get.asm"
// #import "tests/core/list/tcl-first.asm"
// #import "tests/core/list/tcl-last.asm"
// #import "tests/core/list/tcl-firstlast.asm"
// #import "tests/core/list/tcl-next.asm"
// #import "tests/core/list/tcl-iterate.asm"
// #import "tests/core/list/tcl-info.asm"
// #import "tests/core/list/tcl-clear.asm"
// #import "tests/core/list/tcl-remove.asm"
// #import "tests/core/list/tcl-example1.asm"
// #import "tests/core/list/tcl-example2.asm"


// STRING

// #import "tests/core/str/tcs-concat.asm"
// #import "tests/core/str/tcs-split.asm"
// #import "tests/core/str/tcs-split-e000.asm"


// SYSTEM

// #import "tests/system/ts-logging.asm"


// UTILS

// #import "tests/core/util/tcu-num2str.asm"
// #import "tests/core/util/tcu-str2num.asm"


.disk [filename="test.d64", name="ANIMALSHELL", id="2A" ]
{
        // [name="AUTO",		type="prg", segments="auto", noStartAddr=true, hide=true],

// #if CART

	// NO FILES WITH CARTRIDGE
	
// #else 

// #if C128
// [name="BOOT",		type="prg", prgFiles="bin/boot128.prg"],
// #else
// [name="BOOT",		type="prg", prgFiles="bin/boot64.prg"],
// #endif

[name="BOOT",		type="prg", segments="boot"],
[name="LOADER",		type="prg", segments="loader"],
[name="SHELL",		type="prg", segments="shell"],
[name="CORE",		type="prg", segments="core"],
	
// #endif 

// [name="DRIVER",		type="prg", segments="driver"],
[name="COMMAND",		type="prg", segments="command"],
[name="TESTAPP",		type="prg", segments="app"], 

// all programs past this line must be built manually

// [name="DRIVER0",		type="prg", prgFiles="bin/driver0.prg"],
// [name="DRIVER1",		type="prg", prgFiles="bin/driver1.prg"],
// [name="DRIVER2",		type="prg", prgFiles="bin/driver2.prg"],

// [name="CMD0",		type="prg", prgFiles="bin/cmd0.prg"],
// [name="CMD1",		type="prg", prgFiles="bin/cmd1.prg"],
// [name="CMD2",		type="prg", prgFiles="bin/cmd2.prg"],

// some simple commands

[name="DIR",			type="prg", prgFiles="bin/dir.prg"],
[name="PEEK",			type="prg", prgFiles="bin/peek.prg"],
[name="RUN",			type="prg", prgFiles="bin/run.prg"],
// [name="TYPE",			type="prg", prgFiles="bin/type.prg"],
[name="CONVERT",			type="prg", prgFiles="bin/convert.prg"],
[name="VER",			type="prg", prgFiles="bin/ver.prg"],
// [name="SID",			type="prg", prgFiles="bin/sid.prg"],
[name="SIG",			type="prg", prgFiles="bin/sig.prg"],
[name="APPINFO",		type="prg", prgFiles="bin/appinfo.prg"],

// simple test apps

[name="APP0",			type="prg", prgFiles="bin/app0.prg"],
[name="APP1",			type="prg", prgFiles="bin/app1.prg"],
[name="APP2",			type="prg", prgFiles="bin/app2.prg"],
[name="APP3",			type="prg", prgFiles="bin/app3.prg"],
[name="APP4",			type="prg", prgFiles="bin/app4.prg"],
// [name="BARE",			type="prg", prgFiles="bin/bare.prg"],
// [name="BASE",			type="prg", prgFiles="bin/base.prg"],
// [name="SIMPLE",			type="prg", prgFiles="bin/simple.prg"],
[name="HELLOWORLD",		type="prg", prgFiles="bin/helloworld.prg"],
[name="KICK",			type="prg", prgFiles="bin/kick.prg"],

// FILE TEST PROGRAMS

[name="TCF-OPEN",			type="prg", prgFiles="bin/tcf-open.prg"],
[name="TCF-2GETC",		type="prg", prgFiles="bin/tcf-2getc.prg"],
[name="TCF-GETC",			type="prg", prgFiles="bin/tcf-getc.prg"],
[name="TCF-LOADAT",		type="prg", prgFiles="bin/tcf-loadat.prg"],

// heap test programs

// [name="TCH-ALLOC",		type="prg", prgFiles="bin/tch-alloc.prg"],

// io test programs

[name="TCI-PRINT",		type="prg", prgFiles="bin/tci-print.prg"],

// list test programs

[name="TCL-CREATE",		type="prg", prgFiles="bin/tcl-create.prg"],
[name="TCL-CREATE-MULTI",	type="prg", prgFiles="bin/tcl-create-multi.prg"],
[name="TCL-GET",			type="prg", prgFiles="bin/tcl-get.prg"],
[name="TCL-FIRST",		type="prg", prgFiles="bin/tcl-first.prg"],
[name="TCL-LAST",			type="prg", prgFiles="bin/tcl-last.prg"],
[name="TCL-NEXT",			type="prg", prgFiles="bin/tcl-next.prg"],
[name="TCL-FIRSTLAST",		type="prg", prgFiles="bin/tcl-firstlast.prg"],
[name="TCL-APPEND",		type="prg", prgFiles="bin/tcl-append.prg"],
[name="TCL-LARGE",		type="prg", prgFiles="bin/tcl-large.prg"],
[name="TCL-APPEND-VAR",		type="prg", prgFiles="bin/tcl-append-variable.prg"],
[name="TCL-LARGE-VAR",		type="prg", prgFiles="bin/tcl-large-variable.prg"],
[name="TCL-ITERATE",		type="prg", prgFiles="bin/tcl-iterate.prg"],

// string test programs

[name="TCS-CONCAT",		type="prg", prgFiles="bin/tcs-concat.prg"],
[name="TCS-SPLIT",		type="prg", prgFiles="bin/tcs-split.prg"],

// util test programs

[name="TCU-NUM2STR",		type="prg", prgFiles="bin/tcu-num2str.prg"],
[name="TCU-STR2NUM",		type="prg", prgFiles="bin/tcu-str2num.prg"],

// fun stuff

[name="TEST1",  			type="seq", prgFiles="bin/test1-seq.prg"],
[name="TEST2",  			type="seq", prgFiles="bin/test2-seq.prg"],

// other test programs

[name="TEST-DRIVEPOLL",  	type="prg", prgFiles="bin/test-drivepoll.prg"],

// tmpx apps

// [name="TX-HELLO",          	type="prg", prgFiles="tests/tmpx/tx-hello.prg"],

// acme apps

// [name="TA-HELLO",          	type="prg", prgFiles="tests/acme/ta-hello.o"],

// temp programs for trying new things out

// [name="ZZZ-CLOCKR",          	type="prg", prgFiles="bin/zzz-clockr.prg"],
// [name="ZZZ-JIFFYR",          	type="prg", prgFiles="bin/zzz-jiffyr.prg"],

// some $2000 sid music

// [name="BUSTIC.SID",          	type="prg", prgFiles="sids/bustic.sid"],
// [name="MOON.SID",          	type="prg", prgFiles="sids/moon.sid"],
[name="ODE.SID",          	type="prg", prgFiles="sids/ode.sid"],
// [name="SPRING.SID",          	type="prg", prgFiles="sids/spring.sid"],

}
