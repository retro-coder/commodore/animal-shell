
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#import "build_cart_64.asm"

// Vice 3.8 Testing
// @kickass:emulator.runtime=C:\Games0\Retro\Commodore\Emulators\GTK3VICE-3.8-win64\bin\x64sc.exe

// Vice 3.7 Testing
// kickass:emulator.runtime=C:\Games0\Retro\Commodore\Emulators\GTK3VICE-3.7-win64\bin\x128.exe

// External Cartridge in 40 columns >= 3.7
// @kickass:emulator.options=-cart16 ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1542 -8 test.d64 -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 +drivesound
