
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 

// #define LOG_NONE
// #define LOG_WARN
 #define LOG_ERROR
// #define LOG_DEBUG
// #define LOG_DETAIL
// #define LOG_TRACE
// #define LOG_FINE

#import "build/build_disk_64.asm"

// Vice 3.8 Testing
// @kickass:emulator.runtime=C:\Games2\Retro\Commodore\Emulators\GTK3VICE-3.8-win64\bin\x64sc.exe

// Use Vice 3.8 (SuperCPU)
// kickass:emulator.runtime=C:\Games2\Retro\Commodore\Emulators\GTK3VICE-3.8-win64\bin\xscpu64.exe

// Vice 3.7 Testing
// kickass:emulator.runtime=C:\Games2\Retro\Commodore\Emulators\GTK3VICE-3.7-win64\bin\x64sc.exe

// Use Vice 3.6 Testing (SuperCPU)
// kickass:emulator.runtime=C:\Games2\Retro\Commodore\Emulators\GTK3VICE-3.6.1-win64\bin\xscpu64.exe

// Run From 1541 Disk
// @kickass:emulator.options=-autoload test.d64 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -keybuf run\x0d -warp
// kickass:emulator.options=-autoload test.d64 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -keybuf run\x0d -warp

// Run From 1541 Disk (using make)
// kickass:emulator.options=-autoload as64.d64 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -keybuf run\x0d -warp
// kickass:emulator.options=-autoload as64.d64 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -keybuf run\x0d -warp

// Run From 1571 Disk (using make)
// kickass:emulator.options=-autoload as64.d71 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -keybuf run\x0d -warp
// kickass:emulator.options=-autoload as64.d71 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -keybuf run\x0d -warp

// Run From 1581 Disk (using make)
// kickass:emulator.options=-autoload as64.d81 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -keybuf run\x0d -warp
// kickass:emulator.options=-autoload as64.d81 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -keybuf run\x0d -warp

// Run Debugger
// kickass:debugger.runtime=C:\Games0\Retro\Commodore\Tools\icebrolite_114.exe
// kickass:debugger.options=-connect -symbols=c:\Users\paul\Workspace\retro-coder\animal-shell\bin\build_disk_64.dbg -font=C:\Users\paul\AppData\Local\Microsoft\Windows\Fonts\Hack-Regular.ttf,16
