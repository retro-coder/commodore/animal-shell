
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 

// #define LOG_NONE
// #define LOG_WARN
#define LOG_ERROR
// #define LOG_DEBUG
// #define LOG_DETAIL
// #define LOG_TRACE
// #define LOG_FINE

#import "build_disk_128.asm"


// Vice 3.8 Testing
// @kickass:emulator.runtime=C:\Games0\Retro\Commodore\Emulators\GTK3VICE-3.8-win64\bin\x128.exe

// Vice 3.7 Testing
// kickass:emulator.runtime=C:\Games0\Retro\Commodore\Emulators\GTK3VICE-3.7-win64\bin\x128.exe

// 40 columns; vice >= 3.7; 1541
// kickass:emulator.options=-autoload test.d64 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 -keybuf run\x0d +warp
// kickass:emulator.options=-autoload test.d64 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 -keybuf run\x0d +warp

// 40 columns; vice >= 3.7; 1541 (using make)
// kickass:emulator.options=-autoload as128.d64 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 -keybuf run\x0d +warp
// kickass:emulator.options=-autoload as128.d64 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 -keybuf run\x0d +warp

// 40 columns; vice >= 3.7; 1571 (using make)
// kickass:emulator.options=-autoload as128.d71 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 -keybuf run\x0d +warp
// kickass:emulator.options=-autoload as128.d71 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 -keybuf run\x0d +warp

// 40 columns; vice >= 3.7; 1581
// kickass:emulator.options=-autoload as128.d81 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 -keybuf run\x0d +warp
// kickass:emulator.options=-autoload as128.d81 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 -keybuf run\x0d +warp

// 80 columns; vice >= 3.7; 1541
// kickass:emulator.options=-autoload test.d64 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0  -VDCfilter 0 -VDCglfilter 0 -80 -keybuf run\x0d +warp
// kickass:emulator.options=-autoload test.d64 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0  -VDCfilter 0 -VDCglfilter 0 -80 -keybuf run\x0d +warp

// 80 columns; vice >= 3.7; 1571 (using make)
// kickass:emulator.options=-autoload as128.d71 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0  -VDCfilter 0 -VDCglfilter 0 -80 -keybuf run\x0d +warp
// kickass:emulator.options=-autoload as128.d71 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0  -VDCfilter 0 -VDCglfilter 0 -80 -keybuf run\x0d +warp

// 80 columns; vice >= 3.7; 1581 (using make)
// kickass:emulator.options=-autoload as128.d81 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0  -VDCfilter 0 -VDCglfilter 0 -80 -keybuf run\x0d +warp
// kickass:emulator.options=-autoload as128.d81 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0  -VDCfilter 0 -VDCglfilter 0 -80 -keybuf run\x0d +warp

// 40 columns; vice >= 3.7; 1581
// @kickass:emulator.options=-autoload as128.d81 -moncommands ${kickassembler:viceSymbolsFilename} -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 -keybuf run\x0d +warp  -drive9type 1571 -drive10type 4844 -dosCMDHD c:\games0\retro\commodore\roms\doscmdhd.bin -10 c:\games0\retro\commodore\media\cmdhd\dev.dhd
// kickass:emulator.options=-cartfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1542 -8 as128.d64 -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 +drivesound -drive9type 1571 -drive10type 4844 -dosCMDHD c:\games0\retro\commodore\roms\doscmdhd.bin -10 c:\games0\retro\commodore\media\cmdhd\dev.dhd

