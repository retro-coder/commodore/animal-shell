
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

// provide georam support
#define GEORAM

#define CART

// turn this on for an external cartridge @ $8000 (cartridge) 
// the default is to build an internal rom for u36

#define CART_EXT

#importonce 
#import "build_128.asm"
#import "system/sys.asm" 

* = ADDR_CART

#import "build_loader_128.asm"
#import "build_core_128.asm"
#import "build_shell_128.asm"

.segment CART [outBin="cart128g.bin", segments="sys,loader,shell,core"]

// end the cartridge on the 8k boundary
* = $bfff
.byte 0
