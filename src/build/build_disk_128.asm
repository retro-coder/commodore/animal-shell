
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/



// declares a final build > will warn if using any unfriendly code
#define FINAL

// turn this on for a c128 build
#define C128

#import "system/sys.asm" 

// increase the build number
#import "build_update.asm"

#import "build_boot.asm"
#import "build_loader.asm"
#import "build_core.asm"
#import "build_shell.asm"
#import "build_disk.asm"















// #define C128		// turn this on for a c128 build

// .encoding "petscii_mixed"

// // #define TEST

// #define BREAKS


// // #define LOG_NONE
// // #define LOG_WARN
// #define LOG_ERROR
// // #define LOG_DEBUG
// // #define LOG_DETAIL
// // #define LOG_TRACE
// // #define LOG_FINE


// // #import "c128/auto.asm"
// #import "c128/boot.asm"
// #import "c128/loader.asm"
// #import "core/_build.asm"
// #import "shell/_build.asm"
// #import "build_disk.asm"
