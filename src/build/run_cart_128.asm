/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 

// #define LOG_NONE
#define LOG_WARN
// #define LOG_ERROR
// #define LOG_DEBUG
// #define LOG_DETAIL
// #define LOG_TRACE
// #define LOG_FINE

#import "build_cart_128.asm"

// ** RUN

// Vice 3.8 Testing
// @kickass:emulator.runtime=C:\Games0\Retro\Commodore\Emulators\GTK3VICE-3.8-win64\bin\x128.exe

// Vice 3.7 Testing
// kickass:emulator.runtime=C:\Games0\Retro\Commodore\Emulators\GTK3VICE-3.7-win64\bin\x128.exe


// vice >= 3.7; external cartridge; 40 cols; 1541 using build_disk

// @kickass:emulator.options=-cartfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1542 -8 test.d64 -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 +drivesound -drive10type 4844 -dosCMDHD c:\games0\retro\commodore\roms\doscmdhd.bin -10 c:\games0\retro\commodore\media\cmdhd\dev.dhd

// kickass:emulator.options=-cartfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1542 -8 test.d64 -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 +drivesound -drive10type 4844 -dosCMDHD c:\games0\retro\commodore\roms\doscmdhd.bin -10 c:\games0\retro\commodore\media\cmdhd\dev.dhd


// vice >= 3.7; external cartridge; 40 cols; 1541=as128.d64; 1571; cmdhd

// kickass:emulator.options=-cartfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1542 -8 as128.d64 -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 +drivesound -drive9type 1571 -drive10type 4844 -dosCMDHD c:\games0\retro\commodore\roms\doscmdhd.bin -10 c:\games0\retro\commodore\media\cmdhd\dev.dhd


// vice >= 3.7; external cartridge; 80 cols; 1541 using build_disk

// kickass:emulator.options=-cartfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1542 -8 test.d64 -reu +confirmonexit -binarymonitor -remotemonitor -VDCfilter 0 -VDCglfilter 0 -80 +drivesound +VDCshowstatusbar +VICIIshowstatusbar

// kickass:emulator.options=-cartfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1542 -8 test.d64 -reu +confirmonexit +binarymonitor +remotemonitor -VDCfilter 0 -VDCglfilter 0 -80 +drivesound


// vice >= 3.7; external cartridge; 40 cols; 1581 using make

// kickass:emulator.options=-cartfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1581 -8 as128.d81 -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 +drivesound


// vice >= 3.7; external cartridge; 80 cols; 1581 using make

// kickass:emulator.options=-cartfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1581 -8 as128.d81 -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -80 +drivesound


// vice >= 3.7; external cartridge; 40 cols; 1581; 1571; cmdhd

// kickass:emulator.options=-cartfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1581 -8 as128.d81 -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 +drivesound -drive9type 1571 -drive10type 4844 -dosCMDHD c:\games0\retro\commodore\roms\doscmdhd.bin -10 c:\games0\retro\commodore\media\cmdhd\dev.dhd

// ** DEBUG

// kickass:debugger.runtime=C:\Games0\Retro\Commodore\Tools\icebrolite_114.exe

// kickass:debugger.options=-connect -symbols=c:\Users\paul\Workspace\retro-coder\animal-shell\bin\build_cart_128.dbg -font=C:\Users\paul\AppData\Local\Microsoft\Windows\Fonts\Hack-Regular.ttf,16

