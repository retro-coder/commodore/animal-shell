
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#import "build_64.asm"

// turn this on if you are building a cartridge (default)
#define CART

// turn this on for an external cartridge @ $8000 (cartridge) 
#define CART_EXT

#import "system/sys.asm" 

* = ADDR_CART

#import "build_loader_64.asm"
#import "build_core_64.asm"
#import "build_shell_64.asm"

// output the cartridge binary
.segment CART [outBin="cart64.bin", segments="sys,loader,shell,core"]

// end the cartridge on the 8k boundary
* = $bfff
.byte 0
