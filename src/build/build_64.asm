
#importonce
#import "build_log.asm"

// declares a final build > will warn if using any unfriendly code
#define FINAL

// turn this on for a c64 build
#define C64
