
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 

// #define LOG_NONE
// #define LOG_WARN
#define LOG_ERROR
// #define LOG_DEBUG
// #define LOG_DETAIL
// #define LOG_TRACE
// #define LOG_FINE

#import "build_int_128.asm"

// ** RUN

// Vice 3.8 Testing
// @kickass:emulator.runtime=C:\Games0\Retro\Commodore\Emulators\GTK3VICE-3.8-win64\bin\x128.exe

// Vice 3.7 Testing
// kickass:emulator.runtime=C:\Games0\Retro\Commodore\Emulators\GTK3VICE-3.7-win64\bin\x128.exe

// vice >= 3.7; internal ram; 40 cols; 1541
// @kickass:emulator.options=-intfunc 1 -intfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1542 -8 test.d64 -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 +drivesound
// kickass:emulator.options=-intfunc 1 -intfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1542 -8 test.d64 -reu +confirmonexit +binarymonitor +remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 +drivesound

// vice >= 3.7; external cartridge; 80 cols; 1541
// kickass:emulator.options=-intfunc 1 -intfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1542 -8 test.d64 -reu +confirmonexit -binarymonitor -remotemonitor -VDCfilter 0 -VDCglfilter 0 -80 +drivesound
// kickass:emulator.options=-intfunc 1 -intfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1542 -8 test.d64 -reu +confirmonexit +binarymonitor +remotemonitor -VDCfilter 0 -VDCglfilter 0 -80 +drivesound

// vice >= 3.7; internal ram; 40 cols; 1581
// kickass:emulator.options=-intfunc 1 -intfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1581 -8 as128.d81 -reu +confirmonexit -binarymonitor -remotemonitor -VICIIfilter 0 -VICIIglfilter 0 -40 +drivesound

// vice >= 3.7; external cartridge; 80 cols; 1581
// kickass:emulator.options=-intfunc 1 -intfrom ${kickassembler:buildFilename} -moncommands ${kickassembler:viceSymbolsFilename} -drive8type 1581 -8 as128.d81 -reu +confirmonexit -binarymonitor -remotemonitor -VDCfilter 0 -VDCglfilter 0 -80 -80 +drivesound
