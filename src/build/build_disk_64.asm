
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce 

.encoding "petscii_mixed"

// #define TEST

#define C64


#define LOG_NONE
// #define LOG_WARN
// #define LOG_ERROR
// #define LOG_DEBUG
// #define LOG_DETAIL
// #define LOG_TRACE
// #define LOG_FINE


#import "build_boot.asm"
#import "c64/loader.asm"
#import "core/_build.asm"
#import "shell/_build.asm"
#import "build_disk.asm"

