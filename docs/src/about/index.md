## About Animal Shell
The purpose of the Animal Shell is simple. Provide an easier way to work with your Commodore 8-bit computer hardware, and make it simpler to write programs for them. 

## Organization of the Documentation

This documentation is organized into several sections:

[**About**](/about) contains this introduction as well as information about the Shell, its history, its licensing, authors and also Frequently Asked Questions.

[**Getting Started**](/gs) contains all the information for using the Shell. It starts with some Step by Step tutorials, and it is **the best place to start if you are a new!**

The [**Manual**](/manual) can be read or referenced when needed, in any order. It has feature specific information on how to use the Shell.

The [**Developers**](/dev) documentation provides all the information that you need if you want to create your own programs for the Shell. It provides a complete set of tutorials on writing you own Commands, Drivers and Applications.

Finally, the [**Reference**](/ref) section provides all of the available programming interfaces and structures that you can use when writing your own programs for Animal Shell.