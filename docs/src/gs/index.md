# Getting Started
This series will intriduce you to Animal Shell and give you an overview of its features.

In the following pages, you will get answers to questions like "What is Animal Shell?" or "What can I do with Animal Shell?". We will introduce some basic concepts, and provide some ways for you to use it effectively.
