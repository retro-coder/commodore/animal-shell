# Animal Shell Docs
Welcome to the official documentation of Animal Shell, the free and open source community-driven shell for the Commodore 64 and 128 8-bit computer systems. If you are new here, we recommend you read the [introduction page](about/introduction.md) to get an overview of what this documentation has to offer.

The table of contents should let you easily access the documentation for the topic of your topic of interest. You can also use the search function in the top-left corner.
## Get Involved
** Coming Soon **
