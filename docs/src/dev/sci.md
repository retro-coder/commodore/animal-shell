# System Call Interface
At the core of the Animal Shell is the SCI or System Call Interface. This is a very simple, but robust, way for developers to interact and build applications in the shell. 

The developer simply uses a `JSR ( Jump to SubRoutine )` call to one of the interface entry point addresses. Most, but not all, interface calls need a set of input and output parameters that can also be included. We will show how to do this later on in the documentation.

## Entry Points
There are currently five different interface entry points that can be used.

INTERFACE | ADDRESS | DESCRIPTION
--- | --- | ---
CORE | $a000 | provides a number of functions used by the shell, open to developers
SHELL | $8800 | provides a number of functions used by the shell, open to developers
COMMAND | $1c00 | used internally when running commands, NOT open to developers
DRIVER | $2800 | used internally when working with drivers, NOT open to developers
LIBRARY | $3400 | used internally when working with libraries, NOT open to developers
APP | $4000 | used to interact with running applications, open to developers

!!! note

    You probably noticed that the `DRIVER` and `LIBRARY` interfaces are not to be used by developers. Instead, the developer should use the `SHELL` calls have been created that will let the application developer work with these entry points. More examples of this will be provided further down in the documentation.

## Using The Call Interface
To make a call, load the zero page memory location $fa with the call signature, and then jump to the address of the system using the `jsr` opcode. For example, to get the current version of Animal Shell, you would do something similar to this.
```asm
    lda     #I_SHELL_VERSION    ; lda #$11
    sta     SYS_CALL            ; sta $fa
    jsr     I_SHELL_CALL        ; jsr $8800
```
The call number can be anything between 1 and 254. The calls for zero (0) and 255 are special, and are reserved for internal use in the Animal Shell. Each of the calls to the interfaces in Animal Shell are documented in the [Programmers Reference](/ref).

For convenience, a Macro for [Kick Assembler](http://www.theweb.dk/KickAssembler/Main.html#frontpage) programmers has been provided to make using the interface easier. For example, the same call above, can be made in one line using the ICall macro like this.
```asm
    ICall(I_SHELL_CALL, I_SHELL_VERSION)
```

## Parameters
When you call an interface, you can also include a set of input and output paramaters. Each of these parameters, must be contained within one page of memory each. You simply provide the address of the parameters in lo/hi (little endian) format, and the call will look for the parameters at that location. As an example, here we are 
```asm

```
### Input Parameters
this is a test

### Output Parameters
this is a test

/*
    sys_call.asm

    System Call Interface for all app, drivers and
    commands for the Animal Shell.

    How To Use The Interface

    The first three bytes of any object using a call interface
    must be

        jsr $addr

    where $addr is the address to the objects call interface
    handler.

    When calling using this interface, the programmer must
    provide the following

    interface

        The actual interface of the system you are calling. For example
        when using the driver interface you will use CALL_I_DRIVER_INT
        or $1c00. When using a command interface you will use
        CALL_I_COMMAND_INT or $2400, and so on.

    call

        The actual call you are trying to make on the interface. This is a
        value between 1 and 254. 0 and 255 are special values that
        are used by the Shell.

    in_addr

        Before calling the function, the caller must populate a
        data structure that contains the input parameters for the
        call. The incoming parameters may not be longer than
        one (1) page of memory (256 bytes).

    out_addr

        An optional location to return the results of the call. By
        default all returns will go to working storage starting
        at $1400. Using this option, you can return it to your
        applcation storage, or any other system storage that
        might be available including REU, RAM and so on.
    
    Example

    As an example, lets use the CALL interface to get
    the name of the current Driver in memory.

    To get the name we need to use the GET call and pass
    it the id of the attribute we want, in this case
    the name which is $01. The API id of the GET call
    is $30. All API call will have their parameters
    documented and provide samples of how to use them
    in code examples.

    Here is the sample code to create the parameters
    using Kick Assembler.

        get_name_parms:
	.byte $02

        get_name_return:
        .fill 30, $00

    The first part allocates two bytes with the
    parameter values we need.

    The second sets aside some storage for us
    to use when the name is returned.

    Next we call the API to get the name.

    We can use the available macro ICall.

        ICall(CALL_I_DRIVER_INT, call, get_name_parms, get_name_return)

    Or we can just straight code it using
    assembler.

        // call (get driver value)
        lda #$30
        sta $fa

        // load zp pointer for parms
        lda #<get_name_parms
        sta $fb
        lda #>get_name_parms
        sta $fc

        // load zp pointer for return
        lda #<get_name_return
        sta $fd
        lda #>get_name_return
        sta $fe

        jsr CALL_I_DRIVER_INT

        // clear bit is set when error
        bcs error               

        !:

        ldx #$00

        // read from return buffer
        lda get_name_return,x   
        bne !-

*/

