# Technical Information

## System Storage

## Application Space
The memory between `$4000` and `$7fff` is available for your applications. There is a guarantee from the Shell, that it will not use `any memory` in this space. As such, you are free to do anything you want in this area for your applications. Because the developer also has an additional area of Application Storage available as well, this space should be used for most of the logic in your application, and use the [applications storage space](#application-storage) for data.

!!! note

	On the C128, this area of memory is used for Basic LO. This means that you will not be able to make calls to any of the `BASIC ROM` functions. There are ways to do this with some memory banking system calls, which will be shown later in this documentation.

## Application Storage
The memory from `$c000` thru `$dfff` is for application developers, there is a guarantee from the Shell, that during normal processing, this memory will be available to your application. However, this also means that trying to making direct calls to the **KERNAL** or using any of the **I/O** space will not be available. See the section on [`Memory Banking`](#memory-banking) to see how you can still use this area of memory for your applications.

## System Storage
Eight Kilo Bytes (8K) of memory has been set aside as protected storage for the Shell. It is not recommended that developers use this space, and instead use the available `system calls` instead.

The memory is allocated differently depending on the system and method in which the shell is started. For example, on the C128, the system storage is always available under the Kernal ROM, but on the C64 it is under the Kernal ROM when running from DISK. But, when running from cartridge, the memory is under the LORAM of the cartridge under BASIC.

SYSTEM | TYPE      | LOCATION | ADDRESS
------ | --------- | -------- | -------
C128   | Cartridge | Kernal   | `$e000`
C128   | Disk      | Kernal   | `$e000`
C64    | Cartridge | LoRam    | `$8000`
C64    | Disk      | Kernal   | `$e000`


## Expanded Memory
The Shell has a very simple implementation of Expanded Memory that can be used with your RAM Expansion Unit (REU) cartridge or a GeoRAM cartridge. Several [System Calls]() are available to the developer for reserving and using expanded memory. The Shell itself uses expanded memory to increase the performance of the Shell and keep multiple drivers, libraries, commands and applications in memory.

[Allocate]

[Stash]

[Fetch]


## Memory Banking
Under most circumstances, the developer does not need to worry about banking memory. If the developer is following the normal Lifecycle of a Shell Application, then the Kernal will always be availble to them. However, if the developer needs the area of memory under the Kernal ROM, or they wish to access the IO or Color Ram registers directly, a set of system calls have been made available to assist with this.

### BASIC

[Bank In](/ref/core#i_core_mem_basic_bank_in)

[Bank Out](/ref/core#i_core_mem_basic_bank_out)

### KERNAL

[Bank In](/ref/core#i_core_bank_kernal_in)

[Bank Out](/ref/core#i_core_bank_kernal_out)

### SYSTEM STORAGE
The controls the availability of System Storage for the Shell. The Shell and Core make extensive use of this when any calls are made using the System Call Interface. Althought it is available to application developers, it is recommended that it not be used because improper use of the calls can leave the Shell in a broken state. Also, making changes to the System Storage will more than likely lead to an unstable and broken Shell. Use with **CAUTION**.

[Bank In System Storage](/ref/core#i_core_bank_sys_in)

[Bank Out System Storage](/ref/core#i_core_bank_sys_out)

### IO


## Lists
Lists are one of the fundamental structures of data used by the Animal Shell. With lists, you are able to store a collection of data for use in your applications.

### Structure
When a list is created using the [I_CORE_LIST_CREATE](/ref/core#i_core_list_create) call, it prepares the memory for the list by creating a structure containg a number of pointers and lookup tables.

NAME      	| SIZE 	| TYPE 	| DESCRIPTION
--       	| -- 	| --	| --
index	  	| 1	| byte  | current index from last system call
count	  	| 1	| byte  | count of items in the list
length	  	| 1	| byte  | length of each item in the list
size	  	| 1	| byte  | maximum number of items in the list (<= 255)
storage	  	| 1	| byte  | storage type to use for the list
delimeter	| 1    	| byte  | storage delimeter for each item in the list
lo		| 2 	| bytes | the pointer to the lo byte lookup table 
hi		| 2	| bytes | the pointer to the hi byte lookup table 
next		| 2 	| bytes | pointer for the next item in list
data		| 2	| bytes | pointer to the start of the list data
lo_table	| 1-n	| bytes | lo byte table has 1 byte for each item 
hi_table	| 1-n	| bytes | hi byte table has 1 byte for each item 
data_table	| 1-n	| bytes | contains the data for the table (length * size) bytes 

As a developer, you must ensure that you have enough memory for the list structure including the data, and also that it does not overlap any existing memory. A few examples are provided below to help you calculate how much memory you will need to reserve.

For larger lists of data, you can use a combination of the available Application Storage and the ability to swap in and out [Expanded Memory](#expanded-memory).

### Storage Types
When you are creating your list, you have the choice of two different storage types.

The standard type, which is the default, will prepare the memory and prebuild the lookup tables. This method is perfect for storing structures that have a consistent size.

The second type, is more flexible and can store data that is variable in size. This storage type is recommended when you are not sure the size that each list item will be when you are loading the table.

The second type is a better option when storing strings and will save memory, at the cost of taking slightly more time when storing the data. It also requires a delimeter for the data you want to store. The length of each item will still be honored by the list when appending data, except that it is now treated as a maximum size of an item. If the length of each item is not specified (zero) then there is no maximum and the list will continue to load data until the delimeter is found.

!!! example

	=== "Example 1"

		A standard list with 10 items which are one byte each.
		```
		12 + (10 * 1) + (10 * 1) + (10 * 1) = 42 bytes allocated (1 page)
		```

	=== "Example 2"

		A standard list with 20 items which are 16 bytes each.
		```
		12 + (20 * 1) + (20 * 1) + (20 * 16) = 372 bytes allocated (2 pages)
		```

	=== "Example 3"

		A variable list with 128 items which may be up to 32 bytes each.
		```
		12 + (128 * 1) + (128 * 1) + (128 * 32) = up to 4,364 bytes may be used (17 pages)
		```

``` mermaid
graph LR
  A[Start] --> B{Error?};
  B -->|Yes| C[Hmm...];
  C --> D[Debug];
  D --> B;
  B ---->|No| E[Yay!];
```