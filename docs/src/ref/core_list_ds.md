### ds_core_list
Returns important information about a list that was created using the [`I_CORE_LIST_CREATE`](core.md#i_core_list_create) call. The developer can use this information for whatever they need, however it is recommended that none of the values be changed or modified. Making any changes to the structure or data in the list might bring unexpected results when the system calls are used at a later time. Lists can be used by the developer to store collections of data. For more information on lists and how to use them, please see the [`LISTS`](/dev/technical#lists) topic in the [`TECHNICAL REFERENCE`](/dev/technical).


NAME | TYPE | DESCRIPTION |
-- | -- | -- |
index | byte | the current index from a system call
count | byte | total number of items in the list
length | byte | the number of items in the list
size | byte | the size of each item in the list
storage | byte | the type of storage method to use
delim | byte | delimeter used for each item
lo | pointer | location of the lo byte lookup table
hi | pointer | location of the hi byte lookup table
next | pointer | location for the next item
data | pointer | location to the start of the data table

Used By:
[`I_CORE_LIST_INFO`](core.md#i_core_list_info)
	