### p_core_mem_alloc_in
NAME    | TYPE  | DESCRIPTION 
--      | --    | --
size    | byte  | number of pages to allocate 

Used By:
[`I_CORE_MEM_ALLOC`](core.md#i_core_mem_alloc),
---

### p_core_mem_fetch_in
NAME      | TYPE | DESCRIPTION 
--        | --   | --
mem_bank  | byte | what bank in expanded memory
mem_page  | byte | what page in expanded memory
mem_size  | byte | how many pages to fetch in expanded memory
addr_page | byte | starting page in RAM on computer
addr_bank | byte | c128 bank to use on computer

Used By:
[`I_CORE_MEM_FETCH`](core.md#i_core_mem_fetch),
---

### p_core_mem_stash_in
NAME      | TYPE | DESCRIPTION 
--        | --   | --
mem_bank  | byte | what bank in expanded memory
mem_page  | byte | what page in expanded memory
mem_size  | byte | how many pages to fetch in expanded memory
addr_page | byte | starting page in RAM on computer
addr_bank | byte | c128 bank to use on computer

Used By:
[`I_CORE_MEM_STASH`](core.md#i_core_mem_stash),
---

### p_core_mem_swap_in
NAME    | TYPE  | DESCRIPTION 
--      | --    | --

Used By:
[`I_CORE_MEM_SWAP`](core.md#i_core_mem_swap),
---

### p_core_mem_dealloc_in
NAME    | TYPE  | DESCRIPTION 
--      | --    | --

Used By:
[`I_CORE_MEM_DEALLOC`](core.md#i_core_dealloc_swap),
---
