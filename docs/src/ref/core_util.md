## UTIL

### I_CORE_UTIL_NUM2STR
Converts a Number to a printable String.

| PARAMETER | DATA STRUCTURE |
| -- | -- |
| Input | [p_core_util_num2str_in](ds.md#p_core_util_num2str_in) |
| Output | address |

---

### I_CORE_UTIL_STR2NUM
Converts a String to a Number

| PARAMETER | DATA STRUCTURE |
| -- | -- |
| Input | [p_core_util_str2num_in](ds.md#p_core_util_str2num_in) |
| Output | [p_core_util_str2num_in](ds.md#p_core_util_str2num_out) |

=== "KickAss"

    ``` csharp
	in_parm: p_core_util_splitstr_in
	out_parm: p_core_util_splitstr_out

	string:
	.text @"the quick brown fox\$00"

	// list table pointer to store results in
	lda	#$00
	sta	in_parm.pointer

	// the character to split the string by (default is space 0x20)
	lda	#$20
	sta	in_parm.char

	// the lo/hi address of the string to split
	lda	#<string
	sta	in_parm.address
	lda	#>string
	sta	in_parm.address + 1

	ICall(I_CORE, I_CORE_STR_SPLIT, in_parm, out_parm)
    ```

=== "Generic"

    ``` c++
    #include <iostream>

    int main(void) {
      std::cout << "Hello world!" << std::endl;
      return 0;
    }
    ```

=== "KickAss2"
	<script src="https://gitlab.com/retro-coder/commodore/animal-shell/-/snippets/2567173.js"></script>

=== "Generic2"
	<script src="https://gitlab.com/-/snippets/2229060.js"></script>

---

### I_CORE_UTIL_STR2NUM
Converts a String to a Number

| PARAMETER | DATA STRUCTURE |
| -- | -- |
| Input | [p_core_util_str2num_in](ds.md#p_core_util_str2num_in) |
| Output | [p_core_util_str2num_in](ds.md#p_core_util_str2num_out) |

---
