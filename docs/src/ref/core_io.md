## IO
Provides a series of functions to help with low level input and output for the shell.

### I_CORE_IO_PRINT
Print text at the current cursor location.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_io_print_in`](parms.md#p_core_io_print_in)

---

### I_CORE_IO_NPRINT
Print text at the current cursor location, followed by a linefeed.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_io_print_in`](parms.md#p_core_io_print_in)

---
