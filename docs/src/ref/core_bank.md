## BANK
Provides a series of functions to help with the banking of ROM and RAM on the computer.

### I_CORE_BANK_IO_IN
Changes the memory configuration to make sure that IO addressing between $d000 and $dfff is available Shell.

---

### I_CORE_BANK_IO_OUT
Changes the memory configuration to make sure that RAM under IO memory is available to the Shell.

---

### I_CORE_BANK_KERNAL_IN
Changes the memory configuration to make sure that the Kernal is available to the Shell from address $e000 to $ffff.

---

### I_CORE_BANK_KERNAL_OUT
Changes the bank configuration to make sure that RAM under the Kernal is available to the Shell.

---

### I_CORE_BANK_SYS_IN
Changes the memory configuration to ensure that the [System Storage](/dev/technical#system) **is** available to the Shell. The location of this memory is not the same for all runtime versions of the Shell.

---

### I_CORE_BANK_SYS_OUT
Changes the memory configuration to ensure that the [System Storage](/dev/technical#system) **is not** available to the Shell. The location of this memory is not the same for all runtime versions of the Shell.

---
