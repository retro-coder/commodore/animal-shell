## FILE
Provides a series of Calls to help developers work with files.

### I_CORE_FILE_CLOSE
Close a file that is currently open.

| PARAMETER | DATA STRUCTURE
| -- | -- |
| IN | [`p_core_file_close_in`](parms.md#p_core_file_close_in) |

---

### I_CORE_FILE_GETC
Read a single byte from a file. The byte read is available in the accumulator and at the callers return address.

| PARAMETER | DATA STRUCTURE
| -- | -- |
| IN | [`p_core_file_getc_in`](parms.md#p_core_file_getc_in) |

---

### I_CORE_FILE_LOAD
Load a file at the address specified in the file.

| PARAMETER | DATA STRUCTURE
| -- | -- |
| IN | [`p_core_file_load_in`](parms.md#p_core_file_load_in) |

---

### I_CORE_FILE_LOADAT
Load a file at the address specified in the file.

| PARAMETER | DATA STRUCTURE
| -- | -- |
| IN | [`p_core_file_loadat_in`](parms.md#p_core_file_loadat_in) |

---

### I_CORE_FILE_OPEN
Open a file for reading or writing. The call returns the logical file number in the accumulator and the callers return address.

**Future State**

The call will return a pointer to a [file structure](ds.md#ds_core_file).

| PARAMETER | DATA STRUCTURE
| -- | -- |
| IN | [`p_core_file_open_in`](parms.md#p_core_file_open_in) |
| OUT | address

---
