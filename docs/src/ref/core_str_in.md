### p_core_str_len_in
NAME   | TYPE    | DESCRIPTION 
--     | --      | --
string | pointer | address of the string that you need the length from
delim  | char    | optional delimiter for the string (0 = default)

Used By:
[`I_CORE_STR_LEN`](core.md#i_core_str_len)
---

### p_core_str_split_in
NAME    | TYPE    | DESCRIPTION 
--      | --      | --
string  | pointer | pointer to the string to split 
list    | pointer | pointer to the list to populate
delim   | char    | the character to use for splitting (default is space 0x20)
options | byte    | optional parameters

OPTIONS | DESCRIPTION
--      | --
0       | none
1       | ignore text between quotes

Used By:
[`I_CORE_STR_SPLIT`](core.md#i_core_str_split)
---

### p_core_str_concat_in
NAME | TYPE | DESCRIPTION 
-- | -- | --
stringa | pointer | pointer to the first string to concatenate
stringb | pointer | pointer to the second string to concatenate
options | byte    | optional parameters

OPTIONS | DESCRIPTION
--      | --
0       | none
1       | add space between each string

Used By:
[`I_CORE_STR_CONCAT`](core.md#i_core_str_concat)
---
