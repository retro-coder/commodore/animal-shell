## STRING
A useful set of system calls that can be used by the programmer when working with string data.

### I_CORE_STR_LEN
Returns the length of a string.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_str_len_in`](parms.md#p_core_str_len_in)
OUT | [`p_core_str_len_out`](parms.md#p_core_str_len_out)

---

### I_CORE_STR_SPLIT
Populates a variable list of words in a String separated by a delimeter. The caller must make sure there is enough memory allocated for the returned list by following the steps for creating a list.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [p_core_str_split_in](parms.md#p_core_str_split_in)
OUT | [`p_core_str_split_out`](parms.md#p_core_str_split_out)

Example:

``` asm
in_parm: p_core_str_split_in
out_parm: p_core_str_split_out

string:
.text @"the quick brown fox\$00"

// list table pointer to store results in
lda	#$00
sta	in_parm.pointer

// the character to split the string by (default is space 0x20)
lda	#$20
sta	in_parm.char

// the lo/hi address of the string to split
lda	#<string
sta	in_parm.string
lda	#>string
sta	in_parm.string + 1

ICall(I_CORE, I_CORE_STR_SPLIT, in_parm)
```

---

### I_CORE_STR_CONCAT
Append two strings together with an optional space in between them.

Note:

When calling this function the resulting
string will be in working storage, or
you can specify where to store the string
in memory using the output paramater.

Note:

The strings should be defined like any other
string in Animal Shell with a zero
delimeter.

PARAMETER | DATA STRUCTURE
-- | -- 
IN  | [p_core_str_concat_in](parms.md#p_core_str_concat_in)
OUT | address

Example:

``` asm
in_parm: p_core_str_concat

// first string
hello:
.text @"hello\$00"

// second string
world:
.text @"world\$00"

// allocate 64 bytes for concatenated string
newstr:
.fill 64, $00

// store address of string a manually
lda	#<hello
sta	in_parm.a
lda	#>hello
sta	in_parm.a + 1

// store address of string b using system macro
StorePointer(world, in_parm.b)

// add a space between words
lda	#$ff
sta	in_parm.space

// concatenate the strings
ICall(I_CORE, I_CORE_STR_CONCAT, in_parm, newstr)

// print the result
Print(newstr)
```

---
