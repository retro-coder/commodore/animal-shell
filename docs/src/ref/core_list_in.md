### p_core_list_in
NAME | TYPE    | DESCRIPTION 
--   | --      | --
list | pointer | pointer to where the list is in memory

Used By:
[`I_CORE_LIST_INFO`](core.md#i_core_list_info), [`I_CORE_LIST_FIRST`](core.md#i_core_list_first), [`I_CORE_LIST_LAST`](core.md#i_core_list_last), [`I_CORE_LIST_NEXT`](core.md#i_core_list_next)

---

### p_core_list_append_in
NAME | TYPE    | DESCRIPTION 
--   | --      | --
list | pointer | pointer to where the list is in memory
data | byte    | also clear the data in the list

Used By:
[`I_CORE_LIST_APPEND`](core.md#i_core_list_append)

---

### p_core_list_create_in
NAME 	| TYPE 	  | DESCRIPTION 
-- 	| -- 	  | --
list   	| pointer | pointer to where the list is in memory
length  | byte 	  | length of each list item
size    | byte 	  | maximum number of items
storage | byte 	  | the storage method to use for the list
delim   | byte 	  | the delimeter to use for each item

STORAGE	| DESCRIPTION
-- 	| --
0 	| standard (default)
1 	| variable

!!! note

	The `delim` parameter is only used when the variable storage method for a list is used.

Used By:
[`I_CORE_LIST_CREATE`](core.md#i_core_list_create)

---

### p_core_list_get_in
NAME  | TYPE 	| DESCRIPTION 
--    | -- 	| --
list  | pointer | pointer to where the list is in memory
index | byte 	| index to the item in the list

Used By:
[`I_CORE_LIST_GET`](core.md#i_core_list_get)

---

### p_core_list_remove_in
NAME  | TYPE 	| DESCRIPTION 
--    | -- 	| --
list  | pointer | pointer to where the list is in memory
index | byte 	| index to the item you want to remove

Used By:
[`I_CORE_LIST_REMOVE`](core.md#i_core_list_remove)

---
