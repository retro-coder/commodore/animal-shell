### p_core_io_print_in
NAME   | TYPE    | DESCRIPTION 
--     | --      | --
string | pointer | address of the string to print
length | byte    | number of characters to print (default is all)
delim  | char    | optional delimiter character (default is $00)

Used By:
[`I_CORE_STR_PRINT`](core.md#i_core_str_print), [`I_CORE_STR_NPRINT`](core.md#i_core_str_nprint)
---
