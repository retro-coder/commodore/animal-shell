### p_core_str_len_out
NAME | TYPE | DESCRIPTION 
-- | -- | --
length | byte | the length of the string

Used By:
[`I_CORE_STR_LEN`](core.md#i_core_str_len)

---

### p_core_str_split_out
NAME | TYPE | DESCRIPTION 
-- | -- | --
count | byte | the number of items returned to the split list

Used By:
[`I_CORE_STR_LEN`](core.md#i_core_str_len)

---
