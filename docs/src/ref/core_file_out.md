### p_core_file_open_out
NAME    | TYPE  | DESCRIPTION 
--      | --    | --
file    | byte  | the file number assigned from the open call

---
