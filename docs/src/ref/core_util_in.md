### p_core_util_num2str_in
NAME   | TYPE    | DESCRIPTION
--     | --      | --
type   | byte    | type of conversion
addr   | pointer | address of where number is stored
length | byte    | 1-4 for 8/16/24/32 bit size number

TYPE  | DESCRIPTION
--    | --
blank | decimal (default)
$     | hex
%     | binary
@     | octal

Used by:
[I_CORE_UTIL_NUM2STR](core.md#i_core_util_num2str)
