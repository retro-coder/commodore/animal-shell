## MEM
Provides a series of calls to help with managing [Expanded Memory](/dev/technical/#expanded-memory). Using the expanded memory, a developer can increase the amount of storage for their application, which would otherwise be hindered by the available RAM on the computer.

### I_CORE_MEM_INIT
Initializes the low level structures for managing Expanded Memory. The developer should never need to use this call.

---

### I_CORE_MEM_ALLOC
Allocate pages of memory in Expanded Memory.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_mem_alloc_in`](parms.md#p_core_mem_alloc_in)
OUT | [`p_core_mem_alloc_out`](parms.md#p_core_mem_alloc_out)

---

### I_CORE_MEM_STASH
Stash RAM into Expanded Memory.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_mem_stash_in`](parms.md#p_core_mem_stash_in)

---

### I_CORE_MEM_FETCH
Fetch Expanded Memory to RAM.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_mem_fetch_in`](parms.md#p_core_mem_fetch_in)

---

### I_CORE_MEM_SWAP
Swap RAM with Expanded Memory.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_mem_swap_in`](parms.md#p_core_mem_swap_in)

---

### I_CORE_MEM_DEALLOC
Deallocate Expanded Ram.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_mem_dealloc_in`](parms.md#p_core_mem_dealloc_in)

---
