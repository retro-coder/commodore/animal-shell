### ds_core_file
NAME | TYPE | DESCRIPTION 
-- | -- | --
device | byte | the file device 
logical | byte | the logical file number 
filename | char(16) | the filename plus delimeter 
path | char(240) | total number of items in the list 

Used By:
[`I_CORE_FILE_OPEN`](core.md#i_core_file_open)

### ds_core_file_test
NAME        | TYPE      | DESCRIPTION 
--          | --        | --
device      | byte      | the file device 
logical     | byte      | the logical file number 
filename    | char(16)  | the filename plus delimeter 
path        | char(240) | total number of items in the list 
