### p_core_mem_alloc_out
NAME    | TYPE  | DESCRIPTION 
--      | --    | --
bank    | byte  | the bank where memory was allocated
page    | byte  | the starting page number in that bank


Used By:
[`I_CORE_MEM_ALLOC`](core.md#i_core_mem_alloc),

---
