# Call Parameters
There are two different types of parameters. Input parameters are supplied by the caller, and define the information needed by the interface to complete the request. Output parameters are returned with the information that the caller requested.

!!! note
	For more information about how to use the Call Interface, please see the [`SYSTEM CALL INTERFACE`](/dev/sci) documentation.

## INPUT

{!ref/core_file_in.md!}

{!ref/core_io_in.md!}

{!ref/core_list_in.md!}

{!ref/core_mem_in.md!}

{!ref/core_str_in.md!}

{!ref/core_util_in.md!}

## OUTPUT

{!ref/core_file_out.md!}

{!ref/core_io_out.md!}

{!ref/core_list_out.md!}

{!ref/core_mem_out.md!}

{!ref/core_str_out.md!}

{!ref/core_util_out.md!}

