### p_core_file_open_in
NAME     | TYPE    | DESCRIPTION 
--       | --      | --
filename | pointer | pointer to the zero delimited filename
device   | byte    | device number (8-30)
show     | flag    | show/hide the kernal messages

SHOW | DESCRIPTION
--   | --
0    | hide kernal messages (default)
1    | show kernal messages

Used By:
[`I_CORE_FILE_OPEN`](core.md#i_core_file_open)
---

### p_core_file_close_in
NAME    | TYPE  | DESCRIPTION 
--      | --    | --
file    | byte  | file number

Used By:
[`I_CORE_FILE_CLOSE`](core.md#i_core_file_close)
---

### p_core_file_getc_in
NAME    | TYPE  | DESCRIPTION 
--      | --    | --
file    | byte  | file number

Used By:
[`I_CORE_FILE_GETC`](core.md#i_core_file_getc)
---

### p_core_file_load_in
NAME            | TYPE          | DESCRIPTION 
--              | --            | --
filename        | pointer       | location the zero delimited filename
device          | byte          | device number (8-30)
show            | flag          | show/hide the kernal messages

Used By:
[`I_CORE_FILE_LOAD`](core.md#i_core_file_load)
---

### p_core_file_loadat_in
NAME            | TYPE          | DESCRIPTION 
--              | --            | --
filename        | pointer       | location of the zero delimited filename
device          | byte          | device number (8-30)
show            | flag          | show/hide the kernal messages
address         | address       | optional area of memory to load file into

Used By:
[`I_CORE_FILE_LOADAT`](core.md#i_core_file_loadat)
---
