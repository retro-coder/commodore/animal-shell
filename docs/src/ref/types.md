# Types
For the purpose of documenting the parameters, a series of different types has been identified. These are not type safe, but are here mainly to serve as a reference for the developer, for the documented expectation of the types of data being used in the call paramaters.

## <a name="p_type_address"></a>Address
`p_type_address`

A two byte (word) value that provides a physical address in memory. Unlike a pointer, this value will have the "low byte" and "high byte" in the same order as the memory address.

!!! Example

      Load the Address at `$e000` into a temp location
      ``` asm
      lda   $e0
      sta   TEMP
      lda   $00
      sta   TEMP+1
      ```


## <a name="p_type_byte"></a>Byte
`p_type_byte`

A single byte with a value between 0 and 255.

## <a name="p_type_char"></a>Char
`p_type_char`

A single byte with a value that is represented as a character from a string.

## <a name="p_type_flag"></a>Flag
`p_type_flag`

A single byte with a value of either 0 or 1. Usually used with the Bits type.

## <a name="p_type_page"></a>Page
`p_type_page`

A two byte value that contains a bank and page of memory to use.

## <a name="p_type_pointer"></a>Pointer
`p_type_pointer`

A pointer is a pair of bytes, that together form the start address in memory. They always conform to the convention that the first byte represents the least significant bit, or the "low byte", followed by the byte holding the most significant bits, or the "high byte". This is sometimes referred to as `little endian`.

!!! Example

      Load the Pointer of `$e000` into a temp location
      ``` asm
      lda   $00
      sta   TEMP
      lda   $e0
      sta   TEMP+1
      ```
## <a name="p_type_string"></a>String
`p_type_string`

A zero delimited set of characters that defines a string, which can be a maximum of one page of memory (256 bytes).

## <a name="p_type_word"></a>Word
`p_type_word`

A 16 bit value between 0-65535.

## <a name="p_type_bits"></a>Bits
`p_type_bits`

Used to provide optional flags when making interface calls.

## <a name="p_type_reserved"></a>Reserved
`p_type_reserved`

Reserves a number of bytes that might be used in future versions of the shell.

## <a name="p_type_chars"></a>Chars
`p_type_chars`

Represents a number of characters that are not null terminated. Usually used in a list or table.
