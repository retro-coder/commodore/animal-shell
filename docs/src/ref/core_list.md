## LIST
Lists can be used by the developer to store collections of data. For more information on lists and how to use them, please see the [`LISTS`](/dev/technical#lists) topic in the [`TECHNICAL REFERENCE`](/dev/technical).

### I_CORE_LIST_APPEND
Adds another items to the list.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_list_append_in`](parms.md#p_core_list_append_in)

---

### I_CORE_LIST_CLEAR
Clear the list.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_list_clear_in`](parms.md#p_core_list_clear_in)

---

### I_CORE_LIST_CREATE
Creates a list structure at the address of the caller.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_list_create_in`](parms.md#p_core_list_create_in)

---

### I_CORE_LIST_FIRST
Returns the address to the first item in the list.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_list_in`](parms.md#p_core_list_in)
OUT | [`p_core_list_item_out`](parms.md#p_core_list_item_out)

---

### I_CORE_LIST_GET
Returns the address to the item of the list from the index provided.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_list_get_in`](parms.md#p_core_list_get_in)
OUT | [`p_core_list_item_out`](parms.md#p_core_list_item_out)

---

### I_CORE_LIST_INFO
Returns information about a list.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_list_info_in`](parms.md#p_core_list_info_in)
OUT | [`ds_core_list`](ds.md#ds_core_list) |

---

### I_CORE_LIST_LAST
Returns the address to the last item in the list.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_list_in`](parms.md#p_core_list_in)
OUT | [`p_core_list_item_out`](parms.md#p_core_list_item_out)

---

### I_CORE_LIST_NEXT
Returns the address to the next item in the list.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_list_in`](parms.md#p_core_list_in)
OUT | [`p_core_list_item_out`](parms.md#p_core_list_item_out)

---

### I_CORE_LIST_REMOVE
Removes the item from the list.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_list_remove_in`](parms.md#p_core_list_remove_in)

---

### I_CORE_LIST_VGET
Returns the value of the item in a list from the index provided.

PARAMETER | DATA STRUCTURE
-- | -- 
IN | [`p_core_list_get_in`](parms.md#p_core_list_get_in)
OUT | [`p_type_string`](types.md#p_type_string)

---

