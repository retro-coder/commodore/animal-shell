
# Technical Specifications

**Note**
The contents of this document and meant to be used as a place to store ideas and possibly pseudo-code to implement. It is not meant to be kept current or accurate. The Documentation of Animal Shell will always have the most current information.

**Note**
The contents of this document will use the acronym AS as reference to Animal Shell.

## Ram Banks
Any reference to a *Ram Bank* will mean a 64K block of memory. The C64 has one *Ram Bank* of memory, while the C128 contains two *Ram Banks* of memory. We will label these as Banks A and B respectively.

External memory can be added that can contain anywhere from 128K to 16MB of memory in *Banks* of 64K. We will refer to these as Banks 0 thru n repectively.

Putting the C128 MMU aside for a moment, the CPU can only use one bank of memory when it is operating. The C128 MMU provides a way to utilize its second *bank* of memory. That said, we should assume that any application built for AS will run in Bank A of memory.

## Memory Blocks
AS is split into four separate 16k areas of memory called *Blocks*.

|Bank	|Address	|Description
|---	|---		|---
|0	|$0000-$3fff	|System Block
|1	|$4000-$7fff	|Application Block
|2	|$8000-$bfff	|Core Block
|3	|$c000-$ffff	|Storage Block
 
### System Block
This memory is reserved for system operations and running the shell process.

### Application Block
This block of memory can be used by the application developer for their applications. There will a set of system calls that will eventually be available to the developer that will give them the ability to bank switch the memory in their application.

### Core Block
This block contains the Core libraries for the Shell and other system calls, it should not be used by the application developers.

### System Block
This block is split into two areas. One area that is considered as *System Storage* and is used to store important information for the Shell. The other block is called *Application Storage* and can be used by the developer to store data for their running applications. Both of these blocks are under the Kernal, IO and other system RAM.

## System Storage Area
Use by the Animal Shell, and should not be touched by applications directly. System Calls will be available to get data from this area of memory.

## Application Storage Area
Can be used by the application developer to store data needed for their applications. The developer will usually have this area of memory available when using it during the normal shell cycle. However, if the application developer needs to use any Kernal calls, they should consider using the available system calls to use any memory above RAM. It's okay though, because this Shell is not intended to be used for making games. More information on how to do this will be available in the Programmers Reference.

## Memory Pages
A page of memory contains 256 bytes of memory. the system will provide a mechanism to allocate pages of memory that can be used for application or core functionality.

## Lists
A List is used to store a mutable sequence of data. You can add, remove, and modify elements in a list after it is created. Like an array, a list is of a fixed number of elements, and the data stored should be the same size or type. Unlike an array, you are not limited to primitives and can store any kind of data structure in your list. Lists cannot be reduced or increased in size.

## System Call Interface
The Core of Animal Shell uses what are referred to as *System Calls*. These are essentially a set of API (Application Programmer Interface) calls that are available to the developer for making their own application for the shell. In most cases, the developer should use these calls when performing anything with the Shell. See the Programmers Reference for more information.

The [System Call Interface](sci.md) has more details on how this vital part of the Animal Shell works.

## Boot Process
The Animal Shell has a pretty simple boot process where it essentially loads the Core and Shell into memory and then starts up. That said, because it can be run in either Cartridge or from Disk, each follows a slightly different path on startup.

### Cartridge
- cartridge loaded
- call core init
- call shell init
- call shell run
- call shell start
- call shell main

### Disk
- load/run boot
- load loader
- load shell
- load core
- run loader
- call core init
- call shell init
- call shell run
	- call shell start
	- call shell main
		- run cli
		- rum cmd
		- run app

