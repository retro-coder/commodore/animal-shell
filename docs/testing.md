# Testing Specification
Testing is an integral part in the development of Animal Shell. To that, a simple but robust way to test different functions should be created to aid the developer.

## External Debuggers
There are external debuggers available, and these are very good, but it will be a benefit to the developer to have native tools available for creating and testing for the shell.

## Unit Tests
A set of Macros to help the developer create small applications that will test the functionality of the shell. In its simplest form, we can provide a simple way to compare the results of an operation in memory to an expected result. 

For example, lets say we are testing the call to concatenate the strings "hello" and "world". After the call is made, we can test the area in memory that returns the result to check for "helloworld". If it matches, the test passes, otherwise it fails.

## Test Suite
Provide a way to run multiple test as a Suite, store the results and then present them as a report after completion. It would be great if we can have the results exported to a text file (seq file) so that we can view the results later. If we can do that, we might even be able to automate the testing externally and using some tools that can read files we could run a whole series of tests automatically.

## Sample Programs
A set of Macros are provided to assist the developer in writing sample programs that can be useful when testing the interface calls.
