
"""
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
    
"""


import sys
import os.path
import types
import logging
import subprocess
import glob
import datetime
import shutil
import datetime


from pathlib import Path
from makefiles import *


def update_build_number(config):

    print(f"pass: {config.make_pass}")

    if config.make_pass < 1:

        config.make_pass += 1

        with open('src/build/.build') as f: build = int(f.read())
        build += 1
        with open('src/build/.build', 'w') as f: f.write(str(build))
        config.build = build

    # config.src_path = 'src'
    # make_files = command_ver

    # build_files(config, make_files)


def do_apps(config):

    # config.has_changes = False

    make_files = apps

    print("")
    print(f"Building Applications")
    print('----------------------')

    build_files(config, make_files)

    if config.has_changes or config.force:
        do_apps_disk(config, make_files)


def do_boot(config):

    # config.has_changes = False

    # build 128 boot files

    make_files = boot_128

    print("")
    print(f"Building Boot Files for C128")
    print('-----------------------------')

    build_files(config, make_files)

    # build 64 boot files

    make_files = boot_64

    print("")
    print(f"Building Boot Files for C64")
    print('----------------------------')

    build_files(config, make_files)

    if config.has_changes or config.force:
        make_files = boot_disk_files
        update_build_number(config)
        do_boot_disk(config, make_files)

def do_commands(config):

    make_files = commands

    print("")
    print(f"Building Command Files")
    print('-----------------------')

    build_files(config, make_files)

    # if config.has_changes or config.force:
    #     make_files = boot_disk_files
    #     update_build_number(config)
    #     do_boot_disk(config, make_files)


def do_drivers(config):

    make_files = drivers

    print("")
    print(f"Building Driver Files")
    print('----------------------')

    build_files(config, make_files)

    # if config.has_changes or config.force:
    #     make_files = boot_disk_files
    #     update_build_number(config)
    #     do_boot_disk(config, make_files)


def do_libs(config):

    make_files = libraries

    print("")
    print(f"Building Library Files")
    print('-----------------------')

    build_files(config, make_files)

    # if config.has_changes or config.force:
    #     make_files = boot_disk_files
    #     update_build_number(config)
    #     do_boot_disk(config, make_files)


def do_carts(config):

    # config.has_changes = False

    # build 128 cart files

    make_files = cart_128

    print("")
    print(f"Building Cartridges for C128")
    print('------------------------------')

    build_files(config, make_files)

    # build 64 cart files

    make_files = cart_64

    print("")
    print(f"Building Cartridge for C64")
    print('----------------------------')

    build_files(config, make_files)

    if config.has_changes:
        update_build_number(config)

    do_cartconv(config)


def do_cartconv(config):

    parms = [config.cartconv_bin, "-t", "normal", "-i", f"{config.bin_path}\\cart64.bin", "-o", f"{config.bin_path}\\cart64.crt"]
    logging.debug(parms)
    p = subprocess.run(parms, capture_output=True, text=True)

    parms = [config.cartconv_bin, "-t", "normal", "-i", f"{config.bin_path}\\cart64g.bin", "-o", f"{config.bin_path}\\cart64g.crt"]
    logging.debug(parms)
    p = subprocess.run(parms, capture_output=True, text=True)

    parms = [config.cartconv_bin, "-t", "c128", "-i", f"{config.bin_path}\\cart128.bin", "-o", f"{config.bin_path}\\cart128.crt"]
    logging.debug(parms)
    p = subprocess.run(parms, capture_output=True, text=True)
    
    parms = [config.cartconv_bin, "-t", "c128", "-i", f"{config.bin_path}\\cart128g.bin", "-o", f"{config.bin_path}\\cart128g.crt"]
    logging.debug(parms)
    p = subprocess.run(parms, capture_output=True, text=True)
    
    parms = [config.cartconv_bin, "-t", "c128", "-i", f"{config.bin_path}\\int128.bin", "-o", f"{config.bin_path}\\int128.crt"]
    logging.debug(parms)
    p = subprocess.run(parms, capture_output=True, text=True)

    parms = [config.cartconv_bin, "-t", "c128", "-i", f"{config.bin_path}\\int128g.bin", "-o", f"{config.bin_path}\\int128g.crt"]
    logging.debug(parms)
    p = subprocess.run(parms, capture_output=True, text=True)


def do_test(config):

    config.src_path = ''

    make_files = unit_tests

    print("")
    print(f"Building Unit Testing")
    print('----------------------')

    build_files(config, make_files)

    if config.has_changes or config.force:
        do_test_disk(config, make_files)


def do_samples(config):

    config.src_path = ''
    make_files = app_samples

    print("")
    print(f"Building Sample Apps")
    print('---------------------')

    build_files(config, make_files)

    if config.has_changes or config.force:
        do_samples_disk(config, make_files)


def build_files(config, files):

    file_total = 0
    build_total = 0
    error_total = 0
    create_total = 0
    update_total = 0
    skip_total = 0

    is_updating = False

    for file in files:

        file_total += 1

        # default build object
        if 'out' not in file:
            file['out'] = Path(file['src']).stem + '.prg'

        # default cbm filename
        if 'filename' not in file:
            file['filename'] = Path(file['src']).stem

        # default file to check
        if 'check' not in file:
            file['check'] = []

        # should we force build?
        if 'force' not in file:
            file['force'] = False

        # should we fail on build errors?
        if 'fail' not in file:
            file['fail'] = False

        if 'type' not in file:
            file['type'] = 'p'

        # output path
        # out_path = os.path.abspath(os.path.join(config.bin_path, config.platform))
        out_path = os.path.abspath(config.bin_path)

        # source path
        src_path = os.path.abspath(config.src_path)
        src_path2 = os.path.abspath(os.path.join('.', 'src'))

        # source file
        file_in = os.path.abspath(os.path.join(config.src_path, file['src']))

        # output file path
        file_in_base = Path(os.path.basename(file_in)).stem
        file_out = os.path.abspath(os.path.join(file_in_base, ".prg"))

        if "out" in file:
            file_out = os.path.abspath(os.path.join(out_path, file['out']))

        # listed output
        file_out_base = Path(os.path.basename(file_out)).stem
        list_out = os.path.abspath(os.path.join(out_path, file_out_base + ".lst"))

        # vice symbol file
        vice_out = os.path.abspath(os.path.join(out_path, file_out_base + ".l"))

        # debugger file
        debug_out = os.path.abspath(os.path.join(out_path, file_out_base + ".dbg"))

        # defined platform for 
        defined_platform = "C" + config.platform


        logging.debug('------------------------------------------------------------')
        logging.debug(f"src_path: {src_path}")
        logging.debug(f"file_src: {file['src']}")
        logging.debug(f"file_out: {file['out']}")
        logging.debug(f"file_check: {file['check']}")
        logging.debug(f"file_filename: {file['filename']}")
        logging.debug(f"defined_platform: {defined_platform}")
        logging.debug(f"file_in: {file_in}")
        logging.debug(f"file_out: {file_out}")
        logging.debug(f"file_out_base: {file_out_base}")
        logging.debug(f"list_out: {list_out}")
        logging.debug(f"vice_out: {vice_out}")
        logging.debug(f"debug_out: {debug_out}")
        logging.debug(f"out_path: {out_path}")
        logging.debug('------------------------------------------------------------')

        file_out_check = Path(file_out.strip())
        do_build = False

        logging.debug(f"looking for {file_out_check}")

        # if there is an output file already?

        if file_out_check.is_file():

            logging.debug(f"found object {file_out_check}")

            # get the list of source files to check

            search_files = [file_in]
            search_paths = []

            if 'check' in file:
                search_paths = file['check']

            for search_path in search_paths:
                src_search_path = os.path.join(config.src_path, search_path)
                search_files += glob.glob(src_search_path,include_hidden=True, recursive=True)

            logging.debug(f"checking {len(search_files)} file(s) for source code changes")

            logging.debug(search_files)

            for search_file in search_files:

                if os.path.getmtime(search_file) > os.path.getmtime(file_out):

                    logging.info(f"file {search_file} has newer timestamp than last built object")

                    # and the last creation date is less than our source date
                    logging.debug(f"out: {datetime.datetime.fromtimestamp(os.path.getmtime(file_out)).strftime('%Y-%m-%d %H:%M:%S')}")
                    logging.debug(f"in:  {datetime.datetime.fromtimestamp(os.path.getmtime(search_file)).strftime('%Y-%m-%d %H:%M:%S')}")

                    # then build
                    do_build = True
                    update_total += 1
                    is_updating = True
        else:

            do_build = True
            create_total += 1

        # if we are using the --force then build anyway

        # print(file)

        if file['force'] == True:
            do_build = True

        if config.force:
            do_build = True

        # build_status_msg = "  * Skipped * "

        bin_parm = '-binfile'

        if do_build:

            print(file_in)

            config.has_changes = True
            # build_status_msg = ""
            build_version = f":build_version={config.version}"
            build_date = f":build_date={config.date}"
            build_number = f":build_number={config.build}"
            # subprocess.call([config.java_bin, "-jar", config.kickass_jar, file_in, "-libdir", ".", "-odir", out_path, "-o", file_out, "-bytedumpfile", list_out, "-debugdump", "-define", config.defined_platform], shell=True, stdout=subprocess.DEVNULL)
            # p = subprocess.run([config.java_bin, "-jar", config.kickass_jar, file_in, "-libdir", ".", "-odir", out_path, "-o", file_out, "-bytedumpfile", list_out, "-debugdump", "-define", defined_platform], capture_output=True, text=True)
            # parms = [config.java_bin, "-jar", config.kickass_jar, file_in, "-libdir", src_path, "-libdir", src_path2, "-odir", out_path, "-o", file_out, "-bytedumpfile", list_out, "-debugdump", "-define", defined_platform, build_date, build_number, build_version]
            parms = [config.java_bin, "-jar", config.kickass_jar, file_in, "-libdir", src_path, "-libdir", src_path2, "-odir", out_path, "-o", file_out, "-bytedumpfile", list_out, "-debugdump", build_date, build_number, build_version, "-define", config.log_level]

            if file['type'] == 's':
                parms += ['-binfile']
                
            logging.debug(parms)
            p = subprocess.run(parms, capture_output=True, text=True)
            # p = subprocess.run(parms, capture_output=False, text=True)

            if p.returncode > 0:
                error_total += 1
                print("\n")
                print(p.stdout)
                print('*** Error Building File ***')
                print('')

                if not config.ignore_errors:
                    print('\nDone.')
                    sys.exit(1)
            else:
                if not is_updating:
                    build_total += 1

        else:

            skip_total += 1        

        # print(build_status_msg)

    print("")
    print("Build Summary:")
    print("--------------")
    print(f"Processed:  {file_total}")
    print(f"Created:    {build_total}")
    print(f"Updated:    {update_total}")
    print(f"Skipped:    {skip_total}")
    print(f"Errors:     {error_total}")


def do_run(config):

    logging.debug("do_run")

    disk_file = os.path.abspath(os.path.join(config.bin_path, (config.disk_title + "." + config.disk_type)))
    disk_file_path = Path(disk_file.strip())

    apps_disk_file = os.path.abspath(os.path.join(config.bin_path, 'apps.d81'))
    tests_disk_file = os.path.abspath(os.path.join(config.bin_path, 'tests.d81'))
    samples_disk_file = os.path.abspath(os.path.join(config.bin_path, 'samples.d81'))

    disk_8 = disk_file
    disk_9 = apps_disk_file

    if config.tests:
        disk_9 = tests_disk_file

    if config.samples:
        disk_9 = samples_disk_file

    emulator_bin = os.path.join(config.emulator_path, config.emulator)

    # display settings
    display_parm = []

    # output path
    out_path = os.path.abspath(config.bin_path)

    # jiffy dos and charset roms
    if config.platform == '64':
        jdos_parms = ['-kernal', 'c:\\games0\\retro\\commodore\\roms\\jiffydos\\jiffydos-c64.bin']
        chargen_parms = ['-chargen', 'c:\\games0\\retro\\commodore\\roms\\fonts\\pxlu.bin']

    if config.platform == '128':

        display_parm = ['-40', '-hidevdcwindow']

        if config.vdc:
            display_parm = ['-80', '+hidevdcwindow']

        jdos_parms = ['-kernal', 'c:\\games0\\retro\\commodore\\roms\\jiffydos\\jiffydos-c128.bin']
        chargen_parms = ['-chargen', 'c:\\games0\\retro\\commodore\\roms\\fonts\\pxlu128.bin']

    rom_parms = [ 
    '-dos1541II', 'c:\\games0\\retro\\commodore\\roms\\jiffydos\\jiffydos-1541-ii.bin',
    '-dosCMDHD', 'c:\\games0\\retro\\commodore\\roms\\doscmdhd.bin',
    '-dos1581', 'c:\\games0\\retro\\commodore\\roms\\jiffydos\\jiffydos-1581.bin'
    ]

    monitor_parms = ['-binarymonitor', '-remotemonitor']

    drive8_parms = ['-drive8type', '1581', '-8', disk_8]
    drive9_parms = ['-drive9type', '1581','-9', disk_9]
    # drive8_parms = ['-8', disk_8]
    # drive9_parms = ['-9', disk_9]
    drive10_parms = ['-drive10type', '4844', '-10', 'c:\\games0\\retro\\commodore\\media\\cmdhd\\dev.dhd']

    distro_path = os.path.abspath(config.distro_path)
    drive11_parms = ['-virtualdev11', '-iecdevice11', '-device11', '1', '-fs11', distro_path]
    ramlink_parms = ['-cartramlink', 'c:\\games0\\retro\\commodore\\roms\\jiffydos\\ramlink201.bin', '-ramlink', '-ramlinkimage', 'c:\\games0\\retro\\commodore\\media\\ram\\animalshell.rl', '-ramlinksize', '16', '-ramlinkimagerw', '-ramlinkrtcsave']
    wic64_parms = ['-userportdevice', '23', '-wic64timezone', '24']
    xmem_parms = ['-reu', '-reusize', '512']
    other_parms = ['+keyset']

    if config.georam:
        xmem_parms = ['-georam', '-georamsize', '512']

    drive_parms = rom_parms + drive8_parms + drive9_parms + drive10_parms + drive11_parms + jdos_parms + chargen_parms + monitor_parms + wic64_parms + display_parm + xmem_parms #+ ramlink_parms

    if config.cartridge:

        georam = ''

        if config.georam:
            georam = 'g'

        cart_file = os.path.abspath(os.path.join(out_path, ("cart" + config.platform + georam + ".bin")))
        cart_parm = ["-cartfrom", cart_file]

        if config.platform == '64':
            cart_parm = ["-cart16", cart_file]
            config.internal = False

        if config.internal:

            cart_file = os.path.abspath(os.path.join(out_path, ("int" + config.platform + georam + ".bin")))
            cart_parm = ["-intfunc", "1" ,"-intfrom", cart_file]

        logging.debug(f"cart_file: {cart_file}")
        logging.debug(f"cart_parm: {cart_parm}")

        parms = [emulator_bin] + cart_parm + drive_parms + other_parms

        logging.debug(parms)

        handle = subprocess.Popen(parms, stdout=subprocess.PIPE)
        # handle = subprocess.call(parms)

    else:

        auto = disk_8 + ":boot.128"

        if config.georam:
            auto = disk_8 + ":bootg.128"

        if config.platform == '64':
            auto = disk_8 + ":boot.64"

            if config.georam:
                auto = disk_8 + ":bootg.64"

        parms = [emulator_bin] + drive_parms + ['-autostart', auto] + other_parms

        logging.debug(parms)

        handle = subprocess.Popen(parms, stdout=subprocess.PIPE)
        # handle = subprocess.call(parms)


def do_debug(config):
    logging.debug("do_debug")
    handle = subprocess.Popen([config.debugger_bin, 'bin\\128\\cart128.dbg', '-font=C:\\Users\\paul\\AppData\\Local\\Microsoft\\Windows\\Fonts\\ZedMonoNerdFont-Regular.ttf,20']) 


def do_apps_disk(config, files):

    print("")
    print(f"Building Apps Disk For C{config.platform}")
    print("----------------------------")

    disk = types.SimpleNamespace()
    
    disk.title = 'apps'
    disk.name = disk.title + ',as'
    disk.type = config.disk_type
    disk.file = os.path.abspath(os.path.join(config.bin_path, (disk.title + "." + config.disk_type)))
    disk.file_path = Path(disk.file.strip())

    # output path
    disk.out_path = os.path.abspath(config.bin_path)

    logging.debug(f"disk: {disk}")

    build_disk(config, files, disk)

    
def do_boot_disk(config, files):

    print("")
    print(f"Building Animal Shell Boot Disk")
    print("--------------------------------")

    disk = types.SimpleNamespace()

    disk.title = config.disk_title
    disk.name = disk.title + ',as'
    disk.type = config.disk_type
    # disk.file = os.path.abspath(os.path.join(config.bin_path, config.platform, (disk.title + "." + config.disk_type)))
    disk.file = os.path.abspath(os.path.join(config.bin_path, (disk.title + "." + config.disk_type)))
    disk.file_path = Path(disk.file.strip())

    # output path
    # disk.out_path = os.path.abspath(os.path.join(config.bin_path, config.platform))
    disk.out_path = os.path.abspath(config.bin_path)

    logging.debug(f"disk: {disk}")

    build_disk(config, files, disk)


def do_test_disk(config, files):

    print("")
    print(f"Building Unit Tests Disk")
    print("-------------------------")

    disk = types.SimpleNamespace()

    disk.title = 'tests'
    disk.name = disk.title + ',as'
    disk.type = config.disk_type
    disk.file = os.path.abspath(os.path.join(config.bin_path, (disk.title + "." + config.disk_type)))
    disk.file_path = Path(disk.file.strip())

    # output path
    disk.out_path = os.path.abspath(config.bin_path)

    logging.debug(f"disk: {disk}")

    build_disk(config, files, disk)


def do_samples_disk(config, files):

    print("")
    print(f"Building Samples App Disk")
    print("--------------------------")

    disk = types.SimpleNamespace()

    disk.title = 'samples'
    disk.name = disk.title + ',as'
    disk.type = config.disk_type
    disk.file = os.path.abspath(os.path.join(config.bin_path, (disk.title + "." + config.disk_type)))
    disk.file_path = Path(disk.file.strip())

    # output path
    disk.out_path = os.path.abspath(config.bin_path)

    logging.debug(f"disk: {disk}")

    build_disk(config, files, disk)


def build_disk(config, files, disk):

    build = types.SimpleNamespace()
    build.c1541 = os.path.abspath(config.c1541_bin)

    disk_total = 0
    disk_added = 0
    disk_updated = 0
    disk_missing = 0

    # no disk, then create one

    create_disk = False

    if not disk.file_path.is_file():
        create_disk = True

    if create_disk:
        print(f"Creating disk {disk.file_path} . . .\n")
        subprocess.call([build.c1541, '-format', disk.name, disk.type, disk.file_path], shell=True)
    else:
        print(f"Using existing disk {disk.file_path} . . .")

    print("")

    disk_build_time = os.path.getmtime(disk.file_path)

    print(disk_build_time)

    # output path
    # out_path = os.path.abspath(os.path.join(config.bin_path, config.platform))
    out_path = os.path.abspath(config.bin_path)

    for file in files:

        disk_total += 1

        if 'filename' not in file:
            file['filename'] = Path(file['src']).stem

        if 'out' not in file:
            file['out'] = Path(file['src']).stem + '.prg'

        if 'type' not in file:
            file['type'] = 'p'

        file_out = os.path.abspath(os.path.join(out_path, file['out']))
        file_out_path = Path(file_out)
        disk_out = file['filename']

        logging.debug(file)
        
        # and the last creation date is less than our source date
        
        missing_file = True

        if file_out_path.is_file():

            missing_file = False

            logging.debug(f"file_out: {datetime.datetime.fromtimestamp(os.path.getmtime(file_out)).strftime('%Y-%m-%d %H:%M:%S')}")
            logging.debug(f"disk_build_time:  {datetime.datetime.fromtimestamp(disk_build_time).strftime('%Y-%m-%d %H:%M:%S')}")

        print(f"{file_out} => {file['filename']}", end='')

        disk_file_msg = " * Skipped * "

        file_type = ',' + file['type']

        if not missing_file:

            if create_disk:

                disk_added += 1
                disk_file_msg = ""

                # add the file
                p = subprocess.run([build.c1541, '-attach', disk.file_path, '-write', file_out, disk_out + file_type], capture_output=True, text=True)

            else:

                if os.path.getmtime(file_out) > disk_build_time:

                    disk_updated += 1
                    disk_file_msg = ""

                    logging.info(f"file {file} has newer timestamp than {disk.file_path}")

                    # delete the file
                    p = subprocess.run([build.c1541, '-attach', disk.file_path, '-delete', disk_out], capture_output=True, text=True)

                    # add the file
                    p = subprocess.run([build.c1541, '-attach', disk.file_path, '-write', file_out, disk_out + file_type], capture_output=True, text=True)

        if missing_file:
            disk_missing += 1
            disk_file_msg = " * Missing *"
            
        print(disk_file_msg)

    print("")
    print("Build Disk Summary:")
    print("-------------------")
    print(f"Processed:  {disk_total}")
    print(f"Added:      {disk_added}")
    print(f"Updated:    {disk_updated}")
    print(f"Missing:    {disk_missing}")


def do_distro(config):

    out_path = os.path.abspath(config.distro_path)
    bin_path = os.path.abspath(config.bin_path)

    print(f"Creating distribution at {out_path}")
    if not os.path.exists(out_path):
        os.makedirs(out_path)

    make_files = boot_64 + boot_128 + commands + unit_tests + app_samples + apps + other_apps

    for file in make_files:
        bin_file = os.path.abspath(os.path.join(bin_path, file['out']))
        out_file = os.path.abspath(os.path.join(out_path, file['filename']))

        if os.path.exists(bin_file):

            print(f"{bin_file} ==> {out_file}")
            shutil.copy(bin_file, out_file)


def do_clean(config):

    if config.make_pass > 0:
        print(f"Project was already cleaned * Skipping *")
        return

    clear_path = os.path.abspath(os.path.join(config.bin_path))

    print(f"Removing directory {clear_path}")
    if os.path.exists(clear_path):
        shutil.rmtree(clear_path)

    clear_path = os.path.abspath(os.path.join(config.distro_path))

    print(f"Removing directory {clear_path}")
    if os.path.exists(clear_path):
        shutil.rmtree(clear_path)


def do_option(config):

    logging.debug(config)

    match config.option:

        case 'help':
            do_help()

        case 'boot':
            do_commands(config)
            do_drivers(config)
            do_libs(config)
            do_boot(config)

        case 'carts':
            do_carts(config)

        case 'commands':
            do_commands(config)

        case 'drivers':
            do_drivers(config)

        case 'libs':
            do_libs(config)

        case 'disks':
            do_commands(config)
            do_drivers(config)
            do_libs(config)
            do_boot(config)
            do_apps(config)
            do_samples(config)
            do_test(config)

        case 'build':
            do_carts(config)
            do_boot(config)

        case 'run':
            do_carts(config)
            do_commands(config)
            do_drivers(config)
            do_libs(config)
            do_boot(config)
            do_apps(config)
            do_run(config)

        case 'debug':
            do_debug(config)

        case 'all':
            do_clean(config)
            do_carts(config)
            do_commands(config)
            do_drivers(config)
            do_libs(config)
            do_boot(config)
            do_apps(config)
            do_samples(config)
            do_test(config)
            do_distro(config)

        case 'clean':
            do_clean(config)

        case 'serve':
            do_serve(config)

        case 'apps':
            do_apps(config)

        case 'tests':
            do_test(config)

        case 'samples':
            do_samples(config)

        case 'distro':
            do_commands(config)
            do_drivers(config)
            do_libs(config)
            do_boot(config)
            do_apps(config)
            do_samples(config)
            do_test(config)
            do_distro(config)

        case _:
            do_help()


def do_serve(config):
    # add the file
    subprocess.run(['start', 'mkdocs', 'serve'], shell=True)


def do_help():

    print('Usage: make [target] [options] ...')
    print('')
    print('all      Make everything (clean, build, disk, cart).')
    print('carts    Build cartridges.')
    print('clean    Clean output directory.')
    print('commands Build commands.')
    print('debug    Start debugger.')
    print('drivers  Build drivers.')
    print('boot     Build boot disk.')
    print('doc      Build documentation.')
    print('libs     Build libraries.')
    print('serve    Build and serve documentation.')
    print('run      Run emulator with current build.')
    print('tests    Build unit tests.')
    print('apps     Build applications.')
    print('samples  Build samples disk.')
    print('distro   Build distibution bundle.')
    print('')
    print('Options:')
    print('-F, --force                                  Force building or running.')
    print('-4, --64                                     Run C64 emulator.')
    print('-C, --cart, --cartridge                      Start the emulator with the cartridge attached.')
    print('-G, --georam                                 Run with GeoRAM support (default is REU).')
    print('-IC, --int-cart, --internal-cartridge        Start the emulator with the internal cartridge (C128) attached.')
    #print('-NB, --no-build                              Do not build objects.')
    #print('-f FILE, --file=FILE                         Target a single file for building.')
    #print('--files=FILE1,FILE2,FILEn ...                Target several files for building.')
    print('-80, --vdc                                   Use 80 cols on C128 emulator.')
    print('-T, --tests, --unit-tests                    Mount tests on drive 9.')
    print('-S, --samples                                Mount samples on drive 9.')
    print('-D, --debug                                  Show additional diagnostic information.')
    print('-v, --version                                Show version information.')
    print('-?, -h, --help                               Show this help information.')
    sys.exit(0)


def main():

    config=types.SimpleNamespace()
    config.emulator_path=f"c:\\Games0\\Retro\\Commodore\\Emulators\\GTK3VICE-3.8-win64\\bin"
    config.tass_bin=f"tools/64tass.exe"
    config.c1541_bin=f"{config.emulator_path}\\c1541.exe"
    config.java_bin=f"c:\\Users\\paul\\Me\\Tools\\OpenJava20\\bin\\java.exe"
    config.kickass_jar=f"c:\\Users\\paul\\Me\\Tools\\KickAssembler\\kickass-5.25.jar"
    config.debugger_bin=f"C:\\Games0\\Retro\\Commodore\\Tools\\icebrolite-120.exe"
    config.distro_path=f"c:\\Games0\\Retro\\Commodore\\Media\\animalshell"
    config.cartconv_bin=f"{config.emulator_path}\\cartconv.exe"
    config.bin_path='bin'
    config.src_path='src'
    config.version='0.1.0'
    config.option='version'
    config.disk_type='d81'
    config.platform='128'
    config.force=False
    config.cartridge=False
    config.disk=False
    config.disk_title="animalshell"
    config.debug=False
    # config.c128=True 
    # config.c64=False
    config.internal=False
    config.emulator="x128.exe"
    config.has_changes=False
    config.ignore_errors=True
    config.make_pass = 0
    config.do_build = False
    config.vdc = False
    config.tests = False
    config.samples = False
    config.log_level = "LOG_ERROR"
    config.georam = False

    if len(sys.argv)>1:
        config.option=sys.argv[1]

    if "--force" in sys.argv:
        config.force = True

    if "-C" in sys.argv:
        config.cartridge = True

    if "--cart" in sys.argv:
        config.cartridge = True

    if "--cartridge" in sys.argv:
        config.cartridge = True

    if "-IC" in sys.argv:
        config.cartridge = True
        config.internal = True

    if "--int-cart" in sys.argv:
        config.cartridge = True
        config.internal = True

    if "--internal-cartridge" in sys.argv:
        config.cartridge = True
        config.internal = True

    if "-vdc" in sys.argv or '-80' in sys.argv:
        config.vdc = True

    if "-T" in sys.argv or "--tests" in sys.argv or "--unit-tests" in sys.argv:
        config.tests = True

    if "-S" in sys.argv or "--samples" in sys.argv:
        config.samples = True

    if "--debug" in sys.argv:
        config.debug = True

    if "-v" in sys.argv or "--version" in sys.argv:
        print('Make Animal Shell')
        print(config.version)
        sys.exit(0)

    if "-L1" in sys.argv or "--log-warn" in sys.argv:
        config.log_level = "LOG_WARN"

    if "-L2" in sys.argv or "--log-error" in sys.argv:
        config.log_level = "LOG_ERROR"

    if "-L3" in sys.argv or "--log-debug" in sys.argv:
        config.log_level = "LOG_DEBUG"

    if "-L4" in sys.argv or "--log-detail" in sys.argv:
        config.log_level = "LOG_DETAIL"

    if "-L5" in sys.argv or "--log-trace" in sys.argv:
        config.log_level = "LOG_TRACE"

    if "-L6" in sys.argv or "--log-fine" in sys.argv:
        config.log_level = "LOG_FINE"

    if "-G" in sys.argv or "--georam" in sys.argv:
        config.georam = True

    if "-?" in sys.argv or "-h" in sys.argv or "--help" in sys.argv:
        do_help()
        sys.exit(0)


    if config.debug:
        logging.basicConfig(level=logging.DEBUG)

    config.platform="128"
    config.emulator='x128.exe'

    if "--64" in sys.argv or "-4" in sys.argv:
        config.platform="64"
        config.emulator='x64sc.exe'

    config.date = datetime.datetime.now().strftime("%c")
    config.major = '0'
    config.minor = '1'
    config.patch = '0'
    config.version = f"{config.major}.{config.minor}.{config.patch}"

    with open('src/build/.build') as f: config.build = int(f.read())

    do_option(config)

    print(f"\nDone.")

    sys.exit(0)


if __name__=='__main__':
    main()

