!to "../../../bin/wic64-2.prg", cbm

; Copyright (C) 2022-2025 Paul Hocker. All rights reserved.
; Licensed under the MIT License. (See LICENSE.md in the project root for license information)

; WIC64 Example 1 Simple Execute
;

* = $4000

jmp app_handler

jmp app_handler

* = $4006


signature	!text "##a0"
type		!byte $00
platform	!byte $00
major		!byte $00
minor		!byte $00

name		!byte <d_name, >d_name
description	!byte <d_desc, >d_desc
copyright	!byte <d_copyright, >d_copyright
author		!byte <d_author, >d_author
website		!byte <d_website, >d_website
email		!byte <d_email, >d_email

* = $4020

APP_STATE	!byte $00
APP_NEXT_STATE	!byte $00

* = $4040

d_name		!pet "acmeApp", 0
d_desc		!pet "acmeDesc", 0
d_copyright	!pet "acmeCopyright", 0
d_author	!pet "acmeAuthor", 0
d_website	!pet "acmeWebsite", 0
d_email		!pet "acmeEmail", 0


timeout_error_message: !pet "?timeout error", $00

http_get_request: !byte "R", WIC64_HTTP_GET, <payload_size, >payload_size
http_get_payload: !text "http://x.wic64.net/test/message.txt"

payload_size = * - http_get_payload

status_request: !byte "R", WIC64_GET_STATUS_MESSAGE, $01, $00, $01

status_prefix: !pet "?request failed: ", $00

p_print_ptr	!word $0000


* = *

!src "wic64.h"
!src "wic64.asm"
!src "macros.asm"

app_handler:

nop
nop
nop

; check for INIT

lda	APP_STATE
cmp	#$10	
bne	+
jmp	app_init_handler

+

;	check for START

lda	APP_STATE
cmp	#$20
bne	+
jmp	app_start_handler

+

;	check for STOP

lda	APP_STATE
cmp	#$30
bne	+
jmp	app_stop_handler

+

;	check for RUN

lda	APP_STATE
cmp	#$40
bne	+
jmp	app_run_handler

+

rts

app_init_handler:

rts


app_start_handler:

;lda     #$40
;sta     APP_NEXT_STATE
rts


app_run_handler:


main:

; kernal in
lda	#$66
sta	$fa
lda	#0
sta	$fb
lda	#0
sta	$fc
lda	#0
sta	$fd
lda	#0
sta	$fe
jsr	$a000

    +wic64_execute http_get_request, response
    bcs timeout
    bne error

success:
    
lda	#<response
sta	p_print_ptr
lda	#>response
sta	p_print_ptr + 1
lda	#$30
sta	$fa
lda	#<p_print_ptr
sta	$fb
lda	#>p_print_ptr
sta	$fc
jsr	$a000
jmp done

error:
    +wic64_execute status_request, response
    bcs timeout


lda	#<status_prefix
sta	p_print_ptr
lda	#>status_prefix
sta	p_print_ptr + 1
lda	#$30
sta	$fa
lda	#<p_print_ptr
sta	$fb
lda	#>p_print_ptr
sta	$fc
jsr	$a000
    

lda	#<response
sta	p_print_ptr
lda	#>response
sta	p_print_ptr + 1
lda	#$30
sta	$fa
lda	#<p_print_ptr
sta	$fb
lda	#>p_print_ptr
sta	$fc
jsr	$a000
jmp done

timeout:

lda	#<timeout_error_message
sta	p_print_ptr
lda	#>timeout_error_message
sta	p_print_ptr + 1
lda	#$30
sta	$fa
lda	#<p_print_ptr
sta	$fb
lda	#>p_print_ptr
sta	$fc
jsr	$a000

done:

; kernal out
lda	#$67
sta	$fa
lda	#0
sta	$fb
lda	#0
sta	$fc
lda	#0
sta	$fd
lda	#0
sta	$fe
jsr	$a000

    lda	#$30
    sta	APP_NEXT_STATE
    rts


app_stop_handler

rts

response:

