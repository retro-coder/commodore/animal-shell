!to "../../../bin/wic64-0.prg", cbm

; Copyright (C) 2022-2025 Paul Hocker. All rights reserved.
; Licensed under the MIT License. (See LICENSE.md in the project root for license information)

; WIC64 Example 0 Detect Device & Firmware
;

* = $4000

jmp app_handler

jmp app_handler

* = $4006


signature	!text "##a0"
type		!byte $00
platform	!byte $00
major		!byte $00
minor		!byte $00

name		!byte <d_name, >d_name
description	!byte <d_desc, >d_desc
copyright	!byte <d_copyright, >d_copyright
author		!byte <d_author, >d_author
website		!byte <d_website, >d_website
email		!byte <d_email, >d_email

* = $4020

APP_STATE	!byte $00
APP_NEXT_STATE	!byte $00

* = $4040

d_name		!pet "acmeApp", 0
d_desc		!pet "acmeDesc", 0
d_copyright	!pet "acmeCopyright", 0
d_author	!pet "acmeAuthor", 0
d_website	!pet "acmeWebsite", 0
d_email		!pet "acmeEmail", 0

p_print_ptr	!word $0000


* = *

!src "wic64.h"
!src "wic64.asm"
!src "macros.asm"

app_handler

nop
nop
nop

; check for INIT

lda	APP_STATE
cmp	#$10	
bne	+
jmp	app_init_handler

+

;	check for START

lda	APP_STATE
cmp	#$20
bne	+
jmp	app_start_handler

+

;	check for STOP

lda	APP_STATE
cmp	#$30
bne	+
jmp	app_stop_handler

+

;	check for RUN

lda	APP_STATE
cmp	#$40
bne	+
jmp	app_run_handler

+

rts

app_init_handler

rts


app_start_handler

;lda     #$40
;sta     APP_NEXT_STATE

rts


app_run_handler

main:

; kernal in
lda	#$66
sta	$fa
lda	#0
sta	$fb
lda	#0
sta	$fc
lda	#0
sta	$fd
lda	#0
sta	$fe
jsr	$a000

    +wic64_detect
    
    bcs device_not_present
    bne legacy_firmware

lda	#<new_firmware_text
sta	p_print_ptr
lda	#>new_firmware_text
sta	p_print_ptr + 1
lda	#$30
sta	$fa
lda	#<p_print_ptr
sta	$fb
lda	#>p_print_ptr
sta	$fc
jsr	$a000

    jmp done

device_not_present:

lda	#<device_not_present_error
sta	p_print_ptr
lda	#>device_not_present_error
sta	p_print_ptr + 1
lda	#$30
sta	$fa
lda	#<p_print_ptr
sta	$fb
lda	#>p_print_ptr
sta	$fc
jsr	$a000
    jmp done

legacy_firmware:

lda	#<legacy_firmware_text
sta	p_print_ptr
lda	#>legacy_firmware_text
sta	p_print_ptr + 1
lda	#$30
sta	$fa
lda	#<p_print_ptr
sta	$fb
lda	#>p_print_ptr
sta	$fc
jsr	$a000
    jmp done

done:

; kernal out
lda	#$67
sta	$fa
lda	#0
sta	$fb
lda	#0
sta	$fc
lda	#0
sta	$fd
lda	#0
sta	$fe
jsr	$a000

    lda	#$30
    sta	APP_NEXT_STATE
    rts

device_not_present_error: !pet "?device not present error", $0d, $00
legacy_firmware_text: !pet "legacy firmware detected", $0d, $00
new_firmware_text: !pet "firmware 2.0.0 or later detected", $0d, $00

app_stop_handler

rts
