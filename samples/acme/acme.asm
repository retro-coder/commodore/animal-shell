!to "acme.prg", cbm

; Copyright (C) 2022-2025 Paul Hocker. All rights reserved.
; Licensed under the MIT License. (See LICENSE.md in the project root for license information)

; ACME Cross Assembler Animal Shell Hello World
;

* = $4000

jmp app_handler

jmp app_handler

* = $4006


signature	!text "##a0"
type		!byte $00
platform	!byte $00
major		!byte $00
minor		!byte $00

name		!byte <d_name, >d_name
description	!byte <d_desc, >d_desc
copyright	!byte <d_copyright, >d_copyright
author		!byte <d_author, >d_author
website		!byte <d_website, >d_website
email		!byte <d_email, >d_email

* = $4030

APP_STATE	!byte $00
APP_NEXT_STATE	!byte $00

* = $4040

d_name		!pet "acmeApp", 0
d_desc		!pet "acmeDesc", 0
d_copyright	!pet "acmeCopyright", 0
d_author	!pet "acmeAuthor", 0
d_website	!pet "acmeWebsite", 0
d_email		!pet "acmeEmail", 0

hello		!pet "made with ACME", $0d, $00

p_print_ptr	!word $0000


* = *

app_handler:

nop
nop
nop

; check for INIT

lda	APP_STATE
cmp	#$10	
bne	+
jmp	app_init_handler

+

;	check for START

lda	APP_STATE
cmp	#$20
bne	+
jmp	app_start_handler

+

;	check for STOP

lda	APP_STATE
cmp	#$30
bne	+
jmp	app_stop_handler

+

;	check for RUN

lda	APP_STATE
cmp	#$40
bne	+
jmp	app_run_handler

+

rts

app_init_handler

rts


app_start_handler

lda	#<hello
sta	p_print_ptr
lda	#>hello
sta	p_print_ptr + 1
lda	#$30
sta	$fa
lda	#<p_print_ptr
sta	$fb
lda	#>p_print_ptr
sta	$fc
jsr	$a000

rts


app_run_handler

lda	#$30
sta	APP_NEXT_STATE
rts 


app_stop_handler

rts
