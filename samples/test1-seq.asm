
.segmentdef text 

.encoding "petscii_mixed"

.text @"The quick brown fox jumped over"
.text @"\$0dthe fence."
.text @"\$0d"
.text @"\$0dAnd then he went home."
.text @"\$0d\$00"

//.file [name="test1-seq", segments="text", type="bin"]

.segment TEXT [outBin="test1-seq.bin", segments="text"]
