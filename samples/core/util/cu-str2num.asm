
#importonce

#import "samples/_sample.asm"

.filenamespace testapp

SampleStart("cu-str2num", "Convert String To Number (STR2NUM)")

.segment app_data

	p_str2num: p_core_util_str2num 
	p_num2str: p_core_util_num2str 

.encoding "petscii_mixed"

	s_test1:	.text @"0\$00"
	s_test2:	.text @"128\$00"
	s_test3:	.text @"255\$00"
	s_test4:	.text @"1024\$00"
	s_test5:	.text @"1234567\$00"
	s_test6:	.text @"$80\$00"
	s_test7:	.text @"$c000\$00"
	s_test8:	.text @"$ffff\$00"
	s_test9:	.text @"%10\$00"
	s_test0:	.text @"%10000010010\$00"
	s_testa:	.text @"%101111\$00"

	blank:		.byte $00
	colon:		.text @" : \$00"

.segment app_code

	app_run:

	StorePointer(s_test1, p_str2num.addr)
	ICall2(I_CORE_CALL, I_CORE_UTIL_STR2NUM, p_str2num, num)
	jsr __test__

	StorePointer(s_test2, p_str2num.addr)
	ICall2(I_CORE_CALL, I_CORE_UTIL_STR2NUM, p_str2num, num)
	jsr __test__

	StorePointer(s_test3, p_str2num.addr)
	ICall2(I_CORE_CALL, I_CORE_UTIL_STR2NUM, p_str2num, num)
	jsr __test__

	StorePointer(s_test4, p_str2num.addr)
	ICall2(I_CORE_CALL, I_CORE_UTIL_STR2NUM, p_str2num, num)
	jsr __test__

	StorePointer(s_test5, p_str2num.addr)
	ICall2(I_CORE_CALL, I_CORE_UTIL_STR2NUM, p_str2num, num)
	jsr __test__

	StorePointer(s_test6, p_str2num.addr)
	ICall2(I_CORE_CALL, I_CORE_UTIL_STR2NUM, p_str2num, num)
	jsr __test__

	StorePointer(s_test7, p_str2num.addr)
	ICall2(I_CORE_CALL, I_CORE_UTIL_STR2NUM, p_str2num, num)
	jsr __test__

	StorePointer(s_test8, p_str2num.addr)
	ICall2(I_CORE_CALL, I_CORE_UTIL_STR2NUM, p_str2num, num)
	jsr __test__

	StorePointer(s_test9, p_str2num.addr)
	ICall2(I_CORE_CALL, I_CORE_UTIL_STR2NUM, p_str2num, num)
	jsr __test__

	StorePointer(s_test0, p_str2num.addr)
	ICall2(I_CORE_CALL, I_CORE_UTIL_STR2NUM, p_str2num, num)
	jsr __test__

	StorePointer(s_testa, p_str2num.addr)
	ICall2(I_CORE_CALL, I_CORE_UTIL_STR2NUM, p_str2num, num)
	jsr __test__

        SetAppState(APP_STATE_STOP)
	rts

	__test__:

	StorePointer(num, p_num2str.addr)

	ICall1(I_CORE_CALL, I_CORE_IO_PRINT, p_str2num.addr)
	PrintFromAddress(colon)

	lda #'$'
	sta p_num2str.type
	ICall2(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str, str)
	NPrintFromAddress(str)

	ICall1(I_CORE_CALL, I_CORE_IO_PRINT, p_str2num.addr)
	PrintFromAddress(colon)

	lda #'#'
	sta p_num2str.type
	ICall2(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str, str)
	NPrintFromAddress(str)

	ICall1(I_CORE_CALL, I_CORE_IO_PRINT, p_str2num.addr)
	PrintFromAddress(colon)

	lda #'%'
	sta p_num2str.type
	ICall2(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str, str)
	NPrintFromAddress(str)

	rts

	num: .fill 4, $00
	str: .fill 32, $20

	__AppStateHandler__(APP_STATE_RUN, app_run)

SampleEnd()
