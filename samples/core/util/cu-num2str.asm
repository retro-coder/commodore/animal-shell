
#importonce

/*
*/

#import "samples/_sample.asm"

.filenamespace testapp

SampleStart("cu-num2str", "Test NUM2STR Call")

.segment app_data

p_str2num:

	p_core_util_str2num 


p_num2str:

	p_core_util_num2str 

s_test1:	.text @"0\$00"
s_test2:	.text @"128\$00"
s_test3:	.text @"255\$00"

n_test1:	.byte $00, $00, $00, $00
n_test2:	.byte $87, $38, $00, $00
n_test3:	.byte $ff, $00, $00, $00
n_test4:	.byte $ff, $ff, $00, $00
n_test5:	.byte $ff, $ff, $ff, $00
n_test6:	.byte $ff, $ff, $ff, $ff

.segment app_code

app_run:

	StorePointer(n_test1, p_num2str.addr)
	jsr	__test__
	
	StorePointer(n_test2, p_num2str.addr)
	jsr	__test__
	
	StorePointer(n_test3, p_num2str.addr)
	jsr	__test__
	
	StorePointer(n_test4, p_num2str.addr)
	jsr	__test__
	
	StorePointer(n_test5, p_num2str.addr)
	jsr	__test__
	
	StorePointer(n_test6, p_num2str.addr)
	jsr	__test__
	

        SetAppState(APP_STATE_STOP)
	rts 

	__test__:

	lda	#' '
	sta	p_num2str.type
	ICall2(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str, str)
	NPrintFromAddress(str)

	lda	#'$'
	sta	p_num2str.type
	ICall2(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str, str)
	NPrintFromAddress(str)

	lda	#'%'
	sta	p_num2str.type
	ICall2(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str, str)
	NPrintFromAddress(str)

	lda	#'@'
	sta	p_num2str.type
	ICall2(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str, str)
	NPrintFromAddress(str)

	rts

num: .fill 4, $00
str: .fill 32, $20

__AppStateHandler__(APP_STATE_RUN, app_run)

SampleEnd()
