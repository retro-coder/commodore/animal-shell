
#importonce

#import "samples/_sample.asm"

.filenamespace testapp

SampleStart("ci-print", "Test Print System Calls and Macros")

.segment app_data

        m_print_api:
        .text @"** print using api\$0d"
        .text @"\$0dPaul Wuz Here"
        .text @"\$00"

        m_print_global_macro:
	.text @"\$0d** print using global macro (Print)\$0d\$00"

        m_hello:
	.text @"\$0dhello\$00"

        m_goodbye:
	.text @"\$0dgoodbye\$00"

        blank:
        .text @"\$00"


.segment app_parms "app_parms"

        .pseudopc ADDR_IN {
                in: p_core_io_print_in
        }

.segment app_code

        app_run:

        
        // print using api

	NPrintFromAddress(m_print_api)

        lda	#<m_hello 
        sta	in.string
        lda	#>m_hello 
        sta	in.string + 1

        ICall(I_CORE_CALL, I_CORE_IO_PRINT)

        lda	#<m_goodbye
        sta	in.string 
        lda	#>m_goodbye
        sta	in.string + 1

        ICall(I_CORE_CALL, I_CORE_IO_PRINT)

        // print using global alias

	NPrintFromAddress(m_print_global_macro)
	
        NPrintFromAddress(m_hello)
        NPrintFromAddress(m_goodbye)

        // stop the app

        NPrintFromAddress(blank)        
        NPrintFromAddress(blank)        

        SetAppState(APP_STATE_STOP)

        rts

        __AppStateHandler__(APP_STATE_RUN, app_run)

SampleEnd()

