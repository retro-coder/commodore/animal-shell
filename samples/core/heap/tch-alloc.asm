
#importonce

/*
*/

#import "samples/_sample.asm"

.filenamespace testapp

TestStart("tch-alloc", "Test Heap Allocation")

.segment app_data

i_alloc:

	ds_core_heap_alloc_in 

i_num2str:

	p_core_util_num2str 

o_alloc:

	ds_core_heap_alloc_out 

number:

	.fill 4, $00


.segment app_code

app_run:

	lda	#$10
	sta	i_alloc.size
	ICall2(I_CORE_CALL, I_CORE_HEAP_ALLOC, i_alloc, o_alloc)

	lda	o_alloc.pointer 
	sta	number 
	lda	o_alloc.pointer + 1
	sta	number + 1

	lda	#' '
	sta	i_num2str.type
	StorePointer(number, i_num2str.addr)
	ICall1(I_CORE_CALL, I_CORE_UTIL_NUM2STR, i_num2str)
	
	NPrintFromPointer(ADDR_OUT)

	lda	o_alloc.end
	sta	number 
	lda	o_alloc.end + 1
	sta	number + 1

	lda	#' '
	sta	i_num2str.type
	StorePointer(number, i_num2str.addr)
	ICall1(I_CORE_CALL, I_CORE_UTIL_NUM2STR, i_num2str)
	
	NPrintFromPointer(ADDR_OUT)

        SetAppState(APP_STATE_STOP)
	
	rts 


__AppStateHandler__(APP_STATE_RUN, app_run)

TestEnd()
