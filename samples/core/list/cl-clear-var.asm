
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "samples/_sample.asm"  
#import "cl-macros.asm"

SampleStart("cl-clear-var", "Clear a Variable List")

.segment app_data

	p_clear: p_core_list_clear_in
	p_get: p_core_list_get_in
	p_append: p_core_list_append_in 

	var1:
	.text @"1234\$00"
	
	var2:
	.text @"aaaaaa\$00"
	
	var3:
	.text @"bbb\$00"

	blank:
	.text @" \$00"

	.pseudopc $5000 {
		list:
	}

.segment app_code

	app_run:

	.var length = 5
	.var size = 5
	.var address = $5000

	// create and print out the first 3 list items
 
	CreateVariableListData(address, length, size, false)

	StorePointer(list, p_get.list)
	lda	#0
	sta	p_get.index
	ICall1(I_CORE_CALL, I_CORE_LIST_GET, p_get)
	NPrintFromPointer(ADDR_OUT)
	
	lda	#1
	sta	p_get.index
	ICall1(I_CORE_CALL, I_CORE_LIST_GET, p_get)
	NPrintFromPointer(ADDR_OUT)

	lda	#2
	sta	p_get.index
	ICall1(I_CORE_CALL, I_CORE_LIST_GET, p_get)
	NPrintFromPointer(ADDR_OUT)

	StorePointer(list, p_clear.list)
	lda	#0
	sta	p_clear.options

	ICall1(I_CORE_CALL, I_CORE_LIST_CLEAR, p_clear)

	// append three new list items

	StorePointer(list, p_append.list)
	StorePointer(var1, p_append.item)
	ICall1(I_CORE_CALL, I_CORE_LIST_APPEND, p_append)
	
	StorePointer(var2, p_append.item)
	ICall1(I_CORE_CALL, I_CORE_LIST_APPEND, p_append)

	StorePointer(var3, p_append.item)
	ICall1(I_CORE_CALL, I_CORE_LIST_APPEND, p_append)

	// print the three new list items

	NPrintFromAddress(blank)	

	StorePointer(list, p_get.list)
	lda	#0
	sta	p_get.index
	ICall1(I_CORE_CALL, I_CORE_LIST_GET, p_get)
	NPrintFromPointer(ADDR_OUT)
	
	lda	#1
	sta	p_get.index
	ICall1(I_CORE_CALL, I_CORE_LIST_GET, p_get)
	NPrintFromPointer(ADDR_OUT)

	lda	#2
	sta	p_get.index
	ICall1(I_CORE_CALL, I_CORE_LIST_GET, p_get)
	NPrintFromPointer(ADDR_OUT)

	SetAppState(APP_STATE_STOP)

	rts

	__AppStateHandler__(APP_STATE_RUN, app_run)

SampleEnd()
