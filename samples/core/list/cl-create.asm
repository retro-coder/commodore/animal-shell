
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "samples/_sample.asm"  

SampleStart("cl-create", "Create a List")

.segment app_data

	.pseudopc $5000 {
		list:
	}
	

.segment app_parms

	.pseudopc ADDR_IN {
		create_in: p_core_list_create_in 
	}


.segment app_code

	app_run:

	// location of list

	StorePointer(list, create_in.list)

	// each item is 5 bytes

	lda	#5
	sta	create_in.length

	// the list has 5 items

	lda	#5
	sta	create_in.size

	// storage type standard

	lda	#0
	sta	create_in.storage
	sta	create_in.delim

	ICall(I_CORE_CALL, I_CORE_LIST_CREATE)

	SetAppState(APP_STATE_STOP)

      rts

	__AppStateHandler__(APP_STATE_RUN, app_run)

SampleEnd()
