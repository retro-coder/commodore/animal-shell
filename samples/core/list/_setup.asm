#importonce 

#import "core/core.asm"
#import "app/app.asm"

.filenamespace testapp

.segment app_data


.segment app_parms

	.pseudopc ADDR_IN { create_in:	p_core_list_create_in }
	.pseudopc ADDR_IN { append_in:	p_core_list_append_in }

.segment app_data

	test_item1:	.text @"AAAA\$00"
	test_item2:	.text @"BBBB\$00"
	test_item3:	.text @"CCCC\$00"
	test_item4:	.text @"DDDD\$00"
	test_item5:	.text @"EEEE\$00"

	.const __list__ = $5000
	
.segment app_code

	StorePointer(__list__, create_in.address)

	// each item is 5 bytes

	lda	#05
	sta	create_in.length

	// the list has 5 items

	lda	#05
	sta	create_in.size

	ICall1(I_CORE_CALL, I_CORE_LIST_CREATE, create_in)

	// add item 1

	StorePointer(__list__, append_in.address)
	StorePointer(test_item1, append_in.item)
	ICall1(I_CORE_CALL, I_CORE_LIST_APPEND, append_in)

	// add item 2

	StorePointer(__list__, append_in.address)
	StorePointer(test_item2, append_in.item)
	ICall1(I_CORE_CALL, I_CORE_LIST_APPEND, append_in)

	// add item 3

	StorePointer(__list__, append_in.address)
	StorePointer(test_item3, append_in.item)
	ICall1(I_CORE_CALL, I_CORE_LIST_APPEND, append_in)

	// add item 4

	StorePointer(__list__, append_in.address)
	StorePointer(test_item4, append_in.item)
	ICall1(I_CORE_CALL, I_CORE_LIST_APPEND, append_in)

	// add item 5

	StorePointer(__list__, append_in.address)
	StorePointer(test_item5, append_in.item)
	ICall1(I_CORE_CALL, I_CORE_LIST_APPEND, append_in)
