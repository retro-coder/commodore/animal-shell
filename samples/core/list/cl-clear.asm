
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "samples/_sample.asm"  
#import "cl-macros.asm"

SampleStart("cl-clear", "Clear a List")

.segment app_data

	p_clear: p_core_list_clear_in
	p_get: p_core_list_get_in
	p_append: p_core_list_append_in 
	
	hello:
	.text @"1234\$00"

	.pseudopc $5000 {
		list:
	}

.segment app_code

	app_run:

	.var length = 5
	.var size = 5
	.var address = $5000

	CreateListData(address, length, size, false)

	StorePointer(list, p_get.list)
	lda	#0
	sta	p_get.index

	ICall1(I_CORE_CALL, I_CORE_LIST_GET, p_get)
	NPrintFromPointer(ADDR_OUT)
	
	StorePointer(list, p_clear.list)
	lda	#0
	sta	p_clear.options

	ICall1(I_CORE_CALL, I_CORE_LIST_CLEAR, p_clear)

	StorePointer(list, p_append.list)
	StorePointer(hello, p_append.item)
	ICall1(I_CORE_CALL, I_CORE_LIST_APPEND, p_append)
	
	StorePointer(list, p_get.list)
	lda	#0
	sta	p_get.index

	ICall1(I_CORE_CALL, I_CORE_LIST_GET, p_get)
	NPrintFromPointer(ADDR_OUT)

	SetAppState(APP_STATE_STOP)

	rts

__AppStateHandler__(APP_STATE_RUN, app_run)

SampleEnd()
