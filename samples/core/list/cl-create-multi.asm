
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "samples/_sample.asm"  
#import "cl-macros.asm"

SampleStart("cl-create-multi", "Create Multiple Lists")

.segment app_data

	.pseudopc $5000 {
		list1:
	}
	
	.pseudopc $5200 {
		list2:
	}
	
	.pseudopc $5400 {
		list3:
	}
	
	.pseudopc $e000 {
		list4:
	}
	

.segment app_parms

	.pseudopc ADDR_IN {
		create_in: p_core_list_create_in 
	}


.segment app_code

	app_run:

	// ===== LIST1

	// list address

	StorePointer(list1, create_in.list)

	// each item is 5 bytes

	lda	#5
	sta	create_in.length

	// the list has 5 items

	lda	#5
	sta	create_in.size

	ICall(I_CORE_CALL, I_CORE_LIST_CREATE)

	// ===== LIST2

	// list address

	StorePointer(list2, create_in.list)

	// each item is 10 bytes

	lda	#1
	sta	create_in.length

	// the list has 10 items

	lda	#10
	sta	create_in.size

	ICall(I_CORE_CALL, I_CORE_LIST_CREATE)
	
	// ===== LIST3

	// list address

	StorePointer(list3, create_in.list)

	// each item is 1 bytes

	lda	#16
	sta	create_in.length

	// the list has 32 items

	lda	#20
	sta	create_in.size

	ICall(I_CORE_CALL, I_CORE_LIST_CREATE)
	
	// ===== LIST4

	// list address

	StorePointer(list4, create_in.list)

	// each item is 2 bytes

	lda	#2
	sta	create_in.length

	// the list has 64 items

	lda	#64
	sta	create_in.size

	ICall(I_CORE_CALL, I_CORE_LIST_CREATE)	

	// exit

      SetAppState(APP_STATE_STOP)

      rts

__AppStateHandler__(APP_STATE_RUN, app_run)

SampleEnd()
