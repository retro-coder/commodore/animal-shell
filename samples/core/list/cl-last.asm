
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "samples/_sample.asm"  
#import "cl-macros.asm"

SampleStart("cl-last", "Get Last Item From A List")

	.var length = 5
	.var size = 5
	.var address = $5000

.segment app_data

	p_create: p_core_list_create_in
	p_append: p_core_list_append_in
	p_get: p_core_list_get_in

	blank:
	.text @"\$00"

	.pseudopc address {
		list:
	}

	__AppStateHandler__(APP_STATE_RUN, app_run)

.segment app_code

	app_run:

	CreateListData(address, length, size, false)

	StorePointer(list, p_get.list)
	ICall1(I_CORE_CALL, I_CORE_LIST_LAST, p_get)
	NPrintFromPointer(ADDR_OUT)

	SetAppState(APP_STATE_STOP)

	rts

SampleEnd()
