
#importonce 

#import "system/sys.asm"
#import "core/core.asm"
#import "shell/shell.asm"

.var string_data = List()

.eval string_data.add('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9', '$', '#', '%', '@', '!', '-', '=', '+', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z')

.var Z_DATA = $03

.macro CreateListData(address, length, size, rand) {
	
	.segment app_data "__data__"

	__data__:

	.for (var i = 0; i < size; i++) {

		.var rname = ""

		.eval string_data.shuffle()

		.if (rand == true) {

			.for (var j = 0; j < length - 1 ; j++) {
				.eval rname += string_data.get(j)
			}

		} else {

			.var char = string_data.get(0)

			.for (var j = 0; j < length - 1 ; j++) {
				.eval rname += char
			}
		}

		.eval rname += @"\$00"

      .print (rname)

		.text rname

	}

	.segment app_parms

		.pseudopc ADDR_IN { __create_in__:	p_core_list_create_in }
		.pseudopc ADDR_IN { __append_in__:	p_core_list_append_in }

	.segment app_data

		.pseudopc address { __list__:	}

      	.pseudopc Z_DATA { t_data: .word $0000 }

	.segment app_code

		StorePointer(__list__, __create_in__.list)

		lda	#length
		sta	__create_in__.length

		lda	#size
		sta	__create_in__.size

		lda	#0
		sta	__create_in__.storage

		lda   #0
		sta	__create_in__.delim

		ICall1(I_CORE_CALL, I_CORE_LIST_CREATE, __create_in__)

		StorePointer(__list__, __append_in__.list)

		lda     #<__data__
		sta     t_data
		lda     #>__data__ + 1
		sta     t_data + 1

		lda     t_data
		sta     __append_in__.item
		lda     t_data + 1
		sta     __append_in__.item + 1

		ICall1(I_CORE_CALL, I_CORE_LIST_APPEND, __append_in__)


		ldx     #size

		!:

		inc     t_data
		bne     !+
		inc     t_data + 1

		!:

		ldy     #$00
		lda     (t_data), y
		bne     !--

		inc     t_data
		bne     !+
		inc     t_data + 1

		!:

		lda     t_data
		sta     __append_in__.item
		lda     t_data + 1
		sta     __append_in__.item + 1
		lda	  #0
		sta	  __append_in__.length

		txa
		pha

		ICall1(I_CORE_CALL, I_CORE_LIST_APPEND, __append_in__)

		pla
		tax

		dex
		bne     !---

}

.macro CreateVariableListData(address, length, size, rand) {

	.segment app_data "__data__"

	__data__:

	.for (var i = 0; i < size; i++) {

		.var rname = ""

		.eval string_data.shuffle()

		.if (rand == true) {

			.for (var j = 0; j < random() * length ; j++) {
				.eval rname += string_data.get(j)
			}

		} else {

			.var char = string_data.get(0)

			.for (var j = 0; j < random() * length ; j++) {
				.eval rname += char
			}
		}

		.eval rname += @"\$00"
.print (rname)

       .print (rname)

		.text rname

	}

	.segment app_parms

		.pseudopc ADDR_IN { __create_in__:	p_core_list_create_in }
		.pseudopc ADDR_IN { __append_in__:	p_core_list_append_in }

	.segment app_data

		.pseudopc address { __list__:	}

      	.pseudopc Z_DATA { t_data: .word $0000 }

	.segment app_code

		StorePointer(__list__, __create_in__.list)

		lda	#length
		sta	__create_in__.length

		lda	#size
		sta	__create_in__.size

		lda	#1
		sta	__create_in__.storage

		lda   #0
		sta	__create_in__.delim
		
		ICall1(I_CORE_CALL, I_CORE_LIST_CREATE, __create_in__)

		StorePointer(__list__, __append_in__.list)

		lda     #<__data__
		sta     t_data
		lda     #>__data__ + 1
		sta     t_data + 1

		lda     t_data
		sta     __append_in__.item
		lda     t_data + 1
		sta     __append_in__.item + 1
		lda	  #0
		sta	  __append_in__.length

		ICall1(I_CORE_CALL, I_CORE_LIST_APPEND, __append_in__)

		ldx     #size

		!:

		inc     t_data
		bne     !+
		inc     t_data + 1

		!:

		ldy     #$00
		lda     (t_data), y
		bne     !--

		inc     t_data
		bne     !+
		inc     t_data + 1

		!:

		lda     t_data
		sta     __append_in__.item
		lda     t_data + 1
		sta     __append_in__.item + 1

		txa
		pha

		ICall1(I_CORE_CALL, I_CORE_LIST_APPEND, __append_in__)

		pla
		tax

		dex
		bne     !---

}
