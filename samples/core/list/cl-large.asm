
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "samples/_sample.asm"  
#import "cl-macros.asm"

SampleStart("cl-large", "Append Items to a Large List")

.segment app_data

        p_create: p_core_list_create_in
        p_append: p_core_list_append_in
	p_get: p_core_list_get_in
        t_next: .byte $00

        blank:
        .text @"\$00"

	.pseudopc $5000 {
                list:
        }

        __AppStateHandler__(APP_STATE_START, app_start)
        __AppStateHandler__(APP_STATE_RUN, app_run)

.segment app_code

app_start:


        .var length = 16
        .var size = 128
        .var address = $5000

        CreateListData(address, length, size, true)

	StorePointer(list, p_get.list)

        lda     #$00
        sta     t_next

        rts


app_run:

        // store next index
        lda     t_next
	sta	p_get.index

        // get the list item address and print
	ICall1(I_CORE_CALL, I_CORE_LIST_GET, p_get)

	NPrintFromPointer(ADDR_OUT)

        // increase index and check if we are at end of list
        inc     t_next
        lda     t_next
        cmp     #size

        // continue
        bne     !+

        // exit

        SetAppState(APP_STATE_STOP)

        !:

        rts

SampleEnd()
