
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "samples/_sample.asm"  
#import "cl-macros.asm"

SampleStart("cl-info", "Show List Information")

.segment app_data

	p_info_in: p_type_pointer 
	ds_info: ds_core_list_info 

	.pseudopc $5000 {
		list:
	}

.segment app_code

	app_run:

	.var length = 5
	.var size = 5
	.var address = $5000

	CreateListData(address, length, size, false)

	StorePointer(list, p_info_in)
	ICall2(I_CORE_CALL, I_CORE_LIST_INFO, p_info_in, ds_info)

	SetAppState(APP_STATE_STOP)

	rts

	__AppStateHandler__(APP_STATE_RUN, app_run)

SampleEnd()
