
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.encoding "petscii_mixed"

#import "samples/_sample.asm"

.filenamespace testapp

SampleStart("cf-getc2", "Test Reading from Two Open Files Using GETC")

.segment app_data

	filename1:
	.text @"test1,s\$00"

	filename2:
	.text @"test2,s\$00"

	p_open:	p_core_file_open_in 
	p_open_out: .byte $00
	p_getc_in1: p_core_file_getc_in 
	p_getc_in2: p_core_file_getc_in

	blank:
	.text @"\$00"

.segment app_parms 

	__AppStateHandler__(APP_STATE_RUN, app_run)

.segment app_code

	app_run:

	StorePointer(filename1, p_open.filename)
	lda	$ba
	sta	p_open.device
	lda	#$01
	sta	p_open.show
	ICall2(I_CORE_CALL, I_CORE_FILE_OPEN, p_open, p_open_out)

	lda	p_open_out
	sta 	p_getc_in1.file

	StorePointer(filename2, p_open.filename)
	lda	$ba
	sta	p_open.device
	lda	#$01
	sta	p_open.show
	ICall2(I_CORE_CALL, I_CORE_FILE_OPEN, p_open, p_open_out)

	lda	p_open_out
	sta 	p_getc_in2.file

	!:

	ICall1(I_CORE_CALL, I_CORE_FILE_GETC, p_getc_in1)

	bcs	eof1

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)
	lda	ADDR_OUT
	jsr	JBSOUT
	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	jmp	!-

	eof1:

	NPrintFromAddress(blank)

	!:

	ICall1(I_CORE_CALL, I_CORE_FILE_GETC, p_getc_in2)

	bcs	eof2

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)
	lda	ADDR_OUT
	jsr	JBSOUT
	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	jmp	!-

	eof2:

	close1:

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)
	lda	p_getc_in1.file
	jsr	JCLOSE
	// jsr	JCLRCH
	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)
        
	close2:

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)
	lda	p_getc_in2.file
	jsr	JCLOSE
	// jsr	JCLRCH
	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	SetAppState(APP_STATE_STOP)

      rts

SampleEnd()
