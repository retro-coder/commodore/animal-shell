
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.encoding "petscii_mixed"

#import "samples/_sample.asm"

.filenamespace testapp

SampleStart("cf-open", "Test OPEN Call")

.segment app_data

	filename:
	.text @"test1-seq,s\$00"

	p_open:	p_core_file_open_in 
	p_open_out: p_core_file_open_out

	blank:
	.text @"\$00"

.segment app_parms 

	__AppStateHandler__(APP_STATE_RUN, app_run)

.segment app_code

	app_run:

	StorePointer(filename, p_open.filename)
	lda	$ba
	sta	p_open.device
	lda	#$01
	sta	p_open.show
	
	ICall2(I_CORE_CALL, I_CORE_FILE_OPEN, p_open, p_open_out)

	ldx	p_open_out.file

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)

	jsr	$ffc6

	lda	#<ADDR_WORK
	sta	$ae
	lda	#>ADDR_WORK
	sta	$af

	ldy	#$00

	!:

	jsr	$ffb7
	bne	eof
	jsr	$ffcf
	jsr	$ffd2
	sta	($ae),y
	inc	$ae
	bne	!+
	inc	$af

	!:

	jmp	!--

	eof:

	and	#$40
	bne	close

	jmp	error

	close:

	lda	p_open_out.file
	jsr	JCLOSE

	// jsr	$ffcc

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	NPrintFromAddress(blank)
	NPrintFromAddress(blank)
        
	SetAppState(APP_STATE_STOP)

      	rts

	error:

	__LogError__("TEST ERROR!")

	jmp	close

SampleEnd()
