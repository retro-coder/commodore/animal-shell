
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

.encoding "petscii_mixed"

#import "samples/_sample.asm"

.filenamespace testapp

SampleStart("cf-getc", "Test GETC Call")

.segment app_data

	filename:
	.text @"test1,s\$00"

	p_open: p_core_file_open_in 
	p_open_out: p_core_file_open_out 
	p_getc_in: p_core_file_getc_in 

.segment app_parms 

	__AppStateHandler__(APP_STATE_RUN, app_run)

.segment app_code

	app_run:

	StorePointer(filename, p_open.filename)
	
	// open file on current device number
	
	lda	$ba
	sta	p_open.device
	lda	#$01
	sta	p_open.show
	
	ICall2(I_CORE_CALL, I_CORE_FILE_OPEN, p_open, p_open_out)

	lda	p_open_out
	sta 	p_getc_in.file

	!:

	ICall1(I_CORE_CALL, I_CORE_FILE_GETC, p_getc_in)

	bcs	eof

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)
	lda	ADDR_OUT
	jsr	$ffd2
	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	jmp	!-

	eof:

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)
	lda	p_open_out
	jsr	JCLOSE
	// jsr	$ffcc
	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)
        
	SetAppState(APP_STATE_STOP)

      rts

SampleEnd()
