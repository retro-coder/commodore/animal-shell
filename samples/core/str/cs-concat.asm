
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "samples/_sample.asm"  

SampleStart("cs-concat", "Concatenate Strings")

.segment app_data

	the:
	.text @"the\$00"

	quick:
	.text @"quick\$00"

	brown:
	.text @"brown\$00"

	fox:
	.text @"fox\$00"

	jumped:
	.text @"jumped\$00"

	over:
	.text @"over\$00"

	fence:
	.text @"fence\$00"

	newstr:
	.fill 64, $00

.segment app_parms

	p_concat: p_core_str_concat_in 

.segment app_code

app_run:

	lda	#0
	sta	p_concat.options

	jsr	test_concat

	lda	#1
	sta	p_concat.options

	jsr	test_concat

	SetAppState(APP_STATE_STOP)

	rts

	test_concat:

	StorePointer(the, p_concat.stringa)
	StorePointer(quick, p_concat.stringb)
	ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, newstr)

	StorePointer(newstr, p_concat.stringa)
	StorePointer(brown, p_concat.stringb)
	ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, newstr)

	StorePointer(newstr, p_concat.stringa)
	StorePointer(fox, p_concat.stringb)
	ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, newstr)

	StorePointer(newstr, p_concat.stringa)
	StorePointer(jumped, p_concat.stringb)
	ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, newstr)

	StorePointer(newstr, p_concat.stringa)
	StorePointer(over, p_concat.stringb)
	ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, newstr) 

	StorePointer(newstr, p_concat.stringa)
	StorePointer(the, p_concat.stringb)
	ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, newstr)

	StorePointer(newstr, p_concat.stringa)
	StorePointer(fence, p_concat.stringb)
	ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, newstr)

	NPrintFromAddress(newstr)

	rts

	__AppStateHandler__(APP_STATE_RUN, app_run)

SampleEnd()
