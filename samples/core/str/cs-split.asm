
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "samples/_sample.asm"

.encoding "petscii_mixed"

SampleStart("cs-split", "Split Strings")

.segment app_data

	p_str_split: p_core_str_split_in 
	p_list:	p_core_list_in 
	p_list_create: p_core_list_create_in
	p_list_clear: p_core_list_clear_in
	

.segment app_data

	string0:
	.text @"the quick brown fox\$00"

	string1:
	// .text @"paul \"wuz\" here\$00"
	.text @"paul \"wuz also\" here\$00"
	// .text @"paul hocker\$00"

	string2:
	.text @"run program1 -v -a -b -c -d -e --version\$00"

	string3:
	.text @"load drivername\$00"

	string4:
	.text @"ls -a -s -l\$00"

	string5:
	.text @"cd 8:/paul/misc/games/t/tetris\$00"

	string6:
	.text @"run \"hello world\"\$00"

	blank:
	.text @"\$00"

	.pseudopc $5000 {
		list:
	}

.segment app_code

	app_run:

	// create the list to populate

	StorePointer(list, p_list_create.list)
	
	lda	#32
	sta	p_list_create.length
	lda	#10
	sta	p_list_create.size
	lda	#1
	sta	p_list_create.storage
	lda	#0
	sta	p_list_create.delim

	ICall1(I_CORE_CALL, I_CORE_LIST_CREATE, p_list_create)

	// set address of list

	StorePointer(list, p_str_split.list)
	StorePointer(list, p_list.list)
	StorePointer(list, p_list_clear.list)

	// setup some parms

	lda	#0
	sta	p_list_clear.options
	lda	#1
	sta	p_str_split.options

	lda	#$20
	sta	p_str_split.delim

	// split string0

	NPrintFromAddress(string0)
	
	StorePointer(string0, p_str_split.string)
	ICall1(I_CORE_CALL, I_CORE_STR_SPLIT, p_str_split)

	jsr print_list

	// split string1

	NPrintFromAddress(string1)
	
	StorePointer(string1, p_str_split.string)
	ICall1(I_CORE_CALL, I_CORE_STR_SPLIT, p_str_split)

	jsr print_list

	// split string2

	StorePointer(string2, p_str_split.string)
	ICall1(I_CORE_CALL, I_CORE_STR_SPLIT, p_str_split)

	jsr	print_list
	
	// split string3

	NPrintFromAddress(string3)

	StorePointer(string3, p_str_split.string)
	ICall1(I_CORE_CALL, I_CORE_STR_SPLIT, p_str_split)

	jsr	print_list
	
	// split string4

	NPrintFromAddress(string4)

	StorePointer(string4, p_str_split.string)
	ICall1(I_CORE_CALL, I_CORE_STR_SPLIT, p_str_split)

	jsr	print_list
	
	// split string5

	NPrintFromAddress(string5)

	StorePointer(string5, p_str_split.string)
	ICall1(I_CORE_CALL, I_CORE_STR_SPLIT, p_str_split)

	jsr	print_list
	
	// split string6

	NPrintFromAddress(string6)

	StorePointer(string6, p_str_split.string)
	ICall1(I_CORE_CALL, I_CORE_STR_SPLIT, p_str_split)

	jsr	print_list

	end:
	
	SetAppState(APP_STATE_STOP)

	rts

	print_list:

	// print first item

	ICall1(I_CORE_CALL, I_CORE_LIST_FIRST, p_list)
	bcs	!++
	NPrintFromPointer(ADDR_OUT)

	!:

	ICall1(I_CORE_CALL, I_CORE_LIST_NEXT, p_list)
	bcs	!+
	NPrintFromPointer(ADDR_OUT)

	jmp	!-

	!:
	
	NPrintFromAddress(blank)
	NPrintFromAddress(blank)

	ICall1(I_CORE_CALL, I_CORE_LIST_CLEAR, p_list_clear)
	
	rts

	__AppStateHandler__(APP_STATE_RUN, app_run)

SampleEnd()
