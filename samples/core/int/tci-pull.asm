#importonce

#import "samples/_sample.asm"

.filenamespace testapp

TestStart("tci-push", "Test Zero Stack Pull")

.segment app_data

stack1:	.fill 32, $01
stack2:	.fill 32, $02
stack3:	.fill 32, $03
stack4:	.fill 32, $04

.segment app_parms

.segment app_code

app_run:

	StorePointer(stack1, $40)
	jsr	copy_to_stack
	jsr	__core__.__internal__.__stack__.push

	StorePointer(stack2, $40)
	jsr	copy_to_stack
	jsr	__core__.__internal__.__stack__.push

	StorePointer(stack3, $40)
	jsr	copy_to_stack
	jsr	__core__.__internal__.__stack__.push

	StorePointer(stack4, $40)
	jsr	copy_to_stack
	jsr	__core__.__internal__.__stack__.pull

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)
	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)

	SetAppState(APP_STATE_STOP)

      rts

copy_to_stack:

	ldx	#32
	ldy	#0

	!:

	lda	($40), y
	sta	CORE_ZERO_TEMP, y

	iny
	dex

	bne	!-

	rts

__AppStateHandler__(APP_STATE_RUN, app_run)

TestEnd()
