
#importonce

#import "samples/_sample.asm"  

SampleStart("xm-loaddrv", "Test Shell Load Driver")

.segment app_data


	mem_alloc: p_core_mem_alloc_in 
	mem_alloc_out: p_core_mem_alloc_out
	mem_stash: p_core_mem_stash_in 
	mem_fetch: p_core_mem_fetch_in 
	file_load: p_core_file_load_in 
	page1: p_type_byte
	page2: p_type_byte

.pseudopc $5000 {
	driver:
	.fill 256,$ee
}

.pseudopc $6000 {
	library:
	.fill 256,$ee
}


.encoding "petscii_mixed"

	filename_d:
	.text @"driver.d\$00"

	filename_l:
	.text @"library.l\$00"

	status:
	.text @".\$0d\$00"


	__AppStateHandler__(APP_STATE_RUN, app_run)
	__AppStateHandler__(APP_STATE_STOP, app_stop)


.segment app_code

	app_run:


	// load driver into memory

	StorePointer(filename_d, file_load.filename)
	lda #8
	sta file_load.device
	lda #0
	sta file_load.show 

	ICall1(I_CORE_CALL, I_CORE_FILE_LOAD, file_load)

	jsr print_status

	// allocate 12 pages

	lda #12
	sta mem_alloc.size

	ICall2(I_CORE_CALL, I_CORE_MEM_ALLOC, mem_alloc, mem_alloc_out)

	jsr print_status

	// stash to expanded memory

	lda mem_alloc_out.bank
	sta mem_stash.mem_bank
	lda mem_alloc_out.page
	sta mem_stash.mem_page
	lda #12
	sta mem_stash.mem_size

	lda #$28
	sta mem_stash.addr_page
	lda #0
	sta mem_stash.addr_bank	

	ICall1(I_CORE_CALL, I_CORE_MEM_STASH, mem_stash)

	jsr print_status

	// pull first page from expanded memory

	lda mem_alloc_out.bank
	sta mem_fetch.mem_bank 
	lda mem_alloc_out.page
	sta mem_fetch.mem_page
	lda #12
	sta mem_fetch.mem_size 
	lda #>driver
	sta mem_fetch.addr_page
	lda #0
	sta mem_fetch.addr_bank

	ICall1(I_CORE_CALL, I_CORE_MEM_FETCH, mem_fetch)	

	jsr print_status

	next:

	// load library into memory

	StorePointer(filename_l, file_load.filename)
	lda #8
	sta file_load.device
	lda #0
	sta file_load.show 

	ICall1(I_CORE_CALL, I_CORE_FILE_LOAD, file_load)

	jsr print_status

	// allocate 12 pages

	lda #12
	sta mem_alloc.size

	ICall2(I_CORE_CALL, I_CORE_MEM_ALLOC, mem_alloc, mem_alloc_out)

	jsr print_status

	// stash to expanded memory

	lda mem_alloc_out.bank
	sta mem_stash.mem_bank
	lda mem_alloc_out.page
	sta mem_stash.mem_page
	lda #12
	sta mem_stash.mem_size

	lda #$34
	sta mem_stash.addr_page
	lda #0
	sta mem_stash.addr_bank	

	ICall1(I_CORE_CALL, I_CORE_MEM_STASH, mem_stash)

	jsr print_status

	// pull first page from expanded memory

	lda mem_alloc_out.bank
	sta mem_fetch.mem_bank 
	lda mem_alloc_out.page
	sta mem_fetch.mem_page
	lda #12
	sta mem_fetch.mem_size 
	lda #>library
	sta mem_fetch.addr_page
	lda #0
	sta mem_fetch.addr_bank

	ICall1(I_CORE_CALL, I_CORE_MEM_FETCH, mem_fetch)	

	jsr print_status

	// check that the headers are correct

	
	SetAppState(APP_STATE_STOP)

	rts

	print_status:

	PrintFromAddress(status)
	rts

	app_stop:

	rts

SampleEnd()
