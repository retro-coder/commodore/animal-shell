
#importonce

#import "samples/_sample.asm"  

SampleStart("appinfo", "Print Application Header Info")

.segment app_virtual

	.pseudopc ADDR_APP_INFO { 
		app_info: ds_app_info 
	}
	

.segment app_data

.encoding "petscii_mixed"

__AppStateHandler__(APP_STATE_RUN, app_run)

.segment app_code

app_run:

	NPrintFromPointer(app_info.name)
	NPrintFromPointer(app_info.description)
	NPrintFromPointer(app_info.copyright)
	NPrintFromPointer(app_info.author)
	NPrintFromPointer(app_info.website)
	NPrintFromPointer(app_info.email)

	SetAppState(APP_STATE_STOP)

	rts

SampleEnd()
