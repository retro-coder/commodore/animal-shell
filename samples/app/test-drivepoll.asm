
/*

https://codebase64.org/doku.php?id=base:detect_drives_on_iec_bus&s[]=drive&s[]=polling


*/

#importonce

#import "samples/_sample.asm"

.filenamespace testapp

.label ldv = $fb
.label status = $90
// .label dv = $ba

.label second = $ff93
.label tksa   = $ff96
.label acptr  = $ffa5
.label ciout  = $ffa8
.label untalk = $ffab
.label unlstn = $ffae
.label listen = $ffb1
.label talk   = $ffb4
.label readst = $ffb7
.label setlfs = $ffba
.label setnam = $ffbd
.label open   = $ffc0
.label close  = $ffc3
.label chkin  = $ffc6
.label chkout = $ffc9
.label clrchn = $ffcc
.label chrin  = $ffcf
.label chrout = $ffd2
.label load   = $ffd5
.label save   = $ffd8
.label getin  = $ffe4
.label clall  = $ffe7

SampleStart("test-drivepoll", "Test Drive Polling")

.segment app_data

	dv: .byte 0
	drives:	.fill 64,$ee

	s_drive:
	.text @"Drive #\$00"

	num1: .byte $00, $00, $00, $00

	p_num2str: p_core_util_num2str 

	drive_num: .byte $00
	drive_type: .byte $00

	str1: .fill 8,$ee
	str2: .fill 8,$ef

	drive_41: .text @"1541\$00"
	drive_71: .text @"1571\$00"
	drive_81: .text @"1581\$00"
	drive_c0: .text @"CMD-HD\$00"
	drive_01: .text @"VICE\$00"
	drive_00: .text @"UNKNOWN\$00"

	space: .text @" \$00"


.segment app_code

	app_run:

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)

	ldy	#23
	lda	#0

	!:

	sta	drives+8, y
	dey
	bpl	!-

	lda	dv
	sta	ldv

	lda	#08
	sta	dv

	!:

	ldy	#0
	sty	status
	jsr	listen
	lda	#$ff
	jsr	second
	lda	status
	bpl	!++

	!:

	jsr	unlstn

	inc	dv
	lda	dv
	cmp	#31
	bne	!--
	beq	!++

	!:

	ldy	dv
	tya	
	sta	drives, y
	bne	!--

	!:

	ldy	#14
	lda	#0
	sta	drives,y

	ldy	#8

	l1:

	lda	drives,y
	bne	!+
	iny
	cpy	#31
	bne	l1
	jmp	end

	!:

	sta	dv
	jsr	open_cmd_channel
	ldx	#<cmdinfo 
	ldy	#>cmdinfo
	jsr	open_cmd_two

	jsr	chrin
	cmp	#70
	bne	!+
	jsr	chrin
	cmp	#68
	bne	l2
	ldy	dv
	lda	#$e0
	jmp	get_next_drive

	!:

	cmp	#72
	bne	!+
	jsr	chrin
	cmp	#68
	bne	l2
	ldy	dv
	lda	#$c0
	bne	get_next_drive

	!:

	cmp	#82
	bne	l2
	jsr	chrin
	cmp	#68
	bne	!+
	ldy	dv
	lda	#$f0
	bne	get_next_drive

	!:

	cmp	#76
	bne	l2
	ldy	dv
	lda	#$80
	bne	get_next_drive

	l2:

	jsr	close_cmd_channel
	jsr	open_cmd_channel
	ldx	#<cbminfo
	ldy	#>cbminfo
	jsr	open_cmd_two

	jsr	chrin
	cmp	#53
	bne	l3
	jsr	chrin
	cmp	#52
	bne	!+
	ldy	dv
	lda	#$41
	bne	get_next_drive
	!:

	cmp	#55
	bne	l3
	ldy	dv
	lda	#$71
	bne	get_next_drive


	l3:

	jsr	close_cmd_channel
	jsr	open_cmd_channel
	ldx	#<info1581
	ldy	#>info1581
	jsr	open_cmd_two

	jsr	chrin
	cmp	#53
	bne	l4
	jsr	chrin
	cmp	#56
	bne	l4
	ldy	dv
	lda	#$81
	bne	get_next_drive

	l4:

	ldy	dv
	lda	#$01
	bne	get_next_drive

	close_cmd_channel:

	jsr	clrchn
	lda	#$0f
	jsr	close
	rts	

	get_next_drive:

	sta	drives, y
	jsr	close_cmd_channel
	ldy	dv
	iny
	jmp	l1

	open_cmd_channel:

	lda	#$0f
	tay
	ldx	dv
	jsr	setlfs
	lda	#$07
	rts	

	open_cmd_two:

	jsr	setnam
	jsr	open
	ldx	#$0f
	jsr	chkin
	rts	

	end:

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	// print the drives

	lda	#8
	sta	drive_num

	loop:

	lda	drive_num
	tax
	lda	drives, x

	bne !+
	jmp next

	!:

	sta drive_type

	// convert drive type code to string

	// sta num1
	// StorePointer(num1, p_num2str.addr)
	// lda #'$'
	// sta p_num2str.type
	// ICall2(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str, str2)

	// convert device #
	lda drive_num
	sta num1
	StorePointer(num1, p_num2str.addr)
	lda #' '
	sta p_num2str.type
	ICall2(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str, str1)


	PrintFromAddress(s_drive)
	PrintFromAddress(str1)
	PrintFromAddress(space)
	

	lda drive_type

	cmp #$41
	bne !+
	NPrintFromAddress(drive_41)
	jmp next

	!:
	cmp #$71
	bne !+
	NPrintFromAddress(drive_71)
	jmp next
	
	!:
	cmp #$81
	bne !+
	NPrintFromAddress(drive_81)
	jmp next

	!:
	cmp #$c0
	bne !+
	NPrintFromAddress(drive_c0)
	jmp next

	!:
	cmp #$01
	bne !+
	NPrintFromAddress(drive_01)
	jmp next

	!:
	NPrintFromAddress(drive_00)

	next:

	inc	drive_num
	lda	drive_num
	cmp	#30

	beq !+
	jmp loop

	!:

	SetAppState(APP_STATE_STOP)

	rts

app_stop:

	rts

cmdinfo:
	.text "m-r"
	.byte $a4,$fe,$02,$0d

cbminfo:
	.text "m-r"
	.byte $c5,$e5,$02,$0d

info1581:

	.text "m-r"
	.byte $e8,$a6,$02,$0d

__AppStateHandler__(APP_STATE_RUN, app_run)
__AppStateHandler__(APP_STATE_STOP, app_stop)

SampleEnd()
