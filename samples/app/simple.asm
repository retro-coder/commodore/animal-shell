
#importonce

#import "samples/_sample.asm"  

SampleStart("simple", "Simple App")

.segment app_data

.encoding "petscii_mixed"

hello:

	.text @"Hello World\$0d\$00"

goodbye:

	.text @"Goodbye World\$0d\$00"


__AppStateHandler__(APP_STATE_RUN, app_run)
__AppStateHandler__(APP_STATE_STOP, app_stop)

.segment app_code

app_run:

	PrintFromAddress(hello)
	SetAppState(APP_STATE_STOP)
	rts

app_stop:

	PrintFromAddress(goodbye)
	rts

SampleEnd()
