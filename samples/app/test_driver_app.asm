#importonce

/*
	Test Shell Driver Calls

	Basically the Hello World for
	drivers in the Shell.

	It will test the call interfaces
	that come standard with all
	drivers in the Shell.

	Drivers that provide additional
	calls will need a separate
	test program.

*/

#import "samples/_sample.asm"

TestStart("tsd-calls.asm", "Test Shell Driver Calls")

.segment app_code

app_run:

        SetAppState(APP_STATE_STOP)
        rts


__AppStateHandler__(APP_STATE_RUN, app_run)

TestEnd()
