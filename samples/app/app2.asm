
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "samples/_sample.asm"

SampleStart("app2", "Test Application Two (2)")

.segment app_data 

text:

	.text @"hello world\$00"

.segment app_code

app_start:

        PrintFromAddress(text)
        rts

app_run:

        rts


__AppStateHandler__(APP_STATE_START, app_start)
__AppStateHandler__(APP_STATE_RUN, app_run)

SampleEnd()
