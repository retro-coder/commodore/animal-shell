
/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

/*
	Animal Shell Application

	This code contains the bare
	minimum to build an application
	using the provided Macros.

	The application contains no
	state handlers and therefore
	does nothing except chew
	up cycles waiting to be
	stopped or cancelled.
	
*/

#import "app/app.asm"

__AppStart__()

__AppEnd__()

.file [name="minimal.prg", segments="app"]
