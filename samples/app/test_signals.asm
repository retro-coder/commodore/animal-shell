/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

/*
        App to Test Signals
*/

#import "app/app.asm"

.filenamespace testapp

__AppStart__()

__AppSignalHandler__(SIGSTART) 
{
	__LogDebug__("app:signal:sigstart")
	rts
}

__AppSignalHandler__(SIGKILL) 
{
	__LogDebug__("app:signal:sigkill")
	rts
}

__AppSignalHandler__(SIGCONT) 
{
	__LogDebug__("app:signal:sigcont")
	rts
}

__AppSignalHandler__(SIGINT) 
{
	__LogDebug__("app:signal:sigint")
	rts
}

__AppSignalHandler__(SIGRUN) 
{
	__LogDebug__("app:signal:sigrun")
	rts
}

__AppSignalHandler__(SIGSTOP) 
{
	__LogDebug__("app:signal:sigstop")
	rts
}

__AppSignalHandler__(SIGTERM) 
{
	__LogDebug__("app:signal:sigterm")
	rts
}

__AppSignalHandler__(SIGNMI) 
{
	__LogDebug__("app:signal:signmi")
	rts
}

__AppSignalHandler__(SIGCONT) 
{
	__LogDebug__("app:signal:sigcont")
	rts
}

__AppSignalHandler__(SIGVIC) 
{
	__LogDebug__("app:signal:sigvic")
	rts
}

__AppEnd__()

.file [name="test-signals.prg", segments="app"]
