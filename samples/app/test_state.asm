#importonce

#import "app/app.asm"

.filenamespace testapp

.segment app_data

title:

.text @"\$0d"
.text @"test_state.asm\$0d"
.byte $00

state_init:

.text @"\$0dapp:state:init\$00"

state_start:

.text @"\$0dapp:state:start\$00"

state_stop:

.text @"\$0dapp:state:stop\$00"

state_run:

.text @"\$0dapp:state:run\$00"

sigkill:

.text @"\$0dapp:signal:sigkill\$00"

.segment app_data

data:	

.byte $00

run:

.byte $00


.segment app_code

__AppStart__()

__AppSignalHandler__(SIGKILL)
{
	Print(sigkill)
        rts
}

__AppStateHandler__(APP_STATE_INIT)
{
	Print(state_init)

        // reset the run flag
        
        lda     #$00
        sta     run

        rts
}

__AppStateHandler__(APP_STATE_START)
{
	Print(title)
	Print(state_start)
	
        rts
}

__AppStateHandler__(APP_STATE_RUN) 
{
        lda     run
        bne     !+

        inc     run

	Print(state_run)
	
        !:

        inc     $d020
        inc     data
        lda     data 
        sta     $0400
        rts
}

__AppStateHandler__(APP_STATE_STOP)
{
	Print(state_stop)
        rts
}

__AppEnd__()

.file [name="test_state.prg", segments="app"]
