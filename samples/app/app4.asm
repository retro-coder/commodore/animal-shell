

/*
	Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

	Licensed under the MIT License.
	(See LICENSE.md in the project root for license information)
*/

#importonce

#import "samples/_sample.asm"  

SampleStart("app4", "Test Application Four (4)")

.segment app_data 

	text:
	.text @"hello world \$00"

	p_num2str: p_core_util_num2str 

	num:
	.fill 4, $00

	__AppStateHandler__(APP_STATE_START, app_start)
	__AppStateHandler__(APP_STATE_RUN, app_run1)
	__AppStateHandler__(APP_STATE_RUN, app_run2)

.segment app_code

	app_start:

	lda     #$00
	sta     num

	StorePointer(num, p_num2str.addr)

	rts

	
	app_run1:

	NPrintFromAddress(dot)
	rts 

	dot: .text @".\$00"

	
	app_run2:

	NPrintFromAddress(text)

	ICall1(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str)
	NPrintFromAddress(ADDR_OUT)

	inc     num

	rts

SampleEnd()
