
#importonce

#import "app/app.asm"


/*
	A set of macros and functions for
	writing sample applications with
	the Animal Shell.
*/

.encoding "petscii_mixed"

.var __name__ = ""

.var __color_black__ = @"\$90"
.var __color_white__ = @"\$05"
.var __color_red__ = @"\$1c"
.var __color_cyan__ = @"\$9f"
.var __color_purple__ = @"\$9c"
.var __color_green__ = @"\$1e"
.var __color_blue__ = @"\$1f"
.var __color_yellow__ = @"\$9e"
.var __color_orange__ = @"\$81"
.var __color_brown__ = @"\$95"
.var __color_pink__ = @"\$96"
.var __color_dgray__ = @"\$97"
.var __color_dgrey__ = @"\$97"
.var __color_gray__ = @"\$98"
.var __color_grey__ = @"\$98"
.var __color_lgreen__ = @"\$99"
.var __color_lblue__ = @"\$9a"
.var __color_lgrey__ = @"\$9b"
.var __color_lgray__ = @"\$9b"

.var __lf__ = @"\$0d"
.var __end__ = @"\$00"
.var __reverse_on__ = @"\$12"
.var __reverse_off__ = @"\$92"
.var __clear__ = @"\$93"

.macro SampleStart(name, title) {

.segment app_virtual "v_app_info"

	.pseudopc ADDR_APP_INFO {
		__app_info__: ds_app_info
	}

	.var info = AppInfo()

	.eval __name__ = name
	.eval info.name = name
	.eval info.copyright = "Copyright 2022-2025 Paul Hocker"
	.eval info.author = "Paul Hocker"
	.eval info.email = "paul@spocker.net"

	__AppInfo__(info)

.segment app_data

	s_line:
	.text __color_lgrey__
	.text @"\$0d"
	.fill 40, $e3
	.text @"\$0d\$0d\$00"

	s_header:
	.text __color_lgrey__
	.fill 40, $e3
	.text @"\$0d\$0d\$00"

	s_name:
	.text __color_green__
	.text @"\$0d"
	.text name
	.text @":"
	.text @"\$0d\$00"

	s_title:
	.text __color_lgreen__
	.text title
	.text @"\$0d\$00"

	__AppStart__()

.segment app_code 

	app_init:

	PrintFromAddress(s_name)
	PrintFromAddress(s_title)
	PrintFromAddress(s_header)

	ICall(I_CORE_CALL, I_CORE_BANK_IO_IN)
	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_IN)

	// reset the jiffy clock to zero


	// lda 65534
	// cmp #23
	// bne !+
	// jmp c128
	// !:
	// cmp #72
	// bne !+

	ldx #$fe
	cpx $d030
	bcc c64

	c128:
	lda 	#$00
	tax
	tay
	jsr	$f665
	jmp !+

	c64:
	lda 	#$00
	tax
	tay
	jsr	$f6e4

	!:

	ICall(I_CORE_CALL, I_CORE_BANK_KERNAL_OUT)

	rts

	__AppStateHandler__(APP_STATE_INIT, app_init)
}

.macro SampleEnd() {

	.segment app_data

	s_break:
	.text __lf__ + __end__

	s_time:
	{
		.text __color_lgreen__
		.text @"\$0d"
		.text @"Elapsed Time: "

		message:

		.fill 20, $00
	}

	s_dec:	.text @".\$00"

	s_jif:	.text @" jiffies\$00"

	s_footer:

		.text __color_lgrey__
		.text @"\$0d"
		.fill 40, $e4
		.text @"\$0d\$00"

	jiffy:
	{
		h: 	.fill 4, $00
		m: 	.fill 4, $00
		l: 	.fill 4, $00
		
	}

	p_concat:

		p_core_str_concat_in

	p_num2str:

		p_core_util_num2str

	.segment app_code

	app_stop:

		lda	$a0
		sta	jiffy.h
		lda	$a1
		sta	jiffy.m
		lda	$a2
		sta	jiffy.l

		StorePointer(jiffy.m, p_num2str.addr)
		ICall1(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str)

		StorePointer(s_time, p_concat.stringa)
		StorePointer(ADDR_OUT, p_concat.stringb)
		ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, s_time)

		StorePointer(s_time, p_concat.stringa)
		StorePointer(s_dec, p_concat.stringb)
		ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, s_time)

		StorePointer(jiffy.l, p_num2str.addr)
		ICall1(I_CORE_CALL, I_CORE_UTIL_NUM2STR, p_num2str)

		StorePointer(s_time, p_concat.stringa)
		StorePointer(ADDR_OUT, p_concat.stringb)
		ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, s_time)

		StorePointer(s_time, p_concat.stringa)
		StorePointer(s_jif, p_concat.stringb)
		ICall2(I_CORE_CALL, I_CORE_STR_CONCAT, p_concat, s_time)

		PrintFromAddress(s_footer)
		PrintFromAddress(s_time)

		// clear addr_out

		ldx #19
		lda #$00
		!:
		sta s_time.message, x
		dex
		bne !-
		sta s_time.message, x

		PrintFromAddress(s_break)		

		rts

	__AppStateHandler__(APP_STATE_STOP, app_stop)

	__AppEnd__()

	.file [name=__name__ + ".prg", segments="app"]
}
