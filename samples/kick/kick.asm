
/*

Copyright (C) 2022-2025 Paul Hocker. All rights reserved.

Licensed under the MIT License.
(See LICENSE.md in the project root for license information)

*/

#importonce

/*

Kick Assembler Animal Shell Hello World

This application shows you how
to create a simple application
with Kick Assembler
for the Shell without any
of the fancy Macros for 
Kick Assembler.

*/

.filenamespace kick

// not really needed but makes the build much cleaner
.segmentdef kick [start=$4000]
.segment kick

// need to make sure text is encoded correctly
.encoding "petscii_mixed"

* = $4000 "jumps"

/*

The first 64 bytes of memory in
an application needs follow
a specific layout
for the Shell to use it
properly.

All Shell applications are
called by the Shell at address
$4000.

Your first action must be
a jump to your code that will
be handling the state of
the application.

*/

jmp app_handler

// A second jump call is reserved for future use.

jmp $ffff


* = $4006 "info"

/*

The next 48 bytes provide 
the following information space
that is used by the Shell
to provide information about
the application in addition
to specific platform and
version information.

If this information is not
provide there is a risk that
the calls to the application
interfaces methods to get
this information will fail and
might actually break the 
shell.

*/
app_info: {

        // the signature of the file tells the shell what it is,
        // in this case an application
        signature:	.text "##a0"
        // future use
        type:		.byte $00
        // the platform it should run on 
        // 0 = ALL; 1 = C128; 2 = C64
        platform:	.byte $00
        // major version of the application
        major:		.byte $00
        // minor version of the application
        minor:		.byte $00
        // pointer to memory where the name is stored
        name:		.byte <app_data.d_name, >app_data.d_name
        // pointer to memory where the description is stored
        description:	.byte <app_data.d_desc, >app_data.d_desc
        // pointer to memory where the copyright is stored
        copyright:	.byte <app_data.d_copyright, >app_data.d_copyright
        // pointer to memory where the author name is stored
        author:		.byte <app_data.d_author, >app_data.d_author
        // pointer to memory where the website url is stored
        website:	.byte <app_data.d_website, >app_data.d_website
        // pointer to memory where email address is stored
        email:		.byte <app_data.d_email, >app_data.d_email
        // reserved for future use
        reserved:	.fill 22,0
}

/*

The last thirty-two (16) bytes are used
by the Shell. It uses this
space to keep track of important
information about the app running
like its current state or
incoming signal.

*/

* = $4030 "sys"

app_sys_data:

APP_STATE:		.byte $00
APP_NEXT_STATE:		.byte $00
        		.fill 14, $ee

* = $4040 "data"

/*

Shell programs usually
put data directly before any
logic code.

This is optional and the developer
can put data anywhere they want 
in the application.

This area is a good place for the
developer to put call interface
data structures and return data
structures.

*/
app_data: {

        d_name:		.text @"kickAPP\$00"
        d_desc:		.text @"kickDESCRIPTION\$00"
        d_copyright:	.text @"kickCOPYRIGHT\$00"
        d_author:	.text @"kickAUTHOR\$00"
        d_website:	.text @"kickWEBSITE\$00"
        d_email:	.text @"kickEMAIL\$00"
        
        hello:		.text @"made with KickAssembler\$0d\$00"

}

/*

This application is going to use
the Print Call which needs a
data structure that contains 
a pointer in lo/hi format
to the memory address where
the text is stored.

For example, if the text is stored
at address $c000, your would
put $00 in the first byte of
the parameters and $c0 in the
second byte of the parameter.

*/
p_print: {
        ptr: .word $0000
}

* = * "code"

/*

When the Shell calls the application
it puts the State of the application
in address $4020

The Framework tries to put the code
for handling the States in a nice
order, but you can handle them any
way you want.

The examples below will briefly explain
what each State should handle, but
you should still reference the 
Developers Guide for more information
on how to handle states.

*/
app_handler: {

        nop
        nop
        nop	

        // check for INIT

        lda APP_STATE
        cmp #$10	
        bne !+
        jmp app_init_handler

        // check for START

        !:
        lda APP_STATE
        cmp #$20
        bne !+
        jmp app_start_handler

        // check for STOP

        !:
        lda APP_STATE
        cmp #$30
        bne !+
        jmp app_stop_handler

        // check for RUN

        !:
        lda APP_STATE
        cmp #$40
        bne !+
        jmp app_run_handler

        !:
        rts

}

/*

Called after the application
is loaded into memory and
before the app is started.

This will only ever be
called once in the apps
lifetime.

*/
app_init_handler: {
        rts
}

/*

Call before the application
is run, it provides another
opportunity for the
developer to do any setup
that might be required by
the application.

This will only ever be
called once in the apps
lifetime.

*/
app_start_handler: {

        // store the pointer of the string to print
        lda #<app_data.hello
        sta p_print.ptr
        lda #>app_data.hello
        sta p_print.ptr + 1

        // set the core call interface to run
        lda #$30
        sta $fa

        // set the parameter pointer for the call
        lda #<p_print
        sta $fb
        lda #>p_print
        sta $fc

        // call the core interface
        jsr $a000

        // profit

        rts

}

/*

Called every cycle of the
from the shell interface
when the application is
running.

It is a best practice to 
always return to the shell
by issuing a RTS when
you are finished your
logic code.

*/
app_run_handler: {

        // change the state to stop the app
        lda #$30
        sta APP_NEXT_STATE
        rts 

}

/*

Called when the application
is about to stop and return
to the shell.

This will only ever be
called once in the apps
lifetime.

*/
app_stop_handler: {
rts
}

.file [name="kick.prg", segments="kick"]
