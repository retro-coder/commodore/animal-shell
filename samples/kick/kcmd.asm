// https://codebase64.org/doku.php?id=base:sending_a_command_to_a_disk_drive

.encoding "petscii_mixed"

* = $c000

.const setnam = $ffbd
.const setlfs = $ffba
.const open   = $ffc0
.const close  = $ffc3
.const chkin  = $ffc6
.const chrin  = $ffcf
.const bsout  = $ffd2
.const clrch  = $ffcc
.const chkout = $ffc9
.const chrout = $ffd2

.const temp   = 253
.const charret = $0d
.const space = $20

run:

// jmp status

lda #$00
ldy #$00
ldy #$00
jsr setnam

lda #$0f
ldx $ba
bne skip
ldx #8

skip:

ldy #$0f
jsr setlfs

jsr open
bcs error

ldx #$0f
jsr chkout

ldy #$00

loop1:

lda cmd,y
beq !+
jsr chrout
iny 
jmp loop1

!:

// lda #$0f
// jsr close 
// jsr clrch

status:

lda #$00        // length is 0
jsr setnam      // - call setname

lda #$0f        // file # (15)
ldx $ba		// current device
ldy #$0f        // channel # (15)
jsr setlfs      //- set logical file #

jsr open        // - and open it.

//specify file as input

ldx #$0f        // file 15 is input
jsr chkin       // - so specify it.

//now read in file
loop2:

jsr chrin       // - read char
jsr bsout       // - print char
cmp #charret    // is it return?
bne loop2       // - if not jmp back

//now close the file

close_all:

lda #$0f        //file #
jsr close       //- close the file
jsr clrch       // restore i/o

rts

error:

jmp close_all

rts

.encoding "petscii_upper"

cmd:	.text "UI"
        .byte $0d
        .byte $00

