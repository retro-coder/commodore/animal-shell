// https://codebase64.org/doku.php?id=magazines:chacking3&s[]=directory

.encoding "petscii_mixed"

* = $1300

.label load_address = $2000

        lda #2      // file number 2
        ldx #15       // last used device number
skip:   ldy #0      // secondary address 2
        jsr $ffba     // call setlfs

        lda #8
        ldx #<fname
        ldy #>fname
        jsr $ffbd     // call setnam

        jsr $ffc0     // call open

        jsr $ffb7
        cmp #4
        jmp close

        // ---

        bcs device_error    // if carry set, the file could not be opened

        lda #15
        ldx $ba
        ldy #15
        jsr $ffba
        lda #0
        jsr $ffbd

        jsr $ffc0

        ldx #15
        jsr $ffc6

        loop:

        jsr $ffcf
        jsr $ffd2
        cmp #13
        beq eof
        jmp loop

        eof:

        close:

        lda #15
        jsr $ffc3
        lda #2
        jsr $ffc3
        jsr $ffcc
        rts

        device_error:

        jmp close

fname:  
// .text "kick"
.text "kopenerr"
