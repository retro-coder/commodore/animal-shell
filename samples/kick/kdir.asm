// https://codebase64.org/doku.php?id=magazines:chacking3&s[]=directory

.encoding "petscii_mixed"

* = $c000

.const setnam = $ffbd
.const setlfs = $ffba
.const open   = $ffc0
.const close  = $ffc3
.const chkin  = $ffc6
.const chrin  = $ffcf
.const bsout  = $ffd2
.const clrch  = $ffcc

.const temp   = 253
.const charret = $0d
.const space = $20

start:

jsr read_dir    // Initial jump table -- Note: Will read error after
jmp read_err    // showing directory

read_dir:

lda #$01        // length is 1        
ldx #<dir       // lo byte pointer to file name.
ldy #>dir       // hi byte pointer to file name.
jsr setnam      // - call setnam

lda #$01        // file # 1
ldx $ba         // current device number
ldy #$00        // channel # 0
jsr setlfs      // - call setlfs

jsr open        // - call open

// read in the bytes and display (skipping line links etc)

ldx #$01        // file #1
jsr chkin       // - call chkin to set input file #.

jsr chrin       // - ignore starting address (2 bytes)
jsr chrin 

skip:

jsr chrin       // - ignore pointer to next line (2 bytes)

bck1:

jsr chrin

line:

jsr chrin       // - get line # lo.

sta temp        // - store lo of line # @ temp
jsr chrin       // - get hi of line #

// ldx temp
// jsr $bdcd 

lda #space
jsr bsout       // - print space

gtasc:

ldx #$01        // file #1
jsr chkin       // - call chkin to set input file #.

jsr chrin       // - start printing filename until 
                //end of line.
beq chck        //(Zero signifies eol).
jsr bsout       // - Print character
sec
bcs gtasc       //and jump back.

chck:
lda #charret    // - Else we need to start the next line
jsr bsout       //Print a carriage return.

jsr chrin       // - And get the next pointer
bne bck1        // If non-zero go, strip other ptr,
                //and continue.

jsr chrin       // - Else check 2nd byte of pointer
bne line        // as if both 0 then = end of directory.

// had 3 0's in a row so end of prog
// now close the file.

lda #$01        // file # to close
jsr close       // - so close it
jsr clrch       // - clear all channels
rts             //- and return to basic

//FILENAME string
dir:           .text "$"
pat:           .text "$hack*" 

read_err:

lda #$00        // length is 0
jsr setnam      // - call setname

lda #$0f        // file # (15)
ldx #$08        // device # (08)
ldy #$0f        // channel # (15)
jsr setlfs      //- set logical file #

jsr open        // - and open it.

//specify file as input

ldx #$0f        // file 15 is input
jsr chkin       // - so specify it.

//now read in file
loop:

jsr chrin       // - read char
jsr bsout       // - print char
cmp #charret    // is it return?
bne loop        // - if not jmp back

//now close the file

lda #$0f        //file #
jsr close       //- close the file
jsr clrch       // restore i/o

rts
